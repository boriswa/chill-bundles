.. Copyright (C)  2014 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

.. _activity-bundle:

Activity bundle
###############

This bundle provides the ability to record people in the software. This bundle is required by other bundle.

.. contents:: Table of content
   :local:

Entities provided
*****************

.. todo::

   Describe the entities provided.


Configuration options
*********************

Those options are available under `chill_activity` key.

Example of configuration:

.. code-block:: yaml

   chill_activity:
      form:
         time_duration: 
            - { label: '12 minutes', seconds: 720 }
            - { label: '30 minutes', seconds: 1800 }

form.time_duration *array*
   The duration which might be suggested when the user create or update an activity. The value must be an array of object, where each object must have a :code:`label` and a :code:`seconds` key. The label provide which is shown to user (the label will be translated, if possible) and the seconds the duration.

   Example: see the example above

   Default value: the values available are 5, 10, 15, 20, 25, 30, 45 minutes, and 1 hour, 1 hour 15, 1 hour 30, 1 hour 45 and 2 hours.

.. _activity-bundle-macros:

Macros
******

Activity reason sticker
=======================

Macro file
   `ChillActivityBundle:ActivityReason:macro.html.twig`
Macro envelope
   :code:`reason(r)`

   :code:`p` is an instance of :class:`Chill\ActivityBundle\Entity\ActivityReason`

When to use this macro ?
   When you want to represent an activity reason.
Example usage :
   .. code-block:: html+jinja

      {% import 'ChillActivityBundle:ActivityReason:macro.html.twig' as m %}

      {{ m.reason(r) }}
