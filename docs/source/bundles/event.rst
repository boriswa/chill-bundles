.. Copyright (C)  2016 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

.. _event-bundle:

Event bundle
############

Template & Menu
===============

The event bundle has a special template with a specific menu for actions on
events. This menu is called `event`.

ChillEventBundle::layout.html.twig
----------------------------------

This layout extends `ChillMainBundle::layoutWithVerticalMenu.html.twig` and add the menu `event`

It proposes a new block :

* event_content

  * where to display content relative to the event.
