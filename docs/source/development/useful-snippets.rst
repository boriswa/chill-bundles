



Useful snippets
###############

Dependency Injection
********************

Configure route automatically
=============================

Add the route for the current bundle automatically on the main app.

.. code-block:: php

    namespace Chill\MyBundle\DependencyInjection;

    use Symfony\Component\DependencyInjection\ContainerBuilder;
    use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;


    class ChillMyExtension extends Extension implements PrependExtensionInterface
    {
        // ...

        public function prepend(ContainerBuilder $container)
        {
            $this->prependRoutes($container);

        }
            
        public function prependRoutes(ContainerBuilder $container) 
        {
            //add routes for custom bundle
             $container->prependExtensionConfig('chill_main', array(
               'routing' => array(
                  'resources' => array(
                     '@ChillMyBundle/Resources/config/routing.yml'
                  )
               )
            ));
        }


Security
********

Get the circles a user can reach
================================

.. code-block:: php

   use Symfony\Component\Security\Core\Role\Role;

   $authorizationHelper = $this->get('chill.main.security.authorization.helper');
   $circles = $authorizationHelper
       ->getReachableCircles(
             $this->getUser(), # from a controller
             new Role('CHILL_ROLE'), 
             $center 
       );


Controller
**********

Secured controller for person
=============================

.. literalinclude:: useful-snippets/controller-secured-for-person.php
   :language: php
