.. Copyright (C)  2016 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".


Logging
*******

.. seealso::
   
   Symfony documentation: `How to user Monolog to write logs <http://symfony.com/doc/current/cookbook/logging/monolog.html>`_
      The symfony cookbook page about logging.


A channel for custom logging has been created to store sensitive data. 

The channel is named ``chill``. 

The installer of chill should be aware that this channel may contains sensitive data and encrypted during backup.

Logging to channel `chill`
============================

You should use the service named ``chill.main.logger``, as this : 

.. code-block:: php

   $logger = $this->get('chill.main.logger');

You should store data into context, not in the log himself, which should remains the same for the action. 

Example of usage : 

.. code-block:: php

   $logger->info("An action has been performed about a person", array(
      'person_lastname' => $person->getLastName(),
      'person_firstname' => $person->getFirstName(),
      'person_id' => $person->getId(),
      'by_user' => $user->getUsername()
   ));

For further processing, it is a good idea to separate all fields (like firstname, lastname, ...) into different context keys.

By convention, you should store the username of the user performing the action under the ``by_user`` key.

