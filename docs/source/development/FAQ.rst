.. Copyright (C)  2014 Champs Libres Cooperative SCRLFS
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".

.. _faq:


Frequently asked questions
####################

Continuous integration
***********

Pipeline fails, but php-cs-fixer doesn't alert me when running it locally ?
========================================

It is possible that you run php-cs-fixer on your local instance of chill and no fixes are made.
Everything seems fine, so you push. However once the pipeline is run in gitlab, you're notified that it failed due to php
cs errors.

In this case it's likely that you have to update your version of php-cs-fixer.
php-cs-fixer is installed when building the docker image: https://gitea.champs-libres.be/Chill-project/chill-skeleton-basic/src/branch/main/Dockerfile#L50

Consequently, to update php-cs-fixer we have to update the image by building it again.

For this the following commands can be used,

.. code-block:: php

    docker compose build --pull php
    # replace existing containers
    docker compose up -d --force-recreate php
