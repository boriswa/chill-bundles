<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DependencyInjection;

use Chill\MainBundle\DependencyInjection\Widget\Factory\WidgetFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This class load config for chillMainExtension.
 */
class ChillMainExtension extends Extension implements Widget\HasWidgetFactoriesExtensionInterface
{
    /**
     * widget factory.
     *
     * @var WidgetFactoryInterface[]
     */
    protected $widgetFactories = [];

    public function addWidgetFactory(WidgetFactoryInterface $factory)
    {
        $this->widgetFactories[] = $factory;
    }

    public function getConfiguration(array $config, ContainerBuilder $container)
    {
        return new Configuration($this->widgetFactories, $container);
    }

    /**
     * @return WidgetFactoryInterface[]
     */
    public function getWidgetFactories()
    {
        return $this->widgetFactories;
    }

    public function load(array $configs, ContainerBuilder $container)
    {
        // configuration for main bundle
        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);

        // add the key 'widget' without the key 'enable'
        $container->setParameter(
            'chill_main.widgets',
            ['homepage' => $config['widgets']['homepage']]
        );

        // ...
    }
}
