Chill documentation
====================

Documentation of the Chill software.

The compiled documentation may be read at http://docs.chill.social

Compilation into HTML
=====================

To compile this documentation :

1. Install [sphinx-doc](http://sphinx-doc.org)
    ``` bash
            $ virtualenv .venv # creation of the virtual env (only the first time)
            $ source .venv/bin/activate # activate the virtual env
    (.venv) $ pip install -r requirements.txt
    ```
2. Install submodules : $ git submodule update --init;
3. run `make html` from the root directory
4. The base file is located on build/html/index.html
    ``` bash
    $ cd build/html
    $ python -m http.server 8888 # will serve the site on the port 8888
    ```

Contribute
===========

Issue tracker : https://gitlab.com/Chill-Projet/chill-bundles/-/issues

Licence
=======

This documentation is available under the [GNU Free Documentation Licence](http://www.gnu.org/licenses/fdl-1.3.en.html).
