# Chill framework

Documentation of the Chill software.

The online documentation can be found at http://docs.chill.social

See the [`docs`][1] directory for more.

[1]: docs/README.md
