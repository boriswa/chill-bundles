---

# Select what we should cache between builds
cache:
    paths:
        - /vendor/
        - .cache

# Bring in any services we need http://docs.gitlab.com/ee/ci/docker/using_docker_images.html#what-is-a-service
# See http://docs.gitlab.com/ee/ci/services/README.html for examples.
services:
    - name: postgis/postgis:14-3.3-alpine
      alias: db
      command:
          - postgres
          - "-c"
          - max_connections=1000
    - name: redis
      alias: redis

# Set any variables we need
variables:
    GIT_DEPTH: 1
    # Configure postgres environment variables (https://hub.docker.com/r/_/postgres/)
    POSTGRES_USER: postgres
    POSTGRES_PASSWORD: postgres
    # configure database access
    DATABASE_URL: postgresql://postgres:postgres@db:5432/postgres?serverVersion=14&charset=utf8
    # fetch the chill-app using git submodules
    # GIT_SUBMODULE_STRATEGY: recursive
    REDIS_HOST: redis
    REDIS_PORT: 6379
    REDIS_URL: redis://redis:6379
    DEFAULT_CARRIER_CODE: BE
    # force a timezone
    TZ: Europe/Brussels
    # avoid direct deprecations (using symfony phpunit bridge: https://symfony.com/doc/4.x/components/phpunit_bridge.html#internal-deprecations
    SYMFONY_DEPRECATIONS_HELPER: max[total]=99999999&max[self]=0&max[direct]=0&verbose=1

stages:
    - Composer install
    - Tests
    - Deploy

build:
    stage: Composer install
    image: gitea.champs-libres.be/chill-project/chill-skeleton-basic/base-image:php82
    before_script:
        - composer config -g cache-dir "$(pwd)/.cache"
    script:
        - composer install --optimize-autoloader --no-ansi --no-interaction --no-progress
    cache:
        paths:
            - .cache/
    artifacts:
        expire_in: 1 day
        paths:
            - bin
            - vendor/

code_style:
    stage: Tests
    image: gitea.champs-libres.be/chill-project/chill-skeleton-basic/base-image:php82
    script:
        - php-cs-fixer fix --dry-run -v --show-progress=none
    cache:
        paths:
            - .cache/
    artifacts:
        expire_in: 1 day
        paths:
            - bin
            - vendor/

phpstan_tests:
    stage: Tests
    image: gitea.champs-libres.be/chill-project/chill-skeleton-basic/base-image:php82
    script:
        - bin/phpstan analyze --memory-limit=2G
    cache:
        paths:
            - .cache/
    artifacts:
        expire_in: 1 day
        paths:
            - bin
            - vendor/

rector_tests:
    stage: Tests
    image: gitea.champs-libres.be/chill-project/chill-skeleton-basic/base-image:php82
    script:
        - tests/console cache:clear
        - bin/rector process --dry-run
    cache:
        paths:
            - .cache/
    artifacts:
        expire_in: 1 day
        paths:
            - bin
            - vendor/

# psalm_tests:
#     stage: Tests
#     image: gitea.champs-libres.be/chill-project/chill-skeleton-basic/base-image:php82
#     script:
#         - bin/psalm
#     allow_failure: true
#     artifacts:
#         expire_in: 30 min
#         paths:
#             - bin
#             - tests/app/vendor/

unit_tests:
    stage: Tests
    image: gitea.champs-libres.be/chill-project/chill-skeleton-basic/base-image:php82
    script:
        - php tests/console doctrine:migrations:migrate -n --env=test
        - php tests/console chill:db:sync-views --env=test
        - php -d memory_limit=2G tests/console cache:clear --env=test
        - php -d memory_limit=3G tests/console doctrine:fixtures:load -n
        - php -d memory_limit=4G bin/phpunit --colors=never --exclude-group dbIntensive
    artifacts:
        expire_in: 1 day
        paths:
            - bin
            - vendor/

release:
    stage: Deploy
    image: registry.gitlab.com/gitlab-org/release-cli:latest
    rules:
        - if: $CI_COMMIT_TAG
    script:
        - echo "running release_job"
    release:
        tag_name: '$CI_COMMIT_TAG'
        description: "./.changes/v$CI_COMMIT_TAG.md"
