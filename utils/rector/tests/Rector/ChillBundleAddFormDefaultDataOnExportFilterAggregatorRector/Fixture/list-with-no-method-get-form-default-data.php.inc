<?php

namespace Utils\Rector\Tests\ChillBundleAddFormDefaultDataOnExportFilterAggregatorRector\Fixture;

use Chill\MainBundle\Export\ListInterface;
use Symfony\Component\Form\FormBuilderInterface;

class MyClass implements ListInterface
{
    public function getTitle()
    {
        // TODO: Implement getTitle() method.
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // TODO: Implement buildForm() method.
    }

    public function getAllowedFormattersTypes()
    {
        // TODO: Implement getAllowedFormattersTypes() method.
    }

    public function getDescription()
    {
        // TODO: Implement getDescription() method.
    }

    public function getLabels($key, array $values, $data)
    {
        // TODO: Implement getLabels() method.
    }

    public function getQueryKeys($data)
    {
        // TODO: Implement getQueryKeys() method.
    }

    public function getResult($query, $data)
    {
        // TODO: Implement getResult() method.
    }

    public function getType()
    {
        // TODO: Implement getType() method.
    }

    public function initiateQuery(array $requiredModifiers, array $acl, array $data = [])
    {
        // TODO: Implement initiateQuery() method.
    }

    public function requiredRole(): string
    {
        // TODO: Implement requiredRole() method.
    }

    public function supportsModifiers()
    {
        // TODO: Implement supportsModifiers() method.
    }
}
?>
-----
<?php

namespace Utils\Rector\Tests\ChillBundleAddFormDefaultDataOnExportFilterAggregatorRector\Fixture;

use Chill\MainBundle\Export\ListInterface;
use Symfony\Component\Form\FormBuilderInterface;

class MyClass implements ListInterface
{
    public function getTitle()
    {
        // TODO: Implement getTitle() method.
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // TODO: Implement buildForm() method.
    }
    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getAllowedFormattersTypes()
    {
        // TODO: Implement getAllowedFormattersTypes() method.
    }

    public function getDescription()
    {
        // TODO: Implement getDescription() method.
    }

    public function getLabels($key, array $values, $data)
    {
        // TODO: Implement getLabels() method.
    }

    public function getQueryKeys($data)
    {
        // TODO: Implement getQueryKeys() method.
    }

    public function getResult($query, $data)
    {
        // TODO: Implement getResult() method.
    }

    public function getType()
    {
        // TODO: Implement getType() method.
    }

    public function initiateQuery(array $requiredModifiers, array $acl, array $data = [])
    {
        // TODO: Implement initiateQuery() method.
    }

    public function requiredRole(): string
    {
        // TODO: Implement requiredRole() method.
    }

    public function supportsModifiers()
    {
        // TODO: Implement supportsModifiers() method.
    }
}
?>
