<?php

namespace Utils\Rector\Tests\ChillBundleAddFormDefaultDataOnExportFilterAggregatorRector\Fixture;

use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Form\Type\PickRollingDateType;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class MyClass implements FilterInterface
{
    public function describeAction($data, $format = 'string')
    {
        // TODO: Implement describeAction() method.
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('foo', PickRollingDateType::class, [
            'label' => 'Test thing',
        ]);

        $builder->add('baz', TextType::class, [
            'label' => 'OrNiCar',
        ]);
    }

    public function getTitle()
    {
        // TODO: Implement getTitle() method.
    }

    public function addRole(): ?string
    {
        // TODO: Implement addRole() method.
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        // TODO: Implement alterQuery() method.
    }

    public function applyOn()
    {
        // TODO: Implement applyOn() method.
    }
}
?>
-----
<?php

namespace Utils\Rector\Tests\ChillBundleAddFormDefaultDataOnExportFilterAggregatorRector\Fixture;

use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Form\Type\PickRollingDateType;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class MyClass implements FilterInterface
{
    public function describeAction($data, $format = 'string')
    {
        // TODO: Implement describeAction() method.
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('foo', PickRollingDateType::class, [
            'label' => 'Test thing',
        ]);

        $builder->add('baz', TextType::class, [
            'label' => 'OrNiCar',
        ]);
    }
    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getTitle()
    {
        // TODO: Implement getTitle() method.
    }

    public function addRole(): ?string
    {
        // TODO: Implement addRole() method.
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        // TODO: Implement alterQuery() method.
    }

    public function applyOn()
    {
        // TODO: Implement applyOn() method.
    }
}
?>
