## v2.15.2 - 2024-01-11
### Fixed
* Fix the id_seq used when creating a new accompanying period participation during fusion of two person files 
### DX
* Set placeholder to False for expanded EntityType form fields where required is set to False. 
