## v2.3.0 - 2023-06-27
### Feature
* ([#110](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/110)) Edit saved exports options: the saved exports options (forms, filters, aggregators) are now editable.
* ([#103](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/103)) Get an unified list of document in person and accompanying period context
* [export] Set the default date of calculation of the accompanying period's list as "today"
* Force accompanying period user history to be unique for the same period and stardate/enddate [:warning: may encounter migration issue]

  If some issue is encountered during migration, use this SQL to find the line which are in conflict, examine the problem and delete some of the concerning line
*
  ```sql
  -- to see the line which are in conflict with another one
  SELECT o.*
  FROM chill_person_accompanying_period_user_history o
  JOIN chill_person_accompanying_period_user_history c ON o.id < c.id AND o.accompanyingperiod_id = c.accompanyingperiod_id
  WHERE tsrange(o.startdate, o.enddate, '[)') && tsrange(c.startdate, c.enddate, '[)')
  ORDER BY accompanyingperiod_id;
  -- to examine line in conflict for a given accompanyingperiod_id (given by the previous query)
  SELECT * FROM chill_person_accompanying_period_user_history WHERE accompanyingperiod_id = IIIIDDDD order by startdate, enddate;
  ```
* Rename label of filter in French: "parcours actif" => "parcours ouvert", and "filtrer les parcours ouverts" => "Filtrer les parcours dont la date d'ouverture"

### Traduction francophone des principaux changements

* Les exports enregistrés sont éditables par l'utilisateur;
* L'onglet "Document" dans les parcours et les dossiers d'usager affiche désormais les documents ajoutés à différents endroits.

  Pour les parcours, il s'agit de:

  - documents ajoutés directement dans le parcours;
  - documents des échanges;
  - documents des rendez-vous;
  - documents des évaluations;
  - documents directement ajoutés dans le dossier des usagers concernés par le parcours;

  Pour les usagers, il s'agit de:

  - documents des échanges;
  - documents des parcours;
  - documents des rendez-vous;
  - documents des actions, des échanges, des rendez-vous, des évaluations ajoutés dans les parcours.
* Dans la liste des parcours, la date de calcul des éléments associés est "aujourd'hui" par défaut.
* Dans les exports, renommage des libellés des filtres: "parcours actif" => "parcours ouvert", et "filtrer les parcours ouverts" => "Filtrer les parcours dont la date d'ouverture"
