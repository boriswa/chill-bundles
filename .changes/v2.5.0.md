## v2.5.0 - 2023-07-14
### Feature
* Allow filtering on the basis of a user within general tasks lists
* ([#120](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/120)) Adding OrderFilter to the list of social actions.
* ([#125](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/125)) [export] Add a list for people with their associated course
* [export] Add ordering by person's lastname or course opening date in list which concerns accompanying course or peoples
* ([#128](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/128)) [Export] allow to group activities by localisation
* ([#129](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/129)) [export] Add a filter "filter course having an activity between two dates"
* ([#112](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/112)) [addresses] Add a cronjob to re-associate addresses with addresses reference every 6 hours
* Improve filtering layout

### Fixed
* reimplement the visualization of all calculator results
* ([#117](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/117)) Repair my unread notification list with actions and evaluations documents
* ([#126](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/126)) Correct bug in thirdparty API search query: simplify address joins clause for child and parent kind

### DX
* Documentation for database principles
* [cronjob] when a cronjob is executed, it may return an array of data that will be passed as argument on the next execution

### UX
* ([#93](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/93)) Better integration of address details button: look, position, title tag
* ([#93](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/93)) Show address detail button on person and household banners
* Improve residential address position on show onthefly modale

### Traduction francophone des principaux changements

* Ajout d'un filtre "par utilisateur" aux pages de tâche
* Filtre des actions d'accompagnement par date, type, intervenant
* export: liste des usagers concernés avec détail de leurs parcours
* export: ajout d'un regroupement des échanges par localisation
* export: ajout d'un filtre "parcours ayant reçu un échange entre deux dates"
* ajout d'une tâche cron pour associer les adresses à une adresse de référence
* correction: réparation de la liste des notifications sur la page d'accueil, dans le cas où une notification concerne une action ou un document dans une évaluation
* correction: réparation de la recherche des tiers ayant des codes postaux similaires entre les parents et enfants
* meilleure intégration du bouton "détail d'une adresse": améliration de la taille et de la position
* bouton permettant de visualiser les détails d'une adresse (modale avec carte) dans la bannière "Usager" et "Ménage"
* amélioration de la modale permettant de voir les détails d'un usager: les adresses de résidence sont dans la continuité des autres adresses, et non plus dans une colonne séparée
* améliore le design et l'expérience utilisateur des filtres
