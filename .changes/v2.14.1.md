## v2.14.1 - 2023-11-29
### Fixed
* Export: fix list person with custom fields 
* ([#100](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/100)) Add a paginator to budget elements (resource and charge types) in the admin 
* Fix error in ListEvaluation when "handling agents" are alone 
