<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Status.
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="chill_event_status")
 *
 * @ORM\HasLifecycleCallbacks
 */
class Status
{
    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $active = true;

    /**
     * @ORM\Id
     *
     * @ORM\Column(name="id", type="integer")
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    private $name;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Chill\EventBundle\Entity\EventType",
     * inversedBy="statuses")
     */
    private ?EventType $type = null;

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get label.
     *
     * @return array
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get type.
     *
     * @return EventType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return Status
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Set label.
     *
     * @param array $name
     *
     * @return Status
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set type.
     *
     * @return Status
     */
    public function setType(?EventType $type = null)
    {
        $this->type = $type;

        return $this;
    }
}
