<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Repository;

use Chill\EventBundle\Entity\Event;
use Chill\EventBundle\Entity\EventType;

interface EventACLAwareRepositoryInterface
{
    /**
     * @param array{q?: string, dates?: array{from?: \DateTimeImmutable|null, to?: \DateTimeImmutable|null}, event_types?: list<EventType>} $filters
     */
    public function countAllViewable(array $filters): int;

    /**
     * @param array{q?: string, dates?: array{from?: \DateTimeImmutable|null, to?: \DateTimeImmutable|null}, event_types?: list<EventType>} $filters
     *
     * @return list<Event>
     */
    public function findAllViewable(array $filters, int $offset = 0, int $limit = 50): array;
}
