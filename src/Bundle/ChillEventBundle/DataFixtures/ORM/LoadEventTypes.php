<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\DataFixtures\ORM;

use Chill\EventBundle\Entity\EventType;
use Chill\EventBundle\Entity\Role;
use Chill\EventBundle\Entity\Status;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Load a set of `EventType`, their `Role` and `Status`.
 */
class LoadEventTypes extends AbstractFixture implements OrderedFixtureInterface
{
    public static $refs = [];

    public function getOrder()
    {
        return 30000;
    }

    public function load(ObjectManager $manager)
    {
        /*
         * Echange de savoirs
         */
        $type = (new EventType())
            ->setActive(true)
            ->setName(['fr' => 'Échange de savoirs', 'en' => 'Exchange of knowledge']);
        $manager->persist($type);

        $this->addReference('event_type_knowledge', $type);
        self::$refs[] = 'event_type_knowledge';

        $role = (new Role())
            ->setActive(true)
            ->setName(['fr' => 'Participant', 'nl' => 'Deelneemer', 'en' => 'Participant'])
            ->setType($type);
        $manager->persist($role);

        $role = (new Role())
            ->setActive(true)
            ->setName(['fr' => 'Animateur'])
            ->setType($type);
        $manager->persist($role);

        $status = (new Status())
            ->setActive(true)
            ->setName(['fr' => 'Inscrit'])
            ->setType($type);
        $manager->persist($status);

        $status = (new Status())
            ->setActive(true)
            ->setName(['fr' => 'Présent'])
            ->setType($type);
        $manager->persist($status);

        /*
         * Formation
         */
        $type = (new EventType())
            ->setActive(true)
            ->setName(['fr' => 'Formation', 'en' => 'Course', 'nl' => 'Opleiding']);
        $manager->persist($type);

        $this->addReference('event_type_course', $type);
        self::$refs[] = 'event_type_course';

        $role = (new Role())
            ->setActive(true)
            ->setName(['fr' => 'Participant', 'nl' => 'Deelneemer', 'en' => 'Participant'])
            ->setType($type);
        $manager->persist($role);

        $status = (new Status())
            ->setActive(true)
            ->setName(['fr' => 'Inscrit'])
            ->setType($type);
        $manager->persist($status);

        $status = (new Status())
            ->setActive(true)
            ->setName(['fr' => 'En liste d\'attente'])
            ->setType($type);
        $manager->persist($status);

        /*
         * Visite
         */
        $type = (new EventType())
            ->setActive(true)
            ->setName(['fr' => 'Visite', 'en' => 'Visit']);
        $manager->persist($type);

        $this->addReference('event_type_visit', $type);
        self::$refs[] = 'event_type_visit';

        $role = (new Role())
            ->setActive(true)
            ->setName(['fr' => 'Participant', 'nl' => 'Deelneemer', 'en' => 'Participant'])
            ->setType($type);
        $manager->persist($role);

        $status = (new Status())
            ->setActive(true)
            ->setName(['fr' => 'Présent'])
            ->setType($type);
        $manager->persist($status);

        $status = (new Status())
            ->setActive(true)
            ->setName(['fr' => 'Absent'])
            ->setType($type);
        $manager->persist($status);

        $status = (new Status())
            ->setActive(true)
            ->setName(['fr' => 'Excusé'])
            ->setType($type);
        $manager->persist($status);

        $status = (new Status())
            ->setActive(true)
            ->setName(['fr' => 'Inscrit'])
            ->setType($type);
        $manager->persist($status);

        /*
         * Réunion
         */
        $type = (new EventType())
            ->setActive(true)
            ->setName(['fr' => 'Réunion', 'en' => 'Meeting']);
        $manager->persist($type);

        $this->addReference('event_type_meeting', $type);
        self::$refs[] = 'event_type_meeting';

        $role = (new Role())
            ->setActive(true)
            ->setName(['fr' => 'Participant', 'nl' => 'Deelneemer', 'en' => 'Participant'])
            ->setType($type);
        $manager->persist($role);

        $status = (new Status())
            ->setActive(true)
            ->setName(['fr' => 'Présent'])
            ->setType($type);
        $manager->persist($status);

        $status = (new Status())
            ->setActive(true)
            ->setName(['fr' => 'Absent'])
            ->setType($type);
        $manager->persist($status);

        $status = (new Status())
            ->setActive(true)
            ->setName(['fr' => 'Excusé'])
            ->setType($type);
        $manager->persist($status);

        /*
         * Atelier
         */
        $type = (new EventType())
            ->setActive(true)
            ->setName(['fr' => 'Atelier', 'en' => 'Workshop']);
        $manager->persist($type);

        $this->addReference('event_type_workshop', $type);
        self::$refs[] = 'event_type_workshop';

        $role = (new Role())
            ->setActive(true)
            ->setName(['fr' => 'Participant', 'nl' => 'Deelneemer', 'en' => 'Participant'])
            ->setType($type);
        $manager->persist($role);

        $status = (new Status())
            ->setActive(true)
            ->setName(['fr' => 'Présent'])
            ->setType($type);
        $manager->persist($status);

        $status = (new Status())
            ->setActive(true)
            ->setName(['fr' => 'Absent'])
            ->setType($type);
        $manager->persist($status);

        $status = (new Status())
            ->setActive(true)
            ->setName(['fr' => 'Excusé'])
            ->setType($type);
        $manager->persist($status);

        $status = (new Status())
            ->setActive(true)
            ->setName(['fr' => 'Inscrit'])
            ->setType($type);
        $manager->persist($status);

        /*
         * Séance d'info
         */
        $type = (new EventType())
            ->setActive(true)
            ->setName(['fr' => "Séance d'info", 'en' => 'Info']);
        $manager->persist($type);

        $this->addReference('event_type_info', $type);
        self::$refs[] = 'event_type_info';

        $role = (new Role())
            ->setActive(true)
            ->setName(['fr' => 'Participant', 'nl' => 'Deelneemer', 'en' => 'Participant'])
            ->setType($type);
        $manager->persist($role);

        $status = (new Status())
            ->setActive(true)
            ->setName(['fr' => 'Présent'])
            ->setType($type);
        $manager->persist($status);

        $status = (new Status())
            ->setActive(true)
            ->setName(['fr' => 'Absent'])
            ->setType($type);
        $manager->persist($status);

        $status = (new Status())
            ->setActive(true)
            ->setName(['fr' => 'Excusé'])
            ->setType($type);
        $manager->persist($status);

        $status = (new Status())
            ->setActive(true)
            ->setName(['fr' => 'Inscrit'])
            ->setType($type);
        $manager->persist($status);

        $manager->flush();
    }
}
