<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Event;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231128122635 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Append more fields on event: location, documents, and comment';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE chill_event_event_documents (event_id INT NOT NULL, storedobject_id INT NOT NULL, PRIMARY KEY(event_id, storedobject_id))');
        $this->addSql('CREATE INDEX IDX_5C1B638671F7E88B ON chill_event_event_documents (event_id)');
        $this->addSql('CREATE INDEX IDX_5C1B6386EE684399 ON chill_event_event_documents (storedobject_id)');
        $this->addSql('ALTER TABLE chill_event_event_documents ADD CONSTRAINT FK_5C1B638671F7E88B FOREIGN KEY (event_id) REFERENCES chill_event_event (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_event_event_documents ADD CONSTRAINT FK_5C1B6386EE684399 FOREIGN KEY (storedobject_id) REFERENCES chill_doc.stored_object (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_event_event ADD location_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_event_event ADD organizationCost NUMERIC(10, 4) DEFAULT 0.0');
        $this->addSql('ALTER TABLE chill_event_event ADD comment_comment TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_event_event ADD comment_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_event_event ADD comment_userId INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_event_event ADD CONSTRAINT FK_FA320FC864D218E FOREIGN KEY (location_id) REFERENCES chill_main_location (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_FA320FC864D218E ON chill_event_event (location_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_event_event_documents DROP CONSTRAINT FK_5C1B638671F7E88B');
        $this->addSql('ALTER TABLE chill_event_event_documents DROP CONSTRAINT FK_5C1B6386EE684399');
        $this->addSql('DROP TABLE chill_event_event_documents');
        $this->addSql('ALTER TABLE chill_event_event DROP location_id');
        $this->addSql('ALTER TABLE chill_event_event DROP organizationCost');
        $this->addSql('ALTER TABLE chill_event_event DROP comment_comment');
        $this->addSql('ALTER TABLE chill_event_event DROP comment_date');
        $this->addSql('ALTER TABLE chill_event_event DROP comment_userId');
    }
}
