<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Tests\Controller;

use Chill\MainBundle\Pagination\PaginatorFactory;
use Chill\MainBundle\Test\PrepareClientTrait;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Twig\Environment;

/**
 * @internal
 *
 * @coversNothing
 */
class EventListControllerTest extends WebTestCase
{
    use ProphecyTrait;
    use PrepareClientTrait;

    private readonly PaginatorFactory $paginatorFactory;
    private readonly Environment $environment;

    protected function setUp(): void
    {
    }

    public function testList(): void
    {
        $client = $this->getClientAuthenticated();

        $client->request('GET', '/fr/event/event/list');
        self::assertResponseIsSuccessful();
    }
}
