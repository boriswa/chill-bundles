<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Security\Authorization;

use Chill\EventBundle\Entity\Participation;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Security\Authorization\AbstractChillVoter;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\MainBundle\Security\ProvideRoleHierarchyInterface;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;

class ParticipationVoter extends AbstractChillVoter implements ProvideRoleHierarchyInterface
{
    final public const CREATE = 'CHILL_EVENT_PARTICIPATION_CREATE';

    final public const ROLES = [
        self::SEE,
        self::SEE_DETAILS,
        self::CREATE,
        self::UPDATE,
    ];

    final public const SEE = 'CHILL_EVENT_PARTICIPATION_SEE';

    final public const SEE_DETAILS = 'CHILL_EVENT_PARTICIPATION_SEE_DETAILS';

    final public const UPDATE = 'CHILL_EVENT_PARTICIPATION_UPDATE';

    /**
     * @var AccessDecisionManagerInterface
     */
    protected $accessDecisionManager;

    /**
     * @var AuthorizationHelper
     */
    protected $authorizationHelper;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(
        AccessDecisionManagerInterface $accessDecisionManager,
        AuthorizationHelper $authorizationHelper,
        LoggerInterface $logger
    ) {
        $this->accessDecisionManager = $accessDecisionManager;
        $this->authorizationHelper = $authorizationHelper;
        $this->logger = $logger;
    }

    public function getRoles(): array
    {
        return self::ROLES;
    }

    public function getRolesWithHierarchy(): array
    {
        return [
            'Event' => self::ROLES,
        ];
    }

    public function getRolesWithoutScope(): array
    {
        return [];
    }

    public function supports($attribute, $subject)
    {
        return ($subject instanceof Participation && \in_array($attribute, self::ROLES, true))
            || ($subject instanceof Person && \in_array($attribute, [self::CREATE, self::SEE], true))
            || (null === $subject && self::SEE === $attribute);
    }

    /**
     * @param string        $attribute
     * @param Participation $subject
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $this->logger->debug(sprintf('Voting from %s class', self::class));

        if (!$token->getUser() instanceof User) {
            return false;
        }

        if ($subject instanceof Participation) {
            return $this->authorizationHelper->userHasAccess($token->getUser(), $subject, $attribute);
        }

        if ($subject instanceof Person) {
            return $this->authorizationHelper->userHasAccess($token->getUser(), $subject, $attribute);
        }

        // subject is null. We check that at least one center is reachable
        $centers = $this->authorizationHelper
            ->getReachableCenters($token->getUser(), $attribute);

        return \count($centers) > 0;

        if (!$this->accessDecisionManager->decide($token, [PersonVoter::SEE], $person)) {
            return false;
        }

        return $this->authorizationHelper->userHasAccess(
            $token->getUser(),
            $subject,
            $attribute
        );
    }
}
