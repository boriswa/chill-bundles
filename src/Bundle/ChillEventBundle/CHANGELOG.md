
Branch sf3
==========

- fix symfony3 php depreciations ;
- add time to event dates ;
- add new moderator field on events ;
- misc improvements form styles ;
- adapt webpack config for styles sheets ;
- add a new page 'events participation' in menu person, that list all events participation for a person ;
- subscribe a person to an event from person context ;
- improve message translation ;
- add a first step to pick center in new event form ;
- add events in history timeline ;
- export participations list for an event ;
- add event administration pages ;
- add remove participation and remove event feature ;
- fix the way the bundle compile assets ;
    This modification will require to update Chill-Standard to the latest version.
    At least, the file `webpack.config.js` should be upgrade [to the last 
    version](https://framagit.org/Chill-project/Chill-Standard/-/blob/c7a7de68ec49d97c9e1481b72c1f848f9b5cb2d7/webpack.config.js)
- fix redirection when only one participation edit ;



