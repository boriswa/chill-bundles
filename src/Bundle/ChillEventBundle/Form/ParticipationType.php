<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Form;

use Chill\EventBundle\Entity\EventType;
use Chill\EventBundle\Entity\Status;
use Chill\EventBundle\Form\Type\PickRoleType;
use Chill\EventBundle\Form\Type\PickStatusType;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * A type to create a participation.
 *
 * If the `event` option is defined, the role will be restricted
 */
final class ParticipationType extends AbstractType
{
    public function __construct(private readonly TranslatableStringHelperInterface $translatableStringHelper)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // local copy of variable for Closure
        $translatableStringHelper = $this->translatableStringHelper;

        // add role
        $builder->add('role', PickRoleType::class, [
            'event_type' => $options['event_type'],
        ]);

        // add a status
        $builder->add('status', PickStatusType::class, [
            'event_type' => $options['event_type'],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined('event_type')
            ->setAllowedTypes('event_type', ['null', EventType::class])
            ->setDefault('event_type', 'null');
    }
}
