<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Controller;

use Chill\EventBundle\Entity\EventType;
use Chill\EventBundle\Form\EventTypeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class EventTypeController.
 */
class EventTypeController extends AbstractController
{
    /**
     * Creates a new EventType entity.
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/event/event_type/create", name="chill_eventtype_admin_create", methods={"POST"})
     */
    public function createAction(Request $request)
    {
        $entity = new EventType();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('chill_eventtype_admin', ['id' => $entity->getId()]);
        }

        return $this->render('@ChillEvent/EventType/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a EventType entity.
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/event/event_type/{id}/delete", name="chill_eventtype_admin_delete", methods={"POST", "DELETE"})
     */
    public function deleteAction(Request $request, mixed $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository(EventType::class)->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find EventType entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirectToRoute('chill_eventtype_admin');
    }

    /**
     * Displays a form to edit an existing EventType entity.
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/event/event_type/{id}/edit", name="chill_eventtype_admin_edit")
     */
    public function editAction(mixed $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(EventType::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventType entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('@ChillEvent/EventType/edit.html.twig', [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all EventType entities.
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/event/event_type/", name="chill_eventtype_admin", options={null})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository(EventType::class)->findAll();

        return $this->render('@ChillEvent/EventType/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * Displays a form to create a new EventType entity.
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/event/event_type/new", name="chill_eventtype_admin_new")
     */
    public function newAction()
    {
        $entity = new EventType();
        $form = $this->createCreateForm($entity);

        return $this->render('@ChillEvent/EventType/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a EventType entity.
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/event/event_type/{id}/show", name="chill_eventtype_admin_show")
     */
    public function showAction(mixed $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(EventType::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventType entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('@ChillEvent/EventType/show.html.twig', [
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Edits an existing EventType entity.
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/event/event_type/{id}/update", name="chill_eventtype_admin_update", methods={"POST", "PUT"})
     */
    public function updateAction(Request $request, mixed $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(EventType::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventType entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();

            return $this->redirectToRoute('chill_eventtype_admin', ['id' => $id]);
        }

        return $this->render('@ChillEvent/EventType/edit.html.twig', [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to create a EventType entity.
     *
     * @param EventType $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(EventType $entity)
    {
        $form = $this->createForm(EventTypeType::class, $entity, [
            'action' => $this->generateUrl('chill_eventtype_admin_create'),
            'method' => 'POST',
        ]);

        $form->add('submit', SubmitType::class, ['label' => 'Create']);

        return $form;
    }

    /**
     * Creates a form to delete a EventType entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(mixed $id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl(
                'chill_eventtype_admin_delete',
                ['id' => $id]
            ))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, ['label' => 'Delete'])
            ->getForm();
    }

    /**
     * Creates a form to edit a EventType entity.
     *
     * @param EventType $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(EventType $entity)
    {
        $form = $this->createForm(EventTypeType::class, $entity, [
            'action' => $this->generateUrl(
                'chill_eventtype_admin_update',
                ['id' => $entity->getId()]
            ),
            'method' => 'PUT',
        ]);

        $form->add('submit', SubmitType::class, ['label' => 'Update']);

        return $form;
    }
}
