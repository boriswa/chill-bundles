Chill Event Bundle
====================

This bundle extend [chill software](https://www.chill.social). This bundle allow to define event and participation to those events.

Documentation & installation
============================

This bundle can be installed with the Chill software.

Read documentation here : http://chill.readthedocs.org

Issues and bug tracking
=======================

The issues tracker is here : https://git.framasoft.org/Chill-project/Chill-Event/issues
