<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\WopiBundle\Tests\Controller;

use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\DocStoreBundle\Service\StoredObjectManagerInterface;
use Chill\MainBundle\Entity\User;
use Chill\WopiBundle\Controller\Convert;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Log\NullLogger;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;

/**
 * @internal
 *
 * @coversNothing
 */
final class ConvertTest extends TestCase
{
    use ProphecyTrait;

    public function testConversionFailed(): void
    {
        $storedObject = (new StoredObject())->setType('application/vnd.oasis.opendocument.text');

        $httpClient = new MockHttpClient([
            new MockResponse('not authorized', ['http_code' => 401]),
        ], 'http://collabora:9980');

        $security = $this->prophesize(Security::class);
        $security->getUser()->willReturn(new User());

        $storeManager = $this->prophesize(StoredObjectManagerInterface::class);
        $storeManager->read($storedObject)->willReturn('content');

        $parameterBag = new ParameterBag(['wopi' => ['server' => 'http://collabora:9980']]);

        $convert = new Convert(
            $httpClient,
            $this->makeRequestStack(),
            $security->reveal(),
            $storeManager->reveal(),
            new NullLogger(),
            $parameterBag
        );

        $response = $convert($storedObject);

        $this->assertNotEquals(200, $response->getStatusCode());
    }

    public function testEverythingWentFine(): void
    {
        $storedObject = (new StoredObject())->setType('application/vnd.oasis.opendocument.text');

        $httpClient = new MockHttpClient([
            new MockResponse('1234', ['http_code' => 200]),
        ], 'http://collabora:9980');

        $security = $this->prophesize(Security::class);
        $security->getUser()->willReturn(new User());

        $storeManager = $this->prophesize(StoredObjectManagerInterface::class);
        $storeManager->read($storedObject)->willReturn('content');

        $parameterBag = new ParameterBag(['wopi' => ['server' => 'http://collabora:9980']]);

        $convert = new Convert(
            $httpClient,
            $this->makeRequestStack(),
            $security->reveal(),
            $storeManager->reveal(),
            new NullLogger(),
            $parameterBag
        );

        $response = $convert($storedObject);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('1234', $response->getContent());
    }

    private function makeRequestStack(): RequestStack
    {
        $requestStack = new RequestStack();
        $requestStack->push(new Request());

        return $requestStack;
    }
}
