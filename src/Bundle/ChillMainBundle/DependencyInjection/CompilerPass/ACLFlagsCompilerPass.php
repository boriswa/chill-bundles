<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DependencyInjection\CompilerPass;

use Chill\MainBundle\Form\PermissionsGroupType;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ACLFlagsCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $permissionGroupType = $container->getDefinition(PermissionsGroupType::class);

        foreach ($container->findTaggedServiceIds('chill_main.flags') as $id => $tags) {
            $reference = new Reference($id);

            foreach ($tags as $tag) {
                match ($tag['scope']) {
                    PermissionsGroupType::FLAG_SCOPE => $permissionGroupType->addMethodCall('addFlagProvider', [$reference]),
                    default => throw new \LogicException(sprintf("This tag 'scope' is not implemented: %s, on service with id %s", $tag['scope'], $id)),
                };
            }
        }
    }
}
