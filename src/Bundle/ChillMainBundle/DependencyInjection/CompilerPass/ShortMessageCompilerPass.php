<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DependencyInjection\CompilerPass;

use Chill\MainBundle\Service\ShortMessage\NullShortMessageSender;
use Chill\MainBundle\Service\ShortMessage\ShortMessageTransporter;
use Chill\MainBundle\Service\ShortMessageOvh\OvhShortMessageSender;
use libphonenumber\PhoneNumberUtil;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;
use Symfony\Component\DependencyInjection\Reference;

class ShortMessageCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $config = $container->resolveEnvPlaceholders($container->getParameter('chill_main.short_messages'), true);
        // weird fix for special characters
        $config['dsn'] = str_replace(['%%'], ['%'], (string) $config['dsn']);
        $dsn = parse_url($config['dsn']);
        parse_str($dsn['query'] ?? '', $dsn['queries']);

        if ('null' === $dsn['scheme'] || false === $config['enabled']) {
            $defaultTransporter = new Reference(NullShortMessageSender::class);
        } elseif ('ovh' === $dsn['scheme']) {
            if (!class_exists('\\'.\Ovh\Api::class)) {
                throw new RuntimeException('Class \\Ovh\\Api not found');
            }

            foreach (['user', 'host', 'pass'] as $component) {
                if (!\array_key_exists($component, $dsn)) {
                    throw new RuntimeException(sprintf('The component %s does not exist in dsn. Please provide a dsn like ovh://applicationKey:applicationSecret@endpoint?consumerKey=xxxx&sender=yyyy&service_name=zzzz', $component));
                }

                $container->setParameter('chill_main.short_messages.ovh_config_'.$component, $dsn[$component]);
            }

            foreach (['consumer_key', 'sender', 'service_name'] as $param) {
                if (!\array_key_exists($param, $dsn['queries'])) {
                    throw new RuntimeException(sprintf('The parameter %s does not exist in dsn. Please provide a dsn like ovh://applicationKey:applicationSecret@endpoint?consumerKey=xxxx&sender=yyyy&service_name=zzzz', $param));
                }
                $container->setParameter('chill_main.short_messages.ovh_config_'.$param, $dsn['queries'][$param]);
            }

            $ovh = new Definition();
            $ovh
                ->setClass('\\'.\Ovh\Api::class)
                ->setArgument(0, $dsn['user'])
                ->setArgument(1, $dsn['pass'])
                ->setArgument(2, $dsn['host'])
                ->setArgument(3, $dsn['queries']['consumer_key']);
            $container->setDefinition(\Ovh\Api::class, $ovh);

            $ovhSender = new Definition();
            $ovhSender
                ->setClass(OvhShortMessageSender::class)
                ->setArgument(0, new Reference(\Ovh\Api::class))
                ->setArgument(1, $dsn['queries']['service_name'])
                ->setArgument(2, $dsn['queries']['sender'])
                ->setArgument(3, new Reference(LoggerInterface::class))
                ->setArgument(4, new Reference(PhoneNumberUtil::class));
            $container->setDefinition(OvhShortMessageSender::class, $ovhSender);

            $defaultTransporter = new Reference(OvhShortMessageSender::class);
        } else {
            throw new RuntimeException(sprintf('Cannot find a sender for this dsn: %s', $config['dsn']));
        }

        $container->getDefinition(ShortMessageTransporter::class)
            ->setArgument(0, $defaultTransporter);
    }
}
