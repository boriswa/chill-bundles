<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DependencyInjection\CompilerPass;

use Chill\MainBundle\DependencyInjection\Widget\AbstractWidgetsCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Compile the service definition to register widgets.
 */
class WidgetsCompilerPass extends AbstractWidgetsCompilerPass
{
    public function process(ContainerBuilder $container)
    {
        $this->doProcess($container, 'chill_main', 'chill_main.widgets');
    }
}
