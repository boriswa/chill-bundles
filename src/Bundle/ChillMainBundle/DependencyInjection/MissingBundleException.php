<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DependencyInjection;

/**
 * Description of MissingBundleException.
 */
class MissingBundleException extends \Exception
{
    public function __construct($missingBundleName)
    {
        $message = "The bundle {$missingBundleName} is missing.";

        parent::__construct($message, 500);
    }
}
