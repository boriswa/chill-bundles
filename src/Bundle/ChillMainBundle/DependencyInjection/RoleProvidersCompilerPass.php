<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class RoleProvidersCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('chill.main.role_provider')) {
            throw new \LogicException('service chill.main.role_provider is not defined. It is required by RoleProviderCompilerPass');
        }

        $definition = $container->getDefinition(
            'chill.main.role_provider'
        );

        $taggedServices = $container->findTaggedServiceIds(
            'chill.role'
        );

        foreach ($taggedServices as $id => $tagAttributes) {
            $definition->addMethodCall(
                'addProvider',
                [new Reference($id)]
            );
        }
    }
}
