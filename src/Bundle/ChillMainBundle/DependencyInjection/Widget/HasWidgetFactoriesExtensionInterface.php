<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DependencyInjection\Widget;

use Chill\MainBundle\DependencyInjection\Widget\Factory\WidgetFactoryInterface;

/**
 * Register widget factories to an extension.
 */
interface HasWidgetFactoriesExtensionInterface
{
    /**
     * register a widget factory.
     *
     * @param \Chill\MainBundle\DependencyInjection\Widget\WidgetFactoryInterface $factory
     */
    public function addWidgetFactory(WidgetFactoryInterface $factory);

    /**
     * get all the widget factories registered for this extension.
     *
     * @return WidgetFactoryInterface[]
     */
    public function getWidgetFactories();
}
