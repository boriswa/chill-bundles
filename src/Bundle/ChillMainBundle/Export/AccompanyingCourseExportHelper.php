<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Export;

use Doctrine\ORM\QueryBuilder;

final readonly class AccompanyingCourseExportHelper
{
    public static function addClosingMotiveExclusionClause(QueryBuilder $qb): QueryBuilder
    {
        $qb->leftJoin('acp.closingMotive', 'cm')
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->eq('cm.isCanceledAccompanyingPeriod', 'false'),
                    $qb->expr()->isNull('acp.closingMotive')
                )
            );

        return $qb;
    }
}
