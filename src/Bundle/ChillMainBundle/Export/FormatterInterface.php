<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Export;

use Symfony\Component\Form\FormBuilderInterface;

interface FormatterInterface
{
    public const TYPE_LIST = 'list';

    public const TYPE_TABULAR = 'tabular';

    /**
     * build a form, which will be used to collect data required for the execution
     * of this formatter.
     *
     * @uses appendAggregatorForm
     *
     * @param string        $exportAlias       Alias of the export which is being executed. An export gets the data and implements the \Chill\MainBundle\Export\ExportInterface
     * @param array<string> $aggregatorAliases Array of the aliases of the aggregators. An aggregator do the "group by" on the data. $aggregatorAliases
     */
    public function buildForm(
        FormBuilderInterface $builder,
        $exportAlias,
        array $aggregatorAliases
    );

    /**
     * get the default data for the form build by buildForm.
     */
    public function getFormDefaultData(array $aggregatorAliases): array;

    public function getName();

    /**
     * Generate a response from the data collected on differents ExportElementInterface.
     *
     * @param mixed[] $result          The result, as given by the ExportInterface
     * @param mixed[] $formatterData   collected from the current form
     * @param string  $exportAlias     the id of the current export
     * @param array   $filtersData     an array containing the filters data. The key are the filters id, and the value are the data
     * @param array   $aggregatorsData an array containing the aggregators data. The key are the filters id, and the value are the data
     *
     * @return \Symfony\Component\HttpFoundation\Response The response to be shown
     */
    public function getResponse(
        $result,
        $formatterData,
        $exportAlias,
        array $exportData,
        array $filtersData,
        array $aggregatorsData
    );

    public function getType();
}
