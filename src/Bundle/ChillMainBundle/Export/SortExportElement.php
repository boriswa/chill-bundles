<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Export;

use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class SortExportElement
{
    public function __construct(
        private TranslatorInterface $translator,
    ) {
    }

    /**
     * @param array<int|string, FilterInterface> $elements
     */
    public function sortFilters(array &$elements): void
    {
        uasort($elements, fn (FilterInterface $a, FilterInterface $b) => $this->translator->trans($a->getTitle()) <=> $this->translator->trans($b->getTitle()));
    }

    /**
     * @param array<int|string, AggregatorInterface> $elements
     */
    public function sortAggregators(array &$elements): void
    {
        uasort($elements, fn (AggregatorInterface $a, AggregatorInterface $b) => $this->translator->trans($a->getTitle()) <=> $this->translator->trans($b->getTitle()));
    }
}
