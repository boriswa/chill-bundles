<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Export;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Response;

interface DirectExportInterface extends ExportElementInterface
{
    /**
     * Add a form to collect data from the user.
     */
    public function buildForm(FormBuilderInterface $builder);

    /**
     * Get the default data, that can be use as "data" for the form.
     */
    public function getFormDefaultData(): array;

    /**
     * Generate the export.
     */
    public function generate(array $acl, array $data = []): Response;

    /**
     * get a description, which will be used in UI (and translated).
     */
    public function getDescription(): string;

    /**
     * authorized role.
     */
    public function requiredRole(): string;
}
