<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Export;

/**
 * The common methods between different object used to build export (i.e. : ExportInterface,
 * FilterInterface, AggregatorInterface).
 */
interface ExportElementInterface
{
    /**
     * get a title, which will be used in UI (and translated).
     *
     * @return string
     */
    public function getTitle();
}
