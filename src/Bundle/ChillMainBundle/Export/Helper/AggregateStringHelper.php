<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Export\Helper;

/**
 * This class provides support to transform aggregates datas in correct format string into column of exports.
 */
class AggregateStringHelper
{
    public function getLabelMulti(string $key, array $values, string $header)
    {
        return static function ($value) use ($header) {
            if ('_header' === $value) {
                return $header;
            }

            if (null === $value || '' === $value) {
                return '';
            }

            return implode(
                '|',
                json_decode((string) $value, true, 512, JSON_THROW_ON_ERROR)
            );
        };
    }
}
