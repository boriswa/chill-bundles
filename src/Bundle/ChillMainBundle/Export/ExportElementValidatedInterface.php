<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Export;

use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Add a validation method to validate data collected by Export Element.
 *
 * Implements this interface on other `ExportElementInterface` to validate
 * the form submitted by users.
 *
 * **note**: When this interface is implemented on filters or aggregators,
 * the form is validated **only** if the filter/aggregator is `enabled` by the
 * user.
 *
 * @see http://symfony.com/doc/current/validation/custom_constraint.html#class-constraint-validator Example of building violations
 */
interface ExportElementValidatedInterface
{
    /**
     * validate the form's data and, if required, build a contraint
     * violation on the data.
     *
     * @param mixed $data the data, as returned by the user
     */
    public function validateForm(mixed $data, ExecutionContextInterface $context);
}
