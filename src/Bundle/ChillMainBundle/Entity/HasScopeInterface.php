<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

/**
 * Interface for entities which have a scop.
 */
interface HasScopeInterface
{
    /**
     * Return the linked scope.
     *
     * @return Scope
     */
    public function getScope();
}
