<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Context;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Country.
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="country")
 *
 * @ORM\Cache(usage="READ_ONLY", region="country_cache_region")
 *
 * @ORM\HasLifecycleCallbacks
 */
class Country
{
    /**
     * @ORM\Column(type="string", length=3)
     *
     * @groups({"read", "docgen:read"})
     *
     * @Context({"is-translatable": true}, groups={"docgen:read"})
     */
    private string $countryCode = '';

    /**
     * @ORM\Id
     *
     * @ORM\Column(name="id", type="integer")
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @groups({"read", "docgen:read"})
     */
    private ?int $id = null;

    /**
     * @var array<string, string>
     *
     * @ORM\Column(type="json")
     *
     * @groups({"read", "docgen:read"})
     *
     * @Context({"is-translatable": true}, groups={"docgen:read"})
     */
    private array $name = [];

    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get name.
     */
    public function getName(): array
    {
        return $this->name;
    }

    public function setCountryCode(?string $countryCode): self
    {
        $this->countryCode = (string) $countryCode;

        return $this;
    }

    /**
     * @param array<string, string> $name
     */
    public function setName(array $name): self
    {
        $this->name = $name;

        return $this;
    }
}
