<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * Language.
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="language")
 *
 * @ORM\Cache(usage="READ_ONLY", region="language_cache_region")
 *
 * @ORM\HasLifecycleCallbacks
 */
class Language
{
    /**
     * @ORM\Id
     *
     * @ORM\Column(type="string")
     *
     * @Serializer\Groups({"docgen:read"})
     */
    private ?string $id = null;

    /**
     * @var string array
     *
     * @ORM\Column(type="json")
     *
     * @Serializer\Groups({"docgen:read"})
     *
     * @Serializer\Context({"is-translatable": true}, groups={"docgen:read"})
     */
    private array $name = [];

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string array
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set id.
     *
     * @param string $id
     *
     * @return Language
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set name.
     *
     * @param string array $name
     *
     * @return Language
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}
