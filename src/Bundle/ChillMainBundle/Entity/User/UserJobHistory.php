<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity\User;

use App\Repository\UserJobHistoryRepository;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\UserJob;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="chill_main_user_job_history")
 *
 * @ORM\Entity(repositoryClass=UserJobHistoryRepository::class)
 */
class UserJobHistory
{
    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $endDate = null;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity=UserJob::class)
     */
    private ?UserJob $job = null;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $startDate;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private User $user;

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJob(): ?UserJob
    {
        return $this->job;
    }

    public function getStartDate(): ?\DateTimeImmutable
    {
        return $this->startDate;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setEndDate(?\DateTimeImmutable $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function setJob(?UserJob $job): UserJobHistory
    {
        $this->job = $job;

        return $this;
    }

    public function setStartDate(\DateTimeImmutable $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function setUser(User $user): UserJobHistory
    {
        $this->user = $user;

        return $this;
    }
}
