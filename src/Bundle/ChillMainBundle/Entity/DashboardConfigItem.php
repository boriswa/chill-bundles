<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="chill_main_dashboard_config_item")
 */
class DashboardConfigItem
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"dashboardConfigItem:read", "read"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string")
     *
     * @Serializer\Groups({"dashboardConfigItem:read", "read"})
     *
     * @Assert\NotNull
     */
    private string $type = '';

    /**
     * @ORM\Column(type="string")
     *
     * @Serializer\Groups({"dashboardConfigItem:read", "read"})
     *
     * @Assert\NotNull
     */
    private string $position = '';

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private ?User $user = null;

    /**
     * @ORM\Column(type="json", options={"default": "[]", "jsonb": true})
     *
     * @Serializer\Groups({"dashboardConfigItem:read"})
     */
    private array $metadata = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPosition(): string
    {
        return $this->position;
    }

    public function setPosition(string $position): void
    {
        $this->position = $position;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getMetadata(): array
    {
        return $this->metadata;
    }

    public function setMetadata(array $metadata): void
    {
        $this->metadata = $metadata;
    }
}
