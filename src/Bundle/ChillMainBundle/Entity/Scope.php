<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Context;
use Symfony\Component\Serializer\Annotation\DiscriminatorMap;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="scopes")
 *
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="acl_cache_region")
 *
 * @DiscriminatorMap(typeProperty="type", mapping={
 *     "scope": Scope::class
 * })
 */
class Scope
{
    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": true})
     */
    private bool $active = true;

    /**
     * @ORM\Id
     *
     * @ORM\Column(name="id", type="integer")
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups({"read", "docgen:read"})
     */
    private ?int $id = null;

    /**
     * translatable names.
     *
     * @ORM\Column(type="json")
     *
     * @Groups({"read", "docgen:read"})
     *
     * @Context({"is-translatable": true}, groups={"docgen:read"})
     */
    private array $name = [];

    /**
     * @var Collection<RoleScope>
     *
     * @ORM\OneToMany(
     *     targetEntity="Chill\MainBundle\Entity\RoleScope",
     * mappedBy="scope")
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private Collection $roleScopes;

    /**
     * Scope constructor.
     */
    public function __construct()
    {
        $this->roleScopes = new ArrayCollection();
    }

    public function addRoleScope(RoleScope $roleScope): self
    {
        $this->roleScopes->add($roleScope);

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): array
    {
        return $this->name;
    }

    public function getRoleScopes(): Collection
    {
        return $this->roleScopes;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): Scope
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return $this
     */
    public function setName(array $name): self
    {
        $this->name = $name;

        return $this;
    }
}
