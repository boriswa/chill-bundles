<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Chill\MainBundle\Doctrine\Model\Point;
use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * PostalCode.
 *
 * @ORM\Entity
 *
 * @ORM\Table(
 *     name="chill_main_postal_code",
 *     uniqueConstraints={
 *
 *         @ORM\UniqueConstraint(name="postal_code_import_unicity", columns={"code", "refpostalcodeid", "postalcodesource"},
 *         options={"where": "refpostalcodeid is not null"})
 *     },
 *     indexes={
 *
 *         @ORM\Index(name="search_name_code", columns={"code", "label"}),
 *         @ORM\Index(name="search_by_reference_code", columns={"code", "refpostalcodeid"})
 *     })
 *
 *     @ORM\HasLifecycleCallbacks
 */
class PostalCode implements TrackUpdateInterface, TrackCreationInterface
{
    use TrackCreationTrait;

    use TrackUpdateTrait;

    /**
     * This is an internal column which is populated by database.
     *
     * This column will ease the search operations
     *
     * @ORM\Column(type="text", options={"default": ""})
     */
    private string $canonical = '';

    /**
     * @ORM\Column(type="point", nullable=true)
     *
     * @groups({"read"})
     */
    private ?Point $center = null;

    /**
     * @ORM\Column(type="string", length=100)
     *
     * @groups({"write", "read"})
     */
    private ?string $code = null;

    /**
     * @ORM\ManyToOne(targetEntity="Chill\MainBundle\Entity\Country")
     *
     * @groups({"write", "read"})
     */
    private ?Country $country = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true, options={"default": null})
     */
    private ?\DateTimeImmutable $deletedAt = null;

    /**
     * @ORM\Id
     *
     * @ORM\Column(name="id", type="integer")
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @groups({"write", "read"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255, name="label")
     *
     * @groups({"write", "read"})
     */
    private ?string $name = null;

    /**
     * @ORM\Column(name="origin", type="integer", nullable=true)
     *
     * @groups({"write", "read"})
     */
    private int $origin = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @groups({"read"})
     */
    private ?string $postalCodeSource = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @groups({"read"})
     */
    private ?string $refPostalCodeId = null;

    public function getCenter(): ?Point
    {
        return $this->center;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get country.
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get origin.
     *
     * @return int
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    public function getPostalCodeSource(): ?string
    {
        return $this->postalCodeSource;
    }

    public function getRefPostalCodeId(): ?string
    {
        return $this->refPostalCodeId;
    }

    public function setCenter(?Point $center): self
    {
        $this->center = $center;

        return $this;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return PostalCode
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Set country.
     *
     * @return PostalCode
     */
    public function setCountry(?Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return PostalCode
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set origin.
     *
     * @param int $origin
     *
     * @return PostalCode
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;

        return $this;
    }

    public function setPostalCodeSource(?string $postalCodeSource): self
    {
        $this->postalCodeSource = $postalCodeSource;

        return $this;
    }

    public function setRefPostalCodeId(?string $refPostalCodeId): self
    {
        $this->refPostalCodeId = $refPostalCodeId;

        return $this;
    }
}
