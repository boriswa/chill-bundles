<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="chill_main_news")
 */
class NewsItem implements TrackCreationInterface, TrackUpdateInterface
{
    use TrackCreationTrait;

    use TrackUpdateTrait;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @Groups({"read"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="text")
     *
     * @Groups({"read"})
     *
     * @Assert\NotBlank
     *
     * @Assert\NotNull
     */
    private string $title = '';

    /**
     * @ORM\Column(type="text")
     *
     * @Groups({"read"})
     *
     * @Assert\NotBlank
     *
     * @Assert\NotNull
     */
    private string $content = '';

    /**
     * @ORM\Column(type="date_immutable", nullable=false)
     *
     * @Assert\NotNull
     *
     * @Groups({"read"})
     */
    private ?\DateTimeImmutable $startDate = null;

    /**
     * @ORM\Column(type="date_immutable", nullable=true, options={"default": null})
     *
     * @Assert\GreaterThanOrEqual(propertyPath="startDate")
     *
     * @Groups({"read"})
     */
    private ?\DateTimeImmutable $endDate = null;

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function getStartDate(): ?\DateTimeImmutable
    {
        return $this->startDate;
    }

    public function setStartDate(?\DateTimeImmutable $startDate): void
    {
        $this->startDate = $startDate;
    }

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeImmutable $endDate): void
    {
        $this->endDate = $endDate;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
}
