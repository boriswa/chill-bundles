<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity\Embeddable;

use Chill\MainBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
class PrivateCommentEmbeddable
{
    /**
     * @ORM\Column(type="json", nullable=false, options={"default": "{}"})
     *
     * @var array<int, string>
     */
    private array $comments = [];

    public function getCommentForUser(User $user): string
    {
        return $this->comments[$user->getId()] ?? '';
    }

    public function getComments(): ?array
    {
        return $this->comments;
    }

    public function hasCommentForUser(User $user): bool
    {
        return \array_key_exists($user->getId(), $this->comments)
            && '' !== $this->comments[$user->getId()];
    }

    public function merge(PrivateCommentEmbeddable $newComment): self
    {
        $currentComments = $this->getComments() ?? [];

        $mergedComments = $newComment->getComments() + $currentComments;

        $this->setComments($mergedComments);

        return $this;
    }

    public function setCommentForUser(User $user, ?string $content): self
    {
        $this->comments[$user->getId()] = trim((string) $content);

        return $this;
    }

    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }
}
