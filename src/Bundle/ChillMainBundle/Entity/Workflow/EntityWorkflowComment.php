<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity\Workflow;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 *
 * @ORM\Table("chill_main_workflow_entity_comment")
 */
class EntityWorkflowComment implements TrackCreationInterface, TrackUpdateInterface
{
    use TrackCreationTrait;

    use TrackUpdateTrait;

    /**
     * @ORM\Column(type="text", options={"default": ""})
     */
    private string $comment = '';

    /**
     * @ORM\ManyToOne(targetEntity=EntityWorkflow::class, inversedBy="comments")
     */
    private ?EntityWorkflow $entityWorkflow = null;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    public function getComment(): string
    {
        return $this->comment;
    }

    public function getEntityWorkflow(): ?EntityWorkflow
    {
        return $this->entityWorkflow;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @internal use @see{EntityWorkflow::addComment}
     */
    public function setEntityWorkflow(?EntityWorkflow $entityWorkflow): self
    {
        $this->entityWorkflow = $entityWorkflow;

        return $this;
    }
}
