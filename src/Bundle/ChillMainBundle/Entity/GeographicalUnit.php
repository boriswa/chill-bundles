<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="chill_main_geographical_unit", uniqueConstraints={
 *
 *     @ORM\UniqueConstraint(name="geographical_unit_refid", columns={"layer_id", "unitRefId"})
 * })
 *
 * @ORM\Entity(readOnly=true)
 */
class GeographicalUnit
{
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private string $geom;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity=GeographicalUnitLayer::class, inversedBy="units")
     */
    private ?GeographicalUnitLayer $layer = null;

    /**
     * @ORM\Column(type="text", nullable=false, options={"default": ""})
     */
    private string $unitName;

    /**
     * @ORM\Column(type="text", nullable=false, options={"default": ""})
     */
    private string $unitRefId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLayer(): ?GeographicalUnitLayer
    {
        return $this->layer;
    }

    public function getUnitName(): ?string
    {
        return $this->unitName;
    }

    public function setLayer(?GeographicalUnitLayer $layer): GeographicalUnit
    {
        $this->layer = $layer;

        return $this;
    }

    public function setUnitName(?string $unitName): self
    {
        $this->unitName = $unitName;

        return $this;
    }

    public function setUnitRefId(string $unitRefId): GeographicalUnit
    {
        $this->unitRefId = $unitRefId;

        return $this;
    }

    protected function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }
}
