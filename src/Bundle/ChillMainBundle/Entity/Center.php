<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="centers")
 */
class Center implements HasCenterInterface, \Stringable
{
    /**
     * @var Collection<GroupCenter>
     *
     * @ORM\OneToMany(
     *     targetEntity="Chill\MainBundle\Entity\GroupCenter",
     *     mappedBy="center"
     * )
     */
    private Collection $groupCenters;

    /**
     * @ORM\Id
     *
     * @ORM\Column(name="id", type="integer")
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Groups({"docgen:read"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Serializer\Groups({"docgen:read"})
     */
    private string $name = '';

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive = true;

    /**
     * @var Collection<Regroupment>
     *
     * @ORM\ManyToMany(targetEntity=Regroupment::class, mappedBy="centers")
     */
    private Collection $regroupments;

    /**
     * Center constructor.
     */
    public function __construct()
    {
        $this->groupCenters = new ArrayCollection();
        $this->regroupments = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    /**
     * @return $this
     */
    public function addGroupCenter(GroupCenter $groupCenter)
    {
        $this->groupCenters->add($groupCenter);

        return $this;
    }

    /**
     * @return $this|Center
     */
    public function getCenter()
    {
        return $this;
    }

    public function getGroupCenters(): ArrayCollection|Collection
    {
        return $this->groupCenters;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Collection<Regroupment>
     */
    public function getRegroupments(): Collection
    {
        return $this->regroupments;
    }

    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
}
