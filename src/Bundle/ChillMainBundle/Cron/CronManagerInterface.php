<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Cron;

interface CronManagerInterface
{
    /**
     * Execute one job, with a given priority, or the given job (identified by his key).
     */
    public function run(?string $forceJob = null): void;
}
