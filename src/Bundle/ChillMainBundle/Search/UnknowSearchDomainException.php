<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Search;

/**
 * Throw by search provider when the search name is not found.
 */
class UnknowSearchDomainException extends \Exception
{
    private $domain;

    public function __construct($domain)
    {
        parent::__construct("The domain {$domain} is not found");
        $this->domain = $domain;
    }

    public function getDomain()
    {
        return $this->domain;
    }
}
