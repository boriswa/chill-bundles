<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Search;

class SearchApiNoQueryException extends \RuntimeException
{
    private readonly string $pattern;

    private readonly array $types;

    public function __construct(string $pattern = '', array $types = [], private readonly array $parameters = [], $code = 0, ?\Throwable $previous = null)
    {
        $typesStr = \implode(', ', $types);
        $message = "No query for this search: pattern : {$pattern}, types: {$typesStr}";
        $this->pattern = $pattern;
        $this->types = $types;

        parent::__construct($message, $code, $previous);
    }
}
