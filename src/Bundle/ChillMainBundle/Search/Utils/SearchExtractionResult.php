<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Search\Utils;

class SearchExtractionResult
{
    public function __construct(private readonly string $filteredSubject, private readonly array $found)
    {
    }

    public function getFilteredSubject(): string
    {
        return $this->filteredSubject;
    }

    public function getFound(): array
    {
        return $this->found;
    }

    public function hasResult(): bool
    {
        return [] !== $this->found;
    }
}
