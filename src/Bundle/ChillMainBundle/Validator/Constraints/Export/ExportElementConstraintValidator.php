<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Validator\Constraints\Export;

use Chill\MainBundle\Export\ExportElementValidatedInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * This validator validate the _export element_ if this element implements
 * {@link ExportElementValidatedInterface}.
 */
class ExportElementConstraintValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if ($constraint->element instanceof ExportElementValidatedInterface) {
            if (true === $value['enabled']) {
                $constraint->element->validateForm($value['form'], $this->context);
            }
        }
    }
}
