const buildLinkCreate = function(workflowName, relatedEntityClass, relatedEntityId) {
    let params = new URLSearchParams();
    params.set('entityClass', relatedEntityClass);
    params.set('entityId', relatedEntityId);
    params.set('workflow', workflowName);

    return `/fr/main/workflow/create?`+params.toString();
};

export {
    buildLinkCreate,
};
