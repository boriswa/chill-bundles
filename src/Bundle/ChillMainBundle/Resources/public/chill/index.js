/*
 * CHILL Entrypoint
 * Load from node-modules
 */

/// import jQuery
const $ = require('jquery');
global.$ = global.jQuery = $;

/// import select2
const select2 = require('select2');
global.select2 = select2;

require('select2/dist/css/select2.css');
require('select2-bootstrap-theme/dist/select2-bootstrap.css');


/*
 * Load Chill themes assets
 */

require('./chillmain.scss');

import { chill } from './js/chill.js';
global.chill = chill;

require('./js/date');
require('./js/counter.js');

/// Load fonts
require('./fonts/OpenSans/OpenSans.scss')

/// Load images
require('./img/favicon.ico');
require('./img/logo-chill-sans-slogan_white.png');
require('./img/logo-chill-outil-accompagnement_white.png');


/*
 * Load local libs
 * Some libs are only used in a few pages, they are loaded on a case by case basis
 */

require('../lib/collection/index.js');

require('../lib/breadcrumb/index.js');
require('../lib/download-report/index.js');
require('../lib/select_interactive_loading/index.js');

//require('../lib/show_hide/index.js');
//require('../lib/tabs/index.js');

