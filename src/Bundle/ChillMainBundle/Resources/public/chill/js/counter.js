/**
 *
 * This script search for span.counter elements like
 *   <span class="counter">Il y a 4 notifications</span>
 * and return
 *   <span class="counter">Il y a <span>4</span> notifications</span>
 *
 */
const isNum = (v) => !isNaN(v);

const parseCounter = () => {
    document.querySelectorAll('span.counter')
        .forEach(el => {
            let r = [];
            el.innerText
                .trim()
                .split(' ')
                .forEach(w => {
                    if (isNum(w)) {
                        r.push(`<span>${w}</span>`);
                    } else {
                        r.push(w);
                    }
                })
            ;
            el.innerHTML = r.join(' ');
        })
    ;
};

window.addEventListener('DOMContentLoaded', function (e) {
    parseCounter();
});

export { parseCounter };
