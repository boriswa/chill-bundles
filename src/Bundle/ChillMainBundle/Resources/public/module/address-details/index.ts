import AddressDetailsButton from "../../vuejs/_components/AddressDetails/AddressDetailsButton.vue";
import {createApp} from "vue";
import {createI18n} from "vue-i18n";
import {_createI18n} from "../../vuejs/_js/i18n";
import {Address} from "../../types";

const i18n = _createI18n({});

document.querySelectorAll<HTMLSpanElement>('span[data-address-details]').forEach((el) => {
  const dataset = el.dataset as {
    addressId: string,
    addressRefStatus: string,
  };

  const app = createApp({
    components: {AddressDetailsButton},
    data() {
      return {
        addressId: Number.parseInt(dataset.addressId),
        addressRefStatus: dataset.addressRefStatus,
      }
    },
    template: '<address-details-button :address_id="addressId" :address_ref_status="addressRefStatus" @update-address="onUpdateAddress"></address-details-button>',
    methods: {
      onUpdateAddress: (address: Address): void => {
        if (address.refStatus === 'to_review' || address.refStatus === 'reviewed') {
          // in this two case, the address content do not change
          return;
        }
        if (window.confirm("L'adresse a été modifiée. Vous pouvez continuer votre travail. Cependant, pour afficher les données immédiatement, veuillez recharger la page. \n\n Voulez-vous recharger la page immédiatement ?")) {
          window.location.reload();
        }
      }
    }
  });

  app.use(i18n);
  app.mount(el);
});
