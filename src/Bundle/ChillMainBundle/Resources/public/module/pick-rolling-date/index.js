import {ShowHide} from 'ChillMainAssets/lib/show_hide/index';

document.addEventListener('DOMContentLoaded', function(_e) {
    document.querySelectorAll('div[data-rolling-date]').forEach( (picker) => {
        const
            roll_wrapper = picker.querySelector('div.roll-wrapper'),
            fixed_wrapper = picker.querySelector('div.fixed-wrapper');

        new ShowHide({
            froms: [roll_wrapper],
            container: [fixed_wrapper],
            test: function (elems) {
                for (let el of elems) {
                    for (let select_roll of el.querySelectorAll('select[data-roll-picker]')) {
                        return select_roll.value === 'fixed_date';
                    }
                }
                return false;
            }
        })
    });
});

