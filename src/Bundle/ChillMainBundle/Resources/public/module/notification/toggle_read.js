import {createApp} from "vue";
import NotificationReadToggle from "ChillMainAssets/vuejs/_components/Notification/NotificationReadToggle.vue";
import { _createI18n } from "ChillMainAssets/vuejs/_js/i18n";

const i18n = _createI18n({});

window.addEventListener('DOMContentLoaded', function (e) {
    document.querySelectorAll('.notification_toggle_read_status')
        .forEach(function (el, i) {
            createApp({
                template: `<notification-read-toggle
                    :notificationId="notificationId"
                    :buttonClass="buttonClass"
                    :buttonNoText="buttonNoText"
                    :showUrl="showUrl"
                    :isRead="isRead"
                    @markRead="onMarkRead"
                    @markUnread="onMarkUnread">
                </notification-read-toggle>`,
                components: {
                    NotificationReadToggle,
                },
                data() {
                    return {
                        notificationId: el.dataset.notificationId,
                        buttonClass: el.dataset.buttonClass,
                        buttonNoText: 'false' === el.dataset.buttonText,
                        showUrl: el.dataset.showButtonUrl,
                        isRead: 1 === Number.parseInt(el.dataset.notificationCurrentIsRead),
                        container: el.dataset.container
                    }
                },
                computed: {
                    getContainer() {
                        return document.querySelectorAll(`div.${this.container}`);
                    }
                },
                methods: {
                    onMarkRead() {
                        if (typeof this.getContainer[i] !== 'undefined') {
                            this.getContainer[i].classList.replace('read', 'unread');
                        } else { throw 'data-container attribute is missing' }
                        this.isRead = false;
                    },
                    onMarkUnread() {
                        if (typeof this.getContainer[i] !== 'undefined') {
                            this.getContainer[i].classList.replace('unread', 'read');
                        } else { throw 'data-container attribute is missing' }
                        this.isRead = true;
                    },
                }
            })
            .use(i18n)
            .mount(el);
        });
});
