// Compile all bootstrap assets from nodes-modules
//require('bootstrap/scss/bootstrap.scss')

// Or compile bootstrap only enabled assets
require('./bootstrap.scss');

// You can specify which plugins you need
import Dropdown from 'bootstrap/js/src/dropdown';
import Modal from 'bootstrap/js/dist/modal';
import Collapse from 'bootstrap/js/src/collapse';
import Carousel from 'bootstrap/js/src/carousel';
import Popover from 'bootstrap/js/src/popover';

//
// Carousel: ACHeaderSlider is a small slider used in banner of AccompanyingCourse Section
// Initialize options, and show/hide controls in first/last slides
//
let ACHeaderSlider = document.querySelector('#ACHeaderSlider');
if (ACHeaderSlider) {
    let controlPrev = ACHeaderSlider.querySelector('button[data-bs-slide="prev"]'),
        controlNext = ACHeaderSlider.querySelector('button[data-bs-slide="next"]'),
        length = ACHeaderSlider.querySelectorAll('.carousel-item').length,
        last = length-1,
        carousel = new Carousel(ACHeaderSlider, {
            interval: false,
            wrap: false,
            ride: false,
            keyboard: false,
            touch: true
        })
    ;
    document.addEventListener('DOMContentLoaded', (e) => {
        controlNext.classList.remove('visually-hidden');
    });
    ACHeaderSlider.addEventListener('slid.bs.carousel', (e) => {
        //console.log('from slide', e.direction, e.relatedTarget, e.from, e.to );
        switch (e.to) {
            case 0:
                controlPrev.classList.add('visually-hidden');
                controlNext.classList.remove('visually-hidden');
                break;
            case last:
                controlPrev.classList.remove('visually-hidden');
                controlNext.classList.add('visually-hidden');
                break;
            default:
                controlPrev.classList.remove('visually-hidden');
                controlNext.classList.remove('visually-hidden');
        }
    })
}

//
// Popover: used in workflow breadcrumb,
// (expected in: contextual help, notification-box, workflow-box )
//
const triggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'));
const popoverList = triggerList.map(function (el) {
    return new Popover(el, {
        html: true,
    });
});