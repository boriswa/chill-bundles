import { createApp } from "vue";
import ListWorkflowModalVue from 'ChillMainAssets/vuejs/_components/EntityWorkflow/ListWorkflowModal.vue';
import { _createI18n } from "ChillMainAssets/vuejs/_js/i18n";

const i18n = _createI18n({});

// list workflow
document.querySelectorAll('[data-list-workflows]')
    .forEach(function (el) {
        const app = {
            components: {
                ListWorkflowModalVue,
            },
            template:
                '<list-workflow-modal-vue ' +
                    ':workflows="workflows" ' +
                    ':allowCreate="allowCreate" ' +
                    ':relatedEntityClass="relatedEntityClass" ' +
                    ':relatedEntityId="relatedEntityId" ' +
                    ':workflowsAvailables="workflowsAvailables" ' +
                '></list-workflow-modal-vue>',
            data() {
                return {
                    workflows: JSON.parse(el.dataset.workflows),
                    allowCreate: el.dataset.allowCreate === "1",
                    relatedEntityClass: el.dataset.relatedEntityClass,
                    relatedEntityId: Number.parseInt(el.dataset.relatedEntityId),
                    workflowsAvailables: JSON.parse(el.dataset.workflowsAvailables),
                }
            }
        };
        createApp(app).use(i18n).mount(el);
    })
;