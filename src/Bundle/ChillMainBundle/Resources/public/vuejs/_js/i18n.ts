import { createI18n } from 'vue-i18n';
import datetimeFormats from '../i18n/datetimeFormats';

const messages = {
   fr: {
      action: {
         actions: "Actions",
         show: "Voir",
         edit: "Modifier",
         create: "Créer",
         remove: "Enlever",
         delete: "Supprimer",
         save: "Enregistrer",
         valid: "Valider",
         valid_and_see: "Valider et voir",
         add: "Ajouter",
         show_modal: "Ouvrir une modale",
         ok: "OK",
         cancel: "Annuler",
         close: "Fermer",
         back: "Retour",
         check_all: "cocher tout",
         reset: "réinitialiser",
         redirect: {
            person: "Quitter la page et ouvrir la fiche de l'usager",
            thirdparty: "Quitter la page et voir le tiers",
         },
         refresh: 'Rafraîchir',
         addContact: 'Ajouter un contact'
      },
      nav: {
         next: "Suivant",
         previous: "Précédent",
         top: "Haut",
         bottom: "Bas",
      },
      renderbox: {
         person: "Usager",
         birthday: {
            man: "Né le",
            woman: "Née le"
         },
         deathdate: "Date de décès",
         household_without_address: "Le ménage de l'usager est sans adresse",
         no_data: "Aucune information renseignée",
         type: {
            thirdparty: "Tiers",
            person: "Usager"
         },
         holder: "Titulaire",
         years_old: "1 an | {n} an | {n} ans",
         residential_address: "Adresse de résidence",
         located_at: "réside chez"
      },
   }
};

const _createI18n = (appMessages: any, legacy?: boolean) => {
   Object.assign(messages.fr, appMessages.fr);
   return createI18n({
      legacy: typeof legacy === undefined ? true : legacy,
      locale: 'fr',
      fallbackLocale: 'fr',
      // @ts-ignore
      datetimeFormats,
      messages,
   })
};

export { _createI18n }

export const multiSelectMessages = {
  fr: {
    multiselect: {
      placeholder: 'Choisir',
      tag_placeholder: 'Créer un nouvel élément',
      select_label: '"Entrée" ou cliquez pour sélectionner',
      deselect_label: '"Entrée" ou cliquez pour désélectionner',
      select_group_label: 'Appuyer sur "Entrée" pour sélectionner ce groupe',
      deselect_group_label: 'Appuyer sur "Entrée" pour désélectionner ce groupe',
      selected_label: 'Sélectionné'
    }
  }
};
