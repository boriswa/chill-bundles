import { createApp } from 'vue';
import { _createI18n } from 'ChillMainAssets/vuejs/_js/i18n';
import { addressMessages } from './i18n';
import App from './App.vue';

const i18n = _createI18n( addressMessages );

let containers = document.querySelectorAll('.address-container');
containers.forEach((container) => {

    const app = createApp({
        template: `<app v-bind:addAddress="this.addAddress" ></app>`,
        data() {
            return {
                addAddress: {
                    context: {
                        target: {
                            name: container.dataset.targetName,
                            id: parseInt(container.dataset.targetId)
                        },
                        edit: container.dataset.mode === 'edit',  //boolean
                        addressId: parseInt(container.dataset.addressId) || null,
                        backUrl: container.dataset.backUrl || null,
                        defaults: JSON.parse(container.dataset.addressDefaults)
                    },
                    options: {
                        /// Options override default.
                        /// null value take default component value defined in AddAddress data()
                        button: {
                            text:  {
                                create: container.dataset.buttonText || null,
                                edit: container.dataset.buttonText || null
                            },
                            size: container.dataset.buttonSize || null,
                            displayText: container.dataset.buttonDisplayText !== 'false'  //boolean, default: true
                        },

                        /// Modal title text if create or edit address (trans chain, see i18n)
                        title: {
                            create: container.dataset.modalTitle || null,
                            edit: container.dataset.modalTitle || null
                        },

                        /// Display panes in Modal for step123
                        openPanesInModal: container.dataset.openPanesInModal !== 'false',  //boolean, default: true

                        /// Display actions buttons of panes in a sticky-form-button navbar
                        stickyActions: container.dataset.stickyActions === 'true',  //boolean, default: false

                        /// Use Date fields
                        useDate: {
                            validFrom: container.dataset.useValidFrom === 'true',  //boolean, default: false
                            validTo: container.dataset.useValidTo === 'true'       //boolean, default: false
                        },

                        /// Don't display show renderbox Address: showPane display only a button
                        onlyButton: container.dataset.onlyButton === 'true'       //boolean, default: false
                    }
                }
            }
        }
    })
    .use(i18n)
    .component('app', App)
    .mount(container);

    //console.log('container dataset', container.dataset);
});
