import { createApp } from "vue";
import { _createI18n } from 'ChillMainAssets/vuejs/_js/i18n';
import { ontheflyMessages } from './i18n.js';
import App from "./App.vue";

const i18n = _createI18n( ontheflyMessages );

let containers = document.querySelectorAll('.onthefly-container');

containers.forEach((container) => {

    const app = createApp({
        template: `<app :onTheFly="this.onTheFly" ></app>`,
        data() {
            return {
                onTheFly: {
                    context: {
                        action:  container.dataset.action,
                        type:  container.dataset.targetName,
                        id: parseInt(container.dataset.targetId),
                    },
                    options: {
                        buttonText: container.dataset.buttonText || null,
                        displayBadge: container.dataset.displayBadge || false,
                        isDead: container.dataset.isDead || false,
                        parent: container.dataset.parent ? JSON.parse(container.dataset.parent) : null,
                    }
                }
            }
        }
    })
    .use(i18n)
    .component('app', App)
    .mount(container);

    //console.log('container dataset', container.dataset);
    //console.log('data-parent', container.dataset.parent);
});
