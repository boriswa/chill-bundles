<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Test;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Prepare a client authenticated with a user.
 */
trait PrepareClientTrait
{
    /**
     * Create a new client with authentication information.
     *
     * @param string $username the username (default 'center a_social')
     * @param string $password the password (default 'password')
     *
     * @throws \LogicException
     */
    public function getClientAuthenticated(
        $username = 'center a_social',
        $password = 'password'
    ): KernelBrowser {
        if (!$this instanceof WebTestCase) {
            throw new \LogicException(sprintf('The current class does not implements %s', WebTestCase::class));
        }

        return static::createClient([], [
            'PHP_AUTH_USER' => $username,
            'PHP_AUTH_PW' => $password,
        ]);
    }
}
