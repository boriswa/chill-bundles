<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Test;

use Chill\MainBundle\Entity\Scope;

/**
 * A trait to prepare center.
 *
 * **Usage :** You must set up trait with `setUpTrait` before use
 * and use tearDownTrait after usage.
 *
 * @codeCoverageIgnore
 */
trait PrepareScopeTrait
{
    /**
     * prepare a mocked scope, with and id and name given.
     *
     * The name will be present in both lang `fr` and `en`.
     */
    protected function prepareScope(int $id, string $name): Scope
    {
        $scope = new Scope();
        // set the name
        $scope->setName(['fr' => $name, 'en' => $name]);
        $reflection = new \ReflectionClass($scope);
        $idProperty = $reflection->getProperty('id');
        $idProperty->setAccessible(true);
        $idProperty->setValue($scope, $id);

        return $scope;
    }
}
