<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Utils;

interface PermissionsGroupFlagProvider
{
    /**
     * Return an array of flags.
     *
     * @return string[] an array. Keys are ignored.
     */
    public function getPermissionsGroupFlags(): array;
}
