<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Type;

use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Entity\PermissionsGroup;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ComposedGroupCenterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('permissionsgroup', EntityType::class, [
            'class' => PermissionsGroup::class,
            'choice_label' => static fn (PermissionsGroup $group) => $group->getName(),
        ])->add('center', EntityType::class, [
            'class' => Center::class,
            'query_builder' => static function (EntityRepository $er) {
                $qb = $er->createQueryBuilder('c');
                $qb->where($qb->expr()->eq('c.isActive', 'TRUE'))
                    ->orderBy('c.name', 'ASC');

                return $qb;
            },
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', \Chill\MainBundle\Entity\GroupCenter::class);
    }

    public function getBlockPrefix()
    {
        return 'composed_groupcenter';
    }
}
