<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Available options :.
 *
 * - `button_add_label`
 * - `button_remove_label`
 * - `identifier`: an identifier to identify the kind of collecton. Useful if some
 *  javascript should be launched associated to `add_entry`, `remove_entry` events.
 * - `empty_collection_explain`
 */
class ChillCollectionType extends AbstractType
{
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['button_add_label'] = $options['button_add_label'];
        $view->vars['button_remove_label'] = $options['button_remove_label'];
        $view->vars['allow_delete'] = (int) $options['allow_delete'];
        $view->vars['allow_add'] = (int) $options['allow_add'];
        $view->vars['identifier'] = $options['identifier'];
        $view->vars['empty_collection_explain'] = $options['empty_collection_explain'];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'button_add_label' => 'Add an entry',
                'button_remove_label' => 'Remove entry',
                'identifier' => '',
                'empty_collection_explain' => '',
            ]);
    }

    public function getParent()
    {
        return \Symfony\Component\Form\Extension\Core\Type\CollectionType::class;
    }
}
