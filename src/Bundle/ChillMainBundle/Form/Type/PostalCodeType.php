<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Type;

use Chill\MainBundle\Entity\PostalCode;
use Chill\MainBundle\Form\ChoiceLoader\PostalCodeChoiceLoader;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * A form to pick between PostalCode.
 */
class PostalCodeType extends AbstractType
{
    /**
     * @var PostalCodeChoiceLoader
     */
    protected $choiceLoader;

    /**
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var UrlGeneratorInterface
     */
    protected $urlGenerator;

    public function __construct(
        TranslatableStringHelper $helper,
        UrlGeneratorInterface $urlGenerator,
        PostalCodeChoiceLoader $choiceLoader,
        TranslatorInterface $translator
    ) {
        $this->translatableStringHelper = $helper;
        $this->urlGenerator = $urlGenerator;
        $this->choiceLoader = $choiceLoader;
        $this->translator = $translator;
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['attr']['data-postal-code'] = 'data-postal-code';
        $view->vars['attr']['data-select-interactive-loading'] = true;
        $view->vars['attr']['data-search-url'] = $this->urlGenerator
            ->generate('chill_main_postal_code_search');
        $view->vars['attr']['data-placeholder'] = $this->translator->trans($options['placeholder']);
        $view->vars['attr']['data-no-results-label'] = $this->translator->trans('select2.no_results');
        $view->vars['attr']['data-error-load-label'] = $this->translator->trans('select2.error_loading');
        $view->vars['attr']['data-searching-label'] = $this->translator->trans('select2.searching');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        // create a local copy for usage in Closure
        $helper = $this->translatableStringHelper;
        $resolver
            ->setDefault('class', PostalCode::class)
            ->setDefault('choice_label', static fn (PostalCode $code) => $code->getCode().' '.$code->getName().' ['.
                  $helper->localize($code->getCountry()->getName()).']')
            ->setDefault('choice_loader', $this->choiceLoader)
            ->setDefault('placeholder', 'Select a postal code');
    }

    public function getParent()
    {
        return EntityType::class;
    }
}
