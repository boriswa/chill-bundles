<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Type;

use Chill\MainBundle\Entity\Embeddable\PrivateCommentEmbeddable;
use Chill\MainBundle\Form\DataMapper\PrivateCommentDataMapper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PrivateCommentType extends AbstractType
{
    public function __construct(protected PrivateCommentDataMapper $dataMapper)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comments', ChillTextareaType::class, [
                'disable_editor' => $options['disable_editor'],
                'label' => $options['label'],
            ])
            ->setDataMapper($this->dataMapper);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['fullWidth'] = true;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefined('disable_editor')
            ->setAllowedTypes('disable_editor', 'bool')
            ->setDefaults([
                'data_class' => PrivateCommentEmbeddable::class,
                'disable_editor' => false,
            ]);
    }
}
