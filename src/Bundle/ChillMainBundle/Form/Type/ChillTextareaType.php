<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Create a Textarea.
 *
 * By default, add a WYSIWYG editor.
 *
 * Options:
 *
 * * `disable_editor`: set true to disable editor
 */
final class ChillTextareaType extends AbstractType
{
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if (!$options['disable_editor']) {
            $view->vars['attr']['ckeditor'] = true;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefined('disable_editor')
            ->setDefault('disable_editor', false)
            ->setAllowedTypes('disable_editor', 'bool');
    }

    public function getParent()
    {
        return TextareaType::class;
    }
}
