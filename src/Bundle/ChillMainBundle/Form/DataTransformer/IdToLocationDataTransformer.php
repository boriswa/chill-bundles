<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\DataTransformer;

use Chill\MainBundle\Repository\LocationRepository;

class IdToLocationDataTransformer extends IdToEntityDataTransformer
{
    public function __construct(LocationRepository $repository)
    {
        parent::__construct($repository, false);
    }
}
