<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form;

use Chill\MainBundle\Entity\NewsItem;
use Chill\MainBundle\Form\Type\ChillDateType;
use Chill\MainBundle\Form\Type\ChillTextareaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'required' => true,
            ])
            ->add('content', ChillTextareaType::class, [
                'required' => false,
            ])
            ->add(
                'startDate',
                ChillDateType::class,
                [
                    'required' => true,
                    'input' => 'datetime_immutable',
                    'label' => 'news.startDate',
                ]
            )
            ->add('endDate', ChillDateType::class, [
                'required' => false,
                'input' => 'datetime_immutable',
                'label' => 'news.endDate',
            ]);
    }

    /**
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', NewsItem::class);
    }
}
