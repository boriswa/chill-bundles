<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form;

use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Entity\Workflow\EntityWorkflowStep;
use Chill\MainBundle\Form\Type\ChillCollectionType;
use Chill\MainBundle\Form\Type\ChillTextareaType;
use Chill\MainBundle\Form\Type\PickUserDynamicType;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\MainBundle\Workflow\EntityWorkflowManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Workflow\Registry;
use Symfony\Component\Workflow\Transition;

class WorkflowStepType extends AbstractType
{
    public function __construct(private readonly EntityWorkflowManager $entityWorkflowManager, private readonly Registry $registry, private readonly TranslatableStringHelperInterface $translatableStringHelper)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var EntityWorkflow $entityWorkflow */
        $entityWorkflow = $options['entity_workflow'];
        $handler = $this->entityWorkflowManager->getHandler($entityWorkflow);
        $workflow = $this->registry->get($entityWorkflow, $entityWorkflow->getWorkflowName());
        $place = $workflow->getMarking($entityWorkflow);
        $placeMetadata = $workflow->getMetadataStore()->getPlaceMetadata(array_keys($place->getPlaces())[0]);

        if (true === $options['transition']) {
            if (null === $options['entity_workflow']) {
                throw new \LogicException('if transition is true, entity_workflow should be defined');
            }

            $transitions = $this->registry
                ->get($options['entity_workflow'], $entityWorkflow->getWorkflowName())
                ->getEnabledTransitions($entityWorkflow);

            $choices = array_combine(
                array_map(
                    static fn (Transition $transition) => $transition->getName(),
                    $transitions
                ),
                $transitions
            );

            if (\array_key_exists('validationFilterInputLabels', $placeMetadata)) {
                $inputLabels = $placeMetadata['validationFilterInputLabels'];

                $builder->add('transitionFilter', ChoiceType::class, [
                    'multiple' => false,
                    'label' => 'workflow.My decision',
                    'choices' => [
                        'forward' => 'forward',
                        'backward' => 'backward',
                        'neutral' => 'neutral',
                    ],
                    'choice_label' => fn (string $key) => $this->translatableStringHelper->localize($inputLabels[$key]),
                    'choice_attr' => static fn (string $key) => [
                        $key => $key,
                    ],
                    'mapped' => false,
                    'expanded' => true,
                    'data' => 'forward',
                ]);
            }

            $builder
                ->add('transition', ChoiceType::class, [
                    'label' => 'workflow.Next step',
                    'mapped' => false,
                    'multiple' => false,
                    'expanded' => true,
                    'choices' => $choices,
                    'constraints' => [new NotNull()],
                    'choice_label' => function (Transition $transition) use ($workflow) {
                        $meta = $workflow->getMetadataStore()->getTransitionMetadata($transition);

                        if (\array_key_exists('label', $meta)) {
                            return $this->translatableStringHelper->localize($meta['label']);
                        }

                        return $transition->getName();
                    },
                    'choice_attr' => static function (Transition $transition) use ($workflow) {
                        $toFinal = true;
                        $isForward = 'neutral';

                        $metadata = $workflow->getMetadataStore()->getTransitionMetadata($transition);

                        if (\array_key_exists('isForward', $metadata)) {
                            if ($metadata['isForward']) {
                                $isForward = 'forward';
                            } else {
                                $isForward = 'backward';
                            }
                        }

                        foreach ($transition->getTos() as $to) {
                            $meta = $workflow->getMetadataStore()->getPlaceMetadata($to);

                            if (
                                !\array_key_exists('isFinal', $meta) || false === $meta['isFinal']
                            ) {
                                $toFinal = false;
                            }
                        }

                        return [
                            'data-is-transition' => 'data-is-transition',
                            'data-to-final' => $toFinal ? '1' : '0',
                            'data-is-forward' => $isForward,
                        ];
                    },
                ])
                ->add('future_dest_users', PickUserDynamicType::class, [
                    'label' => 'workflow.dest for next steps',
                    'multiple' => true,
                    'mapped' => false,
                    'suggested' => $options['suggested_users'],
                ])
                ->add('future_cc_users', PickUserDynamicType::class, [
                    'label' => 'workflow.cc for next steps',
                    'multiple' => true,
                    'mapped' => false,
                    'required' => false,
                    'suggested' => $options['suggested_users'],
                ])
                ->add('future_dest_emails', ChillCollectionType::class, [
                    'label' => 'workflow.dest by email',
                    'help' => 'workflow.dest by email help',
                    'mapped' => false,
                    'allow_add' => true,
                    'entry_type' => EmailType::class,
                    'button_add_label' => 'workflow.Add an email',
                    'button_remove_label' => 'workflow.Remove an email',
                    'empty_collection_explain' => 'workflow.Any email',
                    'entry_options' => [
                        'constraints' => [
                            new NotNull(), new NotBlank(), new Email(),
                        ],
                        'label' => 'Email',
                    ],
                ]);
        }

        if (
            $handler->supportsFreeze($entityWorkflow)
            && !$entityWorkflow->isFreeze()
        ) {
            $builder
                ->add('freezeAfter', CheckboxType::class, [
                    'required' => false,
                    'label' => 'workflow.Freeze',
                    'help' => 'workflow.The associated element will be freezed',
                ]);
        }

        $builder
            ->add('comment', ChillTextareaType::class, [
                'required' => false,
                'label' => 'Comment',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefined('class')
            ->setRequired('transition')
            ->setAllowedTypes('transition', 'bool')
            ->setRequired('entity_workflow')
            ->setAllowedTypes('entity_workflow', EntityWorkflow::class)
            ->setDefault('suggested_users', [])
            ->setDefault('constraints', [
                new Callback(
                    function ($step, ExecutionContextInterface $context, $payload) {
                        /** @var EntityWorkflowStep $step */
                        $form = $context->getObject();
                        $workflow = $this->registry->get($step->getEntityWorkflow(), $step->getEntityWorkflow()->getWorkflowName());
                        $transition = $form['transition']->getData();
                        $toFinal = true;

                        if (null === $transition) {
                            $context
                                ->buildViolation('workflow.You must select a next step, pick another decision if no next steps are available');
                        } else {
                            foreach ($transition->getTos() as $to) {
                                $meta = $workflow->getMetadataStore()->getPlaceMetadata($to);

                                if (
                                    !\array_key_exists('isFinal', $meta) || false === $meta['isFinal']
                                ) {
                                    $toFinal = false;
                                }
                            }
                            $destUsers = $form['future_dest_users']->getData();
                            $destEmails = $form['future_dest_emails']->getData();

                            if (!$toFinal && [] === $destUsers && [] === $destEmails) {
                                $context
                                    ->buildViolation('workflow.You must add at least one dest user or email')
                                    ->atPath('future_dest_users')
                                    ->addViolation();
                            }
                        }
                    }
                ),
                new Callback(
                    function ($step, ExecutionContextInterface $context, $payload) {
                        $form = $context->getObject();

                        foreach ($form->get('future_dest_users')->getData() as $u) {
                            if (in_array($u, $form->get('future_cc_users')->getData(), true)) {
                                $context
                                    ->buildViolation('workflow.The user in cc cannot be a dest user in the same workflow step')
                                    ->atPath('ccUsers')
                                    ->addViolation();
                            }
                        }
                    }
                ),
            ]);
    }
}
