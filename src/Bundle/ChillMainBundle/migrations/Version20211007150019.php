<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Create civility table.
 */
final class Version20211007150019 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE chill_main_civility_id_seq');
        $this->addSql('DROP TABLE chill_main_civility');
    }

    public function getDescription(): string
    {
        return 'create civility table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_main_civility_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_main_civility (id INT NOT NULL, name JSON NOT NULL, abbreviation JSON NOT NULL, active BOOLEAN NOT NULL, PRIMARY KEY(id))');
    }
}
