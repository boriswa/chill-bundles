<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221010142417 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE scopes DROP active');
    }

    public function getDescription(): string
    {
        return 'Allow a scope to be desactivated';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE scopes ADD active BOOLEAN DEFAULT true NOT NULL');
    }
}
