<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * @Author Mathieu Jaumotte mathieu.jaumotte@champs-libres.coop
 */
final class Version20211012141336 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activity DROP CONSTRAINT FK_AC74095A64D218E');
        $this->addSql('ALTER TABLE activity DROP location_id');

        $this->addSql('ALTER TABLE chill_calendar.calendar DROP CONSTRAINT FK_712315AC64D218E');
        $this->addSql('ALTER TABLE chill_calendar.calendar DROP location_id');

        $this->addSql('ALTER TABLE chill_main_location DROP CONSTRAINT FK_90E4736AB8B0DA8E');

        $this->addSql('DROP SEQUENCE chill_main_location_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_main_location_type_id_seq CASCADE');

        $this->addSql('DROP TABLE chill_main_location');
        $this->addSql('DROP TABLE chill_main_location_type');
    }

    public function getDescription(): string
    {
        return 'Add Location and LocationType Entities (for activity and calendar)';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_main_location_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_main_location_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_main_location (id INT NOT NULL, address_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, phonenumber1 VARCHAR(64) DEFAULT NULL, phonenumber2 VARCHAR(64) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, availableForUsers BOOLEAN NOT NULL, createdAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updatedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, locationType_id INT NOT NULL, createdBy_id INT DEFAULT NULL, updatedBy_id INT DEFAULT NULL, PRIMARY KEY(id))');

        $this->addSql('CREATE INDEX IDX_90E4736AB8B0DA8E ON chill_main_location (locationType_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_90E4736AF5B7AF75 ON chill_main_location (address_id)');
        $this->addSql('CREATE INDEX IDX_90E4736A3174800F ON chill_main_location (createdBy_id)');
        $this->addSql('CREATE INDEX IDX_90E4736A65FF1AEC ON chill_main_location (updatedBy_id)');

        $this->addSql('COMMENT ON COLUMN chill_main_location.createdAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_main_location.updatedAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE chill_main_location_type (id INT NOT NULL, title JSON NOT NULL, availableForUsers BOOLEAN NOT NULL, addressRequired VARCHAR(32) DEFAULT \'optional\' NOT NULL, contactData VARCHAR(32) DEFAULT \'optional\' NOT NULL, PRIMARY KEY(id))');

        $this->addSql('ALTER TABLE chill_main_location ADD CONSTRAINT FK_90E4736AB8B0DA8E FOREIGN KEY (locationType_id) REFERENCES chill_main_location_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_main_location ADD CONSTRAINT FK_90E4736AF5B7AF75 FOREIGN KEY (address_id) REFERENCES chill_main_address (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_main_location ADD CONSTRAINT FK_90E4736A3174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_main_location ADD CONSTRAINT FK_90E4736A65FF1AEC FOREIGN KEY (updatedBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('ALTER TABLE activity ADD location_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE activity ADD CONSTRAINT FK_AC74095A64D218E FOREIGN KEY (location_id) REFERENCES chill_main_location (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_AC74095A64D218E ON activity (location_id)');

        $this->addSql('ALTER TABLE chill_calendar.calendar ADD location_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_calendar.calendar ADD CONSTRAINT FK_712315AC64D218E FOREIGN KEY (location_id) REFERENCES chill_main_location (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_712315AC64D218E ON chill_calendar.calendar (location_id)');
    }
}
