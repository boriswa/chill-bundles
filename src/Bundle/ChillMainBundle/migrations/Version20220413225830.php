<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220413225830 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_address ALTER COLUMN postcode_id DROP NOT NULL');
    }

    public function getDescription(): string
    {
        return 'Set postal code in chill_main_address NOT NULL';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_address ALTER COLUMN postcode_id SET NOT NULL');
    }
}
