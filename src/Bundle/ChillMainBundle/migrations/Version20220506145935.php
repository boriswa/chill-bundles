<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220506145935 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX search_by_reference_code');
    }

    public function getDescription(): string
    {
        return 'Add index to search postal code by references';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE INDEX search_by_reference_code ON chill_main_postal_code (code, refpostalcodeid)');
    }
}
