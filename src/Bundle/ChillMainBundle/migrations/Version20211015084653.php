<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211015084653 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX IDX_90E4736AF5B7AF75');
        $this->addSql('CREATE UNIQUE INDEX uniq_90e4736af5b7af75 ON chill_main_location (address_id)');
    }

    public function getDescription(): string
    {
        return 'Location entity: change Address to ManyToOne';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX uniq_90e4736af5b7af75');
        $this->addSql('CREATE INDEX IDX_90E4736AF5B7AF75 ON chill_main_location (address_id)');
    }
}
