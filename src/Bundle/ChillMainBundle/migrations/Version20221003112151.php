<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221003112151 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException();

        /* for memory
        $this->addSql('ALTER TABLE chill_main_geographical_unit DROP CONSTRAINT FK_360A2B2FEA6EFDCD');
        $this->addSql('DROP SEQUENCE chill_main_geographical_unit_layer_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_main_geographical_unit_layer');
        $this->addSql('ALTER TABLE chill_main_geographical_unit ADD layername VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_main_geographical_unit DROP layer_id');
        $this->addSql('ALTER TABLE chill_main_geographical_unit DROP unitRefId');
        $this->addSql('ALTER TABLE chill_main_geographical_unit ALTER geom TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE chill_main_geographical_unit ALTER unitName TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE chill_main_geographical_unit ALTER unitName DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_main_geographical_unit ALTER unitName DROP NOT NULL');
         */
    }

    public function getDescription(): string
    {
        return 'Add a proper entity for GeographicalUnitLayer';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_main_geographical_unit_layer_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_main_geographical_unit_layer (id INT NOT NULL, name JSONB DEFAULT \'[]\'::jsonb NOT NULL, refid TEXT DEFAULT \'\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql("COMMENT ON COLUMN chill_main_geographical_unit_layer.name IS '(DC2Type:json)';");

        $this->addSql('INSERT INTO chill_main_geographical_unit_layer (id, name, refid)
            SELECT DISTINCT nextval(\'chill_main_geographical_unit_layer_id_seq\'), jsonb_build_object(\'fr\', layername), layername FROM chill_main_geographical_unit');

        $this->addSql('ALTER TABLE chill_main_geographical_unit ADD layer_id INT DEFAULT NULL');

        $this->addSql('UPDATE chill_main_geographical_unit SET layer_id = layer.id FROM chill_main_geographical_unit_layer AS layer WHERE layer.refid = chill_main_geographical_unit.layername');

        $this->addSql('ALTER TABLE chill_main_geographical_unit ADD unitRefId TEXT DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE chill_main_geographical_unit DROP layername');
        $this->addSql("COMMENT ON COLUMN chill_main_geographical_unit.geom IS '(DC2Type:text)';");
        $this->addSql('ALTER TABLE chill_main_geographical_unit ALTER unitname TYPE TEXT');
        $this->addSql('ALTER TABLE chill_main_geographical_unit ALTER unitname SET DEFAULT \'\'');
        $this->addSql('ALTER TABLE chill_main_geographical_unit ALTER unitname SET NOT NULL');
        $this->addSql('ALTER TABLE chill_main_geographical_unit ADD CONSTRAINT FK_360A2B2FEA6EFDCD FOREIGN KEY (layer_id) REFERENCES chill_main_geographical_unit_layer (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_360A2B2FEA6EFDCD ON chill_main_geographical_unit (layer_id)');
    }
}
