<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230306145728 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'fix inconsistencies in chill_main_address table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_address ADD createdAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_main_address ADD updatedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_main_address ADD createdBy_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_main_address ADD updatedBy_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER street TYPE TEXT');
        $this->addSql('UPDATE chill_main_address SET street=\'\' WHERE street IS NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER street SET DEFAULT \'\'');
        $this->addSql('ALTER TABLE chill_main_address ALTER streetnumber TYPE TEXT');
        $this->addSql('UPDATE chill_main_address SET streetnumber=\'\' WHERE streetnumber IS NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER streetnumber SET DEFAULT \'\'');
        $this->addSql('ALTER TABLE chill_main_address ALTER floor SET DEFAULT \'\'');
        $this->addSql('UPDATE chill_main_address SET floor=\'\' WHERE floor IS NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER floor SET NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER corridor SET DEFAULT \'\'');
        $this->addSql('UPDATE chill_main_address SET corridor=\'\' WHERE corridor IS NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER corridor SET NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER steps SET DEFAULT \'\'');
        $this->addSql('UPDATE chill_main_address SET steps=\'\' WHERE steps IS NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER steps SET NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER buildingname TYPE TEXT');
        $this->addSql('ALTER TABLE chill_main_address ALTER buildingname SET DEFAULT \'\'');
        $this->addSql('UPDATE chill_main_address SET buildingname=\'\' WHERE buildingname IS NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER buildingname SET NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER flat SET DEFAULT \'\'');
        $this->addSql('UPDATE chill_main_address SET flat=\'\' WHERE flat IS NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER flat SET NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER distribution TYPE TEXT');
        $this->addSql('ALTER TABLE chill_main_address ALTER distribution SET DEFAULT \'\'');
        $this->addSql('UPDATE chill_main_address SET distribution=\'\' WHERE distribution IS NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER distribution SET NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER extra TYPE TEXT');
        $this->addSql('ALTER TABLE chill_main_address ALTER extra SET DEFAULT \'\'');
        $this->addSql('UPDATE chill_main_address SET extra=\'\' WHERE extra IS NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER extra SET NOT NULL');
        $this->addSql('UPDATE chill_main_address SET confidential=FALSE WHERE confidential IS NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER confidential SET NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER refstatuslastupdate TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('COMMENT ON COLUMN chill_main_address.point IS \'(DC2Type:point)\'');
        $this->addSql('COMMENT ON COLUMN chill_main_address.createdAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_main_address.updatedAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_main_address.refStatusLastUpdate IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE chill_main_address ADD CONSTRAINT FK_165051F63174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_main_address ADD CONSTRAINT FK_165051F665FF1AEC FOREIGN KEY (updatedBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_165051F63174800F ON chill_main_address (createdBy_id)');
        $this->addSql('CREATE INDEX IDX_165051F665FF1AEC ON chill_main_address (updatedBy_id)');
        $this->addSql('COMMENT ON COLUMN chill_main_address_reference.point IS \'(DC2Type:point)\'');
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('down method is not double-checked');

        $this->addSql('ALTER TABLE chill_main_address DROP CONSTRAINT FK_165051F63174800F');
        $this->addSql('ALTER TABLE chill_main_address DROP CONSTRAINT FK_165051F665FF1AEC');
        $this->addSql('DROP INDEX IDX_165051F63174800F');
        $this->addSql('DROP INDEX IDX_165051F665FF1AEC');
        $this->addSql('ALTER TABLE chill_main_address ADD customs JSONB DEFAULT \'[]\'');
        $this->addSql('ALTER TABLE chill_main_address DROP createdAt');
        $this->addSql('ALTER TABLE chill_main_address DROP updatedAt');
        $this->addSql('ALTER TABLE chill_main_address DROP createdBy_id');
        $this->addSql('ALTER TABLE chill_main_address DROP updatedBy_id');
        $this->addSql('ALTER TABLE chill_main_address ALTER buildingName TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE chill_main_address ALTER buildingName DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_main_address ALTER buildingName DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER confidential SET DEFAULT false');
        $this->addSql('ALTER TABLE chill_main_address ALTER confidential DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER corridor DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_main_address ALTER corridor DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER distribution TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE chill_main_address ALTER distribution DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_main_address ALTER distribution DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER extra TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE chill_main_address ALTER extra DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_main_address ALTER extra DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER flat DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_main_address ALTER flat DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER floor DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_main_address ALTER floor DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER isNoAddress SET DEFAULT false');
        $this->addSql('ALTER TABLE chill_main_address ALTER point TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE chill_main_address ALTER refStatusLastUpdate TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE chill_main_address ALTER steps DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_main_address ALTER steps DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address ALTER street TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE chill_main_address ALTER street DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_main_address ALTER streetNumber TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE chill_main_address ALTER streetNumber DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN chill_main_address.refstatuslastupdate IS NULL');
    }
}
