<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add extension btree_gist.
 */
final class Version20210528090000 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP EXTENSION btree_gist');
    }

    public function getDescription(): string
    {
        return 'add extension btree_gist';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE EXTENSION IF NOT EXISTS btree_gist');
    }
}
