<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add mainLocation to User.
 */
final class Version20220112161136 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE users DROP CONSTRAINT FK_1483A5E9DB622A42');
        $this->addSql('DROP INDEX IDX_1483A5E9DB622A42');
        $this->addSql('ALTER TABLE users DROP mainLocation_id');
    }

    public function getDescription(): string
    {
        return 'Add mainLocation to User';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE users ADD mainLocation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E9DB622A42 FOREIGN KEY (mainLocation_id) REFERENCES chill_main_location (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_1483A5E9DB622A42 ON users (mainLocation_id)');
    }
}
