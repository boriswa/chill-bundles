<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211228215919 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE chill_main_notification_comment_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_main_notification_comment');
    }

    public function getDescription(): string
    {
        return 'Notifications: add comment on notifications';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_main_notification_comment_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_main_notification_comment (id INT NOT NULL, notification_id INT NOT NULL, content TEXT NOT NULL, createdAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updateAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, createdBy_id INT DEFAULT NULL, updatedBy_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_983BD2CFEF1A9D84 ON chill_main_notification_comment (notification_id)');
        $this->addSql('CREATE INDEX IDX_983BD2CF3174800F ON chill_main_notification_comment (createdBy_id)');
        $this->addSql('CREATE INDEX IDX_983BD2CF65FF1AEC ON chill_main_notification_comment (updatedBy_id)');
        $this->addSql('COMMENT ON COLUMN chill_main_notification_comment.createdAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_main_notification_comment.updateAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE chill_main_notification_comment ADD CONSTRAINT FK_983BD2CFEF1A9D84 FOREIGN KEY (notification_id) REFERENCES chill_main_notification (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_main_notification_comment ADD CONSTRAINT FK_983BD2CF3174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_main_notification_comment ADD CONSTRAINT FK_983BD2CF65FF1AEC FOREIGN KEY (updatedBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
