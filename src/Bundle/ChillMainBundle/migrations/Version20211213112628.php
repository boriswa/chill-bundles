<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add order to civility.
 */
final class Version20211213112628 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_civility DROP "ordering"');
    }

    public function getDescription(): string
    {
        return 'Add order to civility';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_civility ADD "ordering" FLOAT DEFAULT 0.0');
    }
}
