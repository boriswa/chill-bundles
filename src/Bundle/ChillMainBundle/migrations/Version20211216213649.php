<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211216213649 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX address_refid');
    }

    public function getDescription(): string
    {
        return 'add an index on address reference refid';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE INDEX address_refid ON chill_main_address_reference (refId) WHERE refid != \'\'');
    }
}
