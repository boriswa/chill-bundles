<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220506131307 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX chill_internal_address_reference_canonicalized');
        $this->addSql('create index chill_internal_address_reference_canonicalized
            on chill_main_address_reference using gist (postcode_id, addresscanonical gist_trgm_ops);');
    }

    public function getDescription(): string
    {
        return 'Adapt search index on address reference canonicalized';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX chill_internal_address_reference_canonicalized');
        $this->addSql('create index chill_internal_address_reference_canonicalized
            on chill_main_address_reference using gist (postcode_id, addresscanonical gist_trgm_ops) WHERE deletedat IS NULL;');
    }
}
