<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220128211748 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_workflow_entity_step RENAME COLUMN isFinal TO finalizeAfter;');
    }

    public function getDescription(): string
    {
        return 'rename workflow entity step from finalizeAfter to isFinal';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_workflow_entity_step RENAME COLUMN finalizeAfter TO isFinal;');
    }
}
