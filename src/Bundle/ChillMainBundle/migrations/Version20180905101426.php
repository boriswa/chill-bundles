<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180905101426 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE permission_groups DROP COLUMN flags');
        $this->addSql('ALTER TABLE group_centers ALTER permissionsgroup_id SET DEFAULT NULL');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE permission_groups ADD flags JSONB DEFAULT \'[]\' NOT NULL');
        $this->addSql('ALTER TABLE group_centers ALTER permissionsgroup_id DROP NOT NULL');
    }
}
