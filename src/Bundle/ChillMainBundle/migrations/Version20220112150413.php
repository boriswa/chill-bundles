<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add editableByUsers field to ChillMain/LocationType.
 */
final class Version20220112150413 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_location_type DROP editableByUsers');
    }

    public function getDescription(): string
    {
        return 'Add editableByUsers field to ChillMain/LocationType';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_location_type ADD editableByUsers BOOLEAN DEFAULT TRUE');
    }
}
