<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add current location to User entity.
 */
final class Version20211116162847 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE users DROP CONSTRAINT FK_1483A5E93C219753');
        $this->addSql('DROP INDEX IDX_1483A5E93C219753');
        $this->addSql('ALTER TABLE users DROP currentLocation_id');
    }

    public function getDescription(): string
    {
        return 'Add current location to User entity';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE users ADD currentLocation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E93C219753 FOREIGN KEY (currentLocation_id) REFERENCES chill_main_location (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_1483A5E93C219753 ON users (currentLocation_id)');
    }
}
