<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220711150006 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_workflow_entity_step
            DROP CONSTRAINT chill_custom_only_one_step_opened');
    }

    public function getDescription(): string
    {
        return 'Add a constraint to ensure that only one step is available at a time';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_workflow_entity_step
            ADD CONSTRAINT chill_custom_only_one_step_opened
                EXCLUDE (
                    entityworkflow_id WITH =
                ) WHERE (transitionafter IS NULL)
                DEFERRABLE INITIALLY DEFERRED');
    }
}
