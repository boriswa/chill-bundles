<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add active on Location and LocationType.
 */
final class Version20211022094429 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_location_type DROP active');
        $this->addSql('ALTER TABLE chill_main_location DROP active');
    }

    public function getDescription(): string
    {
        return 'Add active on Location and LocationType';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_location ADD active BOOLEAN DEFAULT TRUE;');
        $this->addSql('ALTER TABLE chill_main_location_type ADD active BOOLEAN DEFAULT TRUE;');
    }
}
