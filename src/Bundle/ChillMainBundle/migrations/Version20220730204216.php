<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220730204216 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX chill_main_address_reference_unicity');
    }

    public function getDescription(): string
    {
        return 'Add an unique constraint on addresses references';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE UNIQUE INDEX chill_main_address_reference_unicity ON chill_main_address_reference (refId, source)');
    }
}
