<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add civility to User.
 */
final class Version20220516085659 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE users DROP CONSTRAINT FK_1483A5E923D6A298');
        $this->addSql('DROP INDEX IDX_1483A5E923D6A298');
        $this->addSql('ALTER TABLE users DROP civility_id');
    }

    public function getDescription(): string
    {
        return 'Add civility to User';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE users ADD civility_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E923D6A298 FOREIGN KEY (civility_id) REFERENCES chill_main_civility (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_1483A5E923D6A298 ON users (civility_id)');
    }
}
