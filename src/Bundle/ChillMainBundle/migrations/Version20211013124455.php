<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211013124455 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activitytype DROP locationVisible');
        $this->addSql('ALTER TABLE activitytype DROP locationLabel');

        // fix old migration !?
        $this->addSql('ALTER TABLE activitytype ALTER category_id SET DEFAULT 1');
    }

    public function getDescription(): string
    {
        return 'add location visible admin option';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activitytype ADD locationVisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD locationLabel VARCHAR(255) DEFAULT \'\' NOT NULL');

        // fix old migration !?
        $this->addSql('ALTER TABLE activitytype ALTER category_id DROP DEFAULT');
    }
}
