<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220506223243 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE users ALTER attributes DROP NOT NULL');
    }

    public function getDescription(): string
    {
        return 'Force user attribute to be an array';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE users SET attributes = \'{}\'::jsonb WHERE attributes IS NULL');
        $this->addSql('ALTER TABLE users ALTER attributes SET NOT NULL');
        $this->addSql('ALTER TABLE users ALTER attributes SET DEFAULT \'{}\'::jsonb');
    }
}
