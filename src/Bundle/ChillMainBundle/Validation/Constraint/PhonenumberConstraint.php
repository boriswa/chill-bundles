<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class PhonenumberConstraint extends Constraint
{
    public $notLandlineMessage = 'This is not a landline phonenumber';

    public $notMobileMessage = 'This is not a mobile phonenumber';

    public $notValidMessage = 'This is not a valid phonenumber';

    /**
     * The type of phone: landline (not able to receive sms) or mobile (can receive sms).
     *
     * @var string 'landline', 'mobile' or 'any'
     */
    public $type;

    public function validatedBy()
    {
        return \Chill\MainBundle\Validation\Validator\ValidPhonenumber::class;
    }
}
