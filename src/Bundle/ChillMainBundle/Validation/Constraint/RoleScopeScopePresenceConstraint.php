<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Check that a role scope has a scope if required.
 */
class RoleScopeScopePresenceConstraint extends Constraint
{
    public $messageNullRequired = 'The role "%role%" should not be associated with a scope.';

    public $messagePresenceRequired = 'The role "%role%" require to be associated with '
        .'a scope.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validatedBy()
    {
        return 'role_scope_scope_presence';
    }
}
