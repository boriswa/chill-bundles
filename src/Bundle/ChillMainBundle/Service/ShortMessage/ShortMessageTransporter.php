<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Service\ShortMessage;

class ShortMessageTransporter implements ShortMessageTransporterInterface
{
    public function __construct(private readonly ShortMessageSenderInterface $sender)
    {
    }

    public function send(ShortMessage $shortMessage): void
    {
        $this->sender->send($shortMessage);
    }
}
