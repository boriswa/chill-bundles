<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Service\AddressGeographicalUnit;

interface CollateAddressWithReferenceOrPostalCodeInterface
{
    /**
     * @throws \Throwable
     */
    public function __invoke(int $sinceId = 0): int;
}
