<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Pagination;

/**
 * Represents a page included in a pagination.
 */
interface PageInterface
{
    public function generateUrl();

    public function getFirstItemNumber();

    public function getLastItemNumber();

    /**
     * get the page number.
     *
     * The first page number is 1.
     */
    public function getNumber();
}
