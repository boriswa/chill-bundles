<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Pagination;

use Generator;

/**
 * Represent a set of numbered pages.
 *
 * Allow to calculate and render pagination for a set of pages.
 *
 * The items are elements that `could` be shown. The item are divided and shown
 * into pages. Each page is numbered and count a given number of item per page.
 *
 * The first page number is 1, although the first result number is 0.
 */
interface PaginatorInterface extends \Countable
{
    /**
     * get the number of pages for this pagination.
     */
    public function countPages(): int;

    /**
     * get the current page.
     */
    public function getCurrentPage(): PageInterface;

    /**
     * get the first result for the current page.
     */
    public function getCurrentPageFirstItemNumber(): int;

    /*
     * get the number of items per page
     */
    public function getItemsPerPage(): int;

    /**
     * get the next page.
     *
     * @throws \RuntimeException if the pagination has not next page
     */
    public function getNextPage(): PageInterface;

    /**
     * get page by his number.
     *
     * @throws \RuntimeException if the pagination has no page with specified number
     */
    public function getPage(int $number): PageInterface;

    /**
     * get a generator to generate pages.
     *
     * @return \Generator which return PageInterface elements
     */
    public function getPagesGenerator(): iterable;

    /**
     * get the previous page.
     *
     * @throws \RuntimeException if the pagination has not previous page
     */
    public function getPreviousPage(): PageInterface;

    /**
     * get the number of results for this paginator.
     */
    public function getTotalItems(): int;

    /**
     * check if the current page has a next page.
     */
    public function hasNextPage(): bool;

    /**
     * check if the page with the given number exists.
     */
    public function hasPage($number): bool;

    /**
     * check if the current page has a page before.
     */
    public function hasPreviousPage(): bool;

    /**
     * check if the given page is the current page.
     */
    public function isCurrentPage(PageInterface $page): bool;

    /*
     * set the number of items per page
     */
    public function setItemsPerPage(int $itemsPerPage);
}
