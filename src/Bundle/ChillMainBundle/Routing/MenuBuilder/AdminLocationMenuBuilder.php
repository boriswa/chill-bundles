<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Routing\MenuBuilder;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AdminLocationMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        // all the entries below must have ROLE_ADMIN permissions
        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            return;
        }

        $menu->addChild('Location and location type', [
            'route' => 'chill_main_location_admin',
        ])
            ->setAttribute('class', 'list-group-item-header')
            ->setExtras([
                'order' => 1300,
            ]);

        $menu->addChild('Location type list', [
            'route' => 'chill_crud_main_location_type_index',
        ])->setExtras(['order' => 1310]);

        $menu->addChild('Location list', [
            'route' => 'chill_crud_main_location_index',
        ])->setExtras(['order' => 1320]);
    }

    public static function getMenuIds(): array
    {
        return ['admin_section', 'admin_location'];
    }
}
