<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Routing;

use Knp\Menu\MenuItem;

/**
 * Implements a builder for menu.
 *
 * @template TParams of array
 */
interface LocalMenuBuilderInterface
{
    /**
     * @param TParams $parameters
     */
    public function buildMenu($menuId, MenuItem $menu, array $parameters);

    /**
     * return an array of menu ids.
     *
     * @internal this method is static to 1. keep all config in the class,
     *      instead of tags arguments; 2. avoid a "supports" method, which could lead
     *      to parsing all instances to get only one or two working instance.
     */
    public static function getMenuIds(): array;
}
