<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Timeline;

class TimelineSingleQuery
{
    private bool $distinct = false;

    public function __construct(private ?string $id = null, private ?string $date = null, private ?string $key = null, private ?string $from = null, private ?string $where = null, private array $parameters = [])
    {
    }

    public function buildSql(): string
    {
        $parameters = [];

        return \strtr(
            'SELECT {distinct} {id} AS id, '
                .'{date} AS "date", '
                ."'{key}' AS type "
                .'FROM {from} '
                .'WHERE {where}',
            [
                '{distinct}' => $this->distinct ? 'DISTINCT' : '',
                '{id}' => $this->getId(),
                '{date}' => $this->getDate(),
                '{key}' => $this->getKey(),
                '{from}' => $this->getFrom(),
                '{where}' => $this->getWhere(),
            ]
        );
    }

    public static function fromArray(array $a)
    {
        return new TimelineSingleQuery(
            $a['id'] ?? null,
            $a['date'] ?? null,
            $a['type'] ?? $a['key'] ?? null,
            $a['FROM'] ?? $a['from'] ?? null,
            $a['WHERE'] ?? $a['where'] ?? null,
            $a['parameters'] ?? null
        );
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function getFrom(): string
    {
        return $this->from;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function getWhere(): string
    {
        return $this->where;
    }

    public function isDistinct(): bool
    {
        return $this->distinct;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function setDistinct(bool $distinct): self
    {
        $this->distinct = $distinct;

        return $this;
    }

    public function setFrom(string $from): self
    {
        $this->from = $from;

        return $this;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }

    public function setParameters(array $parameters): self
    {
        $this->parameters = $parameters;

        return $this;
    }

    public function setWhere(string $where): self
    {
        $this->where = $where;

        return $this;
    }
}
