<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Command;

use Chill\MainBundle\Service\EntityInfo\ViewEntityInfoManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SynchronizeEntityInfoViewsCommand extends Command
{
    public function __construct(
        private readonly ViewEntityInfoManager $viewEntityInfoManager,
    ) {
        parent::__construct('chill:db:sync-views');
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Update or create sql views which provide info for various entities');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->viewEntityInfoManager->synchronizeOnDB();

        return 0;
    }
}
