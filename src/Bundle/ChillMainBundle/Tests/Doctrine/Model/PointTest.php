<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Doctrine\Model;

use Chill\MainBundle\Doctrine\Model\Point;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test the point model methods.
 *
 * @internal
 *
 * @coversNothing
 */
final class PointTest extends KernelTestCase
{
    public function testFromArrayGeoJson()
    {
        $array = [
            'type' => 'Point',
            'coordinates' => [4.8634, 50.47382],
        ];
        $lon = 4.8634;
        $lat = 50.47382;
        $point = $this->preparePoint($lon, $lat);

        $this->assertEquals($point, Point::fromArrayGeoJson($array));
    }

    public function testFromGeoJson()
    {
        $geojson = '{"type":"Point","coordinates":[4.8634,50.47382]}';
        $lon = 4.8634;
        $lat = 50.47382;
        $point = $this->preparePoint($lon, $lat);

        $this->assertEquals($point, Point::fromGeoJson($geojson));
    }

    public function testFromLonLat()
    {
        $lon = 4.8634;
        $lat = 50.47382;
        $point = $this->preparePoint($lon, $lat);

        $this->assertEquals($point, Point::fromLonLat($lon, $lat));
    }

    public function testGetLat()
    {
        $lon = 4.8634;
        $lat = 50.47382;
        $point = $this->preparePoint($lon, $lat);

        $this->assertEquals($lat, $point->getLat());
    }

    public function testGetLon()
    {
        $lon = 4.8634;
        $lat = 50.47382;
        $point = $this->preparePoint($lon, $lat);

        $this->assertEquals($lon, $point->getLon());
    }

    public function testJsonSerialize()
    {
        $lon = 4.8634;
        $lat = 50.47382;
        $point = $this->preparePoint($lon, $lat);

        $this->assertEquals(
            $point->jsonSerialize(),
            [
                'type' => 'Point',
                'coordinates' => [4.8634, 50.47382],
            ]
        );
    }

    public function testToArrayGeoJson()
    {
        $lon = 4.8634;
        $lat = 50.47382;
        $point = $this->preparePoint($lon, $lat);

        $this->assertEquals(
            $point->toArrayGeoJson(),
            [
                'type' => 'Point',
                'coordinates' => [4.8634, 50.47382],
            ]
        );
    }

    public function testToGeojson()
    {
        $lon = 4.8634;
        $lat = 50.47382;
        $point = $this->preparePoint($lon, $lat);

        $this->assertEquals($point->toGeoJson(), '{"type":"Point","coordinates":[4.8634,50.47382]}');
    }

    public function testToWKT()
    {
        $lon = 4.8634;
        $lat = 50.47382;
        $point = $this->preparePoint($lon, $lat);

        $this->assertEquals($point->toWKT(), 'SRID=4326;POINT(4.8634 50.47382)');
    }

    private function preparePoint($lon, $lat)
    {
        return Point::fromLonLat($lon, $lat);
    }
}
