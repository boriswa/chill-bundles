<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Doctrine\DQL;

use Chill\MainBundle\Entity\Address;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class AgeTest extends KernelTestCase
{
    private EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->entityManager = self::$container->get(EntityManagerInterface::class);
    }

    public function generateQueries(): iterable
    {
        yield [
            'SELECT AGE(a.validFrom, a.validTo) FROM '.Address::class.' a',
            [],
        ];

        yield [
            'SELECT AGE(:date0, :date1) FROM '.Address::class.' a',
            [
                'date0' => new \DateTimeImmutable('now'),
                'date1' => new \DateTimeImmutable('2020-01-01'),
            ],
        ];

        yield [
            'SELECT AGE(a.validFrom, :date1) FROM '.Address::class.' a',
            [
                'date1' => new \DateTimeImmutable('now'),
            ],
        ];

        yield [
            'SELECT AGE(:date0, a.validFrom) FROM '.Address::class.' a',
            [
                'date0' => new \DateTimeImmutable('now'),
            ],
        ];
    }

    /**
     * @dataProvider generateQueries
     */
    public function testWorking(string $dql, array $args)
    {
        $dql = $this->entityManager->createQuery($dql)->setMaxResults(3);

        foreach ($args as $key => $value) {
            $dql->setParameter($key, $value);
        }

        $results = $dql->getResult();

        $this->assertIsArray($results);
    }
}
