<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Doctrine\DQL;

use Chill\MainBundle\Entity\Country;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class JsonExtractTest extends KernelTestCase
{
    private EntityManagerInterface $em;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->em = self::$container->get(EntityManagerInterface::class);
    }

    public function dataGenerateDql(): iterable
    {
        yield ['SELECT JSON_EXTRACT(c.name, \'fr\') FROM '.Country::class.' c', []];

        yield ['SELECT JSON_EXTRACT(c.name, :lang) FROM '.Country::class.' c', ['lang' => 'fr']];
    }

    /**
     * @dataProvider dataGenerateDql
     */
    public function testJsonExtract(string $dql, array $args)
    {
        $results = $this->em->createQuery($dql)
            ->setMaxResults(2)
            ->setParameters($args)
            ->getResult();

        $this->assertIsArray($results, 'simply test that the query return a result');
    }
}
