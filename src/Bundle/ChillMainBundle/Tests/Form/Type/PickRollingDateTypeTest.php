<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Form\Type;

use Chill\MainBundle\Form\Type\PickRollingDateType;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Symfony\Component\Form\PreloadedExtension;
use Symfony\Component\Form\Test\TypeTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class PickRollingDateTypeTest extends TypeTestCase
{
    public function testSubmitValidData()
    {
        $formData = [
            'roll' => 'year_previous_start',
            'fixedDate' => null,
        ];

        $form = $this->factory->create(PickRollingDateType::class);

        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());

        /** @var RollingDate $rollingDate */
        $rollingDate = $form->getData();

        $this->assertInstanceOf(RollingDate::class, $rollingDate);
        $this->assertEquals(RollingDate::T_YEAR_PREVIOUS_START, $rollingDate->getRoll());
    }

    protected function getExtensions(): array
    {
        $type = new PickRollingDateType();

        return [
            new PreloadedExtension([$type], []),
        ];
    }
}
