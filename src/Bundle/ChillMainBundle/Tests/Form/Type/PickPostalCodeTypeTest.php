<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Form\Type;

use Chill\MainBundle\Entity\PostalCode;
use Chill\MainBundle\Form\Type\DataTransformer\PostalCodeToIdTransformer;
use Chill\MainBundle\Form\Type\PickPostalCodeType;
use Chill\MainBundle\Repository\PostalCodeRepositoryInterface;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\PreloadedExtension;
use Symfony\Component\Form\Test\TypeTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class PickPostalCodeTypeTest extends TypeTestCase
{
    use ProphecyTrait;

    public function testSubmitValidData(): void
    {
        $builder = $this->factory->createBuilder(FormType::class, ['postal_code' => null]);
        $builder->add('postal_code', PickPostalCodeType::class);
        $form = $builder->getForm();

        $form->submit(['postal_code' => '1']);

        $this->assertTrue($form->isSynchronized());

        $this->assertEquals(1, $form['postal_code']->getData()->getId());
    }

    protected function getExtensions()
    {
        $postalCodeRepository = $this->prophesize(PostalCodeRepositoryInterface::class);
        $postalCodeRepository->find(Argument::any())
            ->will(static function ($args) {
                $postalCode = new PostalCode();
                $reflectionClass = new \ReflectionClass($postalCode);
                $id = $reflectionClass->getProperty('id');
                $id->setAccessible(true);
                $id->setValue($postalCode, (int) $args[0]);

                return $postalCode;
            });

        $type = new PickPostalCodeType(
            new PostalCodeToIdTransformer(
                $postalCodeRepository->reveal()
            )
        );

        return [
            new PreloadedExtension([$type], []),
        ];
    }
}
