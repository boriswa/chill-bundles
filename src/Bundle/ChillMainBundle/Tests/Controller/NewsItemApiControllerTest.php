<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Controller;

use Chill\MainBundle\Test\PrepareClientTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class NewsItemApiControllerTest extends WebTestCase
{
    use PrepareClientTrait;

    public function testListCurrentNewsItems()
    {
        $client = $this->getClientAuthenticated();

        $client->request('GET', '/api/1.0/main/news/current.json');
        $this->assertResponseIsSuccessful('Testing whether the GET request to the news item Api endpoint was successful');

        $responseContent = json_decode($client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);

        if (!empty($responseContent['data'][0])) {
            $this->assertArrayHasKey('title', $responseContent['data'][0]);
        }
    }
}
