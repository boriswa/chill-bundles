<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Controller;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Repository\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class UserControllerTest extends WebTestCase
{
    private \Symfony\Bundle\FrameworkBundle\KernelBrowser $client;

    private array $toDelete = [];

    protected function setUp(): void
    {
        $this->client = self::createClient([], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'password',
            'HTTP_ACCEPT_LANGUAGE' => 'fr_FR',
        ]);
    }

    protected function tearDown(): void
    {
        self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        foreach ($this->toDelete as [$class, $id]) {
            $obj = $em->getRepository($class)->find($id);
            $em->remove($obj);
        }

        $em->flush();
        self::ensureKernelShutdown();
    }

    public function dataGenerateUserId()
    {
        self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        $user = new User();
        $user->setUsername('Test_user '.uniqid());
        $user->setPassword(self::$container->get(UserPasswordEncoderInterface::class)->encodePassword(
            $user,
            'password'
        ));

        $em->persist($user);
        $em->flush();

        $this->toDelete[] = [User::class, $user->getId()];

        yield [$user->getId(), $user->getUsername()];

        self::ensureKernelShutdown();
    }

    public function testList()
    {
        // get the list
        $crawler = $this->client->request('GET', '/fr/admin/main/user');
        $this->assertEquals(
            200,
            $this->client->getResponse()->getStatusCode(),
            'Unexpected HTTP status code for GET /admin/main/user'
        );
    }

    public function testNew()
    {
        $crawler = $this->client->request('GET', '/fr/admin/main/user/new');

        $username = 'Test_user'.uniqid();
        $password = 'Password1234!';

        // Fill in the form and submit it
        $form = $crawler->selectButton('Créer & fermer')->form([
            'chill_mainbundle_user[username]' => $username,
            'chill_mainbundle_user[plainPassword][first]' => $password,
            'chill_mainbundle_user[plainPassword][second]' => $password,
            'chill_mainbundle_user[email]' => $username.'@gmail.com',
            'chill_mainbundle_user[label]' => $username,
        ]);

        $this->client->submit($form);
        $crawler = $this->client->followRedirect();

        // Check data in the show view
        $this->assertStringContainsString(
            'Test_user',
            $crawler->text(),
            'page contains the name of the user'
        );

        // test the auth of the new client
        $this->isPasswordValid($username, $password);
    }

    /**
     * @dataProvider dataGenerateUserId
     */
    public function testUpdate(int $userId, string $username)
    {
        $crawler = $this->client->request('GET', "/fr/admin/main/user/{$userId}/edit");

        $username = 'Foo bar '.uniqid();
        $form = $crawler->selectButton('Enregistrer & fermer')->form([
            'chill_mainbundle_user[username]' => $username,
        ]);

        $this->client->submit($form);
        $crawler = $this->client->followRedirect();
        // Check the element contains an attribute with value equals "Foo"
        $this->assertResponseIsSuccessful();
    }

    /**
     * @dataProvider dataGenerateUserId
     */
    public function testUpdatePassword(int $userId, mixed $username)
    {
        $crawler = $this->client->request('GET', "/fr/admin/user/{$userId}/edit_password");
        $newPassword = '1234Password!';

        $form = $crawler->selectButton('Changer le mot de passe')->form([
            'chill_mainbundle_user_password[new_password][first]' => $newPassword,
            'chill_mainbundle_user_password[new_password][second]' => $newPassword,
        ]);

        $this->client->submit($form);

        $this->assertTrue(
            $this->client->getResponse()->isRedirect(),
            'the response is a redirection'
        );

        $this->isPasswordValid($username, $newPassword);
    }

    protected function isPasswordValid($username, $password)
    {
        /** @var \Symfony\Component\Security\Core\Encoder\UserPasswordEncoder $passwordEncoder */
        $passwordEncoder = self::$container
            ->get(UserPasswordEncoderInterface::class);

        $user = self::$container->get(UserRepositoryInterface::class)
            ->findOneBy(['username' => $username]);

        $this->assertTrue($passwordEncoder->isPasswordValid($user, $password));
    }
}
