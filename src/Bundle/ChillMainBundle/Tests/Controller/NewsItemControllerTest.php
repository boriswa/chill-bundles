<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Controller;

use Chill\MainBundle\Entity\NewsItem;
use Chill\MainBundle\Test\PrepareClientTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Tests the admin pages for news items.
 *
 * @internal
 *
 * @coversNothing
 */
class NewsItemControllerTest extends WebTestCase
{
    use PrepareClientTrait;

    /**
     * @var list<array{0: class-string, 1: int}>
     */
    private static array $entitiesToDelete = [];

    private readonly EntityManagerInterface $em;

    protected function tearDown(): void
    {
        self::ensureKernelShutdown();
    }

    public static function tearDownAfterClass(): void
    {
        self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        foreach (self::$entitiesToDelete as [$class, $id]) {
            $entity = $em->find($class, $id);

            if (null !== $entity) {
                $em->remove($entity);
            }
        }

        $em->flush();
    }

    public static function generateNewsItemIds(): iterable
    {
        self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        $newsItem = new NewsItem();
        $newsItem->setTitle('Lorem Ipsum');
        $newsItem->setContent('some text');
        $newsItem->setStartDate(new \DateTimeImmutable('now'));

        $em->persist($newsItem);
        $em->flush();

        self::$entitiesToDelete[] = [NewsItem::class, $newsItem];

        self::ensureKernelShutdown();

        yield [$newsItem];
    }

    public function testList()
    {
        $client = $this->getClientAuthenticated('admin', 'password');
        $client->request('GET', '/fr/admin/news_item');

        self::assertResponseIsSuccessful('News item admin page shows');
    }

    /**
     * @dataProvider generateNewsItemIds
     */
    public function testShowSingleItem(NewsItem $newsItem)
    {
        $client = $this->getClientAuthenticated('admin', 'password');
        $client->request('GET', "/fr/admin/news_item/{$newsItem->getId()}/view");

        self::assertResponseIsSuccessful('Single news item admin page loads successfully');
    }
}
