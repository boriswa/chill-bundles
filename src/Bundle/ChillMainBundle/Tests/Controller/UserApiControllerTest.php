<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Controller;

use Chill\MainBundle\Test\PrepareClientTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @internal
 *
 * @coversNothing
 */
final class UserApiControllerTest extends WebTestCase
{
    use PrepareClientTrait;

    /**
     * @depends testIndex
     */
    public function testEntity(mixed $existingUser)
    {
        $client = $this->getClientAuthenticated();

        $client->request(Request::METHOD_GET, '/api/1.0/main/user/'.$existingUser['id'].'.json');

        $this->assertResponseIsSuccessful();
    }

    public function testIndex()
    {
        $client = $this->getClientAuthenticated();

        $client->request(Request::METHOD_GET, '/api/1.0/main/user.json');

        $this->assertResponseIsSuccessful();

        $data = \json_decode($client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertTrue(\array_key_exists('count', $data));
        $this->assertGreaterThan(0, $data['count']);
        $this->assertTrue(\array_key_exists('results', $data));

        return $data['results'][0];
    }

    public function testUserCurrentLocation()
    {
        $client = $this->getClientAuthenticated();

        $client->request(Request::METHOD_GET, '/api/1.0/main/user-current-location.json');

        $this->assertResponseIsSuccessful();
    }

    public function testWhoami()
    {
        $client = $this->getClientAuthenticated();

        $client->request(Request::METHOD_GET, '/api/1.0/main/whoami.json');

        $this->assertResponseIsSuccessful();
    }
}
