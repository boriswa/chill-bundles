<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Controller;

use Chill\MainBundle\Test\PrepareClientTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class UserProfileControllerTest extends WebTestCase
{
    use PrepareClientTrait;

    public function testPage()
    {
        $client = $this->getClientAuthenticated();

        $client->request('GET', '/fr/main/user/my-profile');
        $this->assertResponseIsSuccessful('Request GET /main/user/my-profile was successful');
    }
}
