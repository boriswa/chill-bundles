<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Test the search controller.
 *
 * @internal
 *
 * @coversNothing
 */
final class SearchControllerTest extends WebTestCase
{
    public function testDomainUnknow()
    {
        $client = $this->getAuthenticatedClient();

        $crawler = $client->request('GET', '/fr/search', ['q' => '@unknow domain']);

        $this->assertTrue(
            $client->getResponse()->isSuccessful(),
            'The page is loaded without errors'
        );
        $this->assertGreaterThan(
            0,
            $crawler->filter('*:contains("Le domaine de recherche "unknow" est inconnu.")')->count(),
            'Message domain unknow is shown'
        );
    }

    public function testParsingIncorrect()
    {
        $client = $this->getAuthenticatedClient();

        $crawler = $client->request(
            'GET',
            '/fr/search',
            ['q' => '@domaine @domain double domaine']
        );

        $this->assertGreaterThan(0, $crawler->filter('*:contains("Recherche invalide")')
            ->count());
    }

    /**
     * Test the behaviour when no domain is provided in the search pattern :
     * the default search should be enabled.
     */
    public function testSearchPath()
    {
        $client = $this->getAuthenticatedClient();

        $crawler = $client->request('GET', '/fr/search', ['q' => 'default search']);

        $this->assertTrue(
            $client->getResponse()->isSuccessful(),
            'The page is loaded without errors'
        );
    }

    public function testSearchPathEmpty()
    {
        $client = $this->getAuthenticatedClient();

        $crawler = $client->request('GET', '/fr/search?q=');

        $this->assertGreaterThan(0, $crawler->filter('*:contains("Merci de fournir des termes de recherche.")')->count());
    }

    public function testUnknowName()
    {
        $client = $this->getAuthenticatedClient();

        $client->request(
            'GET',
            '/fr/search',
            ['q' => 'default search', 'name' => 'unknow']
        );

        $this->assertTrue($client->getResponse()->isNotFound());
    }

    private function getAuthenticatedClient()
    {
        return self::createClient([], [
            'PHP_AUTH_USER' => 'center b_social',
            'PHP_AUTH_PW' => 'password',
        ]);
    }
}
