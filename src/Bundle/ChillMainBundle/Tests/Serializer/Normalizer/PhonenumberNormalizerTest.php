<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Serializer\Normalizer;

use Chill\MainBundle\Serializer\Normalizer\PhonenumberNormalizer;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberUtil;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class PhonenumberNormalizerTest extends TestCase
{
    use ProphecyTrait;

    public function dataProviderNormalizePhonenumber()
    {
        $phonenumberUtil = PhoneNumberUtil::getInstance();

        yield [$phonenumberUtil->parse('+32486123465'), 'docgen', ['docgen:expects' => PhoneNumber::class], '0486 12 34 65'];

        yield [null, 'docgen', ['docgen:expects' => PhoneNumber::class], ''];
    }

    /**
     * @dataProvider dataProviderNormalizePhonenumber
     */
    public function testNormalize(?PhoneNumber $phonenumber, mixed $format, mixed $context, mixed $expected)
    {
        $parameterBag = $this->prophesize(ParameterBagInterface::class);
        $parameterBag->get(Argument::exact('chill_main'))->willReturn(['phone_helper' => ['default_carrier_code' => 'BE']]);
        $normalizer = new PhonenumberNormalizer($parameterBag->reveal());

        $this->assertEquals($expected, $normalizer->normalize($phonenumber, $format, $context));
    }
}
