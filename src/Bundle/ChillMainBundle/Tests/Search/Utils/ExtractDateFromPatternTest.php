<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Search\Utils;

use Chill\MainBundle\Search\Utils\ExtractDateFromPattern;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class ExtractDateFromPatternTest extends TestCase
{
    public function provideSubjects()
    {
        yield ['15/06/1981', '', 1, '1981-06-15'];

        yield ['15/06/1981 30/12/1987', '', 2, '1981-06-15', '1987-12-30'];

        yield ['diallo 15/06/1981', 'diallo', 1, '1981-06-15'];

        yield ['diallo  31/03/1981', 'diallo', 1, '1981-03-31'];

        yield ['diallo 15-06-1981', 'diallo', 1, '1981-06-15'];

        yield ['diallo 1981-12-08', 'diallo', 1, '1981-12-08'];

        yield ['diallo', 'diallo', 0];
    }

    /**
     * @dataProvider provideSubjects
     */
    public function testExtractDate(string $subject, string $filtered, int $count, ...$datesSearched)
    {
        $extractor = new ExtractDateFromPattern();
        $result = $extractor->extractDates($subject);

        $this->assertCount($count, $result->getFound());
        $this->assertEquals($filtered, $result->getFilteredSubject());
        $this->assertContainsOnlyInstancesOf(\DateTimeImmutable::class, $result->getFound());

        $dates = \array_map(
            static fn (\DateTimeImmutable $d) => $d->format('Y-m-d'),
            $result->getFound()
        );

        foreach ($datesSearched as $date) {
            $this->assertContains($date, $dates);
        }
    }
}
