<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Search;

use Chill\MainBundle\Search\SearchApiQuery;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class SearchApiQueryTest extends TestCase
{
    public function testBuildParams()
    {
        $q = new SearchApiQuery();

        $q
            ->addSelectClause('bada', ['one', 'two'])
            ->addSelectClause('boum', ['three', 'four'])
            ->setWhereClauses('mywhere', ['six', 'seven']);

        $params = $q->buildParameters();

        $this->assertEquals(['six', 'seven'], $q->buildParameters(true));
        $this->assertEquals(['one', 'two', 'three', 'four', 'six', 'seven'], $q->buildParameters());
    }

    public function testMultipleWhereClauses()
    {
        $q = new SearchApiQuery();
        $q->setSelectJsonbMetadata('boum')
            ->setSelectKey('bim')
            ->setSelectPertinence('?', ['gamma'])
            ->setFromClause('badaboum')
            ->andWhereClause('foo', ['alpha'])
            ->andWhereClause('bar', ['beta']);

        $query = $q->buildQuery();

        $this->assertStringContainsString('(foo) AND (bar)', $query);
        $this->assertEquals(['gamma', 'alpha', 'beta'], $q->buildParameters());

        $query = $q->buildQuery(true);

        $this->assertStringContainsString('(foo) AND (bar)', $query);
        $this->assertEquals(['gamma', 'alpha', 'beta'], $q->buildParameters());
    }

    public function testWithoutWhereClause()
    {
        $q = new SearchApiQuery();
        $q->setSelectJsonbMetadata('boum')
            ->setSelectKey('bim')
            ->setSelectPertinence('1')
            ->setFromClause('badaboum');

        $this->assertTrue(\is_string($q->buildQuery()));
        $this->assertEquals([], $q->buildParameters());
    }
}
