<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Security\Resolver;

use Chill\MainBundle\Entity\HasScopeInterface;
use Chill\MainBundle\Entity\HasScopesInterface;
use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Security\Resolver\DefaultScopeResolver;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class DefaultScopeResolverTest extends TestCase
{
    private DefaultScopeResolver $scopeResolver;

    protected function setUp(): void
    {
        $this->scopeResolver = new DefaultScopeResolver();
    }

    public function testHasScopeInterface()
    {
        $scope = new Scope();
        $entity = new class ($scope) implements HasScopeInterface {
            public function __construct(private readonly Scope $scope)
            {
            }

            public function getScope()
            {
                return $this->scope;
            }
        };

        $this->assertTrue($this->scopeResolver->supports($entity));
        $this->assertTrue($this->scopeResolver->isConcerned($entity));
        $this->assertSame($scope, $this->scopeResolver->resolveScope($entity));
    }

    public function testHasScopesInterface()
    {
        $entity = new class ($scopeA = new Scope(), $scopeB = new Scope()) implements HasScopesInterface {
            public function __construct(private readonly Scope $scopeA, private readonly Scope $scopeB)
            {
            }

            public function getScopes(): iterable
            {
                return [$this->scopeA, $this->scopeB];
            }
        };

        $this->assertTrue($this->scopeResolver->supports($entity));
        $this->assertTrue($this->scopeResolver->isConcerned($entity));
        $this->assertIsArray($this->scopeResolver->resolveScope($entity));
        $this->assertSame($scopeA, $this->scopeResolver->resolveScope($entity)[0]);
        $this->assertSame($scopeB, $this->scopeResolver->resolveScope($entity)[1]);
    }
}
