<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests;

use Symfony\Component\BrowserKit\Client;

/**
 * Provide useful methods for tests.
 */
class TestHelper
{
    /**
     * create a client authenticated with an user.
     *
     * @return \Symfony\Component\BrowserKit\AbstractBrowser authenticated client
     */
    public static function getAuthenticatedClientOptions(
        mixed $username = 'center a_social',
        mixed $password = 'password'
    ) {
        return [
            'PHP_AUTH_USER' => $username,
            'PHP_AUTH_PW' => $password,
        ];
    }
}
