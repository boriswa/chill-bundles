<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Doctrine\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class JsonExtract extends FunctionNode
{
    private \Doctrine\ORM\Query\AST\Node|string|null $element = null;

    private ?\Doctrine\ORM\Query\AST\ArithmeticExpression $keyToExtract = null;

    public function getSql(SqlWalker $sqlWalker)
    {
        return sprintf('%s->>%s', $this->element->dispatch($sqlWalker), $this->keyToExtract->dispatch($sqlWalker));
    }

    public function parse(Parser $parser)
    {
        $parser->match(\Doctrine\ORM\Query\TokenType::T_IDENTIFIER);
        $parser->match(\Doctrine\ORM\Query\TokenType::T_OPEN_PARENTHESIS);

        $this->element = $parser->ArithmeticPrimary();

        $parser->match(\Doctrine\ORM\Query\TokenType::T_COMMA);

        $this->keyToExtract = $parser->ArithmeticExpression();

        $parser->match(\Doctrine\ORM\Query\TokenType::T_CLOSE_PARENTHESIS);
    }
}
