<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Doctrine\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;

class Replace extends FunctionNode
{
    protected $from;

    protected $string;

    protected $to;

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker): string
    {
        return 'REPLACE('.
            $this->string->dispatch($sqlWalker).
            ', '.
            $this->from->dispatch($sqlWalker).
            ', '.
            $this->to->dispatch($sqlWalker).
            ')';
    }

    public function parse(\Doctrine\ORM\Query\Parser $parser): void
    {
        $parser->match(\Doctrine\ORM\Query\TokenType::T_IDENTIFIER);
        $parser->match(\Doctrine\ORM\Query\TokenType::T_OPEN_PARENTHESIS);

        $this->string = $parser->StringPrimary();

        $parser->match(\Doctrine\ORM\Query\TokenType::T_COMMA);

        $this->from = $parser->StringPrimary();

        $parser->match(\Doctrine\ORM\Query\TokenType::T_COMMA);

        $this->to = $parser->StringPrimary();

        $parser->match(\Doctrine\ORM\Query\TokenType::T_CLOSE_PARENTHESIS);
    }
}
