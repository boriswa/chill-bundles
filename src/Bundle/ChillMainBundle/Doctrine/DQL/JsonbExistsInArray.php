<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Doctrine\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class JsonbExistsInArray extends FunctionNode
{
    private ?\Doctrine\ORM\Query\AST\Node $expr1 = null;

    private ?\Doctrine\ORM\Query\AST\InputParameter $expr2 = null;

    public function getSql(SqlWalker $sqlWalker): string
    {
        return sprintf(
            '%s ?? %s',
            $this->expr1->dispatch($sqlWalker),
            $this->expr2->dispatch($sqlWalker)
        );
    }

    public function parse(Parser $parser): void
    {
        $parser->match(\Doctrine\ORM\Query\TokenType::T_IDENTIFIER);
        $parser->match(\Doctrine\ORM\Query\TokenType::T_OPEN_PARENTHESIS);
        $this->expr1 = $parser->StringPrimary();
        $parser->match(\Doctrine\ORM\Query\TokenType::T_COMMA);
        $this->expr2 = $parser->InputParameter();
        $parser->match(\Doctrine\ORM\Query\TokenType::T_CLOSE_PARENTHESIS);
    }
}
