<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Doctrine\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Return an aggregation of values in a json representation, as a string.
 *
 * Internally, this function use the postgresql `jsonb_agg` function. Using
 * json allow to aggregate data from different types without have to cast them.
 */
class JsonBuildObject extends FunctionNode
{
    /**
     * @var array|Node[]
     */
    private array $exprs = [];

    public function getSql(SqlWalker $sqlWalker)
    {
        return 'JSONB_BUILD_OBJECT('.implode(', ', array_map(static fn (Node $expr) => $expr->dispatch($sqlWalker), $this->exprs)).')';
    }

    public function parse(Parser $parser)
    {
        $lexer = $parser->getLexer();
        $parser->match(\Doctrine\ORM\Query\TokenType::T_IDENTIFIER);
        $parser->match(\Doctrine\ORM\Query\TokenType::T_OPEN_PARENTHESIS);

        $this->exprs[] = $parser->ArithmeticPrimary();

        while (\Doctrine\ORM\Query\TokenType::T_COMMA === $lexer->lookahead['type']) {
            $parser->match(\Doctrine\ORM\Query\TokenType::T_COMMA);
            $this->exprs[] = $parser->ArithmeticPrimary();
        }

        $parser->match(\Doctrine\ORM\Query\TokenType::T_CLOSE_PARENTHESIS);
    }
}
