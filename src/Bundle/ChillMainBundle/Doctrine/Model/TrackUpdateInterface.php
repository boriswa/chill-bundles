<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Doctrine\Model;

use Chill\MainBundle\Entity\User;

interface TrackUpdateInterface
{
    public function setUpdatedAt(\DateTimeInterface $datetime): self;

    public function setUpdatedBy(User $user): self;
}
