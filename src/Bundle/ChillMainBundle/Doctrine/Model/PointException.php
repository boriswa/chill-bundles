<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Doctrine\Model;

/**
 * Description of PointException.
 */
class PointException extends \Exception
{
    public static function badCoordinates($lon, $lat): self
    {
        return new self("Input coordinates are not valid in the used coordinate system (longitude = {$lon} , latitude = {$lat})");
    }

    public static function badGeoType(): self
    {
        return new self('The geoJSON object type is not valid');
    }

    public static function badJsonString($str): self
    {
        return new self("The JSON string is not valid: {$str}");
    }
}
