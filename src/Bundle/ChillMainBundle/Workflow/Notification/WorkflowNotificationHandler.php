<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow\Notification;

use Chill\MainBundle\Entity\Notification;
use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Notification\NotificationHandlerInterface;
use Chill\MainBundle\Repository\Workflow\EntityWorkflowRepository;
use Chill\MainBundle\Workflow\EntityWorkflowManager;
use Symfony\Component\Security\Core\Security;

class WorkflowNotificationHandler implements NotificationHandlerInterface
{
    public function __construct(private readonly EntityWorkflowRepository $entityWorkflowRepository, private readonly EntityWorkflowManager $entityWorkflowManager, private readonly Security $security)
    {
    }

    public function getTemplate(Notification $notification, array $options = []): string
    {
        return '@ChillMain/Workflow/_notification_include.html.twig';
    }

    public function getTemplateData(Notification $notification, array $options = []): array
    {
        $entityWorkflow = $this->entityWorkflowRepository->find($notification->getRelatedEntityId());

        return [
            'entity_workflow' => $entityWorkflow,
            'handler' => $this->entityWorkflowManager->getHandler($entityWorkflow),
            'notificationCc' => $this->isNotificationCc($notification),
        ];
    }

    private function isNotificationCc(Notification $notification): bool
    {
        $notificationCc = false;

        if (EntityWorkflow::class === $notification->getRelatedEntityClass()) {
            $relatedEntity = $this->entityWorkflowRepository->findOneBy(['id' => $notification->getRelatedEntityId()]);

            if ($relatedEntity->getCurrentStepCreatedBy() !== $this->security->getUser()) {
                $notificationCc = true;
            }
        }

        return $notificationCc;
    }

    public function supports(Notification $notification, array $options = []): bool
    {
        return EntityWorkflow::class === $notification->getRelatedEntityClass();
    }
}
