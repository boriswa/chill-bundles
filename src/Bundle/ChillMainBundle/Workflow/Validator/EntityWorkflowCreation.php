<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow\Validator;

/**
 * Validator which will test that:.
 *
 * * a handler exists;
 * * a related entity does exists;
 * * a workflow can be associated with this entity.
 *
 * @Annotation
 */
class EntityWorkflowCreation extends \Symfony\Component\Validator\Constraint
{
    public string $messageEntityNotFound = 'Related entity is not found';

    public string $messageHandlerNotFound = 'Handler not found for this entity';

    public string $messageWorkflowNotAvailable = 'Workflow is not valid';

    public function getTargets()
    {
        return [self::CLASS_CONSTRAINT];
    }
}
