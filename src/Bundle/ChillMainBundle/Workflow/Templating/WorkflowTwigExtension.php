<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow\Templating;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class WorkflowTwigExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction(
                'chill_entity_workflow_list',
                [WorkflowTwigExtensionRuntime::class, 'listWorkflows'],
                ['needs_environment' => true, 'is_safe' => ['html']]
            ),
            new TwigFunction(
                'chill_workflow_transition_by_string',
                [WorkflowTwigExtensionRuntime::class, 'getTransitionByString']
            ),
        ];
    }
}
