<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\UserJob;
use Doctrine\Persistence\ObjectRepository;

interface UserJobRepositoryInterface extends ObjectRepository
{
    public function find($id): ?UserJob;

    /**
     * @return array|UserJob[]
     */
    public function findAll(): array;

    /**
     * @return array|UserJob[]
     */
    public function findAllActive(): array;

    /**
     * a list of UserJob ordered by name.
     *
     * @return array<UserJob>
     */
    public function findAllOrderedByName(): array;

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return array|object[]|UserJob[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null);

    public function findOneBy(array $criteria): ?UserJob;

    public function getClassName(): string;
}
