<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\SavedExport;
use Chill\MainBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;

/**
 * @implements ObjectRepository<SavedExport>
 */
class SavedExportRepository implements SavedExportRepositoryInterface
{
    private readonly EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository($this->getClassName());
    }

    public function find($id): ?SavedExport
    {
        return $this->repository->find($id);
    }

    /**
     * @return array|SavedExport[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findByUser(User $user, ?array $orderBy = [], ?int $limit = null, ?int $offset = null): array
    {
        $qb = $this->repository->createQueryBuilder('se');

        $qb
            ->where($qb->expr()->eq('se.user', ':user'))
            ->setParameter('user', $user);

        if (null !== $limit) {
            $qb->setMaxResults($limit);
        }

        if (null !== $offset) {
            $qb->setFirstResult($offset);
        }

        foreach ($orderBy as $field => $order) {
            $qb->addOrderBy('se.'.$field, $order);
        }

        return $qb->getQuery()->getResult();
    }

    public function findOneBy(array $criteria): ?SavedExport
    {
        return $this->repository->findOneBy($criteria);
    }

    public function getClassName(): string
    {
        return SavedExport::class;
    }
}
