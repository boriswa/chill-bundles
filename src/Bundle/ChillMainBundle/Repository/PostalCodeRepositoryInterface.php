<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\Country;
use Chill\MainBundle\Entity\PostalCode;
use Doctrine\Persistence\ObjectRepository;

interface PostalCodeRepositoryInterface extends ObjectRepository
{
    public function countByPattern(string $pattern, ?Country $country): int;

    public function find($id, $lockMode = null, $lockVersion = null): ?PostalCode;

    /**
     * @return PostalCode[]
     */
    public function findAll(): array;

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return PostalCode[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array;

    public function findByPattern(string $pattern, ?Country $country, ?int $start = 0, ?int $limit = 50): array;

    public function findOneBy(array $criteria, ?array $orderBy = null): ?PostalCode;

    public function getClassName(): string;
}
