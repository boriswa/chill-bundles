<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\Civility;
use Doctrine\Persistence\ObjectRepository;

interface CivilityRepositoryInterface extends ObjectRepository
{
    public function find($id): ?Civility;

    /**
     * @return array|Civility[]
     */
    public function findAll(): array;

    /**
     * @return array|Civility[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array;

    public function findOneBy(array $criteria): ?Civility;

    public function getClassName(): string;
}
