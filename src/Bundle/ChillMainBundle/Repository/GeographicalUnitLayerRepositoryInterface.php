<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\GeographicalUnitLayer;
use Doctrine\Persistence\ObjectRepository;

interface GeographicalUnitLayerRepositoryInterface extends ObjectRepository
{
    /**
     * @return array|GeographicalUnitLayer[]
     */
    public function findAllHavingUnits(): array;
}
