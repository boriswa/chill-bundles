<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\SavedExport;
use Chill\MainBundle\Entity\User;
use Doctrine\Persistence\ObjectRepository;

/**
 * @implements ObjectRepository<SavedExport>
 */
interface SavedExportRepositoryInterface extends ObjectRepository
{
    public function find($id): ?SavedExport;

    /**
     * @return array|SavedExport[]
     */
    public function findAll(): array;

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array;

    /**
     * @return array|SavedExport[]
     */
    public function findByUser(User $user, ?array $orderBy = [], ?int $limit = null, ?int $offset = null): array;

    public function findOneBy(array $criteria): ?SavedExport;

    public function getClassName(): string;
}
