<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\Scope;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ObjectRepository;

interface ScopeRepositoryInterface extends ObjectRepository
{
    public function createQueryBuilder($alias, $indexBy = null): QueryBuilder;

    public function find($id, $lockMode = null, $lockVersion = null): ?Scope;

    /**
     * @return array|Scope[]
     */
    public function findAll(): array;

    /**
     * @return array|Scope[]
     */
    public function findAllActive(): array;

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return Scope[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array;

    public function findOneBy(array $criteria, ?array $orderBy = null): ?Scope;

    public function getClassName(): string;
}
