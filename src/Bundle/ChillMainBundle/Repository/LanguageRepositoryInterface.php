<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\Language;
use Doctrine\Persistence\ObjectRepository;

interface LanguageRepositoryInterface extends ObjectRepository
{
    public function find($id, $lockMode = null, $lockVersion = null): ?Language;

    /**
     * @return Language[]
     */
    public function findAll(): array;

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return Language[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array;

    public function findOneBy(array $criteria, ?array $orderBy = null): ?Language;

    public function getClassName(): string;
}
