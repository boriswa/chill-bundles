<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\Address;
use Chill\MainBundle\Entity\GeographicalUnit\SimpleGeographicalUnitDTO;
use Doctrine\Persistence\ObjectRepository;

interface GeographicalUnitRepositoryInterface extends ObjectRepository
{
    /**
     * Return the geographical units as @see{SimpleGeographicalUnitDTO} whithin the address is contained.
     *
     * This query is executed in real time (without the refresh of the materialized view which load the addresses).
     *
     * @return SimpleGeographicalUnitDTO[]
     */
    public function findGeographicalUnitContainingAddress(Address $address, int $offset = 0, int $limit = 50): array;

    public function countGeographicalUnitContainingAddress(Address $address): int;
}
