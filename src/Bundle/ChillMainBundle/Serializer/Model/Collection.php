<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Serializer\Model;

use Chill\MainBundle\Pagination\PaginatorInterface;

class Collection
{
    public function __construct(private $items, private readonly PaginatorInterface $paginator)
    {
    }

    public function getItems()
    {
        return $this->items;
    }

    public function getPaginator(): PaginatorInterface
    {
        return $this->paginator;
    }
}
