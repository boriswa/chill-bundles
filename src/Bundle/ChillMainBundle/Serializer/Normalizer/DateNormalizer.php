<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Serializer\Normalizer;

use DateTimeInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class DateNormalizer implements ContextAwareNormalizerInterface, DenormalizerInterface
{
    public function __construct(private readonly RequestStack $requestStack, private readonly ParameterBagInterface $parameterBag)
    {
    }

    public function denormalize($data, $type, $format = null, array $context = [])
    {
        if (null === $data) {
            return null;
        }

        switch ($type) {
            case \DateTime::class:
                $result = \DateTime::createFromFormat(\DateTimeInterface::ISO8601, $data['datetime']);

                break;

            case \DateTimeInterface::class:
            case \DateTimeImmutable::class:
                $result = \DateTimeImmutable::createFromFormat(\DateTimeInterface::ISO8601, $data['datetime']);

                break;

            default:
                throw new UnexpectedValueException();
        }

        if (false === $result) {
            return null;
        }

        return $result;
    }

    public function normalize($date, $format = null, array $context = [])
    {
        /* @var DateTimeInterface $date */
        switch ($format) {
            case 'json':
                return [
                    'datetime' => $date->format(\DateTimeInterface::ISO8601),
                    'datetime8601' => $date->format(\DateTimeInterface::ATOM),
                ];

            case 'docgen':
                if (null === $date) {
                    return [
                        'long' => '', 'short' => '',
                    ];
                }

                $hasTime = '000000' !== $date->format('His');
                $request = $this->requestStack->getCurrentRequest();
                $locale = null !== $request ? $request->getLocale() : $this->parameterBag->get('kernel.default_locale');
                $formatterLong = \IntlDateFormatter::create(
                    $locale,
                    \IntlDateFormatter::LONG,
                    $hasTime ? \IntlDateFormatter::SHORT : \IntlDateFormatter::NONE
                );
                $formatterShort = \IntlDateFormatter::create(
                    $locale,
                    \IntlDateFormatter::SHORT,
                    $hasTime ? \IntlDateFormatter::SHORT : \IntlDateFormatter::NONE
                );

                return [
                    'short' => $formatterShort->format($date),
                    'long' => $formatterLong->format($date),
                ];
        }
    }

    public function supportsDenormalization($data, $type, $format = null): bool
    {
        return \DateTimeInterface::class === $type
            || \DateTime::class === $type
            || \DateTimeImmutable::class === $type
            || (\is_array($data) && \array_key_exists('datetime', $data));
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        if ('json' === $format) {
            return $data instanceof \DateTimeInterface;
        }

        if ('docgen' === $format) {
            return $data instanceof \DateTimeInterface || (
                null === $data
                && \array_key_exists('docgen:expects', $context)
                && (
                    \DateTimeInterface::class === $context['docgen:expects']
                    || \DateTime::class === $context['docgen:expects']
                    || \DateTimeImmutable::class === $context['docgen:expects']
                )
            );
        }

        return false;
    }
}
