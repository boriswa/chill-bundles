<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Controller;

use Chill\MainBundle\CRUD\Controller\ApiController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LocationTypeApiController.
 */
class LocationTypeApiController extends ApiController
{
    public function customizeQuery(string $action, Request $request, $query): void
    {
        $query->andWhere(
            $query->expr()->andX(
                $query->expr()->eq('e.availableForUsers', "'TRUE'"),
                $query->expr()->eq('e.editableByUsers', "'TRUE'"),
                $query->expr()->eq('e.active', "'TRUE'"),
            )
        );
    }
}
