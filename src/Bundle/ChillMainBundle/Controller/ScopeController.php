<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Controller;

use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Form\ScopeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ScopeController.
 */
class ScopeController extends AbstractController
{
    /**
     * Creates a new Scope entity.
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/scope/create", name="admin_scope_create", methods={"POST"})
     */
    public function createAction(Request $request)
    {
        $scope = new Scope();
        $form = $this->createCreateForm($scope);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($scope);
            $em->flush();

            return $this->redirectToRoute('admin_scope');
        }

        return $this->render('@ChillMain/Scope/new.html.twig', [
            'entity' => $scope,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Scope entity.
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/scope/{id}/edit", name="admin_scope_edit")
     */
    public function editAction(mixed $id)
    {
        $em = $this->getDoctrine()->getManager();

        $scope = $em->getRepository(Scope::class)->find($id);

        if (!$scope) {
            throw $this->createNotFoundException('Unable to find Scope entity.');
        }

        $editForm = $this->createEditForm($scope);

        return $this->render('@ChillMain/Scope/edit.html.twig', [
            'entity' => $scope,
            'edit_form' => $editForm->createView(),
        ]);
    }

    /**
     * Lists all Scope entities.
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/scope/", name="admin_scope")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository(Scope::class)->findAll();

        return $this->render('@ChillMain/Scope/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * Displays a form to create a new Scope entity.
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/scope/new", name="admin_scope_new")
     */
    public function newAction()
    {
        $scope = new Scope();
        $form = $this->createCreateForm($scope);

        return $this->render('@ChillMain/Scope/new.html.twig', [
            'entity' => $scope,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a Scope entity.
     */
    public function showAction(mixed $id)
    {
        $em = $this->getDoctrine()->getManager();

        $scope = $em->getRepository(Scope::class)->find($id);

        if (!$scope) {
            throw $this->createNotFoundException('Unable to find Scope entity.');
        }

        return $this->render('@ChillMain/Scope/show.html.twig', [
            'entity' => $scope,
        ]);
    }

    /**
     * Edits an existing Scope entity.
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/scope/{id}/update", name="admin_scope_update", methods={"POST", "PUT"})
     */
    public function updateAction(Request $request, mixed $id)
    {
        $em = $this->getDoctrine()->getManager();

        $scope = $em->getRepository(Scope::class)->find($id);

        if (!$scope) {
            throw $this->createNotFoundException('Unable to find Scope entity.');
        }

        $editForm = $this->createEditForm($scope);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();

            return $this->redirectToRoute('admin_scope_edit', ['id' => $id]);
        }

        return $this->render('@ChillMain/Scope/edit.html.twig', [
            'entity' => $scope,
            'edit_form' => $editForm->createView(),
        ]);
    }

    /**
     * Creates a form to create a Scope entity.
     *
     * @param Scope $scope The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Scope $scope)
    {
        $form = $this->createForm(ScopeType::class, $scope, [
            'action' => $this->generateUrl('admin_scope_create'),
            'method' => 'POST',
        ]);

        $form->add('submit', SubmitType::class, ['label' => 'Create']);

        return $form;
    }

    /**
     * Creates a form to edit a Scope entity.
     *
     * @param Scope $scope The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Scope $scope)
    {
        $form = $this->createForm(ScopeType::class, $scope, [
            'action' => $this->generateUrl('admin_scope_update', ['id' => $scope->getId()]),
            'method' => 'PUT',
        ]);

        $form->add('submit', SubmitType::class, ['label' => 'Update']);

        return $form;
    }
}
