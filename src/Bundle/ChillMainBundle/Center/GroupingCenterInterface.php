<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Center;

/**
 * Interface to declare a groups of centers.
 *
 * This interface is used to declare a groups of centers in
 * `Chill\MainBundle\Form\Export\PickCenterType`.
 */
interface GroupingCenterInterface
{
    /**
     * @param string $group
     *
     * @return \Chill\MainBundle\Entity\Center[]
     */
    public function getCentersForGroup($group);

    /**
     * @param mixed|null $authorizedCenters
     *
     * @return string[]
     */
    public function getGroups($authorizedCenters = null): array;
}
