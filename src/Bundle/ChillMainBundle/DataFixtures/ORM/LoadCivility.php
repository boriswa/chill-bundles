<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DataFixtures\ORM;

use Chill\MainBundle\Entity\Civility;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class LoadCivility extends Fixture implements FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return ['civilities'];
    }

    public function load(ObjectManager $manager): void
    {
        $civilities = [
            ['name' => ['fr' => 'Madame'], 'abbrev' => ['fr' => 'Mme']],
            ['name' => ['fr' => 'Monsieur'], 'abbrev' => ['fr' => 'M.']],
            ['name' => ['fr' => 'Docteur'], 'abbrev' => ['fr' => 'Dr']],
            ['name' => ['fr' => 'Professeur'], 'abbrev' => ['fr' => 'Pr']],
            ['name' => ['fr' => 'Madame la Directrice'], 'abbrev' => ['fr' => 'Mme']],
            ['name' => ['fr' => 'Monsieur le Directeur'], 'abbrev' => ['fr' => 'M.']],
            ['name' => ['fr' => 'Madame la Maire']],
            ['name' => ['fr' => 'Monsieur le Maire']],
            ['name' => ['fr' => 'Maître'], 'abbrev' => ['fr' => 'Me']],
        ];

        foreach ($civilities as $val) {
            $civility = (new Civility())
                ->setName($val['name'])
                ->setAbbreviation($val['abbrev'] ?? [])
                ->setActive(true);
            $manager->persist($civility);
        }

        $manager->flush();
    }
}
