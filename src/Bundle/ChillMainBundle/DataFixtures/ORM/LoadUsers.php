<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DataFixtures\ORM;

use Chill\MainBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

/**
 * Load fixtures users into database.
 *
 * create a user for each permission_group and center.
 * username and password are identicals.
 */
class LoadUsers extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    public static $refs = [
        'center a_social' => [
            'groupCenterRefs' => ['centerA_permission_group_social'],
        ],
        'center a_administrative' => [
            'groupCenterRefs' => ['centerA_permission_group_administrative'],
        ],
        'center a_direction' => [
            'groupCenterRefs' => ['centerA_permission_group_direction'],
        ],
        'center b_social' => [
            'groupCenterRefs' => ['centerB_permission_group_social'],
        ],
        'center b_administrative' => [
            'groupCenterRefs' => ['centerB_permission_group_administrative'],
        ],
        'center b_direction' => [
            'groupCenterRefs' => ['centerB_permission_group_direction'],
        ],
        'multi_center' => [
            'groupCenterRefs' => ['centerA_permission_group_social',
                'centerB_permission_group_social', ],
        ],
    ];

    private ?ContainerInterface $container = null;

    public function getOrder()
    {
        return 1000;
    }

    public function load(ObjectManager $manager)
    {
        foreach (self::$refs as $username => $params) {
            $user = new User();

            $defaultEncoder = new MessageDigestPasswordEncoder('sha512', true, 5000);

            $encoderFactory = new EncoderFactory([
                User::class => $defaultEncoder,
            ]);

            $user
                ->setUsername($username)
                ->setPassword(
                    $encoderFactory
                        ->getEncoder($user)
                        ->encodePassword('password', $user->getSalt())
                )
                ->setEmail(sprintf('%s@chill.social', \str_replace(' ', '', (string) $username)));

            foreach ($params['groupCenterRefs'] as $groupCenterRef) {
                $user->addGroupCenter($this->getReference($groupCenterRef));
            }

            echo 'Creating user '.$username."... \n";
            $manager->persist($user);
            $this->addReference($username, $user);
        }

        $manager->flush();
    }

    public function setContainer(?ContainerInterface $container = null)
    {
        if (null === $container) {
            throw new \LogicException('$container should not be null');
        }

        $this->container = $container;
    }
}
