<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DataFixtures\ORM;

use Chill\MainBundle\Doctrine\Model\Point;
use Chill\MainBundle\Entity\AddressReference;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Load reference addresses into database.
 */
class LoadAddressReferences extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    protected $faker;

    private ?ContainerInterface $container = null;

    public function __construct()
    {
        $this->faker = \Faker\Factory::create('fr_FR');
    }

    public function getOrder()
    {
        return 51;
    }

    public function load(ObjectManager $manager)
    {
        echo "loading some reference address... \n";

        for ($i = 0; 10 > $i; ++$i) {
            $ar = $this->getRandomAddressReference();
            $manager->persist($ar);
        }

        $manager->flush();
    }

    public function setContainer(?ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Create a random reference address.
     *
     * @return AddressReference
     */
    private function getRandomAddressReference()
    {
        $ar = new AddressReference();

        $ar->setRefId($this->faker->numerify('ref-id-######'));
        $ar->setStreet($this->faker->streetName);
        $ar->setStreetNumber((string) random_int(0, 199));
        $ar->setPoint($this->getRandomPoint());
        $ar->setPostcode($this->getReference(
            LoadPostalCodes::$refs[array_rand(LoadPostalCodes::$refs)]
        ));

        $ar->setMunicipalityCode($ar->getPostcode()->getCode());

        return $ar;
    }

    /**
     * Create a random point.
     *
     * @return Point
     */
    private function getRandomPoint()
    {
        $lonBrussels = 4.35243;
        $latBrussels = 50.84676;
        $lon = $lonBrussels + 0.01 * random_int(-5, 5);
        $lat = $latBrussels + 0.01 * random_int(-5, 5);

        return Point::fromLonLat($lon, $lat);
    }
}
