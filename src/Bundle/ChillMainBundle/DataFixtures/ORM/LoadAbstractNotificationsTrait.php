<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DataFixtures\ORM;

use Chill\MainBundle\Entity\Notification;
use Doctrine\Persistence\ObjectManager;

/**
 * Load notififications into database.
 */
trait LoadAbstractNotificationsTrait
{
    public function load(ObjectManager $manager)
    {
        return;

        foreach ($this->notifs as $notif) {
            $entityId = $this->getReference($notif['entityRef'])->getId();

            echo 'Adding notification for '.$notif['entityClass'].'(entity id:'.$entityId.")\n";

            $newNotif = (new Notification())
                ->setMessage($notif['message'])
                ->setSender($this->getReference($notif['sender']))
                ->setRelatedEntityClass($notif['entityClass'])
                ->setRelatedEntityId($entityId)
                ->setDate(new \DateTimeImmutable('now'))
                ->setRead([]);

            foreach ($notif['addressees'] as $addressee) {
                $newNotif->addAddressee($this->getReference($addressee));
            }

            $manager->persist($newNotif);

            $manager->flush();
        }
    }
}
