

Version 1.5.1
=============

- add email to users on fixtures ;
- spare ressource with recursive trigger on inserting user ;
- fix error when no flags are used during edit and creation of permission group ;

Version 1.5.2
=============

- allow to filters users shown by `UserPickerType` based on flags. This flags do an additional filter based on the flags assigned in permissions groups;
- add a method to filters users by permissions groups flags in `UserRepository`

Version 1.5.3
=============

- fix error when interval is hour only

Version 1.5.4
=============

- layout of page "list exports"
- create function "SIMILARITY" (see [posgtgresql documentation](https://www.postgresql.org/docs/9.6/static/pgtrgm.html)) 
- create function "OVERLAPSI", which will detect period of date overlapping, replacing NULL date by infinity or -infinity (see [postgresql page for date time function and operators](https://www.postgresql.org/docs/9.6/static/functions-datetime.html))
- add repository for Center class

Version 1.5.5
=============

- add margin of 0.5rem beyond buttons ;
- add a spreadsheet formatter (format xlsx, ods, csv) for lists
- add possibility to generate DirectExport: exports without formatters, filters and aggregators ;
- add api for grouping centers ;
- select centers as grouped on step "pick centers" in exports ;

Version 1.5.6
=============

- fix long url in report download. The exports parameters are now stored in redis.
- add an option to allow address to be empty if street or postcode is not set. Used for embedding address in another form, when address is not required.

Version 1.5.7
=============

- insert the title of the export inside the "download" page ;
- add twig helper "print_or_message" ;
- add twig helper for routing ;
- add css layout for boxes ;
- collect supplementary query parameters in SearchController ;

Version 1.5.8
=============

- allow to remove interval units from DateInterval

Version 1.5.9
=============

- add optionnal impersonate feature (if firewall option switch_user is true) ;

Version 1.5.10
==============

- allow to group export in UI

Version 1.5.11
==============

- improve return path functions and filters ;

Version 1.5.12 
==============

- make the redirection to admin temporarily: some admin experienced cache problems (403 error)  when they switched from one admin account to a non-admin one ;

Version 1.5.13
==============

- allow to customize logo on login screen and main layout ;
- remove desert background image on page, handle it from cache in login screen; 

Version 1.5.14
==============

- fix errors in pagination
- fix search: usage of parenthesis
- add DQL function REPLACE for replacing in strings: "REPLACE(string, 'from', 'to')"
- add function to format phonenumber
- improve `chill_print_or_message` to support date time;
- add a module `show_hide` for javascript;
- load assets using functions ; 
- load a `runtime.js` assets for objects shared by webpack ;

Version 1.5.15
==============

- create an api for rendering entities
- css: render the placeholder in expanded choice item as italic (the "no specified" choice")
- css: add an extra space around choices expanded widget
- add Tabs parametric feature to easily render tabs panels
- css: add a margin on the button "delete entry" in collection
- module `show_hide`: add the possibility to launch a show hide manually and not on page loading. Useful when show/hide occurs in collection.
- module `show_hide`: add events to module
- [phonenumber validation] allow to validate against mobile **or** landline/voip phonenumbers;
- [phonenumber validation & format] format and validation does not make the app fail when network is not available;

Version 1.5.16
==============

- [translation] in french, replace "Modifier" by "Enregistrer" in the edit form
- [entity render] do not throw an exception when null element are passed to `chill_entity_render_box` and `chill_entity_render_string`

Version 1.5.17
==============

- [chill entity render] fix error when fallback to default entity render (usage of `__toString()`)
- [CRUD] add step delete
- [CRUD] check that action exists before inserting them in edit and view template
- [CRUD] fix error when no crud are created

Version 1.5.18
==============

- [webpack] add namespace for import sass ;
- [activity] move activity.scss to own bundle ;

Version 1.5.19
==============

- [address] add a "homeless" characteristic to addresses ;

Version 1.5.20
==============

- [CRUD] make index query more abstract.

    Improve build and count query in default index action to be customized 
    in one dedicated method.

Version 1.5.21
==============

- [Export list] improve alignment of last line
- [CRUD] Forward query parameters when pushing button "save and new" in "create" page;
- [Show/hide] Take selects input into account;

Version 1.5.23
==============

- [address] allow to add custom fields to addresses

Version 1.5.24
==============

- [bugfix] add missing migration files

