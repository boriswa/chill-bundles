<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Security\Authorization;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

interface VoterHelperInterface
{
    public function supports($attribute, $subject): bool;

    public function voteOnAttribute($attribute, $subject, TokenInterface $token): bool;
}
