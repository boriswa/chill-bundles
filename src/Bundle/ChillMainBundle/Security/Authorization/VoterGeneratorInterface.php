<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Security\Authorization;

interface VoterGeneratorInterface
{
    /**
     * @param string $class      The FQDN of a class
     * @param array  $attributes an array of attributes
     *
     * @return $this
     */
    public function addCheckFor(?string $class, array $attributes): self;

    public function build(): VoterHelperInterface;
}
