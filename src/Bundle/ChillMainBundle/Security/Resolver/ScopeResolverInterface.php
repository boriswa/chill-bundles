<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Security\Resolver;

use Chill\MainBundle\Entity\Scope;

/**
 * Interface to implement to define a ScopeResolver.
 */
interface ScopeResolverInterface
{
    /**
     * get the default priority for this resolver. Resolver with an higher priority will be
     * queried first.
     */
    public static function getDefaultPriority(): int;

    /**
     * Return true if the entity is concerned by scope, false otherwise.
     */
    public function isConcerned($entity, ?array $options = []): bool;

    /**
     * Will return the scope for the entity.
     *
     * @return array|Scope|Scope[]
     */
    public function resolveScope($entity, ?array $options = []);

    /**
     * Return true if this resolve is able to decide "something" on this entity.
     */
    public function supports($entity, ?array $options = []): bool;
}
