<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating\Listing;

use Chill\MainBundle\Pagination\PaginatorFactory;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class Templating extends AbstractExtension
{
    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly FilterOrderGetActiveFilterHelper $filterOrderGetActiveFilterHelper,
    ) {
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('chill_render_filter_order_helper', $this->renderFilterOrderHelper(...), [
                'needs_environment' => true, 'is_safe' => ['html'],
            ]),
        ];
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function renderFilterOrderHelper(
        Environment $environment,
        FilterOrderHelper $helper,
        ?string $template = '@ChillMain/FilterOrder/base.html.twig',
        ?array $options = []
    ): string {
        $otherParameters = [];

        foreach ($this->requestStack->getCurrentRequest()->query->getIterator() as $key => $value) {
            switch ($key) {
                case FilterOrderHelper::FORM_NAME:
                    break;

                case PaginatorFactory::DEFAULT_CURRENT_PAGE_KEY:
                    // when filtering, go back to page 1
                    $otherParameters[PaginatorFactory::DEFAULT_CURRENT_PAGE_KEY] = 1;

                    break;
                default:
                    $otherParameters[$key] = $value;

                    break;
            }
        }

        return $environment->render($template, [
            'helper' => $helper,
            'active' => $this->filterOrderGetActiveFilterHelper->getActiveFilters($helper),
            'form' => $helper->buildForm()->createView(),
            'options' => $options,
            'otherParameters' => $otherParameters,
        ]);
    }
}
