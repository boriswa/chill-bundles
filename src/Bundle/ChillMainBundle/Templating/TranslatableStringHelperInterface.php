<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating;

interface TranslatableStringHelperInterface
{
    /**
     * Return the string in current locale if it exists.
     *
     * If it does not exists; return the name in the first language available.
     *
     * Return a blank string if any strings are available.
     */
    public function localize(array $translatableStrings): ?string;
}
