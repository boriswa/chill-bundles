<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating\Widget;

use Twig\Environment;

interface WidgetInterface
{
    public function render(Environment $env, $place, array $context, array $config);
}
