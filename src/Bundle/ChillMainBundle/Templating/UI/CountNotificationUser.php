<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating\UI;

use Chill\MainBundle\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Show a number of notification to user in the upper right corner.
 */
class CountNotificationUser
{
    /**
     * @var NotificationCounterInterface[]
     */
    protected $counters = [];

    public function addNotificationCounter(NotificationCounterInterface $counter)
    {
        $this->counters[] = $counter;
    }

    public function getSumNotification(UserInterface $u): int
    {
        $sum = 0;

        foreach ($this->counters as $counter) {
            $sum += $counter->addNotification($u);
        }

        return $sum;
    }
}
