<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating\Entity;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @implements ChillEntityRenderInterface<User>
 */
class UserRender implements ChillEntityRenderInterface
{
    final public const DEFAULT_OPTIONS = [
        'main_scope' => true,
        'user_job' => true,
        'absence' => true,
        'at' => null,
    ];

    public function __construct(private readonly TranslatableStringHelper $translatableStringHelper, private readonly \Twig\Environment $engine, private readonly TranslatorInterface $translator)
    {
    }

    public function renderBox($entity, array $options): string
    {
        $opts = \array_merge(self::DEFAULT_OPTIONS, $options);

        return $this->engine->render('@ChillMain/Entity/user.html.twig', [
            'user' => $entity,
            'opts' => $opts,
        ]);
    }

    public function renderString($entity, array $options): string
    {
        $opts = \array_merge(self::DEFAULT_OPTIONS, $options);

        $str = $entity->getLabel();

        if (null !== $entity->getUserJob($opts['at']) && $opts['user_job']) {
            $str .= ' ('.$this->translatableStringHelper
                ->localize($entity->getUserJob($opts['at'])->getLabel()).')';
        }

        if (null !== $entity->getMainScope($opts['at']) && $opts['main_scope']) {
            $str .= ' ('.$this->translatableStringHelper
                ->localize($entity->getMainScope($opts['at'])->getName()).')';
        }

        if ($entity->isAbsent() && $opts['absence']) {
            $str .= ' ('.$this->translator->trans('absence.Absent').')';
        }

        return $str;
    }

    public function supports($entity, array $options): bool
    {
        return $entity instanceof User;
    }
}
