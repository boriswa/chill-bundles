<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating\Entity;

/**
 * Render an entity using `__toString()`.
 */
class ChillEntityRender implements ChillEntityRenderInterface
{
    use BoxUtilsChillEntityRenderTrait;

    public function renderBox($entity, array $options): string
    {
        return $this->getDefaultOpeningBox('default').$entity
            .$this->getDefaultClosingBox();
    }

    public function renderString($entity, array $options): string
    {
        return (string) $entity;
    }

    public function supports($entity, array $options): bool
    {
        return true;
    }
}
