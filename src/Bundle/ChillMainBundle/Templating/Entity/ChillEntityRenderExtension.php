<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating\Entity;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class ChillEntityRenderExtension.
 */
class ChillEntityRenderExtension extends AbstractExtension
{
    /**
     * @var ChillEntityRender
     */
    protected $defaultRender;

    /**
     * @var iterable|ChillEntityRenderInterface[]
     */
    protected $renders = [];

    /**
     * ChillEntityRenderExtension constructor.
     */
    public function __construct(iterable $renders)
    {
        $this->defaultRender = new ChillEntityRender();
        $this->renders = $renders;
    }

    /**
     * @return array|TwigFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('chill_entity_render_string', $this->renderString(...), [
                'is_safe' => ['html'],
            ]),
            new TwigFilter('chill_entity_render_box', $this->renderBox(...), [
                'is_safe' => ['html'],
            ]),
        ];
    }

    public function renderBox($entity, array $options = []): string
    {
        if (null === $entity) {
            return '';
        }

        return $this->getRender($entity, $options)
            ->renderBox($entity, $options);
    }

    public function renderString($entity, array $options = []): string
    {
        if (null === $entity) {
            return '';
        }

        return $this->getRender($entity, $options)
            ->renderString($entity, $options);
    }

    protected function getRender($entity, $options): ?ChillEntityRenderInterface
    {
        foreach ($this->renders as $render) {
            if ($render->supports($entity, $options)) {
                return $render;
            }
        }

        return $this->defaultRender;
    }
}
