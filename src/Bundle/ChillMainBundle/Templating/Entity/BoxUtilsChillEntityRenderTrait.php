<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating\Entity;

trait BoxUtilsChillEntityRenderTrait
{
    protected function getDefaultClosingBox(): string
    {
        return '</section>';
    }

    protected function getDefaultOpeningBox($classSuffix): string
    {
        return '<section class="chill-entity entity-'.$classSuffix.'">';
    }
}
