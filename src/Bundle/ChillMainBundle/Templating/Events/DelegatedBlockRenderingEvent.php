<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating\Events;

use Symfony\Component\EventDispatcher\Event;

/**
 * This event is transmitted on event chill_block.*.
 *
 * You may access to the context as an array :
 *
 * ```
 * $var = $event['context_key']
 * ```
 *
 * The documentation for the bundle where the event is launched should give
 * you the context keys.
 *
 * The keys are read-only: if you try to update the context using array access
 * (example, using `$event['context_key'] = $bar;`, an error will be thrown.
 */
class DelegatedBlockRenderingEvent extends \Symfony\Contracts\EventDispatcher\Event implements \ArrayAccess
{
    /**
     * The returned content of the event.
     *
     * @var string
     */
    protected $content = '';

    /**
     * @var mixed[]
     */
    protected $context;

    public function __construct(array $context)
    {
        $this->context = $context;
    }

    /**
     * add content to the event. This content will be printed in the
     * layout which launched the event.
     *
     * @param string $text
     */
    public function addContent($text)
    {
        $this->content .= $text;
    }

    /**
     * the content of the event.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    public function offsetExists($offset): bool
    {
        return isset($this->context[$offset]);
    }

    public function offsetGet($offset): mixed
    {
        return $this->context[$offset];
    }

    public function offsetSet($offset, $value): never
    {
        throw new \RuntimeException('The event context is read-only, you are not allowed to update it.');
    }

    public function offsetUnset($offset): never
    {
        throw new \RuntimeException('The event context is read-only, you are not allowed to update it.');
    }
}
