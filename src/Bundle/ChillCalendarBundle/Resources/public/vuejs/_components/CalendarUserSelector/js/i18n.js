import { multiSelectMessages } from 'ChillMainAssets/vuejs/_js/i18n'

const calendarUserSelectorMessages = {
  fr: {
    choose_your_calendar_user: "Afficher les plages de disponibilités",
    select_user: "Sélectionnez des calendriers",
    show_my_calendar: "Afficher mon calendrier",
    show_weekends: "Afficher les week-ends"
  }
};
 
Object.assign(calendarUserSelectorMessages.fr, multiSelectMessages.fr);

export {
  calendarUserSelectorMessages
};
 