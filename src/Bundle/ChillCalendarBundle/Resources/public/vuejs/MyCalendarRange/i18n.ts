const appMessages = {
   fr: {
      created_availabilities: "Lieu des plages de disponibilités créées",
      edit_your_calendar_range: "Planifiez vos plages de disponibilités",
      show_my_calendar: "Afficher mon calendrier",
      show_weekends: "Afficher les week-ends",
      copy_range: "Copier",
      copy_range_from_to: "Copier les plages d'un jour à l'autre",
      copy_range_to_next_day: "Copier les plages du jour au jour suivant",
      copy_range_from_day: "Copier les plages du ",
      to_the_next_day: " au jour suivant",
      copy_range_to_next_week: "Copier les plages de la semaine à la semaine suivante",
      copy_range_how_to: "Créez les plages de disponibilités durant une journée et copiez-les facilement au jour suivant avec ce bouton. Si les week-ends sont cachés, le jour suivant un vendredi sera le lundi.",
      new_range_to_save: "Nouvelles plages à enregistrer",
      update_range_to_save: "Plages à modifier",
      delete_range_to_save: "Plages à supprimer",
      by: "Par",
      main_user_concerned: "Utilisateur concerné",
      dateFrom: "De",
      dateTo: "à",
      day: "Jour",
      week: "Semaine",
      month: "Mois",
      today: "Aujourd'hui",
   }
}

export {
   appMessages
};
