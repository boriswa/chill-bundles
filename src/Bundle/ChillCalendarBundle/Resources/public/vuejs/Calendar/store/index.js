import 'es6-promise/auto';
import { createStore } from 'vuex';
import { postLocation } from 'ChillActivityAssets/vuejs/Activity/api';
import getters from './getters';
import actions from './actions';
import mutations from './mutations';
import { mapEntity } from './utils';
import { whoami } from '../api';
import prepareLocations from "ChillActivityAssets/vuejs/Activity/store.locations";

const debug = process.env.NODE_ENV !== 'production';

const store = createStore({
   strict: debug,
   state: {
     activity: mapEntity(window.entity), // activity is the calendar entity actually
     currentEvent: null,
     availableLocations: [],
     /**
      * the current user
      */
     me: null,
     /**
      * store information about current view
      */
     currentView: {
       start: null,
       end: null,
       users: new Map(),
     },
     /**
      * store a list of existing event, to avoid storing them twice
      */
     existingEvents: new Set(),
     /**
      * store user data
      */
     usersData: new Map(),
   },
    getters,
    mutations,
    actions,
});

whoami().then(me => {
    store.commit('setWhoAmiI', me);
});

if (null !== store.getters.getMainUser) {
  store.commit('showUserOnCalendar', {ranges: true, remotes: true, user: store.getters.getMainUser});
}

for (let u of store.state.activity.users) {
  store.commit('showUserOnCalendar', {ranges: false, remotes: false, user: u});
}

prepareLocations(store);

export default store;
