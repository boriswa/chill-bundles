import {EventInput} from '@fullcalendar/core';
import {DateTime, Location, User, UserAssociatedInterface} from '../../../ChillMainBundle/Resources/public/types' ;
import {Person} from "../../../ChillPersonBundle/Resources/public/types";

export interface CalendarRange {
  id: number;
  endDate: DateTime;
  startDate: DateTime;
  user: User;
  location: Location;
  createdAt: DateTime;
  createdBy: User;
  updatedAt: DateTime;
  updatedBy: User;
}

export interface CalendarRangeCreate {
  user: UserAssociatedInterface;
  startDate: DateTime;
  endDate: DateTime;
  location: Location;
}

export interface CalendarRangeEdit {
  startDate?: DateTime,
  endDate?: DateTime
  location?: Location;
}

export interface Calendar {
  id: number;
}

export interface CalendarLight {
  id: number;
  endDate: DateTime;
  startDate: DateTime;
  mainUser: User;
  persons: Person[];
  status: "valid" | "moved" | "canceled";
}

export interface CalendarRemote {
  id: number;
  endDate: DateTime;
  startDate: DateTime;
  title: string;
  isAllDay: boolean;
}

export type EventInputCalendarRange = EventInput & {
  id: string,
  userId: number,
  userLabel: string,
  calendarRangeId: number,
  locationId: number,
  locationName: string,
  start: string,
  end: string,
  is: "range"
};

export function isEventInputCalendarRange(toBeDetermined: EventInputCalendarRange | EventInput): toBeDetermined is EventInputCalendarRange {
  return typeof toBeDetermined.is === "string" && toBeDetermined.is === "range";
}

export {};
