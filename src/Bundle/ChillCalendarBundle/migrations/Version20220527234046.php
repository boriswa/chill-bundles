<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Calendar;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220527234046 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_calendar.calendar ALTER COLUMN privateComment_comments DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_calendar.calendar ALTER COLUMN privateComment_comments SET DEFAULT NULL');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE chill_calendar.calendar SET privateComment_comments=\'{}\' WHERE privateComment_comments IS NULL');
        $this->addSql('ALTER TABLE chill_calendar.calendar ALTER COLUMN privateComment_comments SET NOT NULL');
        $this->addSql('ALTER TABLE chill_calendar.calendar ALTER COLUMN privateComment_comments SET DEFAULT \'{}\'');
    }
}
