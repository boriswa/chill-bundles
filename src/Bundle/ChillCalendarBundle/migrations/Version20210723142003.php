<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Calendar;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Alter startDate and endDate to datetimetz_immutable.
 */
final class Version20210723142003 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_calendar.calendar ALTER startDate TYPE DATE');
        $this->addSql('ALTER TABLE chill_calendar.calendar ALTER startDate DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_calendar.calendar ALTER endDate TYPE DATE');
        $this->addSql('ALTER TABLE chill_calendar.calendar ALTER endDate DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN chill_calendar.calendar.startdate IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_calendar.calendar.enddate IS \'(DC2Type:date_immutable)\'');
    }

    public function getDescription(): string
    {
        return 'Alter startDate and endDate to datetimetz_immutable';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_calendar.calendar ALTER startdate TYPE TIMESTAMP(0) WITH TIME ZONE');
        $this->addSql('ALTER TABLE chill_calendar.calendar ALTER startdate DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_calendar.calendar ALTER enddate TYPE TIMESTAMP(0) WITH TIME ZONE');
        $this->addSql('ALTER TABLE chill_calendar.calendar ALTER enddate DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN chill_calendar.calendar.startDate IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_calendar.calendar.endDate IS \'(DC2Type:datetimetz_immutable)\'');
    }
}
