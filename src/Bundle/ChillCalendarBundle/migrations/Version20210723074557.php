<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Calendar;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Adapt calendar entities.
 */
final class Version20210723074557 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('CREATE TABLE chill_calendar.calendar_to_non_professionals (calendar_id INT NOT NULL, person_id INT NOT NULL, PRIMARY KEY(calendar_id, person_id))');
        $this->addSql('CREATE INDEX idx_fadf2c77217bbb47 ON chill_calendar.calendar_to_non_professionals (person_id)');
        $this->addSql('CREATE INDEX idx_fadf2c77a40a2c8 ON chill_calendar.calendar_to_non_professionals (calendar_id)');
        $this->addSql('ALTER TABLE chill_calendar.calendar_to_non_professionals ADD CONSTRAINT fk_fadf2c77a40a2c8 FOREIGN KEY (calendar_id) REFERENCES chill_calendar.calendar (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.calendar_to_non_professionals ADD CONSTRAINT fk_fadf2c77217bbb47 FOREIGN KEY (person_id) REFERENCES chill_person_person (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.cancel_reason ALTER canceledBy TYPE JSON');
        $this->addSql('ALTER TABLE chill_calendar.cancel_reason ALTER canceledBy DROP DEFAULT');
    }

    public function getDescription(): string
    {
        return 'Adapt calendar entities';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP TABLE chill_calendar.calendar_to_non_professionals');
        $this->addSql('ALTER TABLE chill_calendar.cancel_reason ALTER canceledby TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE chill_calendar.cancel_reason ALTER canceledby DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN chill_calendar.cancel_reason.canceledBy IS NULL');
    }
}
