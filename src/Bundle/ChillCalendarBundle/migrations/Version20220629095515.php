<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Calendar;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220629095515 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('alter table chill_calendar.calendar_range DROP COLUMN location_id');
    }

    public function getDescription(): string
    {
        return 'Add location on calendar range';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_calendar.calendar_range ADD location_id INT DEFAULT NULL');
        $this->addSql('UPDATE chill_calendar.calendar_range SET location_id = n.min FROM (SELECT min(id) FROM public.chill_main_location) AS n');
        $this->addSql('ALTER TABLE chill_calendar.calendar_range ALTER COLUMN location_id SET NOT NULL');
        $this->addSql('ALTER TABLE chill_calendar.calendar_range ADD CONSTRAINT FK_38D57D0564D218E FOREIGN KEY (location_id) REFERENCES chill_main_location (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_38D57D0564D218E ON chill_calendar.calendar_range (location_id)');
    }
}
