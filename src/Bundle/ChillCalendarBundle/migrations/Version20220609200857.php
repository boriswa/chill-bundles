<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Calendar;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220609200857 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX chill_calendar.idx_calendar_range_remote');
        $this->addSql('CREATE INDEX idx_calendar_range_remote ON chill_calendar.calendar_range (remoteId)');
        $this->addSql('DROP INDEX chill_calendar.idx_calendar_remote');
        $this->addSql('CREATE INDEX idx_calendar_remote ON chill_calendar.calendar (remoteId)');
        $this->addSql('DROP INDEX chill_calendar.idx_calendar_invite_remote');
        $this->addSql('CREATE INDEX idx_calendar_invite_remote ON chill_calendar.invite (remoteId)');
    }

    public function getDescription(): string
    {
        return 'Set an unique contraint on remoteId on calendar object which are synced to a remote';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX chill_calendar.idx_calendar_range_remote');
        $this->addSql('CREATE UNIQUE INDEX idx_calendar_range_remote ON chill_calendar.calendar_range (remoteId) WHERE remoteId <> \'\'');
        $this->addSql('DROP INDEX chill_calendar.idx_calendar_remote');
        $this->addSql('CREATE UNIQUE INDEX idx_calendar_remote ON chill_calendar.calendar (remoteId) WHERE remoteId <> \'\'');
        $this->addSql('DROP INDEX chill_calendar.idx_calendar_invite_remote');
        $this->addSql('CREATE UNIQUE INDEX idx_calendar_invite_remote ON chill_calendar.invite (remoteId) WHERE remoteId <> \'\'');
    }
}
