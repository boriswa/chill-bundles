<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\RemoteCalendar\Connector\MSGraph;

use Chill\MainBundle\Entity\User;

interface MSUserAbsenceReaderInterface
{
    /**
     * @throw UserAbsenceSyncException when the data cannot be reached or is not valid from microsoft
     */
    public function isUserAbsent(User $user): ?bool;
}
