<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Repository;

use Chill\CalendarBundle\Entity\Calendar;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;

interface CalendarACLAwareRepositoryInterface
{
    public function countByAccompanyingPeriod(AccompanyingPeriod $period, ?\DateTimeImmutable $startDate, ?\DateTimeImmutable $endDate): int;

    /**
     * Return the number or calendars associated with a person. See condition on @see{self::findByPerson}.
     */
    public function countByPerson(Person $person, ?\DateTimeImmutable $startDate, ?\DateTimeImmutable $endDate): int;

    /**
     * Return the number or calendars associated with an accompanyign period which **does not** match the date conditions.
     */
    public function countIgnoredByAccompanyingPeriod(AccompanyingPeriod $period, ?\DateTimeImmutable $startDate, ?\DateTimeImmutable $endDate): int;

    /**
     * Return the number or calendars associated with a person which **does not** match the date conditions.
     *
     * See condition on @see{self::findByPerson}.
     */
    public function countIgnoredByPerson(Person $person, ?\DateTimeImmutable $startDate, ?\DateTimeImmutable $endDate): int;

    /**
     * @return array|Calendar[]
     */
    public function findByAccompanyingPeriod(AccompanyingPeriod $period, ?\DateTimeImmutable $startDate, ?\DateTimeImmutable $endDate, ?array $orderBy = [], ?int $offset = null, ?int $limit = null): array;

    /**
     * Return all the calendars which are associated with a person, either on @see{Calendar::person} or within.
     *
     * @see{Calendar::persons}. The calendar may be associated with a person, or an accompanyingPeriod.
     *
     * The method may assume that the user is allowed to see the person, but must check that the user is allowed
     * to see the calendar's accompanyingPeriod, if any.
     *
     * @return array|Calendar[]
     */
    public function findByPerson(Person $person, ?\DateTimeImmutable $startDate, ?\DateTimeImmutable $endDate, ?array $orderBy = [], ?int $offset = null, ?int $limit = null): array;
}
