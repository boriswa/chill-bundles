<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Repository;

use Chill\CalendarBundle\Entity\Calendar;
use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ObjectRepository;

class CalendarRepository implements ObjectRepository
{
    private readonly EntityManagerInterface $em;

    private readonly EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Calendar::class);
        $this->em = $entityManager;
    }

    public function countByAccompanyingPeriod(AccompanyingPeriod $period): int
    {
        return $this->repository->count(['accompanyingPeriod' => $period]);
    }

    public function countByUser(User $user, \DateTimeImmutable $from, \DateTimeImmutable $to): int
    {
        return $this->buildQueryByUser($user, $from, $to)
            ->select('COUNT(c)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function createQueryBuilder(string $alias, ?string $indexBy = null): QueryBuilder
    {
        return $this->repository->createQueryBuilder($alias, $indexBy);
    }

    public function find($id): ?Calendar
    {
        return $this->repository->find($id);
    }

    /**
     * @return array|Calendar[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @return array|Calendar[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @return array|Calendar[]
     */
    public function findByAccompanyingPeriod(AccompanyingPeriod $period, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->findBy(
            [
                'accompanyingPeriod' => $period,
            ],
            $orderBy,
            $limit,
            $orderBy
        );
    }

    public function findByNotificationAvailable(\DateTimeImmutable $startDate, \DateTimeImmutable $endDate, ?int $limit = null, ?int $offset = null): array
    {
        $qb = $this->queryByNotificationAvailable($startDate, $endDate)->select('c');

        if (null !== $limit) {
            $qb->setMaxResults($limit);
        }

        if (null !== $offset) {
            $qb->setFirstResult($offset);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return array|Calendar[]
     */
    public function findByUser(User $user, \DateTimeImmutable $from, \DateTimeImmutable $to, ?int $limit = null, ?int $offset = null): array
    {
        $qb = $this->buildQueryByUser($user, $from, $to)->select('c');

        if (null !== $limit) {
            $qb->setMaxResults($limit);
        }

        if (null !== $offset) {
            $qb->setFirstResult($offset);
        }

        return $qb->getQuery()->getResult();
    }

    public function findOneBy(array $criteria): ?Calendar
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * Given a list of remote ids, return an array where
     * keys are the remoteIds, and value is a boolean, true if the
     * id is present in database.
     *
     * @param array<int, string>|list<string> $remoteIds
     *
     * @return array<string, bool>
     */
    public function findRemoteIdsPresent(array $remoteIds): array
    {
        if (0 === \count($remoteIds)) {
            return [];
        }

        $remoteIdsStr = implode(
            ', ',
            array_fill(0, \count($remoteIds), '((?))')
        );

        $sql = "SELECT
            sq.remoteId as remoteid,
            EXISTS (SELECT 1 FROM chill_calendar.calendar c WHERE c.remoteId = sq.remoteId) AS present
            FROM
                (
                    VALUES {$remoteIdsStr}
                ) AS sq(remoteId);
            ";

        $rsm = new ResultSetMapping();
        $rsm
            ->addScalarResult('remoteid', 'remoteId', Types::STRING)
            ->addScalarResult('present', 'present', Types::BOOLEAN);

        $rows = $this->em
            ->createNativeQuery(
                $sql,
                $rsm
            )
            ->setParameters(array_values($remoteIds))
            ->getResult();

        $results = [];

        foreach ($rows as $r) {
            $results[$r['remoteId']] = $r['present'];
        }

        return $results;
    }

    public function getClassName()
    {
        return Calendar::class;
    }

    private function buildQueryByUser(User $user, \DateTimeImmutable $from, \DateTimeImmutable $to): QueryBuilder
    {
        $qb = $this->repository->createQueryBuilder('c');

        return $qb
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('c.mainUser', ':user'),
                    $qb->expr()->gte('c.startDate', ':startDate'),
                    $qb->expr()->lte('c.endDate', ':endDate'),
                )
            )
            ->setParameters([
                'user' => $user,
                'startDate' => $from,
                'endDate' => $to,
            ]);
    }

    private function queryByNotificationAvailable(\DateTimeImmutable $startDate, \DateTimeImmutable $endDate): QueryBuilder
    {
        $qb = $this->repository->createQueryBuilder('c');

        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq('c.sendSMS', ':true'),
                $qb->expr()->gte('c.startDate', ':startDate'),
                $qb->expr()->lt('c.startDate', ':endDate'),
                $qb->expr()->orX(
                    $qb->expr()->eq('c.smsStatus', ':pending'),
                    $qb->expr()->eq('c.smsStatus', ':cancel_pending')
                )
            )
        );

        $qb->setParameters([
            'true' => true,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'pending' => Calendar::SMS_PENDING,
            'cancel_pending' => Calendar::SMS_CANCEL_PENDING,
        ]);

        return $qb;
    }
}
