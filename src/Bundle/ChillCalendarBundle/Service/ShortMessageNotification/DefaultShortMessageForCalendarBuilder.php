<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Service\ShortMessageNotification;

use Chill\CalendarBundle\Entity\Calendar;
use Chill\MainBundle\Service\ShortMessage\ShortMessage;

class DefaultShortMessageForCalendarBuilder implements ShortMessageForCalendarBuilderInterface
{
    public function __construct(private readonly \Twig\Environment $engine)
    {
    }

    public function buildMessageForCalendar(Calendar $calendar): array
    {
        if (true !== $calendar->getSendSMS()) {
            return [];
        }

        $toUsers = [];

        foreach ($calendar->getPersons() as $person) {
            if (false === $person->getAcceptSMS() || null === $person->getAcceptSMS() || null === $person->getMobilenumber()) {
                continue;
            }

            if (Calendar::SMS_PENDING === $calendar->getSmsStatus()) {
                $toUsers[] = new ShortMessage(
                    $this->engine->render('@ChillCalendar/CalendarShortMessage/short_message.txt.twig', ['calendar' => $calendar]),
                    $person->getMobilenumber(),
                    ShortMessage::PRIORITY_LOW
                );
            } elseif (Calendar::SMS_CANCEL_PENDING === $calendar->getSmsStatus()) {
                $toUsers[] = new ShortMessage(
                    $this->engine->render('@ChillCalendar/CalendarShortMessage/short_message_canceled.txt.twig', ['calendar' => $calendar]),
                    $person->getMobilenumber(),
                    ShortMessage::PRIORITY_LOW
                );
            }
        }

        return $toUsers;
    }
}
