// this file loads all assets from the Chill calendar bundle
module.exports = function(encore, entries) {

    entries.push(__dirname + '/Resources/public/chill/chill.js');

    encore.addAliases({
        ChillCalendarAssets: __dirname + '/Resources/public'
    });

    encore.addEntry('vue_calendar', __dirname + '/Resources/public/vuejs/Calendar/index.js');
    encore.addEntry('vue_mycalendarrange', __dirname + '/Resources/public/vuejs/MyCalendarRange/index2.ts');
    encore.addEntry('page_calendar', __dirname + '/Resources/public/chill/index.js');
    encore.addEntry('mod_answer', __dirname + '/Resources/public/module/Invite/answer.js');
};
