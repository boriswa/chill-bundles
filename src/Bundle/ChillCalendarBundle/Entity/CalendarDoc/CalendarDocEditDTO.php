<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Entity\CalendarDoc;

use Chill\CalendarBundle\Entity\CalendarDoc;
use Chill\DocStoreBundle\Entity\StoredObject;
use Symfony\Component\Validator\Constraints as Assert;

class CalendarDocEditDTO
{
    /**
     * @Assert\Valid
     */
    public ?StoredObject $doc = null;

    /**
     * @Assert\NotBlank
     *
     * @Assert\NotNull
     */
    public ?string $title = '';

    public function __construct(CalendarDoc $calendarDoc)
    {
        $this->title = $calendarDoc->getStoredObject()->getTitle();
    }
}
