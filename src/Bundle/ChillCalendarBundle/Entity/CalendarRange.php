<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Entity;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateTrait;
use Chill\MainBundle\Entity\Location;
use Chill\MainBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(
 *     name="chill_calendar.calendar_range",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="idx_calendar_range_remote", columns={"remoteId"}, options={"where": "remoteId <> ''"})}
 * )
 *
 * @ORM\Entity
 */
class CalendarRange implements TrackCreationInterface, TrackUpdateInterface
{
    use RemoteCalendarTrait;

    use TrackCreationTrait;

    use TrackUpdateTrait;

    /**
     * @ORM\OneToOne(targetEntity=Calendar::class, mappedBy="calendarRange")
     */
    private ?Calendar $calendar = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=false)
     *
     * @Groups({"read", "write", "calendar:read"})
     *
     * @Assert\NotNull
     */
    private ?\DateTimeImmutable $endDate = null;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @Groups({"read"})
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity=Location::class)
     *
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({"read", "write", "calendar:read"})
     *
     * @Assert\NotNull
     */
    private ?Location $location = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=false)
     *
     * @groups({"read", "write", "calendar:read"})
     *
     * @Assert\NotNull
     */
    private ?\DateTimeImmutable $startDate = null;

    /**
     * @ORM\ManyToOne(targetEntity="Chill\MainBundle\Entity\User")
     *
     * @Groups({"read", "write", "calendar:read"})
     *
     * @Assert\NotNull
     */
    private ?User $user = null;

    public function getCalendar(): ?Calendar
    {
        return $this->calendar;
    }

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function getStartDate(): ?\DateTimeImmutable
    {
        return $this->startDate;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @internal use {@link (Calendar::setCalendarRange)} instead
     */
    public function setCalendar(?Calendar $calendar): void
    {
        $this->calendar = $calendar;
    }

    public function setEndDate(\DateTimeImmutable $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function setLocation(?Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function setStartDate(\DateTimeImmutable $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
