<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Form\DataTransformer;

use Chill\CalendarBundle\Repository\CalendarRangeRepository;
use Chill\MainBundle\Form\DataTransformer\IdToEntityDataTransformer;

class IdToCalendarRangeDataTransformer extends IdToEntityDataTransformer
{
    public function __construct(CalendarRangeRepository $repository)
    {
        parent::__construct($repository, false);
    }
}
