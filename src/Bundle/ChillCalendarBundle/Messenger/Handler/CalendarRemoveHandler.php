<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Messenger\Handler;

use Chill\CalendarBundle\Messenger\Message\CalendarRemovedMessage;
use Chill\CalendarBundle\RemoteCalendar\Connector\RemoteCalendarConnectorInterface;
use Chill\CalendarBundle\Repository\CalendarRangeRepository;
use Chill\MainBundle\Repository\UserRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Handle the deletion of calendar.
 *
 * @AsMessageHandler
 */
class CalendarRemoveHandler implements MessageHandlerInterface
{
    public function __construct(private readonly RemoteCalendarConnectorInterface $remoteCalendarConnector, private readonly CalendarRangeRepository $calendarRangeRepository, private readonly UserRepositoryInterface $userRepository)
    {
    }

    public function __invoke(CalendarRemovedMessage $message)
    {
        if (null !== $message->getAssociatedCalendarRangeId()) {
            $associatedRange = $this->calendarRangeRepository->find($message->getAssociatedCalendarRangeId());
        } else {
            $associatedRange = null;
        }

        $this->remoteCalendarConnector->removeCalendar(
            $message->getRemoteId(),
            $message->getRemoteAttributes(),
            $this->userRepository->find($message->getCalendarUserId()),
            $associatedRange
        );
    }
}
