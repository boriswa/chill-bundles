<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Messenger\Message;

use Chill\CalendarBundle\Entity\CalendarRange;
use Chill\MainBundle\Entity\User;

class CalendarRangeMessage
{
    final public const CALENDAR_RANGE_PERSIST = 'CHILL_CALENDAR_CALENDAR_RANGE_PERSIST';

    final public const CALENDAR_RANGE_UPDATE = 'CHILL_CALENDAR_CALENDAR_RANGE_UPDATE';

    private ?int $byUserId = null;

    private readonly int $calendarRangeId;

    public function __construct(CalendarRange $calendarRange, private readonly string $action, ?User $byUser)
    {
        $this->calendarRangeId = $calendarRange->getId();

        if (null !== $byUser) {
            $this->byUserId = $byUser->getId();
        }
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function getByUserId(): ?int
    {
        return $this->byUserId;
    }

    public function getCalendarRangeId(): ?int
    {
        return $this->calendarRangeId;
    }
}
