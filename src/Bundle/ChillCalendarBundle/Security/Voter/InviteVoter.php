<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Security\Voter;

use Chill\CalendarBundle\Entity\Invite;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class InviteVoter extends Voter
{
    final public const ANSWER = 'CHILL_CALENDAR_INVITE_ANSWER';

    protected function supports($attribute, $subject): bool
    {
        return $subject instanceof Invite && self::ANSWER === $attribute;
    }

    /**
     * @param string $attribute
     * @param Invite $subject
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        return $token->getUser() === $subject->getUser();
    }
}
