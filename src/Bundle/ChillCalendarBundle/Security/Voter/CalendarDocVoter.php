<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Security\Voter;

use Chill\CalendarBundle\Entity\CalendarDoc;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class CalendarDocVoter extends Voter
{
    final public const EDIT = 'CHILL_CALENDAR_DOC_EDIT';

    final public const SEE = 'CHILL_CALENDAR_DOC_SEE';

    private const ALL = [
        'CHILL_CALENDAR_DOC_EDIT',
        'CHILL_CALENDAR_DOC_SEE',
    ];

    public function __construct(private readonly Security $security)
    {
    }

    protected function supports($attribute, $subject): bool
    {
        return \in_array($attribute, self::ALL, true) && $subject instanceof CalendarDoc;
    }

    /**
     * @param CalendarDoc $subject
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        return match ($attribute) {
            self::EDIT => $this->security->isGranted(CalendarVoter::EDIT, $subject->getCalendar()),
            self::SEE => $this->security->isGranted(CalendarVoter::SEE, $subject->getCalendar()),
            default => throw new \UnexpectedValueException('Attribute not supported: '.$attribute),
        };
    }
}
