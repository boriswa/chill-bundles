<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Export\Aggregator;

use Chill\CalendarBundle\Export\Declarations;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Repository\LocationRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class LocationAggregator implements AggregatorInterface
{
    public function __construct(private LocationRepository $locationRepository)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('calloc', $qb->getAllAliases(), true)) {
            $qb->join('cal.location', 'calloc');
        }
        $qb->addSelect('IDENTITY(cal.location) as location_aggregator');
        $qb->addGroupBy('location_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::CALENDAR_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data): \Closure
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Location';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            $l = $this->locationRepository->find($value);

            return $l->getName();
        };
    }

    public function getQueryKeys($data): array
    {
        return ['location_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group calendars by location';
    }
}
