<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Tests\Export\Filter;

use Chill\CalendarBundle\Entity\Calendar;
use Chill\CalendarBundle\Export\Filter\JobFilter;
use Chill\MainBundle\Entity\UserJob;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class JobFilterTest extends AbstractFilterTest
{
    private JobFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        // add a fake request with a default locale (used in translatable string)
        $request = $this->prophesize();

        $request->willExtend(\Symfony\Component\HttpFoundation\Request::class);
        $request->getLocale()->willReturn('fr');

        $this->filter = self::$container->get('chill.calendar.export.job_filter');
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData(): iterable
    {
        self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        $array = $em->createQueryBuilder()
            ->from(UserJob::class, 'uj')
            ->select('uj')
            ->getQuery()
            ->setMaxResults(1)
            ->getResult();

        return [
            [
                'job' => new ArrayCollection($array),
            ],
        ];
    }

    public function getQueryBuilders(): array
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('cal.id')
                ->from(Calendar::class, 'cal'),
        ];
    }
}
