<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Tests\Export\Filter;

use Chill\CalendarBundle\Entity\Calendar;
use Chill\CalendarBundle\Export\Filter\ScopeFilter;
use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class ScopeFilterTest extends AbstractFilterTest
{
    private ScopeFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        // add a fake request with a default locale (used in translatable string)
        $request = $this->prophesize();

        $request->willExtend(\Symfony\Component\HttpFoundation\Request::class);
        $request->getLocale()->willReturn('fr');

        $this->filter = self::$container->get('chill.calendar.export.scope_filter');
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData(): array
    {
        self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        $array = $em->createQueryBuilder()
            ->from(Scope::class, 's')
            ->select('s')
            ->getQuery()
            ->setMaxResults(1)
            ->getResult();

        return [
            [
                'scope' => new ArrayCollection($array),
            ],
        ];
    }

    public function getQueryBuilders(): array
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('cal.id')
                ->from(Calendar::class, 'cal'),
        ];
    }
}
