<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Tests\Entity;

use Chill\CalendarBundle\Entity\Calendar;
use Chill\MainBundle\Entity\User;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class CalendarTest extends TestCase
{
    public function testAddUser()
    {
        $calendar = new Calendar();

        $this->assertCount(0, $calendar->getInvites());
        $this->assertCount(0, $calendar->getUsers());

        $calendar->addUser($user0 = new User());

        $this->assertCount(1, $calendar->getInvites());
        $this->assertCount(1, $calendar->getUsers());
        $this->assertSame($user0, $calendar->getUsers()->first());

        $calendar->addUser($user1 = new User());

        $this->assertCount(2, $calendar->getInvites());
        $this->assertCount(2, $calendar->getUsers());
        $this->assertContains($user0, $calendar->getUsers());
        $this->assertContains($user1, $calendar->getUsers());

        $calendar->removeUser($user0);

        $this->assertCount(1, $calendar->getInvites());
        $this->assertCount(1, $calendar->getUsers());
        $this->assertNotSame($user0, $calendar->getUsers()->first());
        $this->assertSame($user1, $calendar->getUsers()->first());

        $calendar->removeUser($user1);

        $this->assertCount(0, $calendar->getInvites());
        $this->assertCount(0, $calendar->getUsers());
    }
}
