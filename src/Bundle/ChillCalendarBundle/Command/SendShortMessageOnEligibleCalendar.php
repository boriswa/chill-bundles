<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Command;

use Chill\CalendarBundle\Service\ShortMessageNotification\BulkCalendarShortMessageSender;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendShortMessageOnEligibleCalendar extends Command
{
    public function __construct(private readonly BulkCalendarShortMessageSender $messageSender)
    {
        parent::__construct();
    }

    public function getName()
    {
        return 'chill:calendar:send-short-messages';
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->messageSender->sendBulkMessageToEligibleCalendars();

        return 0;
    }
}
