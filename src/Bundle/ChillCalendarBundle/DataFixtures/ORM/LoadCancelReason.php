<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\DataFixtures\ORM;

use Chill\CalendarBundle\Entity\CancelReason;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class LoadCancelReason extends Fixture implements FixtureGroupInterface
{
    public static $references = [];

    public static function getGroups(): array
    {
        return ['calendar'];
    }

    public function getOrder(): int
    {
        return 40001;
    }

    public function load(ObjectManager $manager): void
    {
        $arr = [
            ['name' => CancelReason::CANCELEDBY_USER],
            ['name' => CancelReason::CANCELEDBY_PERSON],
            ['name' => CancelReason::CANCELEDBY_DONOTCOUNT],
        ];

        foreach ($arr as $a) {
            echo 'Creating calendar cancel reason : '.$a['name']."\n";
            $cancelReason = (new CancelReason())
                ->setCanceledBy($a['name'])
                ->setActive(true);
            $manager->persist($cancelReason);
            $reference = 'CancelReason_'.$a['name'];
            $this->addReference($reference, $cancelReason);
            static::$references[] = $reference;
        }

        $manager->flush();
    }
}
