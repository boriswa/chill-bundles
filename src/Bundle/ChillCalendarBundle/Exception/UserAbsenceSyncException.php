<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Exception;

class UserAbsenceSyncException extends \LogicException
{
    public function __construct(string $message = '', int $code = 20_230_706, ?\Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
