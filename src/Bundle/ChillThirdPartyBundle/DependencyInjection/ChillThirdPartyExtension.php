<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdPartyBundle\DependencyInjection;

use Chill\ThirdPartyBundle\Controller\ThirdPartyCategoryController;
use Chill\ThirdPartyBundle\Controller\ThirdPartyController;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Chill\ThirdPartyBundle\Entity\ThirdPartyCategory;
use Chill\ThirdPartyBundle\Form\ThirdPartyCategoryType;
use Chill\ThirdPartyBundle\Form\ThirdPartyType;
use Chill\ThirdPartyBundle\Security\Voter\ThirdPartyVoter;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @see http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ChillThirdPartyExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../config'));
        $loader->load('services.yaml');
        $loader->load('services/controller.yaml');
        $loader->load('services/form.yaml');
        $loader->load('services/security.yaml');
        $loader->load('services/3partytype.yaml');
        $loader->load('services/search.yaml');
        $loader->load('services/templating.yaml');
        $loader->load('services/menu.yaml');
        $loader->load('services/fixtures.yaml');
        $loader->load('services/serializer.yaml');
        $loader->load('services/repository.yaml');
        $loader->load('services/doctrineEventListener.yaml');
    }

    public function prepend(ContainerBuilder $container)
    {
        $this->preprendRoutes($container);
        $this->prependRoleHierarchy($container);
    }

    protected function prependRoleHierarchy(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('security', [
            'role_hierarchy' => [
                ThirdPartyVoter::CREATE => [ThirdPartyVoter::SHOW],
                ThirdPartyVoter::UPDATE => [ThirdPartyVoter::SHOW],
            ],
        ]);
    }

    protected function preprendRoutes(ContainerBuilder $container)
    {
        // declare routes for 3party bundle
        $container->prependExtensionConfig('chill_main', [
            'routing' => [
                'resources' => [
                    '@ChillThirdPartyBundle/config/routes.yaml',
                ],
            ],
            'cruds' => [
                [
                    'class' => ThirdParty::class,
                    'controller' => ThirdPartyController::class,
                    'name' => '3party_3party',
                    'base_path' => '/3party/3party',
                    'form_class' => ThirdPartyType::class,
                    'actions' => [
                        'index' => [
                            'template' => '@ChillThirdParty/ThirdParty/index.html.twig',
                            'role' => ThirdPartyVoter::SHOW,
                        ],
                        'new' => [
                            'template' => '@ChillThirdParty/ThirdParty/new.html.twig',
                            'role' => ThirdPartyVoter::CREATE,
                        ],
                        'edit' => [
                            'template' => '@ChillThirdParty/ThirdParty/update.html.twig',
                            'role' => ThirdPartyVoter::UPDATE,
                        ],
                        'view' => [
                            'template' => '@ChillThirdParty/ThirdParty/view.html.twig',
                            'role' => ThirdPartyVoter::SHOW,
                        ],
                    ],
                ],
                [
                    'class' => ThirdPartyCategory::class,
                    'name' => 'thirdparty_thirdparty-category',
                    'base_path' => '/admin/thirdparty/thirdparty-category',
                    'form_class' => ThirdPartyCategoryType::class,
                    'controller' => ThirdPartyCategoryController::class,
                    'actions' => [
                        'index' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillThirdParty/ThirdPartyCategory/index.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillThirdParty/ThirdPartyCategory/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillThirdParty/ThirdPartyCategory/edit.html.twig',
                        ],
                    ],
                ],
            ],
            'apis' => [
                [
                    'class' => ThirdParty::class,
                    'name' => 'thirdparty',
                    'base_path' => '/api/1.0/thirdparty/thirdparty',
                    // 'base_role' => \Chill\ThirdPartyBundle\Security\Authorization\ThirdPartyVoter::SHOW,
                    // 'controller' => \Chill\ThirdPartyBundle\Controller\ThirdPartyApiController::class,
                    'actions' => [
                        '_entity' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                                Request::METHOD_POST => true,
                                Request::METHOD_PUT => true,
                                Request::METHOD_PATCH => true,
                            ],
                            'roles' => [
                                Request::METHOD_GET => ThirdPartyVoter::SHOW,
                                Request::METHOD_HEAD => ThirdPartyVoter::SHOW,
                                Request::METHOD_POST => ThirdPartyVoter::CREATE,
                                Request::METHOD_PUT => ThirdPartyVoter::CREATE,
                                Request::METHOD_PATCH => ThirdPartyVoter::CREATE,
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }
}
