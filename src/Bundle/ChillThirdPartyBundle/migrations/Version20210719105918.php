<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\ThirdParty;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add new fields to ThirdParty Entity.
 */
final class Version20210719105918 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_3party.thirdparty_category DROP CONSTRAINT FK_7049563712469DE2');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP CONSTRAINT FK_D952467B384D4799');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP CONSTRAINT FK_D952467BBA930D69');
        $this->addSql('DROP SEQUENCE chill_3party.party_category_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_3party.party_civility_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_3party.party_profession_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_3party.party_category');
        $this->addSql('DROP TABLE chill_3party.party_civility');
        $this->addSql('DROP TABLE chill_3party.party_profession');
        $this->addSql('DROP TABLE chill_3party.thirdparty_category');
        $this->addSql('ALTER INDEX chill_3party.idx_14dc44755932f377 RENAME TO idx_c65d43975932f377');
        $this->addSql('ALTER INDEX chill_3party.idx_14dc4475c7d3a8e6 RENAME TO idx_c65d4397c7d3a8e6');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP CONSTRAINT FK_D952467B727ACA70');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP CONSTRAINT FK_D952467B16FE72E1');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP parent_id');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP civility');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP profession');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP updated_by');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP name_company');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP acronym');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP created_at');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP updated_at');
    }

    public function getDescription(): string
    {
        return 'Add new fields to ThirdParty Entity + new join tables';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_3party.party_category_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_3party.party_civility_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_3party.party_profession_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_3party.party_category (id INT NOT NULL, name JSON NOT NULL, active BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE chill_3party.party_civility (id INT NOT NULL, name JSON NOT NULL, active BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE chill_3party.party_profession (id INT NOT NULL, name JSON NOT NULL, active BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE chill_3party.thirdparty_category (thirdparty_id INT NOT NULL, category_id INT NOT NULL, PRIMARY KEY(thirdparty_id, category_id))');
        $this->addSql('CREATE INDEX IDX_70495637C7D3A8E6 ON chill_3party.thirdparty_category (thirdparty_id)');
        $this->addSql('CREATE INDEX IDX_7049563712469DE2 ON chill_3party.thirdparty_category (category_id)');
        $this->addSql('ALTER TABLE chill_3party.thirdparty_category ADD CONSTRAINT FK_70495637C7D3A8E6 FOREIGN KEY (thirdparty_id) REFERENCES chill_3party.third_party (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_3party.thirdparty_category ADD CONSTRAINT FK_7049563712469DE2 FOREIGN KEY (category_id) REFERENCES chill_3party.party_category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD civility INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD profession INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD updated_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD name_company VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD acronym VARCHAR(64) DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT \'2015-01-01\' NOT NULL');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD CONSTRAINT FK_D952467B727ACA70 FOREIGN KEY (parent_id) REFERENCES chill_3party.third_party (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD CONSTRAINT FK_D952467B384D4799 FOREIGN KEY (civility) REFERENCES chill_3party.party_civility (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD CONSTRAINT FK_D952467BBA930D69 FOREIGN KEY (profession) REFERENCES chill_3party.party_profession (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD CONSTRAINT FK_D952467B16FE72E1 FOREIGN KEY (updated_by) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D952467B727ACA70 ON chill_3party.third_party (parent_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D952467B384D4799 ON chill_3party.third_party (civility)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D952467BBA930D69 ON chill_3party.third_party (profession)');
        $this->addSql('CREATE INDEX IDX_D952467B16FE72E1 ON chill_3party.third_party (updated_by)');
        $this->addSql('ALTER INDEX chill_3party.idx_c65d4397c7d3a8e6 RENAME TO IDX_14DC4475C7D3A8E6');
        $this->addSql('ALTER INDEX chill_3party.idx_c65d43975932f377 RENAME TO IDX_14DC44755932F377');
    }
}
