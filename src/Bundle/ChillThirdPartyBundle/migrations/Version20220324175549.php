<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\ThirdParty;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220324175549 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql("
            CREATE OR REPLACE FUNCTION chill_3party.canonicalize() RETURNS TRIGGER
                LANGUAGE plpgsql
            AS
                $$
                BEGIN
                    NEW.canonicalized =
                        UNACCENT(
                            LOWER(
                                NEW.name ||
                                CASE WHEN COALESCE(NEW.name_company, '') <> '' THEN ' ' ELSE '' END ||
                                COALESCE(NEW.name_company, '') ||
                                CASE WHEN COALESCE(NEW.acronym, '') <> '' THEN ' ' ELSE '' END ||
                                COALESCE(NEW.acronym, '')
                                )
                            )
                    ;

                    return NEW;
                END
                $$
        ");
        $this->addSql("
            UPDATE chill_3party.third_party
                SET canonicalized =
                    UNACCENT(
                            LOWER(
                                name ||
                                CASE WHEN COALESCE(name_company, '') <> '' THEN ' ' ELSE '' END ||
                                COALESCE(name_company, '') ||
                                CASE WHEN COALESCE(acronym, '') <> '' THEN ' ' ELSE '' END ||
                                COALESCE(acronym, '')
                                )
                        )
        ");
    }

    public function getDescription(): string
    {
        return 'indexing of firstname on third parties';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
            UPDATE chill_3party.third_party
                SET canonicalized =
                    UNACCENT(
                            LOWER(
                                name ||
                                CASE WHEN firstname <> '' THEN ' ' ELSE '' END ||
                                firstname ||
                                CASE WHEN COALESCE(name_company, '') <> '' THEN ' ' ELSE '' END ||
                                COALESCE(name_company, '') ||
                                CASE WHEN COALESCE(acronym, '') <> '' THEN ' ' ELSE '' END ||
                                COALESCE(acronym, '')
                                )
                        )
        ");
        $this->addSql("
            CREATE OR REPLACE FUNCTION chill_3party.canonicalize() RETURNS TRIGGER
                LANGUAGE plpgsql
            AS
                $$
                BEGIN
                    NEW.canonicalized =
                        UNACCENT(
                            LOWER(
                                NEW.name ||
                                CASE WHEN NEW.firstname <> '' THEN ' ' ELSE '' END ||
                                NEW.firstname ||
                                CASE WHEN COALESCE(NEW.name_company, '') <> '' THEN ' ' ELSE '' END ||
                                COALESCE(NEW.name_company, '') ||
                                CASE WHEN COALESCE(NEW.acronym, '') <> '' THEN ' ' ELSE '' END ||
                                COALESCE(NEW.acronym, '')
                                )
                            )
                    ;

                    return NEW;
                END
                $$
        ");
    }
}
