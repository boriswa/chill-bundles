<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\ThirdParty;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

final class Version20220302143821 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_3party.third_party ALTER telephone TYPE VARCHAR(64)');
        $this->addSql('ALTER TABLE chill_3party.third_party ALTER telephone DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN chill_3party.third_party.telephone IS NULL');
    }

    public function getDescription(): string
    {
        return 'Upgrade phonenumber on third parties';
    }

    public function up(Schema $schema): void
    {
        $carrier_code = $this->container
            ->getParameter('chill_main')['phone_helper']['default_carrier_code'];

        if (null === $carrier_code) {
            throw new \RuntimeException('no carrier code');
        }

        $this->addSql('ALTER TABLE chill_3party.third_party ALTER telephone TYPE VARCHAR(35)');
        $this->addSql('ALTER TABLE chill_3party.third_party ALTER telephone DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_3party.third_party ALTER telephone TYPE VARCHAR(35)');
        $this->addSql('COMMENT ON COLUMN chill_3party.third_party.telephone IS \'(DC2Type:phone_number)\'');

        $this->addSql(
            'UPDATE chill_3party.third_party SET '.
            $this->buildMigrationPhonenumberClause($carrier_code, 'telephone')
        );
    }

    private function buildMigrationPhoneNumberClause(string $defaultCarriercode, string $field): string
    {
        $util = PhoneNumberUtil::getInstance();

        $countryCode = $util->getCountryCodeForRegion($defaultCarriercode);

        return sprintf('%s=CASE
                WHEN %s = \'\' THEN NULL
                WHEN LEFT(%s, 1) = \'0\'
                    THEN \'+%s\' || replace(replace(substr(%s, 2), \'(0)\', \'\'), \' \', \'\')
                ELSE replace(replace(%s, \'(0)\', \'\'),\' \', \'\')
            END', $field, $field, $field, $countryCode, $field, $field);
    }
}
