<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdPartyBundle\Entity;

use Chill\ThirdPartyBundle\Repository\ThirdPartyCategoryRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="chill_3party.party_category")
 *
 * @ORM\Entity(repositoryClass=ThirdPartyCategoryRepository::class)
 */
class ThirdPartyCategory
{
    /**
     * @ORM\Column(type="boolean")
     */
    private bool $active = true;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"docgen:read"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="json")
     *
     * @Serializer\Groups({"docgen:read"})
     */
    private array $name = [];

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?array
    {
        return $this->name;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function setName(array $name): self
    {
        $this->name = $name;

        return $this;
    }
}
