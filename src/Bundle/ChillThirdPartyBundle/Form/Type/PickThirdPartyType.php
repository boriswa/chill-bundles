<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdPartyBundle\Form\Type;

use Chill\MainBundle\Search\SearchInterface;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Chill\ThirdPartyBundle\Form\ChoiceLoader\ThirdPartyChoiceLoader;
use Chill\ThirdPartyBundle\Search\ThirdPartySearch;
use Chill\ThirdPartyBundle\ThirdPartyType\ThirdPartyTypeManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @deprecated use the @see{PickThirdPartyDynamicType::class}
 *
 * @note do remove ThirdPartyChoiceLoader if this class is removed
 */
class PickThirdPartyType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var ThirdPartyTypeManager
     */
    protected $typesManager;

    /**
     * @var UrlGeneratorInterface
     */
    protected $urlGenerator;

    public function __construct(
        EntityManagerInterface $em,
        UrlGeneratorInterface $urlGenerator,
        TranslatorInterface $translator,
        ThirdPartyTypeManager $typesManager
    ) {
        $this->em = $em;
        $this->urlGenerator = $urlGenerator;
        $this->translator = $translator;
        $this->typesManager = $typesManager;
    }

    public function buildView(\Symfony\Component\Form\FormView $view, \Symfony\Component\Form\FormInterface $form, array $options)
    {
        $view->vars['attr']['class'] = \array_merge(['select2 '], $view->vars['attr']['class'] ?? []);
        $view->vars['attr']['data-3party-picker'] = true;
        $view->vars['attr']['data-select-interactive-loading'] = true;
        $view->vars['attr']['data-search-url'] = $this->urlGenerator
            ->generate(
                'chill_main_search',
                [
                    'name' => ThirdPartySearch::NAME,
                    SearchInterface::REQUEST_QUERY_KEY_ADD_PARAMETERS => ['t' => $options['types']],
                    '_format' => 'json', ]
            );
        $view->vars['attr']['data-placeholder'] = $this->translator->trans($options['placeholder']);
        $view->vars['attr']['data-no-results-label'] = $this->translator->trans('select2.no_results');
        $view->vars['attr']['data-error-load-label'] = $this->translator->trans('select2.error_loading');
        $view->vars['attr']['data-searching-label'] = $this->translator->trans('select2.searching');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired('center')
            ->setAllowedTypes('center', [\Chill\MainBundle\Entity\Center::class])
            ->setDefined('types')
            ->setRequired('types')
            ->setAllowedValues('types', function ($types) {
                if (false === \is_array($types)) {
                    return false;
                }

                // return false if one element is not contained in allowed types
                return 0 === \count(\array_diff($types, $this->typesManager->getTypes()));
            });

        $resolver
            ->setDefault('class', ThirdParty::class)
            ->setDefault('choice_label', static fn (ThirdParty $tp) => $tp->getName())
            ->setDefault('choice_loader', fn (Options $options) => new ThirdPartyChoiceLoader(
                $options['center'],
                $this->em->getRepository(ThirdParty::class)
            ));
    }

    public function getParent(): string
    {
        return EntityType::class;
    }
}
