ThirdParty Module
=================

This module add third parties to chill.

Those parties are elements which can be handled in other entities: people, contacts, etc.

Each third party is associated to multiple types, which can help to filter parties in element form. Exemple: do not show list of an hospital where you should display only doctors.

How to add a third party type
-----------------------------

Create a service which implements `Chill\ThirdPartyBundle\ThirdPartyType\ThirdPartyProviderInterface` and tag it with: `chill_3party.provider`:

```yaml
services:
    Cire\CireBundle\ThirdPartyType\NotarisType:
        tags:
            - { name: chill_3party.provider }
```


