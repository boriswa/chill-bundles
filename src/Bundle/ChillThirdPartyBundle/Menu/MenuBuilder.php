<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdPartyBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Chill\ThirdPartyBundle\Security\Voter\ThirdPartyVoter;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Add an entry in section to go to third party index page.
 */
class MenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        TranslatorInterface $translator
    ) {
        $this->authorizationChecker = $authorizationChecker;
        $this->translator = $translator;
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if ($this->authorizationChecker->isGranted(ThirdPartyVoter::SHOW)) {
            $menu
                ->addChild(
                    $this->translator->trans('Third parties'),
                    [
                        'route' => 'chill_crud_3party_3party_index',
                    ]
                )
                ->setExtras([
                    'order' => 112,
                ]);
        }
    }

    public static function getMenuIds(): array
    {
        return ['section'];
    }
}
