<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdParty\Tests\Entity;

use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Chill\ThirdPartyBundle\Entity\ThirdPartyCategory;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class ThirdPartyTest extends TestCase
{
    public function testAddingRemovingActivityTypes()
    {
        $tp = new ThirdParty();
        $cat1 = new ThirdPartyCategory();
        $cat2 = new ThirdPartyCategory();

        $tp->addTypesAndCategories('type');
        $tp->addTypesAndCategories($cat1);
        $tp->addTypesAndCategories($cat2);

        $this->assertTrue($tp->getCategories()->contains($cat1));
        $this->assertTrue($tp->getCategories()->contains($cat2));
        $this->assertCount(2, $tp->getCategories());

        $this->assertCount(1, $tp->getThirdPartyTypes());
        $this->assertContains('type', $tp->getThirdPartyTypes());

        $this->assertCount(3, $tp->getTypesAndCategories());
        $this->assertContains($cat1, $tp->getTypesAndCategories());
        $this->assertContains($cat2, $tp->getTypesAndCategories());
        $this->assertContains('type', $tp->getTypesAndCategories());

        // remove type
        $tp->removeTypesAndCategories('type');
        $tp->removeTypesAndCategories($cat2);

        $this->assertTrue(
            $tp->getCategories()->contains($cat1),
            'test that cat1 is still present'
        );
        $this->assertFalse($tp->getCategories()->contains($cat2));
        $this->assertCount(1, $tp->getCategories());

        $this->assertCount(0, $tp->getThirdPartyTypes());
        $this->assertNotContains('type', $tp->getThirdPartyTypes());

        $this->assertCount(1, $tp->getTypesAndCategories());
        $this->assertContains($cat1, $tp->getTypesAndCategories());
        $this->assertNotContains($cat2, $tp->getTypesAndCategories());
        $this->assertNotContains('type', $tp->getTypesAndCategories());
    }

    public function testSyncingActivityTypes()
    {
        $tp = new ThirdParty();
        $tp->setTypesAndCategories([
            'type1',
            'type2',
            $cat1 = new ThirdPartyCategory(),
            $cat2 = new ThirdPartyCategory(),
        ]);

        $this->assertTrue($tp->getCategories()->contains($cat1));
        $this->assertTrue($tp->getCategories()->contains($cat2));
        $this->assertCount(2, $tp->getCategories());

        $this->assertCount(2, $tp->getThirdPartyTypes());
        $this->assertContains('type1', $tp->getThirdPartyTypes());
        $this->assertContains('type2', $tp->getThirdPartyTypes());

        $this->assertCount(4, $tp->getTypesAndCategories());
        $this->assertContains($cat1, $tp->getTypesAndCategories());
        $this->assertContains($cat2, $tp->getTypesAndCategories());
        $this->assertContains('type1', $tp->getTypesAndCategories());
        $this->assertContains('type2', $tp->getTypesAndCategories());

        $tp->setTypesAndCategories([$cat1, 'type1']);

        $this->assertTrue($tp->getCategories()->contains($cat1));
        $this->assertFalse($tp->getCategories()->contains($cat2));
        $this->assertCount(1, $tp->getCategories());

        $this->assertCount(1, $tp->getThirdPartyTypes());
        $this->assertContains('type1', $tp->getThirdPartyTypes());
        $this->assertNotContains('type2', $tp->getThirdPartyTypes());

        $this->assertCount(2, $tp->getTypesAndCategories());
        $this->assertContains($cat1, $tp->getTypesAndCategories());
        $this->assertNotContains($cat2, $tp->getTypesAndCategories());
        $this->assertContains('type1', $tp->getTypesAndCategories());
        $this->assertNotContains('type2', $tp->getTypesAndCategories());
    }
}
