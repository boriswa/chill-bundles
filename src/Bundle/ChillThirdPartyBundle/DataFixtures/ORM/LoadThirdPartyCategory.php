<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdPartyBundle\DataFixtures\ORM;

use Chill\ThirdPartyBundle\Entity\ThirdPartyCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Class LoadThirdPartyCategory.
 */
class LoadThirdPartyCategory extends Fixture implements FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return ['thirdparty_categories'];
    }

    public function load(ObjectManager $manager)
    {
        $categories = [
            ['name' => ['fr' => 'maison médicale']],
            ['name' => ['fr' => 'hôpital']],
            ['name' => ['fr' => 'médecin généraliste']],
            ['name' => ['fr' => 'pharmacien']],
            ['name' => ['fr' => 'assistance aux personnes âgées']],
            ['name' => ['fr' => 'assistante maternelle']],
            ['name' => ['fr' => 'assistant social']],
            ['name' => ['fr' => 'éducateur spécialisé']],
            ['name' => ['fr' => 'infirmier.ère']],
        ];

        foreach ($categories as $val) {
            echo 'Creating thirdparty category : '.$val['name']['fr']."\n";
            $category = (new ThirdPartyCategory())
                ->setName($val['name'])
                ->setActive(true);
            $manager->persist($category);
        }

        $manager->flush();
    }
}
