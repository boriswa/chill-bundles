<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdPartyBundle\ThirdPartyType;

/**
 * Manages types of third parties.
 */
class ThirdPartyTypeManager
{
    /**
     * The prefix used to translate the key of provider.
     */
    final public const THIRD_PARTY_TRANSLATOR_KEY = 'chill_3party.key_label.';

    /**
     * @var ThirdPartyTypeProviderInterface[]
     */
    protected $providers = [];

    /**
     * Add a provider to the manager.
     *
     * Method used during the load of the manager by Dependency Injection
     */
    public function addProvider(ThirdPartyTypeProviderInterface $provider): self
    {
        $this->providers[$provider->getKey()] = $provider;

        return $this;
    }

    /**
     * Get all providers.
     */
    public function getProviders(): array
    {
        return $this->providers;
    }

    /**
     * Get a list of types.
     *
     * @return string[]
     */
    public function getTypes(): array
    {
        return \array_keys($this->providers);
    }
}
