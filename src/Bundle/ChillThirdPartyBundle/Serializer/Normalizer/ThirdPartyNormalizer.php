<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdPartyBundle\Serializer\Normalizer;

use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Chill\ThirdPartyBundle\Entity\ThirdPartyCategory;
use Chill\ThirdPartyBundle\Templating\Entity\ThirdPartyRender;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ThirdPartyNormalizer implements NormalizerAwareInterface, NormalizerInterface
{
    use NormalizerAwareTrait;

    public function __construct(private readonly ThirdPartyRender $thirdPartyRender, private readonly TranslatableStringHelperInterface $translatableStringHelper)
    {
    }

    public function normalize($thirdParty, $format = null, array $context = [])
    {
        if (!$thirdParty instanceof ThirdParty) {
            throw new UnexpectedValueException();
        }

        return [
            'type' => 'thirdparty',
            'firstname' => $thirdParty->getFirstname(),
            'name' => $thirdParty->getName(),
            'text' => $this->thirdPartyRender->renderString($thirdParty, []),
            'id' => $thirdParty->getId(),
            'kind' => $thirdParty->getKind(),
            'category' => array_map(function ($el) {
                if ($el instanceof ThirdPartyCategory) {
                    return [
                        'text' => $this->translatableStringHelper->localize($el->getName()),
                        'type' => 'thirdparty_category',
                    ];
                }

                return [
                    'text' => $el,
                    'type' => 'thirdparty_kind',
                ];
            }, $thirdParty->getTypesAndCategories()),
            'profession' => $this->normalizer->normalize($thirdParty->getProfession(), $format, $context),
            'address' => $this->normalizer->normalize($thirdParty->getAddress(), $format, ['address_rendering' => 'short']),
            'telephone' => $this->normalizer->normalize($thirdParty->getTelephone(), $format, $context),
            'email' => $thirdParty->getEmail(),
            'isChild' => $thirdParty->isChild(),
            'parent' => $this->normalizer->normalize($thirdParty->getParent(), $format, $context),
            'civility' => $this->normalizer->normalize($thirdParty->getCivility(), $format, $context),
            'contactDataAnonymous' => $thirdParty->isContactDataAnonymous(),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof ThirdParty && 'json' === $format;
    }
}
