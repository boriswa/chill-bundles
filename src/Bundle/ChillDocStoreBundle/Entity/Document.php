<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Entity;

use Chill\DocGeneratorBundle\Entity\DocGeneratorTemplate;
use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\MappedSuperclass
 */
class Document implements TrackCreationInterface, TrackUpdateInterface
{
    use TrackCreationTrait;

    use TrackUpdateTrait;

    /**
     * @ORM\ManyToOne(targetEntity="Chill\DocStoreBundle\Entity\DocumentCategory")
     *
     * @ORM\JoinColumns({
     *
     *     @ORM\JoinColumn(name="category_bundle_id", referencedColumnName="bundle_id"),
     *     @ORM\JoinColumn(name="category_id_inside_bundle", referencedColumnName="id_inside_bundle")
     * })
     */
    private ?DocumentCategory $category = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?\DateTimeInterface $date = null;

    /**
     * @ORM\Column(type="text")
     */
    private string $description = '';

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Chill\DocStoreBundle\Entity\StoredObject",
     *     cascade={"persist"}
     * )
     *
     * @Assert\Valid
     *
     * @Assert\NotNull(
     *     message="Upload a document"
     * )
     */
    private ?StoredObject $object = null;

    /**
     * @ORM\ManyToOne(targetEntity="Chill\DocGeneratorBundle\Entity\DocGeneratorTemplate")
     */
    private ?DocGeneratorTemplate $template = null;

    /**
     * @ORM\Column(type="text")
     *
     * @Assert\Length(
     *     min=2, max=250
     * )
     */
    private string $title = '';

    /**
     * @ORM\ManyToOne(targetEntity="Chill\MainBundle\Entity\User")
     */
    private ?\Chill\MainBundle\Entity\User $user = null;

    public function getCategory(): ?DocumentCategory
    {
        return $this->category;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getObject(): ?StoredObject
    {
        return $this->object;
    }

    public function getTemplate(): ?DocGeneratorTemplate
    {
        return $this->template;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setCategory(DocumentCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function setDescription($description): self
    {
        $this->description = (string) $description;

        return $this;
    }

    public function setObject(?StoredObject $object = null)
    {
        $this->object = $object;

        return $this;
    }

    public function setTemplate(?DocGeneratorTemplate $template): self
    {
        $this->template = $template;

        return $this;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function setUser($user): self
    {
        $this->user = $user;

        return $this;
    }
}
