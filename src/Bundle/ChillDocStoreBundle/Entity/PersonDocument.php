<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Entity;

use Chill\MainBundle\Entity\HasCenterInterface;
use Chill\MainBundle\Entity\HasScopeInterface;
use Chill\MainBundle\Entity\Scope;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table("chill_doc.person_document")
 *
 * @ORM\Entity
 */
class PersonDocument extends Document implements HasCenterInterface, HasScopeInterface
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="Chill\PersonBundle\Entity\Person")
     */
    private Person $person;

    /**
     * @ORM\ManyToOne(targetEntity="Chill\MainBundle\Entity\Scope")
     *
     * @var Scope The document's center
     */
    private ?Scope $scope = null;

    public function getCenter()
    {
        return $this->getPerson()->getCenter();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function getScope(): ?Scope
    {
        return $this->scope;
    }

    public function setPerson($person): self
    {
        $this->person = $person;

        return $this;
    }

    public function setScope($scope): self
    {
        $this->scope = $scope;

        return $this;
    }
}
