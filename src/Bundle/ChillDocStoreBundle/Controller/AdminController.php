<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class AdminController.
 */
class AdminController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/document", name="chill_docstore_admin", options={null})
     */
    public function indexAction()
    {
        return $this->render('@ChillDocStore/Admin/layout.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/document_redirect_to_main", name="chill_docstore_admin_redirect_to_admin_index", options={null})
     */
    public function redirectToAdminIndexAction()
    {
        return $this->redirectToRoute('chill_main_admin_central');
    }
}
