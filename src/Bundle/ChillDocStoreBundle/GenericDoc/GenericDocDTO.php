<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\GenericDoc;

use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;

final readonly class GenericDocDTO
{
    public function __construct(
        public string $key,
        public array $identifiers,
        public \DateTimeImmutable $docDate,
        public AccompanyingPeriod|Person $linked,
    ) {
    }

    public function getContext(): string
    {
        return $this->linked instanceof AccompanyingPeriod ? 'accompanying-period' : 'person';
    }
}
