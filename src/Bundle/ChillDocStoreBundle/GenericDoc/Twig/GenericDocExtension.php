<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\GenericDoc\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

final class GenericDocExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('chill_generic_doc_render', [GenericDocExtensionRuntime::class, 'renderGenericDoc'], [
                'needs_environment' => true,
                'is_safe' => ['html'],
            ]),
        ];
    }
}
