<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\GenericDoc\Renderer;

use Chill\DocStoreBundle\GenericDoc\GenericDocDTO;
use Chill\DocStoreBundle\GenericDoc\Providers\PersonDocumentGenericDocProvider;
use Chill\DocStoreBundle\GenericDoc\Twig\GenericDocRendererInterface;
use Chill\DocStoreBundle\GenericDoc\Providers\AccompanyingCourseDocumentGenericDocProvider;
use Chill\DocStoreBundle\Repository\AccompanyingCourseDocumentRepository;
use Chill\DocStoreBundle\Repository\PersonDocumentRepository;

final readonly class AccompanyingCourseDocumentGenericDocRenderer implements GenericDocRendererInterface
{
    public function __construct(
        private AccompanyingCourseDocumentRepository $accompanyingCourseDocumentRepository,
        private PersonDocumentRepository $personDocumentRepository,
    ) {
    }

    public function supports(GenericDocDTO $genericDocDTO, $options = []): bool
    {
        return AccompanyingCourseDocumentGenericDocProvider::KEY === $genericDocDTO->key
            || PersonDocumentGenericDocProvider::KEY === $genericDocDTO->key;
    }

    public function getTemplate(GenericDocDTO $genericDocDTO, $options = []): string
    {
        return '@ChillDocStore/List/list_item.html.twig';
    }

    public function getTemplateData(GenericDocDTO $genericDocDTO, $options = []): array
    {
        if (AccompanyingCourseDocumentGenericDocProvider::KEY === $genericDocDTO->key) {
            return [
                'document' => $doc = $this->accompanyingCourseDocumentRepository->find($genericDocDTO->identifiers['id']),
                'accompanyingCourse' => $doc->getCourse(),
                'options' => $options,
                'context' => $genericDocDTO->getContext(),
            ];
        }

        // this is a person
        return [
            'document' => $doc = $this->personDocumentRepository->find($genericDocDTO->identifiers['id']),
            'person' => $doc->getPerson(),
            'options' => $options,
            'context' => $genericDocDTO->getContext(),
        ];
    }
}
