<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Tests\GenericDoc;

use Chill\DocStoreBundle\GenericDoc\FetchQuery;
use Chill\DocStoreBundle\GenericDoc\FetchQueryInterface;
use Chill\DocStoreBundle\GenericDoc\GenericDocDTO;
use Chill\DocStoreBundle\GenericDoc\GenericDocForPersonProviderInterface;
use Chill\DocStoreBundle\GenericDoc\Manager;
use Chill\DocStoreBundle\GenericDoc\GenericDocForAccompanyingPeriodProviderInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class ManagerTest extends KernelTestCase
{
    use ProphecyTrait;
    private EntityManagerInterface $em;

    private Connection $connection;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->em = self::$container->get(EntityManagerInterface::class);
        $this->connection = self::$container->get(Connection::class);
    }

    public function testCountByAccompanyingPeriod(): void
    {
        $period = $this->em->createQuery('SELECT a FROM '.AccompanyingPeriod::class.' a')
            ->setMaxResults(1)
            ->getSingleResult();

        if (null === $period) {
            throw new \UnexpectedValueException('period not found');
        }

        $manager = new Manager(
            [new SimpleGenericDocAccompanyingPeriodProvider()],
            [new SimpleGenericDocPersonProvider()],
            $this->connection,
        );

        $nb = $manager->countDocForAccompanyingPeriod($period);

        self::assertIsInt($nb);
    }

    public function testCountByPerson(): void
    {
        $person = $this->em->createQuery('SELECT a FROM '.Person::class.' a')
            ->setMaxResults(1)
            ->getSingleResult();

        if (null === $person) {
            throw new \UnexpectedValueException('person found');
        }

        $manager = new Manager(
            [new SimpleGenericDocAccompanyingPeriodProvider()],
            [new SimpleGenericDocPersonProvider()],
            $this->connection,
        );

        $nb = $manager->countDocForPerson($person);

        self::assertIsInt($nb);
    }

    public function testFindDocByAccompanyingPeriod(): void
    {
        $period = $this->em->createQuery('SELECT a FROM '.AccompanyingPeriod::class.' a')
            ->setMaxResults(1)
            ->getSingleResult();

        if (null === $period) {
            throw new \UnexpectedValueException('period not found');
        }

        $manager = new Manager(
            [new SimpleGenericDocAccompanyingPeriodProvider()],
            [new SimpleGenericDocPersonProvider()],
            $this->connection,
        );

        foreach ($manager->findDocForAccompanyingPeriod($period) as $doc) {
            self::assertInstanceOf(GenericDocDTO::class, $doc);
        }
    }

    public function testFindDocByPerson(): void
    {
        $person = $this->em->createQuery('SELECT a FROM '.Person::class.' a')
            ->setMaxResults(1)
            ->getSingleResult();

        if (null === $person) {
            throw new \UnexpectedValueException('person not found');
        }

        $manager = new Manager(
            [new SimpleGenericDocAccompanyingPeriodProvider()],
            [new SimpleGenericDocPersonProvider()],
            $this->connection,
        );

        foreach ($manager->findDocForPerson($person) as $doc) {
            self::assertInstanceOf(GenericDocDTO::class, $doc);
        }
    }

    public function testPlacesForPerson(): void
    {
        $person = $this->em->createQuery('SELECT a FROM '.Person::class.' a')
            ->setMaxResults(1)
            ->getSingleResult();

        if (null === $person) {
            throw new \UnexpectedValueException('person not found');
        }

        $manager = new Manager(
            [new SimpleGenericDocAccompanyingPeriodProvider()],
            [new SimpleGenericDocPersonProvider()],
            $this->connection,
        );

        $places = $manager->placesForPerson($person);

        self::assertEquals(['dummy_person_doc'], $places);
    }

    public function testPlacesForAccompanyingPeriod(): void
    {
        $period = $this->em->createQuery('SELECT a FROM '.AccompanyingPeriod::class.' a')
            ->setMaxResults(1)
            ->getSingleResult();

        if (null === $period) {
            throw new \UnexpectedValueException('period not found');
        }

        $manager = new Manager(
            [new SimpleGenericDocAccompanyingPeriodProvider()],
            [new SimpleGenericDocPersonProvider()],
            $this->connection,
        );

        $places = $manager->placesForAccompanyingPeriod($period);

        self::assertEquals(['accompanying_course_document_dummy'], $places);
    }
}

final readonly class SimpleGenericDocAccompanyingPeriodProvider implements GenericDocForAccompanyingPeriodProviderInterface
{
    public function buildFetchQueryForAccompanyingPeriod(AccompanyingPeriod $accompanyingPeriod, ?\DateTimeImmutable $startDate = null, ?\DateTimeImmutable $endDate = null, ?string $content = null, ?string $origin = null): FetchQueryInterface
    {
        $query = new FetchQuery(
            'accompanying_course_document_dummy',
            sprintf('jsonb_build_object(\'id\', %s)', 'id'),
            'd',
            '(VALUES (1, \'2023-05-01\'::date), (2, \'2023-05-01\'::date)) AS sq (id, d)',
        );

        $query->addWhereClause('d > ?', [new \DateTimeImmutable('2023-01-01')], [Types::DATE_IMMUTABLE]);

        return $query;
    }

    public function isAllowedForAccompanyingPeriod(AccompanyingPeriod $accompanyingPeriod): bool
    {
        return true;
    }
}

final readonly class SimpleGenericDocPersonProvider implements GenericDocForPersonProviderInterface
{
    public function buildFetchQueryForPerson(Person $person, ?\DateTimeImmutable $startDate = null, ?\DateTimeImmutable $endDate = null, ?string $content = null, ?string $origin = null): FetchQueryInterface
    {
        $query = new FetchQuery(
            'dummy_person_doc',
            sprintf('jsonb_build_object(\'id\', %s)', 'id'),
            'd',
            '(VALUES (1, \'2023-05-01\'::date), (2, \'2023-05-01\'::date)) AS sq (id, d)',
        );

        $query->addWhereClause('d > ?', [new \DateTimeImmutable('2023-01-01')], [Types::DATE_IMMUTABLE]);

        return $query;
    }

    public function isAllowedForPerson(Person $person): bool
    {
        return true;
    }
}
