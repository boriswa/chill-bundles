<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\DocStore;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add title on storedObject.
 */
final class Version20220525141646 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_doc.stored_object DROP title');
    }

    public function getDescription(): string
    {
        return 'Add title on storedObject';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_doc.stored_object ADD title TEXT NOT NULL DEFAULT \'\' ');
    }
}
