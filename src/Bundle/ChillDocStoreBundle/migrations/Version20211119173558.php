<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\DocStore;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211119173558 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException();
    }

    public function getDescription(): string
    {
        return 'remove comment on deprecated json_array type';
    }

    public function up(Schema $schema): void
    {
        $columns = [
            'chill_doc.document_category.name',
            'chill_doc.stored_object.key',
            'chill_doc.stored_object.iv',
            'chill_doc.stored_object.datas',
        ];

        foreach ($columns as $col) {
            $this->addSql("COMMENT ON COLUMN {$col} IS NULL");
        }
    }
}
