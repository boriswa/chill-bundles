<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Repository;

use Chill\DocStoreBundle\Entity\DocumentCategory;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;

/**
 * Get an available idInsideBUndle.
 */
class DocumentCategoryRepository implements ObjectRepository
{
    private readonly EntityRepository $repository;

    public function __construct(private readonly EntityManagerInterface $em)
    {
        $this->repository = $em->getRepository(DocumentCategory::class);
    }

    public function find($id): ?DocumentCategory
    {
        return $this->repository->find($id);
    }

    /**
     * @return array|DocumentCategory[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): ?DocumentCategory
    {
        return $this->findOneBy($criteria);
    }

    public function getClassName()
    {
        return DocumentCategory::class;
    }
}
