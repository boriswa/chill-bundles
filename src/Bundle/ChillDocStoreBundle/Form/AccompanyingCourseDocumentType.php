<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Form;

use Chill\DocStoreBundle\Entity\AccompanyingCourseDocument;
use Chill\DocStoreBundle\Entity\Document;
use Chill\DocStoreBundle\Entity\DocumentCategory;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Form\Type\ChillDateType;
use Chill\MainBundle\Form\Type\ChillTextareaType;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccompanyingCourseDocumentType extends AbstractType
{
    /**
     * @var AuthorizationHelper
     */
    protected $authorizationHelper;

    /**
     * @var ObjectManager
     */
    protected $om;

    /**
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;

    /**
     * the user running this form.
     *
     * @var User
     */
    protected $user;

    public function __construct(
        TranslatableStringHelper $translatableStringHelper
    ) {
        $this->translatableStringHelper = $translatableStringHelper;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('description', ChillTextareaType::class, [
                'required' => false,
            ])
            ->add('object', StoredObjectType::class, [
                'error_bubbling' => true,
            ])
            ->add('date', ChillDateType::class)
            // TODO : adapt to using AccompanyingCourseDocument categories. Currently there are none...
            ->add('category', EntityType::class, [
                'placeholder' => 'Choose a document category',
                'class' => DocumentCategory::class,
                'query_builder' => static fn (EntityRepository $er) => $er->createQueryBuilder('c')
                    ->where('c.documentClass = :docClass')
                    ->setParameter('docClass', AccompanyingCourseDocument::class),
                'choice_label' => fn ($entity = null) => $entity ? $this->translatableStringHelper->localize($entity->getName()) : '',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Document::class,
        ]);
    }
}
