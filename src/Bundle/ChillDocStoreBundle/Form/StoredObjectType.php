<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Form;

use ChampsLibres\AsyncUploaderBundle\Form\Type\AsyncUploaderType;
use Chill\DocStoreBundle\Entity\StoredObject;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Form type which allow to join a document.
 */
class StoredObjectType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (true === $options['has_title']) {
            $builder
                ->add('title', TextType::class, [
                    'label' => 'Title',
                ]);
        }

        $builder
            ->add('filename', AsyncUploaderType::class)
            ->add('type', HiddenType::class)
            ->add('keyInfos', HiddenType::class)
            ->add('iv', HiddenType::class);

        $builder
            ->get('keyInfos')
            ->addModelTransformer(new CallbackTransformer(
                $this->transform(...),
                $this->reverseTransform(...)
            ));
        $builder
            ->get('iv')
            ->addModelTransformer(new CallbackTransformer(
                $this->transform(...),
                $this->reverseTransform(...)
            ));

        $builder
            ->addModelTransformer(new CallbackTransformer(
                $this->transformObject(...),
                $this->reverseTransformObject(...)
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('data_class', StoredObject::class);

        $resolver
            ->setDefault('has_title', false)
            ->setAllowedTypes('has_title', ['bool']);
    }

    public function reverseTransform($value)
    {
        if (null === $value) {
            return null;
        }

        return \json_decode((string) $value, true, 512, JSON_THROW_ON_ERROR);
    }

    public function reverseTransformObject($object)
    {
        if (null === $object) {
            return null;
        }

        if (null === $object->getFilename()) {
            // remove the original object
            $this->em->remove($object);

            return null;
        }

        return $object;
    }

    public function transform($object)
    {
        if (null === $object) {
            return null;
        }

        return \json_encode($object, JSON_THROW_ON_ERROR);
    }

    public function transformObject($object = null)
    {
        return $object;
    }
}
