<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Object;

use ChampsLibres\AsyncUploaderBundle\Form\AsyncFileTransformer\AsyncFileTransformerInterface;
use ChampsLibres\AsyncUploaderBundle\Model\AsyncFileInterface;
use Chill\DocStoreBundle\Entity\StoredObject;
use Doctrine\ORM\EntityManagerInterface;

class ObjectToAsyncFileTransformer implements AsyncFileTransformerInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function toAsyncFile($data)
    {
        if ($data instanceof StoredObject) {
            return $data;
        }
    }

    public function toData(AsyncFileInterface $asyncFile)
    {
        $object = $this->em
            ->getRepository(StoredObject::class)
            ->findByFilename($asyncFile->getObjectName());

        return $object ?? (new StoredObject())
            ->setFilename($asyncFile->getObjectName());
    }
}
