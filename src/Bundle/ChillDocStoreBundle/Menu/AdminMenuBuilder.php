<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AdminMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            return;
        }

        $menu->addChild('Documents', [
            'route' => 'chill_docstore_admin',
        ])
            ->setAttribute('class', 'list-group-item-header')
            ->setExtras([
                'order' => 4000,
            ]);

        $menu->addChild('Document category list', [
            'route' => 'chill_docstore_category_admin',
        ])->setExtras(['order' => 4010]);
    }

    public static function getMenuIds(): array
    {
        return ['admin_section', 'admin_docstore'];
    }
}
