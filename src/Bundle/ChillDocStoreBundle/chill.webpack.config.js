module.exports = function(encore)
{
    encore.addAliases({
        ChillDocStoreAssets: __dirname + '/Resources/public'
    });
    encore.addEntry('mod_async_upload', __dirname + '/Resources/public/module/async_upload/index.js');
    encore.addEntry('mod_document_action_buttons_group', __dirname + '/Resources/public/module/document_action_buttons_group/index');
};
