<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\Menu;

use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Security;

final readonly class AdminMenuBuilder implements \Chill\MainBundle\Routing\LocalMenuBuilderInterface
{
    public function __construct(private Security $security)
    {
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        // all the entries below must have ROLE_ADMIN permissions
        if (!$this->security->isGranted('ROLE_ADMIN')) {
            return;
        }

        $menu->addChild('Aside activities', [
            'route' => 'chill_aside_activity_admin',
        ])
            ->setAttribute('class', 'list-group-item-header')
            ->setExtras([
                'order' => 7000,
                'explain' => 'Aside activity type configuration',
            ]);
        $menu
            ->addChild('Aside activity categories', [
                'route' => 'chill_crud_aside_activity_category_index',
            ])
            ->setExtras([
                'order' => 7010,
            ]);
    }

    public static function getMenuIds(): array
    {
        return ['admin_section', 'admin_aside_activity'];
    }
}
