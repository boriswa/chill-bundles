<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\Tests\Export\Export;

use Chill\AsideActivityBundle\Export\Export\ListAsideActivity;
use Doctrine\ORM\AbstractQuery;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class ListAsideActivityTest extends KernelTestCase
{
    private ListAsideActivity $listAsideActivity;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        $this->listAsideActivity = self::$container->get(ListAsideActivity::class);
    }

    public function testExecuteQuery(): void
    {
        $qb = $this->listAsideActivity->initiateQuery([], [], [])
            ->setMaxResults(1);

        $results = $qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);

        self::assertIsArray($results, 'smoke test: test that the result is an array');
    }
}
