<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\AsideActivity;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210706124644 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE AsideActivity DROP CONSTRAINT FK_E9FA2191C54C8C93');
        $this->addSql('DROP SEQUENCE AsideActivity_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE AsideActivityType_id_seq CASCADE');
        $this->addSql('DROP TABLE AsideActivity');
        $this->addSql('DROP TABLE AsideActivityType');
    }

    public function getDescription(): string
    {
        return 'Aside activity category entity created';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE AsideActivity_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE AsideActivityType_id_seq INCREMENT BY 1 MINVALUE 1 START 1000');
        $this->addSql('CREATE TABLE AsideActivity (id INT NOT NULL, type_id INT NOT NULL, agent_id INT NOT NULL, createdAt TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updatedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, duration TIME(0) WITHOUT TIME ZONE DEFAULT NULL, location VARCHAR(100) DEFAULT NULL, note TEXT DEFAULT NULL, createdBy_id INT NOT NULL, updatedBy_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E9FA2191C54C8C93 ON AsideActivity (type_id)');
        $this->addSql('CREATE INDEX IDX_E9FA21913174800F ON AsideActivity (createdBy_id)');
        $this->addSql('CREATE INDEX IDX_E9FA219165FF1AEC ON AsideActivity (updatedBy_id)');
        $this->addSql('CREATE INDEX IDX_E9FA21913414710B ON AsideActivity (agent_id)');
        $this->addSql('COMMENT ON COLUMN AsideActivity.updatedAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE AsideActivityType (id INT NOT NULL, title VARCHAR(255) NOT NULL, isActive BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE AsideActivity ADD CONSTRAINT FK_E9FA2191C54C8C93 FOREIGN KEY (type_id) REFERENCES AsideActivityType (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE AsideActivity ADD CONSTRAINT FK_E9FA21913174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE AsideActivity ADD CONSTRAINT FK_E9FA219165FF1AEC FOREIGN KEY (updatedBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE AsideActivity ADD CONSTRAINT FK_E9FA21913414710B FOREIGN KEY (agent_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        // this up() migration is auto-generated, please modify it to your needs
    }
}
