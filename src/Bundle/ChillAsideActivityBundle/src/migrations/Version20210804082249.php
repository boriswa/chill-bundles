<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\AsideActivity;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210804082249 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE chill_asideactivity.AsideActivity DROP CONSTRAINT FK_A866DA0EC54C8C93');
        $this->addSql('DROP SEQUENCE chill_asideactivity.AsideActivity_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_asideactivity.AsideActivityCategory_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE asideactivity_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE asideactivitytype_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE asideactivitytype (id INT NOT NULL, title VARCHAR(255) NOT NULL, isactive BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE asideactivity (id INT NOT NULL, type_id INT NOT NULL, agent_id INT NOT NULL, createdby_id INT NOT NULL, updatedby_id INT DEFAULT NULL, createdat TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updatedat TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, duration TIME(0) WITHOUT TIME ZONE DEFAULT NULL, location VARCHAR(100) DEFAULT NULL, note TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_e9fa21913174800f ON asideactivity (createdby_id)');
        $this->addSql('CREATE INDEX idx_e9fa2191c54c8c93 ON asideactivity (type_id)');
        $this->addSql('CREATE INDEX idx_e9fa219165ff1aec ON asideactivity (updatedby_id)');
        $this->addSql('CREATE INDEX idx_e9fa21913414710b ON asideactivity (agent_id)');
        $this->addSql('COMMENT ON COLUMN asideactivity.updatedat IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE asideactivity ADD CONSTRAINT fk_e9fa2191c54c8c93 FOREIGN KEY (type_id) REFERENCES asideactivitytype (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE asideactivity ADD CONSTRAINT fk_e9fa21913174800f FOREIGN KEY (createdby_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE asideactivity ADD CONSTRAINT fk_e9fa219165ff1aec FOREIGN KEY (updatedby_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE asideactivity ADD CONSTRAINT fk_e9fa21913414710b FOREIGN KEY (agent_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE chill_asideactivity.AsideActivity');
        $this->addSql('DROP TABLE chill_asideactivity.AsideActivityCategory');
        $this->addSql('DROP SCHEMA chill_asideactivity');
    }

    public function getDescription(): string
    {
        return 'Aside activity entity created';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA chill_asideactivity');
        $this->addSql('ALTER TABLE asideactivity DROP CONSTRAINT fk_e9fa2191c54c8c93');
        $this->addSql('DROP SEQUENCE asideactivity_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE asideactivitytype_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE chill_asideactivity.AsideActivity_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_asideactivity.AsideActivityCategory_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_asideactivity.AsideActivity (id INT NOT NULL, type_id INT NOT NULL, agent_id INT NOT NULL, createdAt TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updatedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, duration INT DEFAULT NULL, location VARCHAR(100) DEFAULT NULL, note TEXT DEFAULT NULL, createdBy_id INT NOT NULL, updatedBy_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A866DA0EC54C8C93 ON chill_asideactivity.AsideActivity (type_id)');
        $this->addSql('CREATE INDEX IDX_A866DA0E3174800F ON chill_asideactivity.AsideActivity (createdBy_id)');
        $this->addSql('CREATE INDEX IDX_A866DA0E65FF1AEC ON chill_asideactivity.AsideActivity (updatedBy_id)');
        $this->addSql('CREATE INDEX IDX_A866DA0E3414710B ON chill_asideactivity.AsideActivity (agent_id)');
        $this->addSql('COMMENT ON COLUMN chill_asideactivity.AsideActivity.createdAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_asideactivity.AsideActivity.updatedAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_asideactivity.AsideActivity.date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE chill_asideactivity.AsideActivityCategory (id INT NOT NULL, title JSON NOT NULL, isActive BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE chill_asideactivity.AsideActivity ADD CONSTRAINT FK_A866DA0EC54C8C93 FOREIGN KEY (type_id) REFERENCES chill_asideactivity.AsideActivityCategory (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_asideactivity.AsideActivity ADD CONSTRAINT FK_A866DA0E3174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_asideactivity.AsideActivity ADD CONSTRAINT FK_A866DA0E65FF1AEC FOREIGN KEY (updatedBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_asideactivity.AsideActivity ADD CONSTRAINT FK_A866DA0E3414710B FOREIGN KEY (agent_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE asideactivitytype');
        $this->addSql('DROP TABLE asideactivity');
    }
}
