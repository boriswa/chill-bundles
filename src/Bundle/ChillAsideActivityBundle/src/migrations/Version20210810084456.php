<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\AsideActivity;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210810084456 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chill_asideactivity.AsideActivity ALTER createdAt TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE chill_asideactivity.AsideActivity ALTER createdAt DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_asideactivity.AsideActivity ALTER updatedAt TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE chill_asideactivity.AsideActivity ALTER updatedAt DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_asideactivity.AsideActivity ALTER date TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE chill_asideactivity.AsideActivity ALTER date DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN chill_asideactivity.AsideActivity.createdat IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_asideactivity.AsideActivity.updatedat IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_asideactivity.AsideActivity.date IS \'(DC2Type:datetime_immutable)\'');
    }

    public function getDescription(): string
    {
        return 'createdat, updatedat and date given a type timestamp';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chill_asideactivity.asideactivity ALTER createdat TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE chill_asideactivity.asideactivity ALTER createdat DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_asideactivity.asideactivity ALTER updatedat TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE chill_asideactivity.asideactivity ALTER updatedat DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_asideactivity.asideactivity ALTER date TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE chill_asideactivity.asideactivity ALTER date DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN chill_asideactivity.asideactivity.createdAt IS NULL');
        $this->addSql('COMMENT ON COLUMN chill_asideactivity.asideactivity.updatedAt IS NULL');
        $this->addSql('COMMENT ON COLUMN chill_asideactivity.asideactivity.date IS NULL');
    }
}
