<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\DataFixtures\ORM;

use Chill\AsideActivityBundle\Entity\AsideActivityCategory;
use Doctrine\Persistence\ObjectManager;

class LoadAsideActivityCategory extends \Doctrine\Bundle\FixturesBundle\Fixture
{
    public function load(ObjectManager $manager)
    {
        foreach (
            [
                'Appel téléphonique',
                'Formation',
            ] as $key => $label
        ) {
            $category = new AsideActivityCategory();
            $category->setTitle(['fr' => $label]);
            $manager->persist($category);
            $this->setReference('aside_activity_category_'.$key, $category);
        }

        $manager->flush();
    }
}
