<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\Export\Filter;

use Chill\AsideActivityBundle\Export\Declarations;
use Chill\MainBundle\Entity\Location;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Form\Type\PickUserLocationType;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Security;

final readonly class ByLocationFilter implements FilterInterface
{
    public function __construct(
        private Security $security
    ) {
    }

    public function getTitle(): string
    {
        return 'export.filter.Filter by aside activity location';
    }

    public function buildForm(FormBuilderInterface $builder): void
    {
        $builder
            ->add('locations', PickUserLocationType::class);
    }

    public function getFormDefaultData(): array
    {
        $user = $this->security->getUser();

        if ($user instanceof User) {
            return [
                'locations' => $user->getCurrentLocation(),
            ];
        }

        return [
            'locations' => null,
        ];
    }

    public function describeAction($data, $format = 'string'): array
    {
        $extractFunction = fn (Location $l): string => $l->getName();
        if ($data['locations'] instanceof Collection) {
            $locations = $data['locations']->map($extractFunction);
        } else {
            $locations = array_map($extractFunction, $data['locations']);
        }

        return ['export.filter.Filtered by aside activity location: only %location%', [
            '%location%' => implode(', ', $locations),
        ]];
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data): void
    {
        $clause = $qb->expr()->in('aside.location', ':locations');

        $qb->andWhere($clause);
        $qb->setParameter('locations', $data['locations']);
    }

    public function applyOn(): string
    {
        return Declarations::ASIDE_ACTIVITY_TYPE;
    }
}
