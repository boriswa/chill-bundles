<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\Form\Type;

use Chill\AsideActivityBundle\Entity\AsideActivityCategory;
use Chill\AsideActivityBundle\Templating\Entity\CategoryRender;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class PickAsideActivityCategoryType extends AbstractType
{
    public function __construct(private readonly CategoryRender $categoryRender)
    {
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'class' => AsideActivityCategory::class,
                'placeholder' => 'Choose the activity category',
                'query_builder' => static function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('ac');
                    $qb->where($qb->expr()->eq('ac.isActive', 'TRUE'))
                        ->addOrderBy('ac.ordering', 'ASC');

                    return $qb;
                },
                'choice_label' => function (AsideActivityCategory $asideActivityCategory) {
                    $options = [];

                    return $this->categoryRender->renderString($asideActivityCategory, $options);
                },
                'attr' => ['class' => 'select2'],
            ]);
    }

    public function getParent(): string
    {
        return EntityType::class;
    }
}
