<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\Repository;

use Chill\AsideActivityBundle\Entity\AsideActivityCategory;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;

class AsideActivityCategoryRepository implements ObjectRepository
{
    private readonly EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(AsideActivityCategory::class);
    }

    public function find($id): ?AsideActivityCategory
    {
        return $this->repository->find($id);
    }

    /**
     * @return AsideActivityCategory[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function findAllActive(): array
    {
        return $this->repository->findBy(['isActive' => true]);
    }

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return AsideActivityCategory[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): ?AsideActivityCategory
    {
        return $this->repository->findOneBy($criteria);
    }

    public function getClassName(): string
    {
        return AsideActivityCategory::class;
    }
}
