<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\Repository;

use Chill\AsideActivityBundle\Entity\AsideActivity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AsideActivity|null find($id, $lockMode = null, $lockVersion = null)
 * @method AsideActivity|null findOneBy(array $criteria, array $orderBy = null)
 * @method AsideActivity[]    findAll()
 * @method AsideActivity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class AsideActivityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AsideActivity::class);
    }

    public function getClassName(): string
    {
        return AsideActivity::class;
    }
}
