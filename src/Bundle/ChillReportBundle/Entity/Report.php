<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ReportBundle\Entity;

use Chill\CustomFieldsBundle\Entity\CustomFieldsGroup;
use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Entity\HasCenterInterface;
use Chill\MainBundle\Entity\HasScopeInterface;
use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Entity\Person;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Report.
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="report")
 *
 * @ORM\HasLifecycleCallbacks
 */
class Report implements HasCenterInterface, HasScopeInterface
{
    /**
     * @ORM\Column(type="json")
     */
    private ?array $cFData = null;

    /**
     * @ORM\ManyToOne(
     * targetEntity="Chill\CustomFieldsBundle\Entity\CustomFieldsGroup")
     */
    private ?CustomFieldsGroup $cFGroup = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?\DateTime $date = null;

    /**
     * @ORM\Id
     *
     * @ORM\Column(name="id", type="integer")
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="Chill\PersonBundle\Entity\Person")
     */
    private ?Person $person = null;

    /**
     * @ORM\ManyToOne(targetEntity="Chill\MainBundle\Entity\Scope")
     */
    private ?Scope $scope = null;

    /**
     * @ORM\ManyToOne(targetEntity="Chill\MainBundle\Entity\User")
     */
    private ?User $user = null;

    /**
     * @return Center
     */
    public function getCenter()
    {
        return $this->person->getCenter();
    }

    /**
     * Get cFData.
     *
     * @return array
     */
    public function getCFData()
    {
        return $this->cFData;
    }

    /**
     * Get cFGroup.
     *
     * @return CustomFieldsGroup
     */
    public function getCFGroup()
    {
        return $this->cFGroup;
    }

    /**
     * Get date.
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get person.
     *
     * @return Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Get scope.
     *
     * @return Scope
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * Get user.
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set cFData.
     *
     * @return Report
     */
    public function setCFData(array $cFData)
    {
        $this->cFData = $cFData;

        return $this;
    }

    /**
     * Set cFGroup.
     *
     * @return Report
     */
    public function setCFGroup(CustomFieldsGroup $cFGroup)
    {
        $this->cFGroup = $cFGroup;

        return $this;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return Report
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Set person.
     *
     * @return Report
     */
    public function setPerson(Person $person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Set scope.
     *
     * @param string $scope
     *
     * @return Report
     */
    public function setScope(Scope $scope)
    {
        $this->scope = $scope;

        return $this;
    }

    /**
     * Set user.
     *
     * @return Report
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }
}
