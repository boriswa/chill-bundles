<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ReportBundle\Form;

use Chill\CustomFieldsBundle\Form\Type\CustomFieldType;
use Chill\MainBundle\Form\Type\AppendScopeChoiceTypeTrait;
use Chill\MainBundle\Form\Type\ChillDateType;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ReportType extends AbstractType
{
    use AppendScopeChoiceTypeTrait;

    /**
     * @var AuthorizationHelper
     */
    protected $authorizationHelper;

    /**
     * @var ObjectManager
     */
    protected $om;

    /**
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;

    /**
     * @var \Chill\MainBundle\Entity\User
     */
    protected $user;

    public function __construct(
        AuthorizationHelper $helper,
        TokenStorageInterface $tokenStorage,
        TranslatableStringHelper $translatableStringHelper,
        ObjectManager $om
    ) {
        $this->authorizationHelper = $helper;
        $this->user = $tokenStorage->getToken()->getUser();
        $this->translatableStringHelper = $translatableStringHelper;
        $this->om = $om;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user')
            ->add(
                'date',
                ChillDateType::class,
                ['required' => true]
            )
            ->add(
                'cFData',
                CustomFieldType::class,
                ['attr' => ['class' => 'cf-fields'],
                    'group' => $options['cFGroup'], ]
            );

        $this->appendScopeChoices(
            $builder,
            $options['role'],
            $options['center'],
            $this->user,
            $this->authorizationHelper,
            $this->translatableStringHelper,
            $this->om
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => \Chill\ReportBundle\Entity\Report::class,
        ]);

        $resolver->setRequired([
            'cFGroup',
        ]);

        $resolver->setAllowedTypes('cFGroup', \Chill\CustomFieldsBundle\Entity\CustomFieldsGroup::class);

        $this->appendScopeChoicesOptions($resolver);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'chill_reportbundle_report';
    }
}
