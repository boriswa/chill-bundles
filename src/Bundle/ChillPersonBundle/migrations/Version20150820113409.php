<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Migration for adapting the Person Bundle to the 'cahier de charge' :
 *  - update of accompanyingPerid.
 */
class Version20150820113409 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->abortIf(
            'postgresql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('ALTER TABLE accompanying_period RENAME COLUMN openingdate TO date_opening;');
        $this->addSql('ALTER TABLE accompanying_period RENAME COLUMN closingdate TO date_closing;');
        $this->addSql('ALTER TABLE accompanying_period RENAME COLUMN remark TO memo;');
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            'postgresql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('ALTER TABLE accompanying_period RENAME COLUMN date_opening TO openingdate;');
        $this->addSql('ALTER TABLE accompanying_period RENAME COLUMN date_closing TO closingdate;');
        $this->addSql('ALTER TABLE accompanying_period RENAME COLUMN memo TO remark;');
    }
}
