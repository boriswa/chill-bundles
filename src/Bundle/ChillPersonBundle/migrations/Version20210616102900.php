<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Create a view for assembling household, person and addresses.
 */
final class Version20210616102900 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP VIEW view_chill_person_household_address');
    }

    public function getDescription(): string
    {
        return 'Create a view for assembling household, person and addresses';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'CREATE VIEW view_chill_person_household_address AS '.
            'SELECT '.
                'members.person_id AS person_id, '.
                'members.household_id AS household_id, '.
                'members.id AS member_id, '.
                'address.id AS address_id, '.
                'CASE WHEN address.validFrom < members.startDate THEN members.startDate ELSE address.validFrom END AS validFrom, '.
                "CASE WHEN COALESCE(address.validTo, 'infinity') < COALESCE(members.endDate, 'infinity') THEN address.validTo ELSE members.endDate END AS validTo ".
            'FROM chill_person_household_members AS members '.
            'JOIN chill_person_household_to_addresses AS household_to_addr ON household_to_addr.household_id = members.household_id '.
            'JOIN chill_main_address AS address ON household_to_addr.address_id = address.id '.
                'AND daterange(address.validFrom, address.validTo) && daterange(members.startDate, members.endDate) '.
            'WHERE members.sharedhousehold IS TRUE '
        );
    }
}
