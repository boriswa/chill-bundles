<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Correction: copie les infos de email vers contactInfo.
 *
 * Previously, data from contact where stored in 'email' column
 */
final class Version20180820120000 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('UPDATE chill_person_person SET email=contactInfo');
        $this->addSql('UPDATE chill_person_person SET contactInfo=\'\'');
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('UPDATE chill_person_person SET contactInfo=email');
        $this->addSql('UPDATE chill_person_person SET email=\'\'');
    }
}
