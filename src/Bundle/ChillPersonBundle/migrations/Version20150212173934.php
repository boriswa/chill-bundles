<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150212173934 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE accompanying_period RENAME TO person_history_file;');
        $this->addSql('ALTER TABLE person RENAME proxyAccompanyingPeriodOpenState TO proxyHistoryOpenState;');
        $this->addSql('ALTER SEQUENCE accompanying_period_id_seq RENAME TO person_history_file_id_seq;');
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs

        $this->addSql('ALTER TABLE person RENAME proxyHistoryOpenState TO proxyAccompanyingPeriodOpenState;');
        $this->addSql('ALTER TABLE person_history_file RENAME TO accompanying_period;');
        $this->addSql('ALTER SEQUENCE person_history_file_id_seq RENAME TO accompanying_period_id_seq;');
    }
}
