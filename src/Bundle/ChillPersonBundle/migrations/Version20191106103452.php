<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add an index for phonenumber and mobile number.
 */
final class Version20191106103452 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX phonenumber_trgm_idx');
        $this->addSql('DROP INDEX mobilenumber_trgm_idx');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE INDEX phonenumber_trgm_idx
            ON public.chill_person_person USING gin
            (phonenumber gin_trgm_ops)');

        $this->addSql('CREATE INDEX mobilenumber_trgm_idx
            ON public.chill_person_person USING gin
            (mobilenumber gin_trgm_ops)');
    }
}
