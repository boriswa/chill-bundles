<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * drop not null in person phonenumber.
 */
final class Version20211112170027 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_person ALTER mobilenumber DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_person_person ALTER phonenumber DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_person_person ALTER mobilenumber SET DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ALTER phonenumber SET DEFAULT NULL');
    }

    public function getDescription(): string
    {
        return 'Drop not null in person table: set default empty value';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE chill_person_person SET mobilenumber = \'\' WHERE mobilenumber IS NULL');
        $this->addSql('UPDATE chill_person_person SET phonenumber = \'\' WHERE phonenumber IS NULL');
        $this->addSql('ALTER TABLE chill_person_person ALTER mobilenumber SET NOT NULL');
        $this->addSql('ALTER TABLE chill_person_person ALTER phonenumber SET NOT NULL');
        $this->addSql('ALTER TABLE chill_person_person ALTER mobilenumber SET DEFAULT \'\'');
        $this->addSql('ALTER TABLE chill_person_person ALTER phonenumber SET DEFAULT \'\'');
    }
}
