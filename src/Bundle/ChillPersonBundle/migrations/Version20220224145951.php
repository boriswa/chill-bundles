<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add title to AccompanyingPeriodWorkEvaluationDocument.
 */
final class Version20220224145951 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_evaluation_document DROP title');
    }

    public function getDescription(): string
    {
        return 'Add title to AccompanyingPeriodWorkEvaluationDocument';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_evaluation_document ADD title TEXT NOT NULL DEFAULT \'\'');
        $this->addSql('UPDATE chill_person_accompanying_period_work_evaluation_document SET title=name->>\'fr\'
            FROM chill_docgen_template cdt WHERE cdt.id = chill_person_accompanying_period_work_evaluation_document.template_id');
    }
}
