<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Migration for adding maritalstatus to person.
 */
class Version20150812110708 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->abortIf(
            'postgresql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('ALTER TABLE person DROP CONSTRAINT fk_person_marital_status;');
        $this->addSql('ALTER TABLE person DROP COLUMN maritalstatus_id;');
        $this->addSql('DROP TABLE marital_status;');
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            'postgresql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('CREATE TABLE marital_status (
            id character varying(10) NOT NULL,
            name json NOT NULL,
            CONSTRAINT marital_status_pkey PRIMARY KEY (id));');
        $this->addSql('ALTER TABLE person ADD COLUMN maritalstatus_id character varying(10)');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT fk_person_marital_status FOREIGN KEY (maritalstatus_id) REFERENCES marital_status (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION');
    }
}
