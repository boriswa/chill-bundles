<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220427150431 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_evaluation_document ALTER createdAt TYPE DATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_evaluation_document ALTER createdAt DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_evaluation_document ALTER updatedAt TYPE DATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_evaluation_document ALTER updatedAt DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work_evaluation_document.createdat IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work_evaluation_document.updatedat IS \'(DC2Type:date_immutable)\'');
    }

    public function getDescription(): string
    {
        return 'change date to datetime for evaluation documents';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_evaluation_document ALTER createdat TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_evaluation_document ALTER createdat DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_evaluation_document ALTER updatedat TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_evaluation_document ALTER updatedat DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work_evaluation_document.createdAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work_evaluation_document.updatedAt IS \'(DC2Type:datetime_immutable)\'');
    }
}
