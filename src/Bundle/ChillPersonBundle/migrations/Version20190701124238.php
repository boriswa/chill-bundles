<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add a link between accompanying periods and users.
 */
final class Version20190701124238 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period DROP user_id');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period ADD user_id INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE chill_person_accompanying_period ADD CONSTRAINT FK_E260A868A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');
        $this->addSql('CREATE INDEX IDX_E260A868A76ED395 ON chill_person_accompanying_period (user_id);');
    }
}
