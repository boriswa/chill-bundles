<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add link to accompanying period work and persons.
 */
final class Version20210623135043 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE chill_person_accompanying_period_work_person');
    }

    public function getDescription(): string
    {
        return 'Add link to accompanying period work and persons';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE chill_person_accompanying_period_work_person (accompanyingperiodwork_id INT NOT NULL, person_id INT NOT NULL, PRIMARY KEY(accompanyingperiodwork_id, person_id))');
        $this->addSql('CREATE INDEX IDX_615F494CB99F6060 ON chill_person_accompanying_period_work_person (accompanyingperiodwork_id)');
        $this->addSql('CREATE INDEX IDX_615F494C217BBB47 ON chill_person_accompanying_period_work_person (person_id)');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_person ADD CONSTRAINT FK_615F494CB99F6060 FOREIGN KEY (accompanyingperiodwork_id) REFERENCES chill_person_accompanying_period_work (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_person ADD CONSTRAINT FK_615F494C217BBB47 FOREIGN KEY (person_id) REFERENCES chill_person_person (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
