<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add location to accompanying period.
 */
final class Version20210727152826 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period DROP CONSTRAINT FK_E260A868D5213D34');
        $this->addSql('ALTER TABLE chill_person_accompanying_period DROP CONSTRAINT FK_E260A8689B07D6BF');
        $this->addSql('DROP INDEX IDX_E260A868D5213D34');
        $this->addSql('DROP INDEX IDX_E260A8689B07D6BF');
        $this->addSql('ALTER TABLE chill_person_accompanying_period DROP personLocation_id');
        $this->addSql('ALTER TABLE chill_person_accompanying_period DROP addressLocation_id');
    }

    public function getDescription(): string
    {
        return 'Add location to accompanying period';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period ADD personLocation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period ADD addressLocation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period ADD CONSTRAINT FK_E260A868D5213D34 FOREIGN KEY (personLocation_id) REFERENCES chill_person_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period ADD CONSTRAINT FK_E260A8689B07D6BF FOREIGN KEY (addressLocation_id) REFERENCES chill_main_address (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E260A868D5213D34 ON chill_person_accompanying_period (personLocation_id)');
        $this->addSql('CREATE INDEX IDX_E260A8689B07D6BF ON chill_person_accompanying_period (addressLocation_id)');
    }
}
