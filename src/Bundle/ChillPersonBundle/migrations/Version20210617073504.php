<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add new fields to Person entity.
 */
final class Version20210617073504 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_person DROP deathdate');
        $this->addSql('ALTER TABLE chill_person_person DROP maritalStatusDate');
        $this->addSql('ALTER TABLE chill_person_person DROP acceptSMS');
        $this->addSql('ALTER TABLE chill_person_person DROP acceptEmail');
        $this->addSql('ALTER TABLE chill_person_person DROP numberOfChildren');
        $this->addSql('ALTER TABLE chill_person_person DROP genderComment_comment');
        $this->addSql('ALTER TABLE chill_person_person DROP genderComment_userId');
        $this->addSql('ALTER TABLE chill_person_person DROP genderComment_date');
        $this->addSql('ALTER TABLE chill_person_person DROP maritalStatusComment_comment');
        $this->addSql('ALTER TABLE chill_person_person DROP maritalStatusComment_userId');
        $this->addSql('ALTER TABLE chill_person_person DROP maritalStatusComment_date');
    }

    public function getDescription(): string
    {
        return 'Add new fields to Person entity';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_person ADD deathdate DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ADD maritalStatusDate DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ADD acceptSMS BOOLEAN DEFAULT false NOT NULL');
        $this->addSql('ALTER TABLE chill_person_person ADD acceptEmail BOOLEAN DEFAULT false NOT NULL');
        $this->addSql('ALTER TABLE chill_person_person ADD numberOfChildren INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ADD genderComment_comment TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ADD genderComment_userId INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ADD genderComment_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ADD maritalStatusComment_comment TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ADD maritalStatusComment_userId INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ADD maritalStatusComment_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
    }
}
