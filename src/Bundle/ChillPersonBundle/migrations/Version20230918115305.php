<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add isCanceledAccompanyingPeriod property to ClosingMotive.
 */
final class Version20230918115305 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add isCanceledAccompanyingPeriod property to ClosingMotive';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_closingmotive ADD isCanceledAccompanyingPeriod BOOLEAN DEFAULT FALSE NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_closingmotive DROP isCanceledAccompanyingPeriod');
    }
}
