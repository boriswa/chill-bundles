<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Create tables for the entites AccomanyingPeriodWork, AccomanyingPeriodWorkGoal & Social/WorkEvaluation.
 */
final class Version20210427125700 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_result DROP CONSTRAINT FK_46E95929B99F6060');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_third_party DROP CONSTRAINT FK_83B57B86B99F6060');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_goal DROP CONSTRAINT FK_911B88FEC55C1209');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_goal_result DROP CONSTRAINT FK_3E37D1F37B14AD03');
        $this->addSql('DROP SEQUENCE chill_person_accompanying_period_work_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_person_accompanying_period_work_goal_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_person_social_work_evaluation_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_person_accompanying_period_work');
        $this->addSql('DROP TABLE chill_person_accompanying_period_work_result');
        $this->addSql('DROP TABLE chill_person_accompanying_period_work_third_party');
        $this->addSql('DROP TABLE chill_person_accompanying_period_work_goal');
        $this->addSql('DROP TABLE chill_person_accompanying_period_work_goal_result');
        $this->addSql('DROP TABLE chill_person_social_work_evaluation');
    }

    public function getDescription(): string
    {
        return 'Create tables for the entites AccomanyingPeriodWork, AccomanyingPeriodWorkGoal & Social/WorkEvaluation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_person_accompanying_period_work_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_person_accompanying_period_work_goal_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_person_social_work_evaluation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_person_accompanying_period_work (id INT NOT NULL, note TEXT NOT NULL, createdAt TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, startDate TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, endDate TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, createdAutomatically BOOLEAN NOT NULL, createdAutomaticallyReason TEXT NOT NULL, accompanyingPeriod_id INT DEFAULT NULL, socialAction_id INT DEFAULT NULL, createdBy_id INT NOT NULL, handlingThierParty_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B694FB3D7FA8EF0 ON chill_person_accompanying_period_work (accompanyingPeriod_id)');
        $this->addSql('CREATE INDEX IDX_B694FB3BF32A3DA ON chill_person_accompanying_period_work (socialAction_id)');
        $this->addSql('CREATE INDEX IDX_B694FB33174800F ON chill_person_accompanying_period_work (createdBy_id)');
        $this->addSql('CREATE INDEX IDX_B694FB382B35EA7 ON chill_person_accompanying_period_work (handlingThierParty_id)');
        $this->addSql('CREATE TABLE chill_person_accompanying_period_work_result (accompanyingperiodwork_id INT NOT NULL, result_id INT NOT NULL, PRIMARY KEY(accompanyingperiodwork_id, result_id))');
        $this->addSql('CREATE INDEX IDX_46E95929B99F6060 ON chill_person_accompanying_period_work_result (accompanyingperiodwork_id)');
        $this->addSql('CREATE INDEX IDX_46E959297A7B643 ON chill_person_accompanying_period_work_result (result_id)');
        $this->addSql('CREATE TABLE chill_person_accompanying_period_work_third_party (accompanyingperiodwork_id INT NOT NULL, thirdparty_id INT NOT NULL, PRIMARY KEY(accompanyingperiodwork_id, thirdparty_id))');
        $this->addSql('CREATE INDEX IDX_83B57B86B99F6060 ON chill_person_accompanying_period_work_third_party (accompanyingperiodwork_id)');
        $this->addSql('CREATE INDEX IDX_83B57B86C7D3A8E6 ON chill_person_accompanying_period_work_third_party (thirdparty_id)');
        $this->addSql('CREATE TABLE chill_person_accompanying_period_work_goal (id INT NOT NULL, goal_id INT DEFAULT NULL, note TEXT NOT NULL, accompanyingPeriodWork_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_911B88FEC55C1209 ON chill_person_accompanying_period_work_goal (accompanyingPeriodWork_id)');
        $this->addSql('CREATE INDEX IDX_911B88FE667D1AFE ON chill_person_accompanying_period_work_goal (goal_id)');
        $this->addSql('CREATE TABLE chill_person_accompanying_period_work_goal_result (accompanyingperiodworkgoal_id INT NOT NULL, result_id INT NOT NULL, PRIMARY KEY(accompanyingperiodworkgoal_id, result_id))');
        $this->addSql('CREATE INDEX IDX_3E37D1F37B14AD03 ON chill_person_accompanying_period_work_goal_result (accompanyingperiodworkgoal_id)');
        $this->addSql('CREATE INDEX IDX_3E37D1F37A7B643 ON chill_person_accompanying_period_work_goal_result (result_id)');
        $this->addSql('CREATE TABLE chill_person_social_work_evaluation (id INT NOT NULL, title JSONB NOT NULL, delay INTERVAL NOT NULL, notificationDelay INTERVAL NOT NULL, socialAction_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2E23F3FEBF32A3DA ON chill_person_social_work_evaluation (socialAction_id)');
        $this->addSql('COMMENT ON COLUMN chill_person_social_work_evaluation.delay IS \'(DC2Type:dateinterval)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_social_work_evaluation.notificationDelay IS \'(DC2Type:dateinterval)\'');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ADD CONSTRAINT FK_B694FB3D7FA8EF0 FOREIGN KEY (accompanyingPeriod_id) REFERENCES chill_person_accompanying_period (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ADD CONSTRAINT FK_B694FB3BF32A3DA FOREIGN KEY (socialAction_id) REFERENCES chill_person_social_action (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ADD CONSTRAINT FK_B694FB33174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ADD CONSTRAINT FK_B694FB382B35EA7 FOREIGN KEY (handlingThierParty_id) REFERENCES chill_3party.third_party (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_result ADD CONSTRAINT FK_46E95929B99F6060 FOREIGN KEY (accompanyingperiodwork_id) REFERENCES chill_person_accompanying_period_work (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_result ADD CONSTRAINT FK_46E959297A7B643 FOREIGN KEY (result_id) REFERENCES chill_person_social_work_result (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_third_party ADD CONSTRAINT FK_83B57B86B99F6060 FOREIGN KEY (accompanyingperiodwork_id) REFERENCES chill_person_accompanying_period_work (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_third_party ADD CONSTRAINT FK_83B57B86C7D3A8E6 FOREIGN KEY (thirdparty_id) REFERENCES chill_3party.third_party (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_goal ADD CONSTRAINT FK_911B88FEC55C1209 FOREIGN KEY (accompanyingPeriodWork_id) REFERENCES chill_person_accompanying_period_work (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_goal ADD CONSTRAINT FK_911B88FE667D1AFE FOREIGN KEY (goal_id) REFERENCES chill_person_social_work_goal (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_goal_result ADD CONSTRAINT FK_3E37D1F37B14AD03 FOREIGN KEY (accompanyingperiodworkgoal_id) REFERENCES chill_person_accompanying_period_work_goal (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_goal_result ADD CONSTRAINT FK_3E37D1F37A7B643 FOREIGN KEY (result_id) REFERENCES chill_person_social_work_result (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_social_work_evaluation ADD CONSTRAINT FK_2E23F3FEBF32A3DA FOREIGN KEY (socialAction_id) REFERENCES chill_person_social_action (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
