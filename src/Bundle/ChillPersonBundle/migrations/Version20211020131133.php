<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Validation added to accompanying period resources and accompanying period.
 */
final class Version20211020131133 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX person_unique');
        $this->addSql('DROP INDEX thirdparty_unique');
    }

    public function getDescription(): string
    {
        return 'Validation added to accompanying period resources and accompanying period.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE UNIQUE INDEX person_unique ON chill_person_accompanying_period_resource (person_id, accompanyingperiod_id) WHERE person_id IS NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX thirdparty_unique ON chill_person_accompanying_period_resource (thirdparty_id, accompanyingperiod_id) WHERE thirdparty_id IS NOT NULL');
    }
}
