<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211213150253 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period DROP CONSTRAINT FK_E260A868B0804E90');
        $this->addSql('DROP INDEX IDX_E260A868B0804E90');
        $this->addSql('ALTER TABLE chill_person_accompanying_period RENAME COLUMN pinnedcomment_id TO initialcomment_id');
        $this->addSql('ALTER TABLE chill_person_accompanying_period ADD CONSTRAINT fk_e260a8683111d50b FOREIGN KEY (initialcomment_id) REFERENCES chill_person_accompanying_period_comment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_e260a8683111d50b ON chill_person_accompanying_period (initialcomment_id)');
    }

    public function getDescription(): string
    {
        return 'rename initialComment attribute to pinnedComment';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period DROP CONSTRAINT fk_e260a8683111d50b');
        $this->addSql('DROP INDEX idx_e260a8683111d50b');
        $this->addSql('ALTER TABLE chill_person_accompanying_period RENAME COLUMN initialcomment_id TO pinnedcomment_id');
        $this->addSql('ALTER TABLE chill_person_accompanying_period ADD CONSTRAINT FK_E260A868B0804E90 FOREIGN KEY (pinnedcomment_id) REFERENCES chill_person_accompanying_period_comment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E260A868B0804E90 ON chill_person_accompanying_period (pinnedcomment_id)');
    }
}
