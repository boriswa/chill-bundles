<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add location to AccompanyingPeriod.
 */
final class Version20211223150721 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period DROP CONSTRAINT FK_E260A86830F20868');
        $this->addSql('DROP INDEX IDX_E260A86830F20868');
        $this->addSql('ALTER TABLE chill_person_accompanying_period DROP administrativeLocation_id');
    }

    public function getDescription(): string
    {
        return 'Add location to AccompanyingPeriod';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period ADD administrativeLocation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period ADD CONSTRAINT FK_E260A86830F20868 FOREIGN KEY (administrativeLocation_id) REFERENCES chill_main_location (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E260A86830F20868 ON chill_person_accompanying_period (administrativeLocation_id)');
    }
}
