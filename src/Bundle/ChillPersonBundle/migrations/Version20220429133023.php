<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220429133023 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE "accompanying_periods_scopes"
            DROP CONSTRAINT "fk_87c4eab032a7a428",
            ADD CONSTRAINT "fk_87c4eab032a7a428" FOREIGN KEY (accompanying_period_id) REFERENCES chill_person_accompanying_period(id)
            ');
    }

    public function getDescription(): string
    {
        return 'apply CASCADE DELETE on entity related to accompanying periods';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE "accompanying_periods_scopes"
            DROP CONSTRAINT "fk_87c4eab032a7a428",
            ADD CONSTRAINT "fk_87c4eab032a7a428" FOREIGN KEY (accompanying_period_id) REFERENCES chill_person_accompanying_period(id) ON DELETE CASCADE
            ');
    }
}
