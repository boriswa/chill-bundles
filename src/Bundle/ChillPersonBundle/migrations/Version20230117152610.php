<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230117152610 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE country ALTER name TYPE JSON');
        $this->addSql('ALTER TABLE country ALTER name SET DEFAULT  \'[]\'::json');
        $this->addSql('ALTER TABLE country ALTER name DROP NOT NULL');
        $this->addSql('COMMENT ON COLUMN country.name IS \'(DC2Type:simple_array)\'');

        $this->addSql('ALTER TABLE chill_person_household_composition_type ALTER label TYPE JSON');
        $this->addSql('ALTER TABLE chill_person_household_composition_type ALTER label SET DEFAULT  \'[]\'::json');
        $this->addSql('ALTER TABLE chill_person_household_composition_type ALTER label DROP NOT NULL');
        $this->addSql('COMMENT ON COLUMN chill_person_household_composition_type.label IS \'(DC2Type:simple_array)\'');
    }

    public function getDescription(): string
    {
        return 'convert json fields to jsonb';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_household_composition_type ALTER label TYPE JSONB');
        $this->addSql('ALTER TABLE chill_person_household_composition_type ALTER label SET DEFAULT  \'[]\'::jsonb');
        $this->addSql('ALTER TABLE chill_person_household_composition_type ALTER label DROP NOT NULL');
        $this->addSql('COMMENT ON COLUMN chill_person_household_composition_type.label IS \'(DC2Type:json)\'');

        $this->addSql('ALTER TABLE country ALTER name TYPE JSONB');
        $this->addSql('ALTER TABLE country ALTER name SET DEFAULT  \'[]\'::jsonb');
        $this->addSql('ALTER TABLE country ALTER name DROP NOT NULL');
        $this->addSql('COMMENT ON COLUMN country.name IS \'(DC2Type:json)\'');
    }
}
