<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add document to AccompanyingPeriodWorkEvaluation.
 */
final class Version20210730094514 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN chill_calendar.calendar_range.startdate IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_calendar.calendar_range.enddate IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('DROP TABLE chill_person_accompanying_period_work_evaluation_document');
        $this->addSql('DROP SEQUENCE chill_person_accompanying_period_work_eval_doc_id_seq');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work DROP CONSTRAINT FK_B694FB365FF1AEC');
        $this->addSql('DROP INDEX IDX_B694FB365FF1AEC');
    }

    public function getDescription(): string
    {
        return 'add documents to AccompanyingPeriodWorkEvaluation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_person_accompanying_period_work_eval_doc_id_seq INCREMENT BY 1 MINVALUE 1 START 1000');
        $this->addSql('CREATE TABLE chill_person_accompanying_period_work_evaluation_document (id INT NOT NULL, createdAt DATE DEFAULT NULL, updatedAt DATE DEFAULT NULL, accompanyingPeriodWorkEvaluation_id INT DEFAULT NULL, createdBy_id INT DEFAULT NULL, updatedBy_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_33EC9229836F75B8 ON chill_person_accompanying_period_work_evaluation_document (accompanyingPeriodWorkEvaluation_id)');
        $this->addSql('CREATE INDEX IDX_33EC92293174800F ON chill_person_accompanying_period_work_evaluation_document (createdBy_id)');
        $this->addSql('CREATE INDEX IDX_33EC922965FF1AEC ON chill_person_accompanying_period_work_evaluation_document (updatedBy_id)');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work_evaluation_document.createdAt IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work_evaluation_document.updatedAt IS \'(DC2Type:date_immutable)\'');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_evaluation_document ADD CONSTRAINT FK_33EC9229836F75B8 FOREIGN KEY (accompanyingPeriodWorkEvaluation_id) REFERENCES chill_person_accompanying_period_work_evaluation (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_evaluation_document ADD CONSTRAINT FK_33EC92293174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_evaluation_document ADD CONSTRAINT FK_33EC922965FF1AEC FOREIGN KEY (updatedBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work.startDate IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work.endDate IS \'(DC2Type:date_immutable)\'');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ADD CONSTRAINT FK_B694FB365FF1AEC FOREIGN KEY (updatedBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_B694FB365FF1AEC ON chill_person_accompanying_period_work (updatedBy_id)');
    }
}
