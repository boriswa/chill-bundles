<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add createdAt, createdBy, updatedAt, updatedBy fields on Person.
 */
final class Version20210618080702 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_person DROP CONSTRAINT FK_BF210A143174800F');
        $this->addSql('ALTER TABLE chill_person_person DROP CONSTRAINT FK_BF210A1465FF1AEC');
        $this->addSql('DROP INDEX IDX_BF210A143174800F');
        $this->addSql('DROP INDEX IDX_BF210A1465FF1AEC');
        $this->addSql('ALTER TABLE chill_person_person DROP createdAt');
        $this->addSql('ALTER TABLE chill_person_person DROP updatedAt');
        $this->addSql('ALTER TABLE chill_person_person DROP createdBy_id');
        $this->addSql('ALTER TABLE chill_person_person DROP updatedBy_id');
        $this->addSql('COMMENT ON COLUMN chill_person_person.deathdate IS NULL');
    }

    public function getDescription(): string
    {
        return 'Add createdAt, createdBy, updatedAt, updatedBy fields on Person';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_person ADD createdAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ADD updatedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ADD createdBy_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ADD updatedBy_id INT DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN chill_person_person.deathdate IS \'(DC2Type:date_immutable)\'');
        $this->addSql('ALTER TABLE chill_person_person ADD CONSTRAINT FK_BF210A143174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_person ADD CONSTRAINT FK_BF210A1465FF1AEC FOREIGN KEY (updatedBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_BF210A143174800F ON chill_person_person (createdBy_id)');
        $this->addSql('CREATE INDEX IDX_BF210A1465FF1AEC ON chill_person_person (updatedBy_id)');
    }
}
