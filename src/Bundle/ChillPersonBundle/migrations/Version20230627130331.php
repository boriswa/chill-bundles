<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230627130331 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add constraint to force unicity on chill_person_accompanying_period_user_history';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_user_history
        ADD CONSTRAINT acc_period_user_history_not_overlaps
        EXCLUDE USING GIST (accompanyingperiod_id with =, tsrange(startdate, enddate) with &&)
        DEFERRABLE INITIALLY DEFERRED');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_user_history DROP CONSTRAINT acc_period_user_history_not_overlaps');
    }
}
