<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Specify ON DELETE behaviour to handle deletion of parents in associated tables.
 */
final class Version20210525211214 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_comment DROP CONSTRAINT fk_cd960ef3d7fa8ef0');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_comment ADD CONSTRAINT fk_cd960ef3d7fa8ef0 FOREIGN KEY (accompanyingperiod_id) REFERENCES chill_person_accompanying_period (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function getDescription(): string
    {
        return 'Specify ON DELETE behaviour to handle deletion of parents in associated tables';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_comment DROP CONSTRAINT FK_CD960EF3D7FA8EF0');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_comment ADD CONSTRAINT FK_CD960EF3D7FA8EF0 FOREIGN KEY (accompanyingPeriod_id) REFERENCES chill_person_accompanying_period (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
