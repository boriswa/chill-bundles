<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * change model: an evaluation may be linked with multiple social actions.
 */
final class Version20211213203147 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE chill_person_social_work_evaluation_action');
        $this->addSql('ALTER TABLE chill_person_social_work_evaluation ADD socialaction_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_social_work_evaluation ADD CONSTRAINT fk_2e23f3febf32a3da FOREIGN KEY (socialaction_id) REFERENCES chill_person_social_action (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_2e23f3febf32a3da ON chill_person_social_work_evaluation (socialaction_id)');
    }

    public function getDescription(): string
    {
        return 'an evaluation may be linked with multiple social actions';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE chill_person_social_work_evaluation_action (evaluation_id INT NOT NULL, socialaction_id INT NOT NULL, PRIMARY KEY(evaluation_id, socialaction_id))');
        $this->addSql('CREATE INDEX IDX_DF34CCFB456C5646 ON chill_person_social_work_evaluation_action (evaluation_id)');
        $this->addSql('CREATE INDEX IDX_DF34CCFB3DC32179 ON chill_person_social_work_evaluation_action (socialaction_id)');
        $this->addSql('ALTER TABLE chill_person_social_work_evaluation_action ADD CONSTRAINT FK_DF34CCFB456C5646 FOREIGN KEY (evaluation_id) REFERENCES chill_person_social_work_evaluation (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_social_work_evaluation_action ADD CONSTRAINT FK_DF34CCFB3DC32179 FOREIGN KEY (socialaction_id) REFERENCES chill_person_social_action (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('INSERT INTO chill_person_social_work_evaluation_action (evaluation_id, socialaction_id) '.
            'SELECT id, socialaction_ID FROM chill_person_social_work_evaluation');

        $this->addSql('ALTER TABLE chill_person_social_work_evaluation DROP CONSTRAINT fk_2e23f3febf32a3da');
        $this->addSql('DROP INDEX idx_2e23f3febf32a3da');
        $this->addSql('ALTER TABLE chill_person_social_work_evaluation DROP socialaction_id');
    }
}
