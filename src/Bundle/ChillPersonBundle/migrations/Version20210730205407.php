<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * add nullable fields to social work evaluation.
 */
final class Version20210730205407 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_social_work_evaluation ALTER delay SET NOT NULL');
        $this->addSql('ALTER TABLE chill_person_social_work_evaluation ALTER notificationDelay SET NOT NULL');
    }

    public function getDescription(): string
    {
        return 'add nullable fields to social work evaluation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_social_work_evaluation ALTER delay DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_person_social_work_evaluation ALTER notificationdelay DROP NOT NULL');
    }
}
