<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Create Social action, Social Issue, Work Goal and Work Result.
 */
final class Version20210426145930 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_social_action DROP CONSTRAINT FK_B7ABFAB8727ACA70');
        $this->addSql('ALTER TABLE chill_person_social_action_goal DROP CONSTRAINT FK_163CA4DD3DC32179');
        $this->addSql('ALTER TABLE chill_person_social_action_result DROP CONSTRAINT FK_CA98C58C3DC32179');
        $this->addSql('ALTER TABLE chill_person_social_action DROP CONSTRAINT FK_B7ABFAB85E7AA58C');
        $this->addSql('ALTER TABLE chill_person_social_issue DROP CONSTRAINT FK_7A484DAE727ACA70');
        $this->addSql('ALTER TABLE chill_person_social_action_goal DROP CONSTRAINT FK_163CA4DD667D1AFE');
        $this->addSql('ALTER TABLE chill_person_social_work_goal_result DROP CONSTRAINT FK_F3BAEEA9667D1AFE');
        $this->addSql('ALTER TABLE chill_person_social_action_result DROP CONSTRAINT FK_CA98C58C7A7B643');
        $this->addSql('ALTER TABLE chill_person_social_work_goal_result DROP CONSTRAINT FK_F3BAEEA97A7B643');
        $this->addSql('DROP SEQUENCE chill_person_social_action_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_person_social_issue_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_person_social_work_goal_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_person_social_work_result_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_person_social_action');
        $this->addSql('DROP TABLE chill_person_social_action_goal');
        $this->addSql('DROP TABLE chill_person_social_action_result');
        $this->addSql('DROP TABLE chill_person_social_issue');
        $this->addSql('DROP TABLE chill_person_social_work_goal');
        $this->addSql('DROP TABLE chill_person_social_work_goal_result');
        $this->addSql('DROP TABLE chill_person_social_work_result');
    }

    public function getDescription(): string
    {
        return 'Create Social action, Social Issue, Work Goal and Work Result';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_person_social_action_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_person_social_issue_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_person_social_work_goal_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_person_social_work_result_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_person_social_action (id INT NOT NULL, issue_id INT DEFAULT NULL, parent_id INT DEFAULT NULL, desactivationDate TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, defaultNotificationDelay INTERVAL NOT NULL, title JSONB NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B7ABFAB85E7AA58C ON chill_person_social_action (issue_id)');
        $this->addSql('CREATE INDEX IDX_B7ABFAB8727ACA70 ON chill_person_social_action (parent_id)');
        $this->addSql('COMMENT ON COLUMN chill_person_social_action.defaultNotificationDelay IS \'(DC2Type:dateinterval)\'');
        $this->addSql('CREATE TABLE chill_person_social_action_goal (socialaction_id INT NOT NULL, goal_id INT NOT NULL, PRIMARY KEY(socialaction_id, goal_id))');
        $this->addSql('CREATE INDEX IDX_163CA4DD3DC32179 ON chill_person_social_action_goal (socialaction_id)');
        $this->addSql('CREATE INDEX IDX_163CA4DD667D1AFE ON chill_person_social_action_goal (goal_id)');
        $this->addSql('CREATE TABLE chill_person_social_action_result (socialaction_id INT NOT NULL, result_id INT NOT NULL, PRIMARY KEY(socialaction_id, result_id))');
        $this->addSql('CREATE INDEX IDX_CA98C58C3DC32179 ON chill_person_social_action_result (socialaction_id)');
        $this->addSql('CREATE INDEX IDX_CA98C58C7A7B643 ON chill_person_social_action_result (result_id)');
        $this->addSql('CREATE TABLE chill_person_social_issue (id INT NOT NULL, parent_id INT DEFAULT NULL, desactivationDate TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, title JSONB NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7A484DAE727ACA70 ON chill_person_social_issue (parent_id)');
        $this->addSql('CREATE TABLE chill_person_social_work_goal (id INT NOT NULL, title JSONB NOT NULL, desactivationDate TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE chill_person_social_work_goal_result (goal_id INT NOT NULL, result_id INT NOT NULL, PRIMARY KEY(goal_id, result_id))');
        $this->addSql('CREATE INDEX IDX_F3BAEEA9667D1AFE ON chill_person_social_work_goal_result (goal_id)');
        $this->addSql('CREATE INDEX IDX_F3BAEEA97A7B643 ON chill_person_social_work_goal_result (result_id)');
        $this->addSql('CREATE TABLE chill_person_social_work_result (id INT NOT NULL, title JSONB NOT NULL, desactivationDate TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE chill_person_social_action ADD CONSTRAINT FK_B7ABFAB85E7AA58C FOREIGN KEY (issue_id) REFERENCES chill_person_social_issue (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_social_action ADD CONSTRAINT FK_B7ABFAB8727ACA70 FOREIGN KEY (parent_id) REFERENCES chill_person_social_action (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_social_action_goal ADD CONSTRAINT FK_163CA4DD3DC32179 FOREIGN KEY (socialaction_id) REFERENCES chill_person_social_action (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_social_action_goal ADD CONSTRAINT FK_163CA4DD667D1AFE FOREIGN KEY (goal_id) REFERENCES chill_person_social_work_goal (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_social_action_result ADD CONSTRAINT FK_CA98C58C3DC32179 FOREIGN KEY (socialaction_id) REFERENCES chill_person_social_action (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_social_action_result ADD CONSTRAINT FK_CA98C58C7A7B643 FOREIGN KEY (result_id) REFERENCES chill_person_social_work_result (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_social_issue ADD CONSTRAINT FK_7A484DAE727ACA70 FOREIGN KEY (parent_id) REFERENCES chill_person_social_issue (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_social_work_goal_result ADD CONSTRAINT FK_F3BAEEA9667D1AFE FOREIGN KEY (goal_id) REFERENCES chill_person_social_work_goal (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_social_work_goal_result ADD CONSTRAINT FK_F3BAEEA97A7B643 FOREIGN KEY (result_id) REFERENCES chill_person_social_work_result (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
