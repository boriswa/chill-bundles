<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Household;

use Chill\PersonBundle\Entity\Household\Household;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class MembersEditorFactory
{
    public function __construct(private readonly EventDispatcherInterface $eventDispatcher, private readonly ValidatorInterface $validator)
    {
    }

    public function createEditor(?Household $household = null): MembersEditor
    {
        return new MembersEditor($this->validator, $household, $this->eventDispatcher);
    }
}
