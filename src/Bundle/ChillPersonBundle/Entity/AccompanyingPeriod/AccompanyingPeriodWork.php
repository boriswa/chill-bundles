<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\AccompanyingPeriod;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Entity\Embeddable\PrivateCommentEmbeddable;
use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\AccompanyingPeriod\SocialIssueConsistency\AccompanyingPeriodLinkedWithSocialIssuesEntityInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Entity\SocialWork\Result;
use Chill\PersonBundle\Entity\SocialWork\SocialAction;
use Chill\PersonBundle\Entity\SocialWork\SocialIssue;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ReadableCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="chill_person_accompanying_period_work")
 *
 * @Serializer\DiscriminatorMap(
 *     typeProperty="type",
 *     mapping={
 *         "accompanying_period_work": AccompanyingPeriodWork::class
 *     }
 * )
 */
class AccompanyingPeriodWork implements AccompanyingPeriodLinkedWithSocialIssuesEntityInterface, TrackCreationInterface, TrackUpdateInterface
{
    /**
     * @ORM\ManyToOne(targetEntity=AccompanyingPeriod::class)
     *
     * @Serializer\Groups({"read", "read:accompanyingPeriodWork:light"})
     *
     * @Serializer\Context(normalizationContext={"groups": {"read"}}, groups={"read:accompanyingPeriodWork:light"})
     */
    private ?AccompanyingPeriod $accompanyingPeriod = null;

    /**
     * @ORM\OneToMany(
     *     targetEntity=AccompanyingPeriodWorkEvaluation::class,
     *     mappedBy="accompanyingPeriodWork",
     *     cascade={"remove", "persist"},
     *     orphanRemoval=true
     * )
     *
     * @Serializer\Groups({"read", "docgen:read"})
     *
     * @ORM\OrderBy({"startDate": "DESC", "id": "DESC"})
     *
     * @var Collection<AccompanyingPeriodWorkEvaluation>
     *
     * @internal /!\ the serialization for write evaluations is handled in `AccompanyingPeriodWorkDenormalizer`
     */
    private Collection $accompanyingPeriodWorkEvaluations;

    /**
     * @ORM\Column(type="datetime_immutable")
     *
     * @Serializer\Groups({"read", "docgen:read", "read:accompanyingPeriodWork:light"})
     */
    private ?\DateTimeImmutable $createdAt = null;

    /**
     * @ORM\Column(type="boolean")
     *
     * @Serializer\Groups({"read", "docgen:read", "read:accompanyingPeriodWork:light"})
     */
    private bool $createdAutomatically = false;

    /**
     * @ORM\Column(type="text")
     *
     * @Serializer\Groups({"read", "docgen:read", "read:accompanyingPeriodWork:light"})
     */
    private string $createdAutomaticallyReason = '';

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     *
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Groups({"read", "docgen:read", "read:accompanyingPeriodWork:light"})
     */
    private ?User $createdBy = null;

    /**
     * @ORM\Column(type="date_immutable", nullable=true, options={"default": null})
     *
     * @Serializer\Groups({"accompanying_period_work:create"})
     * @Serializer\Groups({"accompanying_period_work:edit"})
     * @Serializer\Groups({"read", "docgen:read", "read:accompanyingPeriodWork:light"})
     *
     * @Assert\GreaterThanOrEqual(propertyPath="startDate",
     *     message="accompanying_course_work.The endDate should be greater or equal than the start date"
     * )
     */
    private ?\DateTimeImmutable $endDate = null;

    /**
     * @var Collection<AccompanyingPeriodWorkGoal>
     *
     * @ORM\OneToMany(
     *     targetEntity=AccompanyingPeriodWorkGoal::class,
     *     mappedBy="accompanyingPeriodWork",
     *     cascade={"persist"},
     *     orphanRemoval=true
     * )
     *
     * @Serializer\Groups({"read", "docgen:read"})
     * @Serializer\Groups({"accompanying_period_work:edit"})
     */
    private Collection $goals;

    /**
     * @ORM\ManyToOne(targetEntity=ThirdParty::class)
     *
     * @Serializer\Groups({"read", "docgen:read"})
     * @Serializer\Groups({"accompanying_period_work:edit"})
     *
     * In schema : traitant
     */
    private ?ThirdParty $handlingThierParty = null;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"read", "docgen:read", "read:accompanyingPeriodWork:light", "read:evaluation:include-work"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="text")
     *
     * @Serializer\Groups({"read", "accompanying_period_work:edit", "docgen:read"})
     */
    private string $note = '';

    /**
     * @var Collection<Person>
     *
     * @ORM\ManyToMany(targetEntity=Person::class)
     *
     * @ORM\JoinTable(name="chill_person_accompanying_period_work_person")
     *
     * @Serializer\Groups({"read", "docgen:read", "read:accompanyingPeriodWork:light"})
     * @Serializer\Groups({"accompanying_period_work:edit"})
     * @Serializer\Groups({"accompanying_period_work:create"})
     */
    private Collection $persons;

    /**
     * @ORM\Embedded(class="Chill\MainBundle\Entity\Embeddable\PrivateCommentEmbeddable", columnPrefix="privateComment_")
     *
     * @Serializer\Groups({"read", "accompanying_period_work:edit"})
     */
    private PrivateCommentEmbeddable $privateComment;

    /**
     * @var Collection<int, AccompanyingPeriodWorkReferrerHistory>
     *
     * @ORM\OneToMany(targetEntity=AccompanyingPeriodWorkReferrerHistory::class, cascade={"persist", "remove"}, mappedBy="accompanyingPeriodWork", orphanRemoval=true)
     */
    private Collection $referrersHistory;

    /**
     * @var Collection<Result>
     *
     * @ORM\ManyToMany(targetEntity=Result::class, inversedBy="accompanyingPeriodWorks")
     *
     * @ORM\JoinTable(name="chill_person_accompanying_period_work_result")
     *
     * @Serializer\Groups({"read", "docgen:read"})
     * @Serializer\Groups({"accompanying_period_work:edit"})
     */
    private Collection $results;

    /**
     * @ORM\ManyToOne(targetEntity=SocialAction::class)
     *
     * @Serializer\Groups({"read", "docgen:read", "read:accompanyingPeriodWork:light"})
     * @Serializer\Groups({"accompanying_period_work:create"})
     *
     * @Serializer\Context(normalizationContext={"groups": {"read"}}, groups={"read:accompanyingPeriodWork:light"})
     */
    private ?SocialAction $socialAction = null;

    /**
     * @ORM\Column(type="date_immutable")
     *
     * @Serializer\Groups({"accompanying_period_work:create"})
     * @Serializer\Groups({"accompanying_period_work:edit"})
     * @Serializer\Groups({"read", "docgen:read", "read:accompanyingPeriodWork:light"})
     */
    private ?\DateTimeImmutable $startDate = null;

    /**
     * @var Collection<ThirdParty>
     *
     * @ORM\ManyToMany(targetEntity=ThirdParty::class)
     *
     * @ORM\JoinTable(name="chill_person_accompanying_period_work_third_party")
     *
     * In schema : intervenants
     *
     * @Serializer\Groups({"read", "docgen:read"})
     * @Serializer\Groups({"accompanying_period_work:edit"})
     */
    private Collection $thirdParties;

    /**
     * @ORM\Column(type="datetime_immutable")
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?\DateTimeImmutable $updatedAt = null;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     *
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?User $updatedBy = null;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default": 1})
     *
     * @Serializer\Groups({"read", "accompanying_period_work:edit"})
     *
     * @ORM\Version
     */
    private int $version = 1;

    public function __construct()
    {
        $this->goals = new ArrayCollection();
        $this->privateComment = new PrivateCommentEmbeddable();
        $this->results = new ArrayCollection();
        $this->thirdParties = new ArrayCollection();
        $this->persons = new ArrayCollection();
        $this->accompanyingPeriodWorkEvaluations = new ArrayCollection();
        $this->referrersHistory = new ArrayCollection();
    }

    public function addAccompanyingPeriodWorkEvaluation(AccompanyingPeriodWorkEvaluation $evaluation): self
    {
        if (!$this->accompanyingPeriodWorkEvaluations->contains($evaluation)) {
            $this->accompanyingPeriodWorkEvaluations[] = $evaluation;
            $evaluation->setAccompanyingPeriodWork($this);
        }

        return $this;
    }

    public function addGoal(AccompanyingPeriodWorkGoal $goal): self
    {
        if (!$this->goals->contains($goal)) {
            $this->goals[] = $goal;
            $goal->setAccompanyingPeriodWork($this);
        }

        return $this;
    }

    public function addPerson(Person $person): self
    {
        if (!$this->persons->contains($person)) {
            $this->persons[] = $person;
        }

        return $this;
    }

    public function addReferrer(User $referrer): self
    {
        if (!$this->getReferrers()->contains($referrer)) {
            $this->referrersHistory[] =
                new AccompanyingPeriodWorkReferrerHistory($this, $referrer, new \DateTimeImmutable('today'));
        }

        return $this;
    }

    public function addResult(Result $result): self
    {
        if (!$this->results->contains($result)) {
            $this->results[] = $result;
        }

        return $this;
    }

    public function addThirdParty(ThirdParty $thirdParty): self
    {
        if (!$this->thirdParties->contains($thirdParty)) {
            $this->thirdParties[] = $thirdParty;
        }

        return $this;
    }

    public function getAccompanyingPeriod(): ?AccompanyingPeriod
    {
        return $this->accompanyingPeriod;
    }

    /**
     * @return Collection<AccompanyingPeriodWorkEvaluation>
     */
    public function getAccompanyingPeriodWorkEvaluations(): Collection
    {
        return $this->accompanyingPeriodWorkEvaluations;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCreatedAutomatically(): ?bool
    {
        return $this->createdAutomatically;
    }

    public function getCreatedAutomaticallyReason(): ?string
    {
        return $this->createdAutomaticallyReason;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    /**
     * @return AccompanyingPeriodWorkGoal[]|Collection
     */
    public function getGoals(): Collection
    {
        return $this->goals;
    }

    public function getHandlingThierParty(): ?ThirdParty
    {
        return $this->handlingThierParty;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function getPersons(): Collection
    {
        return $this->persons;
    }

    public function getPrivateComment(): PrivateCommentEmbeddable
    {
        return $this->privateComment;
    }

    /**
     * @return ReadableCollection<int, User>
     *
     * @Serializer\Groups({"read", "docgen:read", "read:accompanyingPeriodWork:light"})
     * @Serializer\Groups({"accompanying_period_work:edit"})
     * @Serializer\Groups({"accompanying_period_work:create"})
     */
    public function getReferrers(): ReadableCollection
    {
        $users = $this->referrersHistory
            ->filter(fn (AccompanyingPeriodWorkReferrerHistory $h) => null === $h->getEndDate())
            ->map(fn (AccompanyingPeriodWorkReferrerHistory $h) => $h->getUser())
            ->getValues()
        ;

        return new ArrayCollection(array_values($users));
    }

    public function getReferrersHistory(): Collection
    {
        return $this->referrersHistory;
    }

    /**
     * @return Collection<int, Result>
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    public function getSocialAction(): ?SocialAction
    {
        return $this->socialAction;
    }

    public function getSocialIssues(): Collection
    {
        return new ArrayCollection([$this->getSocialAction()->getIssue()]);
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    /**
     * @return Collection|ThirdParty[]
     */
    public function getThirdParties(): Collection
    {
        return $this->thirdParties;
    }

    public function getThirdPartys(): Collection
    {
        return $this->getThirdParties();
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    public function setVersion(int $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function removeAccompanyingPeriodWorkEvaluation(AccompanyingPeriodWorkEvaluation $evaluation): self
    {
        $this->accompanyingPeriodWorkEvaluations
            ->removeElement($evaluation);
        $evaluation->setAccompanyingPeriodWork(null);

        return $this;
    }

    public function removeGoal(AccompanyingPeriodWorkGoal $goal): self
    {
        if ($this->goals->removeElement($goal)) {
            // set the owning side to null (unless already changed)
            if ($goal->getAccompanyingPeriodWork() === $this) {
                $goal->setAccompanyingPeriodWork(null);
            }
        }

        return $this;
    }

    public function removePerson(Person $person): self
    {
        $this->persons->removeElement($person);

        return $this;
    }

    public function removeReferrer(User $referrer): self
    {
        foreach ($this->referrersHistory as $history) {
            if ($history->isOpen() && $referrer === $history->getUser()) {
                $history->setEndDate(new \DateTimeImmutable('today'));

                if ($history->isDateRangeEmpty()) {
                    $history->removeAccompanyingPeriodWork();
                    $this->referrersHistory->removeElement($history);
                }
            }
        }

        return $this;
    }

    public function removeResult(Result $result): self
    {
        $this->results->removeElement($result);

        return $this;
    }

    public function removeSocialIssue(SocialIssue $issue): AccompanyingPeriodLinkedWithSocialIssuesEntityInterface
    {
        $this->getSocialIssues()->removeElement($issue);

        return $this;
    }

    public function removeThirdParty(ThirdParty $thirdParty): self
    {
        $this->thirdParties->removeElement($thirdParty);

        return $this;
    }

    /**
     * Internal: you should use `$accompanyingPeriod->removeWork($work);` or
     * `$accompanyingPeriod->addWork($work);`.
     */
    public function setAccompanyingPeriod(?AccompanyingPeriod $accompanyingPeriod): self
    {
        if (
            $this->accompanyingPeriod instanceof AccompanyingPeriod
            && $accompanyingPeriod !== $this->accompanyingPeriod
        ) {
            throw new \LogicException('A work cannot change accompanyingPeriod');
        }

        $this->accompanyingPeriod = $accompanyingPeriod;

        return $this;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function setCreatedAutomatically(bool $createdAutomatically): self
    {
        $this->createdAutomatically = $createdAutomatically;

        return $this;
    }

    public function setCreatedAutomaticallyReason(string $createdAutomaticallyReason): self
    {
        $this->createdAutomaticallyReason = $createdAutomaticallyReason;

        return $this;
    }

    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function setEndDate(?\DateTimeInterface $endDate = null): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function setHandlingThierParty(?ThirdParty $handlingThierParty): self
    {
        $this->handlingThierParty = $handlingThierParty;

        return $this;
    }

    public function setNote(string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function setPrivateComment(PrivateCommentEmbeddable $privateComment): self
    {
        $this->privateComment->merge($privateComment);

        return $this;
    }

    public function setSocialAction(?SocialAction $socialAction): self
    {
        $this->socialAction = $socialAction;

        return $this;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function setUpdatedAt(\DateTimeInterface $datetime): TrackUpdateInterface
    {
        $this->updatedAt = $datetime;

        return $this;
    }

    public function setUpdatedBy(User $user): TrackUpdateInterface
    {
        $this->updatedBy = $user;

        return $this;
    }
}
