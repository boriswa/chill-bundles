<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\AccompanyingPeriod;

use Chill\PersonBundle\Entity\SocialWork\Goal;
use Chill\PersonBundle\Entity\SocialWork\Result;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="chill_person_accompanying_period_work_goal")
 *
 * @Serializer\DiscriminatorMap(
 *     typeProperty="type",
 *     mapping={
 *         "accompanying_period_work_goal": AccompanyingPeriodWorkGoal::class
 *     }
 * )
 */
class AccompanyingPeriodWorkGoal
{
    /**
     * @ORM\ManyToOne(targetEntity=AccompanyingPeriodWork::class, inversedBy="goals")
     */
    private ?AccompanyingPeriodWork $accompanyingPeriodWork = null;

    /**
     * @ORM\ManyToOne(targetEntity=Goal::class)
     *
     * @Serializer\Groups({"accompanying_period_work:edit"})
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?Goal $goal = null;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="text")
     *
     * @Serializer\Groups({"accompanying_period_work:edit"})
     * @Serializer\Groups({"read"})
     */
    private string $note = '';

    /**
     * @var Collection<Result>
     *
     * @ORM\ManyToMany(targetEntity=Result::class, inversedBy="accompanyingPeriodWorkGoals")
     *
     * @ORM\JoinTable(name="chill_person_accompanying_period_work_goal_result")
     *
     * @Serializer\Groups({"accompanying_period_work:edit"})
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private Collection $results;

    public function __construct()
    {
        $this->results = new ArrayCollection();
    }

    public function addResult(Result $result): self
    {
        if (!$this->results->contains($result)) {
            $this->results[] = $result;
        }

        return $this;
    }

    public function getAccompanyingPeriodWork(): ?AccompanyingPeriodWork
    {
        return $this->accompanyingPeriodWork;
    }

    public function getGoal(): ?Goal
    {
        return $this->goal;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @return Collection|Result[]
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    public function removeResult(Result $result): self
    {
        $this->results->removeElement($result);

        return $this;
    }

    public function setAccompanyingPeriodWork(?AccompanyingPeriodWork $accompanyingPeriodWork): self
    {
        if (
            $this->accompanyingPeriodWork instanceof AccompanyingPeriodWork
            && $accompanyingPeriodWork !== $this->accompanyingPeriodWork
            && null !== $accompanyingPeriodWork
        ) {
            throw new \LogicException('Change accompanying period work is not allowed');
        }

        $this->accompanyingPeriodWork = $accompanyingPeriodWork;

        return $this;
    }

    public function setGoal(?Goal $goal): self
    {
        $this->goal = $goal;

        return $this;
    }

    public function setNote(string $note): self
    {
        $this->note = $note;

        return $this;
    }
}
