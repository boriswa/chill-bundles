<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\AccompanyingPeriod;

use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Doctrine\ORM\Mapping as ORM;

/**
 * Informations about AccompanyingPeriod.
 *
 * This entity allow access to some basic information about the AccompanyingPeriod. It is
 * populated from a SQL view, dynamically build from various sources.
 *
 * Usage:
 *
 * - get the user involved with an accompanying period
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="view_chill_person_accompanying_period_info")
 */
class AccompanyingPeriodInfo
{
    public function __construct(
        /**
         * @var AccompanyingPeriod
         *
         * @ORM\ManyToOne(targetEntity=AccompanyingPeriod::class)
         */
        public readonly AccompanyingPeriod $accompanyingPeriod,

        /**
         * @var string
         *
         * @ORM\Column(type="text")
         *
         * @ORM\Id
         */
        public readonly string $relatedEntity,

        /**
         * @var int
         *
         * @ORM\Column(type="integer")
         *
         * @ORM\Id
         */
        public readonly int $relatedEntityId,

        /**
         * @var User
         *
         * @ORM\ManyToOne(targetEntity=User::class)
         */
        public readonly ?User $user,

        /**
         * @var \DateTimeImmutable
         *
         * @ORM\Column(type="datetime_immutable")
         */
        public readonly \DateTimeImmutable $infoDate,

        /**
         * @var array
         *
         * @ORM\Column(type="json")
         */
        public readonly array $metadata,

        /**
         * @var string
         *
         * @ORM\Column(type="text")
         */
        public readonly string $discriminator,
    ) {
    }
}
