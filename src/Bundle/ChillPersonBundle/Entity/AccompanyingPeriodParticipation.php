<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\DiscriminatorMap;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * AccompanyingPeriodParticipation Class.
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="chill_person_accompanying_period_participation")
 *
 * @DiscriminatorMap(typeProperty="type", mapping={
 *     "accompanying_period_participation": AccompanyingPeriodParticipation::class
 * })
 */
class AccompanyingPeriodParticipation
{
    /**
     * @ORM\Column(type="date", nullable=true)
     *
     * @Groups({"read", "docgen:read"})
     */
    private ?\DateTime $endDate = null;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @Groups({"read", "docgen:read"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="date", nullable=false)
     *
     * @Groups({"read", "docgen:read"})
     */
    private ?\DateTime $startDate = null;

    public function __construct(/**
     * @ORM\ManyToOne(targetEntity=AccompanyingPeriod::class, inversedBy="participations", cascade={"persist"})
     *
     * @ORM\JoinColumn(name="accompanyingperiod_id", referencedColumnName="id", nullable=false)
     */
        private ?AccompanyingPeriod $accompanyingPeriod, /**
     * @ORM\ManyToOne(targetEntity=Person::class, inversedBy="accompanyingPeriodParticipations")
     *
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id", nullable=false)
     *
     * @Groups({"read", "docgen:read"})
     */
        private ?Person $person
    ) {
        $this->startDate = new \DateTime('now');
        $person->getAccompanyingPeriodParticipations()->add($this);
    }

    public function getAccompanyingPeriod(): ?AccompanyingPeriod
    {
        return $this->accompanyingPeriod;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function isOpen(): bool
    {
        return null === $this->endDate;
    }

    public function setAccompanyingPeriod(?AccompanyingPeriod $accompanyingPeriod): self
    {
        $this->accompanyingPeriod = $accompanyingPeriod;

        return $this;
    }

    public function setEndDate(?\DateTime $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    private function checkSameStartEnd()
    {
        if ($this->endDate === $this->startDate) {
            $this->accompanyingPeriod->removeParticipation($this);
        }
    }
}
