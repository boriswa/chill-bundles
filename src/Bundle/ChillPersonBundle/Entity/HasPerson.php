<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity;

/**
 * Interface which applies to entities which are associated to a single person.
 */
interface HasPerson
{
    public function getPerson(): ?Person;

    public function setPerson(?Person $person = null): HasPerson;
}
