<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\SocialWork;

use DateInterval;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="chill_person_social_work_evaluation")
 *
 * @Serializer\DiscriminatorMap(typeProperty="type", mapping={
 *     "social_work_evaluation": Evaluation::class
 * })
 */
class Evaluation
{
    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": true})
     */
    private bool $active = true;

    /**
     * @ORM\Column(type="dateinterval", nullable=true, options={"default": null})
     *
     * @Serializer\Groups({"read"})
     */
    private ?\DateInterval $delay = null;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="dateinterval", nullable=true, options={"default": null})
     *
     * @Serializer\Groups({"read"})
     */
    private ?\DateInterval $notificationDelay = null;

    /**
     * @var Collection<SocialAction>
     *
     * @ORM\ManyToMany(
     *     targetEntity=SocialAction::class,
     *     mappedBy="evaluations"
     * )
     */
    private Collection $socialActions;

    /**
     * @ORM\Column(type="json")
     *
     * @Serializer\Groups({"read", "docgen:read"})
     *
     * @Serializer\Context({"is-translatable": true}, groups={"docgen:read"})
     */
    private array $title = [];

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?string $url = null;

    public function __construct()
    {
        $this->socialActions = new ArrayCollection();
    }

    /**
     * @internal do use @see{SocialAction::addEvaluation}
     */
    public function addSocialAction(SocialAction $socialAction): self
    {
        if (!$this->socialActions->contains($socialAction)) {
            $this->socialActions->add($socialAction);
        }

        return $this;
    }

    public function getDelay(): ?\DateInterval
    {
        return $this->delay;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNotificationDelay(): ?\DateInterval
    {
        return $this->notificationDelay;
    }

    public function getSocialActions(): Collection
    {
        return $this->socialActions;
    }

    public function getTitle(): array
    {
        return $this->title;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @return $this
     *
     * @internal do use @see{SocialAction::removeEvaluation}
     */
    public function removeSocialAction(SocialAction $socialAction): self
    {
        if ($this->socialActions->contains($socialAction)) {
            $this->socialActions->removeElement($socialAction);
        }

        return $this;
    }

    public function setActive(bool $active): Evaluation
    {
        $this->active = $active;

        return $this;
    }

    public function setDelay(?\DateInterval $delay): self
    {
        $this->delay = $delay;

        return $this;
    }

    public function setNotificationDelay(?\DateInterval $notificationDelay): self
    {
        $this->notificationDelay = $notificationDelay;

        return $this;
    }

    public function setTitle(array $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }
}
