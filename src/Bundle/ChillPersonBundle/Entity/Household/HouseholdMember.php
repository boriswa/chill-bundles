<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\Household;

use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 *
 * @ORM\Table(
 *     name="chill_person_household_members"
 * )
 */
class HouseholdMember
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?string $comment = null;

    /**
     * @ORM\Column(type="date_immutable", nullable=true, options={"default": null})
     *
     * @Serializer\Groups({"read", "docgen:read"})
     *
     * @Assert\GreaterThanOrEqual(
     *     propertyPath="startDate",
     *     message="household_membership.The end date must be after start date",
     *     groups={"household_memberships"}
     * )
     */
    private ?\DateTimeImmutable $endDate = null;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private bool $holder = false;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="\Chill\PersonBundle\Entity\Household\Household"
     * )
     *
     * @Assert\Valid(groups={"household_memberships"})
     *
     * @Assert\NotNull(groups={"household_memberships"})
     */
    private ?Household $household = null;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="\Chill\PersonBundle\Entity\Person"
     * )
     *
     * @Serializer\Groups({"read", "docgen:read"})
     *
     * @Serializer\Context({"docgen:person:with-household": false})
     *
     * @Assert\Valid(groups={"household_memberships"})
     *
     * @Assert\NotNull(groups={"household_memberships"})
     */
    private ?Person $person = null;

    /**
     * @ORM\ManyToOne(targetEntity=Position::class)
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?Position $position = null;

    /**
     * @ORM\Column(type="boolean", name="sharedhousehold")
     */
    private bool $shareHousehold = true;

    /**
     * @ORM\Column(type="date_immutable", nullable=true, options={"default": null})
     *
     * @Serializer\Groups({"read", "docgen:read"})
     *
     * @Assert\NotNull(groups={"household_memberships"})
     */
    private ?\DateTimeImmutable $startDate = null;

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getHousehold(): ?Household
    {
        return $this->household;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function getPosition(): ?Position
    {
        return $this->position;
    }

    /**
     * @Serializer\Groups({"read"})
     */
    public function getShareHousehold(): ?bool
    {
        return $this->shareHousehold;
    }

    public function getStartDate(): ?\DateTimeImmutable
    {
        return $this->startDate;
    }

    public function isCurrent(?\DateTimeImmutable $at = null): bool
    {
        $at ??= new \DateTimeImmutable('now');

        return $this->getStartDate() < $at && (
            null === $this->getEndDate() || $this->getEndDate() > $at
        );
    }

    public function isHolder(): bool
    {
        return $this->holder;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function setEndDate(?\DateTimeImmutable $endDate = null): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function setHolder(bool $holder): self
    {
        $this->holder = $holder;

        return $this;
    }

    public function setHousehold(?Household $household): self
    {
        if ($this->household instanceof Household) {
            throw new \LogicException('You cannot change household on a membership');
        }

        $this->household = $household;

        return $this;
    }

    public function setPerson(?Person $person): self
    {
        if ($this->person instanceof Person) {
            throw new \LogicException('You cannot change person on a membership');
        }

        $this->person = $person;
        $this->person->addHouseholdParticipation($this);

        return $this;
    }

    public function setPosition(?Position $position): self
    {
        if ($this->position instanceof Position && $this->position !== $position) {
            throw new \LogicException('The position is already set. You cannot change a position of a membership');
        }

        $this->position = $position;

        if (null !== $position) {
            $this->shareHousehold = $position->getShareHousehold();
        }

        return $this;
    }

    public function setShareHousehold(bool $shareHousehold): self
    {
        $this->shareHousehold = $shareHousehold;

        return $this;
    }

    public function setStartDate(\DateTimeImmutable $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }
}
