<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\Household;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 *
 * @ORM\Table(
 *     name="chill_person_household_composition_type"
 * )
 *
 * @Serializer\DiscriminatorMap(typeProperty="type", mapping={
 *     "household_composition_type": HouseholdCompositionType::class
 * })
 */
class HouseholdCompositionType
{
    /**
     * @ORM\Column(type="boolean")
     */
    private bool $active = true;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="json")
     *
     * @Serializer\Groups({"read", "docgen:read"})
     *
     * @Serializer\Context({"is-translatable": true}, groups={"docgen:read"})
     */
    private array $label = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): array
    {
        return $this->label;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): HouseholdCompositionType
    {
        $this->active = $active;

        return $this;
    }

    public function setLabel(array $label): HouseholdCompositionType
    {
        $this->label = $label;

        return $this;
    }
}
