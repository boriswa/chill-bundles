<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\Household;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateTrait;
use Chill\MainBundle\Entity\Embeddable\CommentEmbeddable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 *
 * @ORM\Table(
 *     name="chill_person_household_composition"
 * )
 *
 * @Serializer\DiscriminatorMap(typeProperty="type", mapping={
 *     "household_composition_type": HouseholdCompositionType::class
 * })
 */
class HouseholdComposition implements TrackCreationInterface, TrackUpdateInterface
{
    use TrackCreationTrait;

    use TrackUpdateTrait;

    /**
     * @ORM\Embedded(class=CommentEmbeddable::class, columnPrefix="comment_")
     */
    private CommentEmbeddable $comment;

    /**
     * @ORM\Column(type="date_immutable", nullable=true, options={"default": null})
     *
     * @Assert\GreaterThanOrEqual(propertyPath="startDate", groups={"Default", "household_composition"})
     *
     * @Serializer\Groups({"docgen:read"})
     */
    private ?\DateTimeImmutable $endDate = null;

    /**
     * @ORM\ManyToOne(targetEntity=Household::class, inversedBy="compositions")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Household $household = null;

    /**
     * @ORM\ManyToOne(targetEntity=HouseholdCompositionType::class)
     *
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Groups({"docgen:read"})
     */
    private ?HouseholdCompositionType $householdCompositionType = null;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": null})
     *
     * @Assert\NotNull
     *
     * @Assert\GreaterThanOrEqual(0, groups={"Default", "household_composition"})
     *
     * @Serializer\Groups({"docgen:read"})
     */
    private ?int $numberOfChildren = null;

    /**
     * @ORM\Column(type="date_immutable", nullable=false)
     *
     * @Assert\NotNull(groups={"Default", "household_composition"})
     *
     * @Serializer\Groups({"docgen:read"})
     */
    private ?\DateTimeImmutable $startDate = null;

    public function __construct()
    {
        $this->comment = new CommentEmbeddable();
    }

    public function getComment(): CommentEmbeddable
    {
        return $this->comment;
    }

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getHousehold(): ?Household
    {
        return $this->household;
    }

    public function getHouseholdCompositionType(): ?HouseholdCompositionType
    {
        return $this->householdCompositionType;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumberOfChildren(): ?int
    {
        return $this->numberOfChildren;
    }

    public function getStartDate(): ?\DateTimeImmutable
    {
        return $this->startDate;
    }

    public function setComment(CommentEmbeddable $comment): HouseholdComposition
    {
        $this->comment = $comment;

        return $this;
    }

    public function setEndDate(?\DateTimeImmutable $endDate): HouseholdComposition
    {
        $this->endDate = $endDate;

        if (null !== $this->household) {
            $this->household->householdCompositionConsistency();
        }

        return $this;
    }

    public function setHousehold(?Household $household): HouseholdComposition
    {
        $this->household = $household;

        return $this;
    }

    public function setHouseholdCompositionType(?HouseholdCompositionType $householdCompositionType): HouseholdComposition
    {
        $this->householdCompositionType = $householdCompositionType;

        return $this;
    }

    public function setNumberOfChildren(?int $numberOfChildren): HouseholdComposition
    {
        $this->numberOfChildren = $numberOfChildren;

        return $this;
    }

    public function setStartDate(?\DateTimeImmutable $startDate): HouseholdComposition
    {
        $this->startDate = $startDate;

        if (null !== $this->household) {
            $this->household->householdCompositionConsistency();
        }

        return $this;
    }
}
