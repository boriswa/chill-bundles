<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\Relationships;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Validator\Constraints\Relationship\RelationshipNoDuplicate;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use RuntimeException;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Serializer\Annotation\DiscriminatorMap;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="chill_person_relationships")
 *
 * @DiscriminatorColumn(name="relation_id", type="integer")
 *
 * @DiscriminatorMap(typeProperty="type", mapping={
 *     "relationship": Relationship::class
 * })
 *
 * @RelationshipNoDuplicate
 */
class Relationship implements TrackCreationInterface, TrackUpdateInterface
{
    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private ?\DateTimeImmutable $createdAt = null;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private ?User $createdBy = null;

    /**
     * @ORM\ManyToOne(targetEntity=Person::class)
     *
     * @ORM\JoinColumn(nullable=false)
     *
     * @Assert\NotNull
     *
     * @Serializer\Groups({"read", "write"})
     */
    private ?Person $fromPerson = null;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"read"})
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity=Relation::class)
     *
     * @ORM\JoinColumn(nullable=false, name="relation_id", referencedColumnName="id")
     *
     * @Assert\NotNull
     *
     * @Serializer\Groups({"read", "write"})
     */
    private ?Relation $relation = null;

    /**
     * @ORM\Column(type="boolean")
     *
     * @Assert\Type(
     *     type="bool",
     *     message="This must be of type boolean"
     * )
     *
     * @Serializer\Groups({"read", "write"})
     */
    private bool $reverse;

    /**
     * @ORM\ManyToOne(targetEntity=Person::class)
     *
     * @ORM\JoinColumn(nullable=false)
     *
     * @Assert\NotNull
     *
     * @Serializer\Groups({"read", "write"})
     */
    private ?Person $toPerson = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $updatedAt = null;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private ?User $updatedBy = null;

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function getFromPerson(): ?Person
    {
        return $this->fromPerson;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Return the opposite person of the @see{counterpart} person.
     *
     * this is the from person if the given is associated to the To,
     * or the To person otherwise.
     *
     * @throw RuntimeException if the counterpart is neither in the from or to person
     */
    public function getOpposite(Person $counterpart): Person
    {
        if ($this->fromPerson !== $counterpart && $this->toPerson !== $counterpart) {
            // during tests, comparing using equality does not work. We have to compare the ids
            if (
                ($this->fromPerson->getId() === $counterpart->getId() && $this->toPerson->getId() === $counterpart->getId())
                || null === $counterpart->getId()
            ) {
                throw new \RuntimeException(sprintf('the counterpart is neither the from nor to person for this relationship, expecting counterpart from %d and available %d and %d', $counterpart->getId(), $this->getFromPerson()->getId(), $this->getToPerson()->getId()));
            }
        }

        if ($this->fromPerson === $counterpart || $this->fromPerson->getId() === $counterpart->getId()) {
            return $this->toPerson;
        }

        return $this->fromPerson;
    }

    public function getRelation(): ?Relation
    {
        return $this->relation;
    }

    public function getReverse(): ?bool
    {
        return $this->reverse;
    }

    public function getToPerson(): ?Person
    {
        return $this->toPerson;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function setCreatedBy(?User $user): self
    {
        $this->createdBy = $user;

        return $this;
    }

    public function setFromPerson(?Person $fromPerson): self
    {
        $this->fromPerson = $fromPerson;

        return $this;
    }

    public function setRelation(?Relation $relation): self
    {
        $this->relation = $relation;

        return $this;
    }

    public function setReverse(bool $reverse): self
    {
        $this->reverse = $reverse;

        return $this;
    }

    public function setToPerson(?Person $toPerson): self
    {
        $this->toPerson = $toPerson;

        return $this;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function setUpdatedBy(?User $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }
}
