<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\Relationships;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Serializer\Annotation\DiscriminatorMap;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="chill_person_relations")
 *
 * @DiscriminatorMap(typeProperty="type", mapping={
 *     "relation": Relation::class
 * })
 */
class Relation
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @Serializer\Groups({"read"})
     */
    private bool $isActive = true;

    /**
     * @ORM\Column(type="json", nullable=true)
     *
     * @Serializer\Groups({"read"})
     *
     * @Serializer\Context({"is-translatable": true}, groups={"docgen:read"})
     */
    private array $reverseTitle = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     *
     * @Serializer\Groups({"read"})
     *
     * @Serializer\Context({"is-translatable": true}, groups={"docgen:read"})
     */
    private array $title = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    public function getReverseTitle(): ?array
    {
        return $this->reverseTitle;
    }

    public function getTitle(): ?array
    {
        return $this->title;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function setReverseTitle(?array $reverseTitle): self
    {
        $this->reverseTitle = $reverseTitle;

        return $this;
    }

    public function setTitle(?array $title): self
    {
        $this->title = $title;

        return $this;
    }
}
