<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Controller;

use Chill\MainBundle\CRUD\Controller\ApiController;
use Chill\MainBundle\Serializer\Model\Collection;
use Chill\PersonBundle\Entity\SocialWork\Goal;
use Chill\PersonBundle\Entity\SocialWork\SocialAction;
use Chill\PersonBundle\Repository\SocialWork\ResultRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SocialWorkResultApiController extends ApiController
{
    public function __construct(private readonly ResultRepository $resultRepository)
    {
    }

    public function listByGoal(Request $request, Goal $goal): Response
    {
        $totalItems = $this->resultRepository->countByGoal($goal);
        $paginator = $this->getPaginatorFactory()->create($totalItems);

        $entities = $this->resultRepository->findByGoal(
            $goal,
            ['id' => 'ASC'],
            $paginator->getItemsPerPage(),
            $paginator->getCurrentPageFirstItemNumber()
        );

        $model = new Collection($entities, $paginator);

        return $this->json($model, Response::HTTP_OK, [], ['groups' => ['read']]);
    }

    public function listBySocialAction(Request $request, SocialAction $action): Response
    {
        $totalItems = $this->resultRepository->countBySocialActionWithDescendants($action);
        $paginator = $this->getPaginatorFactory()->create($totalItems);

        $entities = $this->resultRepository->findBySocialActionWithDescendants(
            $action,
            ['id' => 'ASC'],
            $paginator->getItemsPerPage(),
            $paginator->getCurrentPageFirstItemNumber()
        );

        $model = new Collection($entities, $paginator);

        return $this->json($model, Response::HTTP_OK, [], ['groups' => ['read']]);
    }
}
