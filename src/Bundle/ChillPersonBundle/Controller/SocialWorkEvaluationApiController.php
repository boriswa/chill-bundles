<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Controller;

use Chill\MainBundle\Pagination\PaginatorFactory;
use Chill\MainBundle\Serializer\Model\Collection;
use Chill\PersonBundle\Entity\SocialWork\Evaluation;
use Chill\PersonBundle\Entity\SocialWork\SocialAction;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SocialWorkEvaluationApiController extends AbstractController
{
    public function __construct(private readonly PaginatorFactory $paginatorFactory)
    {
    }

    /**
     * @Route("/api/1.0/person/social-work/evaluation/by-social-action/{action_id}.json",
     *     name="chill_person_evaluation_index_by_social_action",
     *     requirements={
     *         "_format": "json"
     *     }
     * )
     *
     * @ParamConverter("action", options={"id": "action_id"})
     */
    public function listEvaluationBySocialAction(SocialAction $action): Response
    {
        $evaluations = $action->getEvaluations()->filter(static fn (Evaluation $eval) => $eval->isActive());

        $pagination = $this->paginatorFactory->create($evaluations->count());

        $evaluations = $evaluations->slice(
            $pagination->getCurrentPageFirstItemNumber(),
            $pagination->getItemsPerPage()
        );
        $collection = new Collection($evaluations, $pagination);

        return $this->json($collection, Response::HTTP_OK, [], ['groups' => ['read']]);
    }
}
