<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Serializer\Normalizer;

use Chill\MainBundle\Repository\Workflow\EntityWorkflowRepository;
use Chill\MainBundle\Workflow\Helper\MetadataExtractor;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWork;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluation;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocument;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Workflow\Registry;

class AccompanyingPeriodWorkNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    private const IGNORE_WORK = 'ignore:work';

    public function __construct(private readonly Registry $registry, private readonly EntityWorkflowRepository $entityWorkflowRepository, private readonly MetadataExtractor $metadataExtractor)
    {
    }

    /**
     * @param AccompanyingPeriodWork $object
     *
     * @throws ExceptionInterface
     */
    public function normalize($object, ?string $format = null, array $context = []): array|\ArrayObject|bool|float|int|string|null
    {
        $initial = $this->normalizer->normalize($object, $format, array_merge(
            $context,
            [self::IGNORE_WORK => spl_object_hash($object)]
        ));

        // due to bug: https://api-platform.com/docs/core/serialization/#collection-relation
        // and also: https://github.com/symfony/symfony/issues/36965
        // we have to rewrite the evaluations as a collection
        $initial['accompanyingPeriodWorkEvaluations'] = $this->normalizer->normalize(
            $object->getAccompanyingPeriodWorkEvaluations()->getValues(),
            $format,
            $context
        );

        // then, we add normalization for things which are not into the entity

        $initial['workflows_availables'] = $this->metadataExtractor->availableWorkflowFor(
            AccompanyingPeriodWork::class,
            $object->getId()
        );

        $initial['workflows_availables_evaluation'] = $this->metadataExtractor->availableWorkflowFor(
            AccompanyingPeriodWorkEvaluation::class
        );

        $initial['workflows_availables_evaluation_documents'] = $this->metadataExtractor->availableWorkflowFor(
            AccompanyingPeriodWorkEvaluationDocument::class
        );

        $workflows = $this->entityWorkflowRepository->findBy([
            'relatedEntityClass' => AccompanyingPeriodWork::class,
            'relatedEntityId' => $object->getId(),
        ]);

        $initial['workflows'] = $this->normalizer->normalize($workflows, 'json', $context);

        return $initial;
    }

    public function supportsNormalization($data, ?string $format = null, array $context = []): bool
    {
        return 'json' === $format
            && $data instanceof AccompanyingPeriodWork
            && !\array_key_exists(self::IGNORE_WORK, $context);
    }
}
