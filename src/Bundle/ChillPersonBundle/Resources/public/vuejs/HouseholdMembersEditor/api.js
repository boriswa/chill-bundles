/*
*/
const householdMove = (payload) => {
  const url = `/api/1.0/person/household/members/move.json`;
  console.log(payload);
  console.log(JSON.stringify(payload));

  return fetch(url, {
     method: 'POST',
     headers: {
        'Content-Type': 'application/json',
     },
     body: JSON.stringify(payload),
    })
    .then(response => {
       if (response.ok) {
         return response.json();
       }
       if (response.status === 422) {
         return response.json();
       }
       throw Error('Error with testing move'); 
    });
};

const fetchHouseholdSuggestionByAccompanyingPeriod = (personId) => {
  const url = `/api/1.0/person/household/suggest/by-person/${personId}/through-accompanying-period-participation.json`;
  return window.fetch(url)
    .then(response => {
      if (response.ok) {
        return response.json();
      }

      throw Error ({m: 'Error while fetching household suggestion', status: response.status});
    }).then(data => Promise.resolve(data.results))
    .catch(e => console.error(e));
  ;
};

const fetchAddressSuggestionByPerson = (personId) => {
  const url = `/api/1.0/person/address/suggest/by-person/${personId}.json`;
  return window.fetch(url)
    .then(response => {
      if (response.ok) {
        return response.json();
      }

      throw Error({m: 'Error while fetch address suggestion', status: response.status});
    });
}

export { 
  householdMove,
  fetchHouseholdSuggestionByAccompanyingPeriod,
  fetchAddressSuggestionByPerson,
};
