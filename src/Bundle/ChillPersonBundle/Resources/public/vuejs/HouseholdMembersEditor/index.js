import { createApp } from 'vue';
import { _createI18n } from 'ChillMainAssets/vuejs/_js/i18n';
import { appMessages } from './js/i18n';
import { store } from './store';
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';

import App from './App.vue';

const i18n = _createI18n(appMessages);

const app = createApp({
   template: `<app></app>`,
})
.use(store)
.use(i18n)
.use(VueToast, {
 position: "bottom-right",
 type: "error",
 duration: 5000,
 dismissible: true
})
.component('app', App)
.mount('#household_members_editor');
