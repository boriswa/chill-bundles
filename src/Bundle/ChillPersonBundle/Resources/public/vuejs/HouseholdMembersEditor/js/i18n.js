
import { personMessages } from 'ChillPersonAssets/vuejs/_js/i18n';
import { ontheflyMessages } from 'ChillMainAssets/vuejs/OnTheFly/i18n';
import { addressMessages } from 'ChillMainAssets/vuejs/Address/i18n';

const appMessages = {
   fr: {
     household_members_editor: {
      household: {
        no_household_choose_one: "Aucun ménage de destination. Choisissez un ménage.",
//        new_household: "Nouveau ménage",
        create_household: "Créer",
        search_household: "Chercher un ménage",
        will_leave_any_household: "Les usagers ne rejoignent pas de ménage",
        leave: "Quitter sans rejoindre un ménage",
        will_leave_any_household_explanation: "Les usagers quitteront leur ménage actuel, et ne seront pas associés à un autre ménage. Par ailleurs, ils seront enregistrés comme étant sans adresse connue.",
        leave_without_household: "Sans nouveau ménage",
        reset_mode: "Modifier la destination",
        household_suggested: "Suggestions de ménage",
        household_suggested_explanation: "Les ménages suivants sont connus et pourraient peut-être correspondre à des ménages recherchés."
        // remove ?
        /*
        where_live_the_household: "À quelle adresse habite ce ménage ?",
        household_live_to_this_address: "Sélectionner l'adresse",
        no_suggestions: "Aucune adresse à suggérer",
        or_create_new_address: "Ou créer une nouvelle adresse",

         */
        // end remove ?
      },
      household_address: {
        mark_no_address: "Ne pas indiquer d'adresse",
        remove_address: "Supprimer l'adresse",
        update_address: "Mettre à jour l'adresse",
        set_address: "Indiquer une adresse",
        create_new_address: "Créer une nouvelle adresse",
      },
      concerned: {
        title: "Usager(s) à (re)positionner dans un ménage",
        persons_will_be_moved: "Les usagers suivants vont être déplacés",
        add_at_least_onePerson: "Indiquez au moins un usager à déplacer",
        remove_concerned: "Ne plus transférer",
        // old ?
        add_persons: "Ajouter d'autres usagers",
        search: "Rechercher des usagers",
        move_to: "Déplacer vers",
        persons_leaving: "Usagers quittant leurs ménages",
        no_person_in_position: "Aucun usager ne sera ajouté à cette position",
        persons_with_household: "Les usagers suivants sont associés à ces ménages:",
        already_belongs_to_household: "est associé au ménage"
      },
       positioning: {
         persons_to_positionnate: 'Usagers à positionner',
         holder: "Titulaire",
         comment: "Commentaire",
         comment_placeholder: "Associer un commentaire",
       },
      app: {
       next: 'Suivant',
       previous: 'Précédent',
       cancel: 'Annuler',
       save: 'Enregistrer',
        steps: {
          concerned: 'Usagers concernés',
          household: 'Ménage de destination',
          household_address: 'Adresse du nouveau ménage',
          positioning: 'Position dans le ménage',
          confirm: 'Confirmation'
        }
      },
      drop_persons_here: "Glissez-déposez ici les usagers pour la position \"{position}\"",
      all_positionnated: "Tous les usagers sont positionnés",
      household_part: "Destination",
      suggestions: "Suggestions",
      hide_household_suggestion: "Masquer les suggestions",
      show_household_suggestion: 'Aucune suggestion | Afficher une suggestion | Afficher {count} suggestions',
      household_for_participants_accompanying_period: "Des ménages partagent le même parcours",
      select_household: "Sélectionner le ménage",
      dates: {
        start_date: "Début de validité",
        end_date: "Fin de validité",
        dates_title: "Depuis le",
      },
      composition: {
        composition: "Composition familiale",
        household_composition: "Composition du ménage",
        number_of_children: "Nombre d'enfants mineurs au sein du ménage",
      },
      confirmation: {
        save: "Enregistrer",
        there_are_warnings: "Impossible de valider actuellement",
        check_those_items: "Veuillez corriger les éléments suivants",
      },
    }
   }
};

Object.assign(appMessages.fr, personMessages.fr, addressMessages.fr, ontheflyMessages.fr);

export {
   appMessages
};
