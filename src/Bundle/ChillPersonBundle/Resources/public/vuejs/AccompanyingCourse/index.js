import { createApp } from 'vue'
import { _createI18n } from 'ChillMainAssets/vuejs/_js/i18n'
import { appMessages } from './js/i18n'
import { initPromise } from './store'
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import App from './App.vue';
import Banner from './components/Banner.vue';

const root = window.vueRootComponent;

/*
*  Load all App component, for AccompanyingCourse edition page
*/
if (root === 'app') {
   initPromise(root).then(store => {

      const i18n = _createI18n(appMessages);

      const app = createApp({
         template: `<app></app>`,
      })
      .use(store)
      .use(i18n)
      .use(VueToast, {
         position: "bottom-right",
         type: "error",
         duration: 5000,
         dismissible: true
      })
      .component('app', App)
      .mount('#accompanying-course')
   });
}

/*
*  Load only Banner sub-component, for all others AccompanyingCourse page
*/
if (root === 'banner') {
   initPromise(root).then(store => {

      const i18n = _createI18n(appMessages);

      const app = createApp({
         template: `<banner></banner>`,
      })
      .use(store)
      .use(i18n)
      .use(VueToast, {
         position: "bottom-right",
         type: "error",
         duration: 5000,
         dismissible: true
      })
      .component('banner', Banner)
      .mount('#banner-accompanying-course');
   });

}
