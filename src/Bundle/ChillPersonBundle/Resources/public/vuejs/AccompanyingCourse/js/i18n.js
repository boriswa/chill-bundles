import { personMessages } from 'ChillPersonAssets/vuejs/_js/i18n';
import { thirdpartyMessages } from 'ChillThirdPartyAssets/vuejs/_js/i18n';
import { addressMessages } from 'ChillMainAssets/vuejs/Address/i18n';
import { ontheflyMessages } from 'ChillMainAssets/vuejs/OnTheFly/i18n';

const appMessages = {
   fr: {
      course: {
         id: "id",
         title: {
            draft: "Création d'un nouveau parcours",
            active: "Modification du parcours"
         },
         opening_date: "Date d'ouverture",
         closing_date: "Date de clôture",
         remark: "Commentaire",
         closing_motive: "Motif de clôture",
         user: "TMS",
         flags: "Indicateurs",
         status: "État",
         step: {
            draft: "Brouillon",
            active: "En file active",
            closed: "Cloturé",
            inactive_short: "Hors file active",
            inactive_long: "Pré-archivé",
         },
         open_at: "ouvert le ",
         by: "par ",
         referrer: "Référent",
         emergency: "urgent",
         confidential: "confidentiel",
         regular: "régulier",
         occasional: "ponctuel",
         absent: "Absent",
      },
      origin: {
         title: "Origine de la demande",
         label: "Origine de la demande",
         placeholder: "Renseignez l'origine de la demande",
         not_valid: "Indiquez une origine à la demande",
      },
      admin_location: {
         title: "Localisation administrative",
         label: "Localisation administrative",
         placeholder: "Renseignez la localisation administrative",
         not_valid: "Indiquez une localisation administrative",
      },
      persons_associated: {
         title: "Usagers concernés",
         counter: "Il n'y a pas encore d'usagers | 1 usager | {count} usagers",
         firstname: "Prénom",
         lastname: "Nom",
         name: "Nom",
         startdate: "Date d'entrée",
         enddate: "Date de sortie",
         add_persons: "Ajouter des usagers",
         date_start_to_end: "Participation du {start} au {end}",
         leave_course: "L'usager quitte le parcours",
         sure: "Êtes-vous sûr ?",
         sure_description: "Une fois confirmé, il ne sera pas possible de faire marche arrière ! La sortie reste cependant consignée dans l'historique du parcours.",
         ok: "Oui, l'usager quitte le parcours",
         show_household_number: "Voir le ménage (n° {id})",
         show_household: "Voir le ménage",
         person_without_household_warning: "Certaines usagers n'appartiennent actuellement à aucun ménage. Veuillez les associer à un ménage dès que possible.",
         update_household: "Associer à un ménage",
         participation_not_valid: "Sélectionnez ou créez au minimum 1 usager",
      },
      requestor: {
         title: "Demandeur",
         add_requestor: "Ajouter un demandeur",
         is_anonymous: "Le demandeur est anonyme",
         counter: "Il n'y a pas encore de demandeur",
         type: "Type",
         person_id: "id",
         text: "Dénomination",
         firstName: "Prénom",
         lastName: "Nom",
         birthdate: "Date de naissance",
         center: "Centre",
         phonenumber: "Téléphone",
         mobilenumber: "Mobile",
         altNames: "Autres noms",
         address: "Adresse",
         location: "Localité",
      },
      social_issue: {
         title: "Problématiques sociales",
         label: "Choisir les problématiques sociales",
         not_valid: "Sélectionnez au minimum une problématique sociale",
      },
      courselocation: {
         title: "Localisation du parcours",
         add_temporary_address: "Ajouter une adresse temporaire",
         edit_temporary_address: "Modifier l'adresse temporaire",
         assign_course_address: "Désigner comme l'adresse du parcours",
         remove_button: "Enlever l'adresse",
         temporary_address_must_be_changed: "Cette adresse est temporaire. Le parcours devrait être localisé auprès d'un usager concerné.",
         associate_at_least_one_person_with_one_household_with_address: "Commencez d'abord par associer un membre du parcours à un ménage, et indiquez une adresse à ce ménage.",
         sure: "Êtes-vous sûr ?",
         sure_description: "Voulez-vous faire de cette adresse l'adresse du parcours ?",
         ok: "Désigner comme adresse du parcours",
         person_locator: "Parcours localisé auprès de {0}",
         not_valid: "Indiquez au minimum une localisation temporaire du parcours",
         no_address: "Il n'y a pas d'adresse associée au parcours"
      },
      scopes: {
         title: "Services concernés",
         add_at_least_one: "Indiquez au moins un service",
      },
      referrer: {
         title: "Référent du parcours",
         label: "Vous pouvez choisir un TMS ou vous assigner directement comme référent",
         placeholder: "Choisir un TMS",
         assign_me: "M'assigner comme référent",
      },
      resources: {
         title: "Interlocuteurs privilégiés",
         counter: "Il n'y a pas encore d'interlocuteur | 1 interlocuteur | {count} interlocuteurs",
         text: "Dénomination",
         description: "Description",
         add_resources: "Ajouter des interlocuteurs",
      },
      comment: {
         title: "Observations",
         label: "Ajout d'une note",
         content: "Rédigez une première note…",
         created_by: "créé par {0}, mis à jour le {1}"
      },
      confirm: {
         title: "Confirmation",
         text_draft: "Le parcours est actuellement à l'état de <b>{0}</b>.",
         text_active: "En validant cette étape, vous lui donnez le statut <b>{0}</b>.",
         alert_validation: "Certaines conditions ne sont pas remplies pour pouvoir confirmer le parcours :",
         participation_not_valid: "sélectionnez au minimum 1 usager",
         socialIssue_not_valid: "sélectionnez au minimum une problématique sociale",
         location_not_valid: "indiquez au minimum une localisation temporaire du parcours",
         origin_not_valid: "Indiquez une origine à la demande",
         adminLocation_not_valid: "Indiquez une localisation administrative à la demande",
         job_not_valid: "Indiquez un métier du référent",
         set_a_scope: "indiquez au moins un service",
         sure: "Êtes-vous sûr ?",
         sure_description: "Une fois le changement confirmé, il ne sera plus possible de le remettre à l'état de brouillon !",
         sure_referrer: "Êtes-vous sûr de vouloir assigner ce parcours à <b>{referrer}</b>",
         ok: "Confirmer le parcours",
         delete: "Supprimer le parcours",
         ok_referrer: "Confirmer le référent",
         no_suggested_referrer: "Il n'y a aucun référent qui puisse être suggéré pour ce parcours. Vérifiez la localisation du parcours, les métiers et service indiqués. Si les données sont correctes, vous pouvez confirmer ce parcours.",
         one_suggested_referrer: "Un unique référent peut être suggéré pour ce parcours",
         choose_suggested_referrer: "Voulez-vous le désigner directement ?",
         choose_button: "Désigner",
         do_not_choose_button: "Ne pas désigner"
      },
      job: {
         label: "Métier",
         placeholder: "Choisir un métier",
         not_valid: "Sélectionnez un métier du référent"
      },
      startdate: {
         change: "Date d'ouverture",
         // update: "La nouvelle date d'ouverture a été enregistrée"
      },
      // catch errors
      'Error while updating AccompanyingPeriod Course.': "Erreur du serveur lors de la mise à jour du parcours d'accompagnement.",
      'Error while retriving AccompanyingPeriod Course.': "Erreur du serveur lors du chargement du parcours d'accompagnement.",
      'Error while confirming AccompanyingPeriod Course.': "Erreur du serveur lors de la confirmation du parcours d'accompagnement.",
      'Error while retriving Social Issues.': "Erreur du serveur lors du chargement des problématique sociales.",
      'Error while sending AccompanyingPeriod Course participation.': "Erreur du serveur lors de l'envoi des infos d'un usager.",
      'Error while sending AccompanyingPeriod Course requestor': "Erreur du serveur lors de l'envoi des infos du demandeur.",
      'Error while sending AccompanyingPeriod Course resource.': "Erreur du serveur lors de l'envoi des infos d'un interlocuteur privilégié.",
      'Error while updating SocialIssue.': "Erreur du serveur lors de la mise à jour d'une problématique sociale.",
      'Error while retriving users.': "Erreur du serveur lors du chargement de la liste des travailleurs.",
      'Error while getting whoami.': "Erreur du serveur lors de la requête 'qui suis-je ?'",
      'Error while retriving origin\'s list.': "Erreur du serveur lors du chargement de la liste des origines de la demande.",
      'Only the referrer can toggle the intensity of an accompanying course': "Seul le référent peut modifier l'intensité d'un parcours.",
      'Only the referrer can toggle the confidentiality of an accompanying course': "Seul le référent peut modifier la confidentialité d'un parcours."
   }
};

Object.assign(appMessages.fr, personMessages.fr, thirdpartyMessages.fr, addressMessages.fr, ontheflyMessages.fr);

export {
   appMessages
};
