import { createApp } from 'vue';
import { _createI18n } from 'ChillMainAssets/vuejs/_js/i18n';
import { store } from './store';
import { personMessages } from 'ChillPersonAssets/vuejs/_js/i18n';
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import App from './App.vue';

const i18n = _createI18n(personMessages);

const app = createApp({
   template: `<app></app>`,
})
.use(store)
.use(VueToast, {
   position: "bottom-right",
   type: "error",
   duration: 10000,
   dismissible: true
})
.use(i18n)
.component('app', App)
.mount('#accompanying_course_work_edit');
