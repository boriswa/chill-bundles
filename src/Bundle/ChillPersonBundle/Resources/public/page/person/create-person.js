import { ShowHide } from 'ShowHide';

const addressForm = document.getElementById("addressForm");
const address = document.getElementById("address");

new ShowHide({
    froms: [addressForm],
    container: [address],
    test: function(froms) {
        for (let f of froms.values()) {
            for (let input of f.querySelectorAll('input').values()) {
                return input.checked;
            }
        }
        return false;
    },
    event_name: 'change'
});
