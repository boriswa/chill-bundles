<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Actions\Remove;

use Chill\PersonBundle\Entity\Person;

interface PersonMoveSqlHandlerInterface
{
    /**
     * @param class-string $className
     */
    public function supports(string $className, string $field): bool;

    /**
     * @param class-string $className
     *
     * @return array<string>
     */
    public function getSqls(string $className, string $field, Person $from, Person $to): array;
}
