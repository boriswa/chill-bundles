<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Actions\Remove\Handler;

use Chill\PersonBundle\Actions\Remove\PersonMoveSqlHandlerInterface;
use Chill\PersonBundle\Entity\Household\HouseholdMember;
use Chill\PersonBundle\Entity\Person;

class PersonMoveHouseholdHandler implements PersonMoveSqlHandlerInterface
{
    public function supports(string $className, string $field): bool
    {
        return HouseholdMember::class === $className;
    }

    public function getSqls(string $className, string $field, Person $from, Person $to): array
    {
        $sqlInsert = sprintf(<<<'SQL'
            INSERT INTO chill_person_household_members (id, person_id, household_id, startdate, enddate, comment, sharedhousehold, position_id, holder)
                SELECT nextval('chill_person_household_members_id_seq'), %d, household_id, startdate, enddate, comment, sharedhousehold, position_id, holder
                    FROM chill_person_household_members cphm
                    WHERE person_id = %d
                        AND NOT EXISTS (
                        SELECT 1 FROM chill_person_household_members cphm_inner
                                 WHERE
                                     person_id = %d
                                   AND daterange(cphm.startdate, cphm.enddate) && daterange(cphm_inner.startdate, cphm_inner.enddate)
                                 );
        SQL, $to->getId(), $from->getId(), $to->getId());

        $deleteSql = sprintf(<<<'SQL'
                DELETE FROM chill_person_household_members WHERE person_id = %d;
                SQL, $from->getId());

        return [$sqlInsert, $deleteSql];
    }
}
