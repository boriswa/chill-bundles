<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Actions\Remove;

use Chill\PersonBundle\Entity\Person;

class PersonMoveManager
{
    public function __construct(
        /**
         * @var iterable<PersonMoveSqlHandlerInterface>
         */
        private readonly iterable $handlers,
    ) {
    }

    /**
     * @param class-string $className
     */
    public function hasHandler(string $className, string $field): bool
    {
        foreach ($this->handlers as $handler) {
            if ($handler->supports($className, $field)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param class-string $className
     *
     * @return array<string>
     */
    public function getSqls(string $className, string $field, Person $from, Person $to): array
    {
        foreach ($this->handlers as $handler) {
            if ($handler->supports($className, $field)) {
                return $handler->getSqls($className, $field, $from, $to);
            }
        }

        return [];
    }
}
