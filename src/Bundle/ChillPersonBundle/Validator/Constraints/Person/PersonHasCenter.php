<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Validator\Constraints\Person;

/**
 * @Annotation
 */
class PersonHasCenter extends \Symfony\Component\Validator\Constraint
{
    public string $message = 'A center is required';

    public function getTargets()
    {
        return [
            self::CLASS_CONSTRAINT,
        ];
    }
}
