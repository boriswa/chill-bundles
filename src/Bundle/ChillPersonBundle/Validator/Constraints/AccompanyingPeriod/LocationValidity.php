<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Validator\Constraints\AccompanyingPeriod;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class LocationValidity extends Constraint
{
    public $messagePeriodMustRemainsLocated = 'The period must remain located';

    public $messagePersonLocatedMustBeAssociated = "The person where the course is located must be associated to the course. Change course's location before removing the person.";

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
