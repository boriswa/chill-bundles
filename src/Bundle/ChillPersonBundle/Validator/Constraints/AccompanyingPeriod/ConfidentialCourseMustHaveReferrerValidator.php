<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Validator\Constraints\AccompanyingPeriod;

use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ConfidentialCourseMustHaveReferrerValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$value instanceof AccompanyingPeriod) {
            throw new UnexpectedTypeException($value, AccompanyingPeriod::class);
        }

        if (!$constraint instanceof ConfidentialCourseMustHaveReferrer) {
            throw new UnexpectedTypeException($constraint, ConfidentialCourseMustHaveReferrer::class);
        }

        if ($value->isConfidential() && null === $value->getUser()) {
            $this->context
                ->buildViolation($constraint->message)
                ->atPath('user')
                ->addViolation();
        }
    }
}
