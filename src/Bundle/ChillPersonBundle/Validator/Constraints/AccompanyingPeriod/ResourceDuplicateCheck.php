<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Validator\Constraints\AccompanyingPeriod;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ResourceDuplicateCheck extends Constraint
{
    public $message = '{{ name }} is already associated to this accompanying course.';
}
