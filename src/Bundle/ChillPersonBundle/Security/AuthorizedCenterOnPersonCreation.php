<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Security;

use Chill\MainBundle\Security\Authorization\AuthorizationHelperForCurrentUserInterface;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AuthorizedCenterOnPersonCreation implements AuthorizedCenterOnPersonCreationInterface
{
    private readonly bool $showCenters;

    public function __construct(private readonly AuthorizationHelperForCurrentUserInterface $authorizationHelperForCurrentUser, ParameterBagInterface $parameterBag)
    {
        $this->showCenters = $parameterBag->get('chill_main')['acl']['form_show_centers'];
    }

    public function getCenters(): array
    {
        if (!$this->showCenters) {
            return [];
        }

        return $this->authorizationHelperForCurrentUser->getReachableCenters(PersonVoter::CREATE);
    }
}
