<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Security\Authorization;

use Chill\PersonBundle\Entity\AccompanyingPeriod\Comment;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class AccompanyingPeriodCommentVoter extends Voter
{
    final public const DELETE = 'CHILL_PERSON_ACCOMPANYING_PERIOD_COMMENT_DELETE';

    final public const EDIT = 'CHILL_PERSON_ACCOMPANYING_PERIOD_COMMENT_EDIT';

    public function __construct(private readonly Security $security)
    {
    }

    protected function supports($attribute, $subject)
    {
        return $subject instanceof Comment;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        return match ($attribute) {
            self::EDIT => $this->security->isGranted(AccompanyingPeriodVoter::EDIT, $subject->getAccompanyingPeriod()),
            self::DELETE => $this->security->isGranted(AccompanyingPeriodVoter::EDIT, $subject->getAccompanyingPeriod()),
            default => throw new \UnexpectedValueException("This attribute {$attribute} is not supported"),
        };
    }
}
