<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Export;

use Chill\MainBundle\Export\Helper\AggregateStringHelper;
use Chill\MainBundle\Export\Helper\DateTimeHelper;
use Chill\MainBundle\Export\Helper\TranslatableStringExportLabelHelper;
use Chill\MainBundle\Export\Helper\UserHelper;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Test\Export\AbstractExportTest;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Export\Export\ListAccompanyingPeriodWorkAssociatePersonOnAccompanyingPeriod;
use Chill\PersonBundle\Export\Helper\FilterListAccompanyingPeriodHelperInterface;
use Chill\PersonBundle\Export\Helper\LabelPersonHelper;
use Chill\PersonBundle\Repository\SocialWork\SocialActionRepository;
use Chill\PersonBundle\Repository\SocialWork\SocialIssueRepository;
use Chill\PersonBundle\Templating\Entity\SocialActionRender;
use Chill\PersonBundle\Templating\Entity\SocialIssueRender;
use Chill\ThirdPartyBundle\Export\Helper\LabelThirdPartyHelper;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @internal
 *
 * @coversNothing
 */
class ListAccompanyingPeriodWorkAssociatePersonOnAccompanyingPeriodTest extends AbstractExportTest
{
    use ProphecyTrait;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
    }

    public function getExport()
    {
        $entityManager = self::$container->get(EntityManagerInterface::class);
        $dateTimeHelper = self::$container->get(DateTimeHelper::class);
        $userHelper = self::$container->get(UserHelper::class);
        $personHelper = self::$container->get(LabelPersonHelper::class);
        $thirdPartyHelper = self::$container->get(LabelThirdPartyHelper::class);
        $translatableStringExportLabelHelper = self::$container->get(TranslatableStringExportLabelHelper::class);
        $socialIssueRender = self::$container->get(SocialIssueRender::class);
        $socialIssueRepository = self::$container->get(SocialIssueRepository::class);
        $socialActionRender = self::$container->get(SocialActionRender::class);
        $rollingDateConverter = self::$container->get(RollingDateConverterInterface::class);
        $aggregateStringHelper = self::$container->get(AggregateStringHelper::class);
        $socialActionRepository = self::$container->get(SocialActionRepository::class);
        $filterListAccompanyingPeriodHelper = $this->prophesize(FilterListAccompanyingPeriodHelperInterface::class);

        yield new ListAccompanyingPeriodWorkAssociatePersonOnAccompanyingPeriod(
            $entityManager,
            $dateTimeHelper,
            $userHelper,
            $personHelper,
            $thirdPartyHelper,
            $translatableStringExportLabelHelper,
            $socialIssueRender,
            $socialIssueRepository,
            $socialActionRender,
            $rollingDateConverter,
            $aggregateStringHelper,
            $socialActionRepository,
            $filterListAccompanyingPeriodHelper->reveal(),
        );
    }

    public function getFormData()
    {
        return [
            ['calc_date' => new RollingDate(RollingDate::T_TODAY)],
        ];
    }

    public function getModifiersCombination()
    {
        return [[Declarations::ACP_TYPE]];
    }
}
