<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Export;

use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Repository\CenterRepositoryInterface;
use Chill\MainBundle\Repository\ScopeRepositoryInterface;
use Chill\MainBundle\Security\Authorization\AuthorizationHelperForCurrentUserInterface;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Test\Export\AbstractExportTest;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Export\Export\ListAccompanyingPeriod;
use Chill\PersonBundle\Export\Helper\FilterListAccompanyingPeriodHelper;
use Chill\PersonBundle\Export\Helper\ListAccompanyingPeriodHelper;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodVoter;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Security\Core\Security;

/**
 * @internal
 *
 * @coversNothing
 */
class ListAccompanyingPeriodTest extends AbstractExportTest
{
    use ProphecyTrait;

    private readonly ListAccompanyingPeriod $listAccompanyingPeriod;

    private readonly CenterRepositoryInterface $centerRepository;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
    }

    public function getExport()
    {
        /** @var EntityManagerInterface::class $em */
        $em = self::$container->get(EntityManagerInterface::class);
        $rollingDateConverter = self::$container->get(RollingDateConverterInterface::class);
        $listAccompanyingPeriodHelper = self::$container->get(ListAccompanyingPeriodHelper::class);
        $centerRepository = self::$container->get(CenterRepositoryInterface::class);
        $scopeRepository = self::$container->get(ScopeRepositoryInterface::class);

        // mock security
        $user = $em->createQuery('SELECT u FROM '.User::class.' u')
            ->setMaxResults(1)->getSingleResult();
        if (null === $user) {
            throw new \RuntimeException('no user found');
        }
        $security = $this->prophesize(Security::class);
        $security->getUser()->willReturn($user);

        // mock authorization helper
        $scopes = $scopeRepository->findAll();
        $scopesConfidentials = [] !== $scopes ? [$scopes[0]] : [];
        $authorizationHelper = $this->prophesize(AuthorizationHelperForCurrentUserInterface::class);
        $authorizationHelper->getReachableScopes(AccompanyingPeriodVoter::SEE_DETAILS, Argument::type(Center::class))
            ->willReturn($scopes);
        $authorizationHelper->getReachableScopes(AccompanyingPeriodVoter::SEE_CONFIDENTIAL_ALL, Argument::type(Center::class))
            ->willReturn($scopesConfidentials);

        yield new ListAccompanyingPeriod(
            $em,
            $rollingDateConverter,
            $listAccompanyingPeriodHelper,
            new FilterListAccompanyingPeriodHelper(
                $security->reveal(),
                $centerRepository,
                $authorizationHelper->reveal(),
                $this->getParameters(true)
            )
        );

        yield new ListAccompanyingPeriod(
            $em,
            $rollingDateConverter,
            $listAccompanyingPeriodHelper,
            new FilterListAccompanyingPeriodHelper(
                $security->reveal(),
                $centerRepository,
                $authorizationHelper->reveal(),
                $this->getParameters(false)
            )
        );
    }

    public function getFormData()
    {
        return [
            ['calc_date' => new RollingDate(RollingDate::T_TODAY)],
        ];
    }

    public function getModifiersCombination()
    {
        return [[Declarations::ACP_TYPE]];
    }
}
