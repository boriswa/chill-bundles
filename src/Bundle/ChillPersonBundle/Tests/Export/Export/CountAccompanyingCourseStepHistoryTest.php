<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace src\Bundle\ChillPersonBundle\Tests\Export\Export;

use Chill\MainBundle\Test\Export\AbstractExportTest;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Export\Export\CountAccompanyingCourseStepHistory;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class CountAccompanyingCourseStepHistoryTest extends AbstractExportTest
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function getExport()
    {
        $em = self::$container->get(EntityManagerInterface::class);

        yield new CountAccompanyingCourseStepHistory($em, $this->getParameters(true));
        yield new CountAccompanyingCourseStepHistory($em, $this->getParameters(false));
    }

    public function getFormData(): array
    {
        return [[]];
    }

    public function getModifiersCombination()
    {
        return [[Declarations::ACP_TYPE], [Declarations::ACP_TYPE, Declarations::ACP_STEP_HISTORY]];
    }
}
