<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace src\Bundle\ChillPersonBundle\Tests\Export\Filter\AccompanyingPeriodStepHistoryFilters;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodStepHistory;
use Chill\PersonBundle\Export\Filter\AccompanyingPeriodStepHistoryFilters\ByDateFilter;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class ByDateFilterTest extends AbstractFilterTest
{
    private RollingDateConverterInterface $rollingDateConverter;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        $this->rollingDateConverter = self::$container->get(RollingDateConverterInterface::class);
    }

    public function getFilter()
    {
        return new ByDateFilter($this->rollingDateConverter);
    }

    public function getFormData()
    {
        return [
            [
                'start_date' => new RollingDate(RollingDate::T_YEAR_CURRENT_START),
                'end_date' => new RollingDate(RollingDate::T_TODAY),
            ],
        ];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        $qb = $em->createQueryBuilder()
            ->select('COUNT(DISTINCT acpstephistory.id) As export_result')
            ->from(AccompanyingPeriodStepHistory::class, 'acpstephistory')
            ->join('acpstephistory.period', 'acp');

        return [
            $qb,
        ];
    }
}
