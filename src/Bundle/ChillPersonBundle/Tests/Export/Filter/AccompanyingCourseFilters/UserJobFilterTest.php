<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Entity\UserJob;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters\UserJobFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class UserJobFilterTest extends AbstractFilterTest
{
    private UserJobFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::$container->get(UserJobFilter::class);
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData(): iterable
    {
        self::bootKernel();
        $jobs = self::$container->get(EntityManagerInterface::class)
            ->createQuery('SELECT j FROM '.UserJob::class.' j')
            ->setMaxResults(1)
            ->getResult();

        yield [
            'jobs' => new ArrayCollection($jobs),
            'date_calc' => new RollingDate(RollingDate::T_TODAY),
        ];

        yield [
            'jobs' => $jobs,
            'date_calc' => new RollingDate(RollingDate::T_TODAY),
        ];
    }

    public function getQueryBuilders(): array
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('acp.id')
                ->from(AccompanyingPeriod::class, 'acp'),
        ];
    }
}
