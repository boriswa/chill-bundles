<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters\NotAssociatedWithAReferenceAddressFilter;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @internal
 *
 * @coversNothing
 */
class NotAssociatedWithAReferenceAddressFilterTest extends AbstractFilterTest
{
    use ProphecyTrait;

    public function getFilter()
    {
        $dateConverter = new class () implements RollingDateConverterInterface {
            public function convert(?RollingDate $rollingDate): ?\DateTimeImmutable
            {
                if (null === $rollingDate) {
                    return null;
                }

                return new \DateTimeImmutable('now');
            }
        };

        return new NotAssociatedWithAReferenceAddressFilter($dateConverter);
    }

    public function getFormData()
    {
        return [
            ['date_calc' => new RollingDate(RollingDate::T_TODAY)],
        ];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('acp.id')
                ->from(AccompanyingPeriod::class, 'acp'),
        ];
    }
}
