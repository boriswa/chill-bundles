<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\SocialWork\SocialAction;
use Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters\SocialActionFilter;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class SocialActionFilterTest extends AbstractFilterTest
{
    private SocialActionFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::$container->get('chill.person.export.filter_socialaction');
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData(): iterable
    {
        self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        $array = $em->createQueryBuilder()
            ->from(SocialAction::class, 'sa')
            ->select('sa')
            ->getQuery()
            ->setMaxResults(1)
            ->getResult();

        return [
            [
                'accepted_socialactions' => $array,
                'start_date_after' => null,
                'start_date_before' => null,
                'end_date_after' => null,
                'end_date_before' => null,
            ],
            [
                'accepted_socialactions' => $array,
                'start_date_after' => new RollingDate(RollingDate::T_YEAR_PREVIOUS_START),
                'start_date_before' => new RollingDate(RollingDate::T_TODAY),
                'end_date_after' => new RollingDate(RollingDate::T_YEAR_PREVIOUS_START),
                'end_date_before' => new RollingDate(RollingDate::T_TODAY),
            ],
            [
                'accepted_socialactions' => [],
                'start_date_after' => new RollingDate(RollingDate::T_YEAR_PREVIOUS_START),
                'start_date_before' => new RollingDate(RollingDate::T_TODAY),
                'end_date_after' => new RollingDate(RollingDate::T_YEAR_PREVIOUS_START),
                'end_date_before' => new RollingDate(RollingDate::T_TODAY),
            ],
            [
                'accepted_socialactions' => [],
                'start_date_after' => null,
                'start_date_before' => null,
                'end_date_after' => null,
                'end_date_before' => null,
            ],
        ];
    }

    public function getQueryBuilders(): array
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->from(AccompanyingPeriod::class, 'acp')
                ->select('acp.id'),
        ];
    }
}
