<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\SocialWork\SocialIssue;
use Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters\SocialIssueFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class SocialIssueFilterTest extends AbstractFilterTest
{
    private SocialIssueFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::$container->get('chill.person.export.filter_socialissue');
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData(): iterable
    {
        self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        $array = $em->createQueryBuilder()
            ->from(SocialIssue::class, 'si')
            ->select('si')
            ->getQuery()
            ->setMaxResults(1)
            ->getResult();

        yield ['accepted_socialissues' => new ArrayCollection($array)];
        yield ['accepted_socialissues' => $array];
    }

    public function getQueryBuilders(): array
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('acp.id')
                ->from(AccompanyingPeriod::class, 'acp'),
        ];
    }
}
