<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters\HasTemporaryLocationFilter;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class HasTemporaryLocationFilterTest extends AbstractFilterTest
{
    private RollingDateConverterInterface $rollingDateConverter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->rollingDateConverter = self::$container->get(RollingDateConverterInterface::class);
    }

    public function getFilter()
    {
        return new HasTemporaryLocationFilter($this->rollingDateConverter);
    }

    public function getFormData()
    {
        return [
            [
                'having_temporarily' => true,
                'calc_date' => new RollingDate(RollingDate::T_TODAY),
            ],
            [
                'having_temporarily' => false,
                'calc_date' => new RollingDate(RollingDate::T_TODAY),
            ],
        ];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->from('ChillPersonBundle:AccompanyingPeriod', 'acp')
                ->select('acp.id'),
        ];
    }
}
