<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace src\Bundle\ChillPersonBundle\Tests\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Templating\Entity\UserRender;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters\ReferrerFilterBetweenDates;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class ReferrerFilterBetweenDatesTest extends AbstractFilterTest
{
    private RollingDateConverterInterface $rollingDateConverter;
    private UserRender $userRender;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        $this->rollingDateConverter = self::$container->get(RollingDateConverterInterface::class);
        $this->userRender = self::$container->get(UserRender::class);
    }

    public function getFilter()
    {
        return new ReferrerFilterBetweenDates($this->rollingDateConverter, $this->userRender);
    }

    public function getFormData()
    {
        self:self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        $users = $em->createQueryBuilder()
            ->from(User::class, 'u')
            ->select('u')
            ->getQuery()
            ->setMaxResults(1)
            ->getResult();

        return [
            [
                'accepted_referrers' => $users[0],
                'start_date' => new RollingDate(RollingDate::T_YEAR_PREVIOUS_START),
                'end_date' => new RollingDate(RollingDate::T_TODAY),
            ],
        ];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        yield $em->createQueryBuilder()
            ->from(AccompanyingPeriod::class, 'acp')
            ->select('acp.id');

        $qb = $em->createQueryBuilder();
        $qb
            ->from(AccompanyingPeriod\AccompanyingPeriodWork::class, 'acpw')
            ->join('acpw.accompanyingPeriod', 'acp')
            ->join('acp.participations', 'acppart')
            ->join('acppart.person', 'person')
        ;

        $qb->select('COUNT(DISTINCT acpw.id) as export_result');

        yield $qb;
    }
}
