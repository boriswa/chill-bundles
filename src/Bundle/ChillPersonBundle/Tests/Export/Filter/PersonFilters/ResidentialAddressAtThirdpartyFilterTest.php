<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Filter\PersonFilters;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Entity\Person\ResidentialAddress;
use Chill\PersonBundle\Export\Filter\PersonFilters\ResidentialAddressAtThirdpartyFilter;
use Chill\ThirdPartyBundle\Entity\ThirdPartyCategory;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr;

/**
 * @internal
 *
 * @coversNothing
 */
final class ResidentialAddressAtThirdpartyFilterTest extends AbstractFilterTest
{
    private ResidentialAddressAtThirdpartyFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::$container->get('chill.person.export.filter_residential_address_at_thirdparty');
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData(): array
    {
        self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        $array = $em->createQueryBuilder()
            ->from(ThirdPartyCategory::class, 'tpc')
            ->select('tpc')
            ->getQuery()
            ->setMaxResults(1)
            ->getResult();

        $data = [];

        foreach ($array as $r) {
            $data[] = [
                'thirdparty_cat' => [$r],
                'date_calc' => new RollingDate(RollingDate::T_TODAY),
            ];
        }

        return $data;
    }

    public function getQueryBuilders(): array
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('person.id')
                ->from(Person::class, 'person')
                ->join(ResidentialAddress::class, 'resaddr', Expr\Join::WITH, 'resaddr.person = person')
                ->join('resaddr.hostThirdParty', 'tparty')
                ->join('tparty.categories', 'tpartycat'),
        ];
    }
}
