<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Filter\EvaluationFilters;

use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Filter\EvaluationFilters\MaxDateFilter;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class MaxDateFilterTest extends AbstractFilterTest
{
    private MaxDateFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::$container->get('chill.person.export.filter_maxdate');
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData(): array
    {
        return [
            ['maxdate' => false],
            ['maxdate' => true],
        ];
    }

    public function getQueryBuilders(): array
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('workeval.id')
                ->from(AccompanyingPeriod::class, 'acp')
                ->join('acp.works', 'acpw')
                ->join('acpw.accompanyingPeriodWorkEvaluations', 'workeval'),
        ];
    }
}
