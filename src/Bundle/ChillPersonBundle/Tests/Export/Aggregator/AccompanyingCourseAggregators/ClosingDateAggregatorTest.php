<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators\ClosingDateAggregator;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class ClosingDateAggregatorTest extends AbstractAggregatorTest
{
    private static ClosingDateAggregator $closingDateAggregator;

    private static EntityManagerInterface $entityManager;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::bootKernel();

        self::$closingDateAggregator = self::$container->get(ClosingDateAggregator::class);
        self::$entityManager = self::$container->get(EntityManagerInterface::class);
    }

    public function getAggregator()
    {
        return self::$closingDateAggregator;
    }

    public function getFormData()
    {
        yield ['frequency' => 'YYYY'];
        yield ['frequency' => 'YYYY-MM'];
        yield ['frequency' => 'YYYY-IV'];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();
        self::$entityManager = self::$container->get(EntityManagerInterface::class);

        $data = [
            self::$entityManager->createQueryBuilder()
                ->select('count(acp.id)')
                ->from(AccompanyingPeriod::class, 'acp'),
        ];

        self::ensureKernelShutdown();

        return $data;
    }
}
