<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators\UserJobAggregator;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class UserJobAggregatorTest extends AbstractAggregatorTest
{
    private UserJobAggregator $aggregator;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->aggregator = self::$container->get('chill.person.export.aggregator_referrer_job');
    }

    public function getAggregator()
    {
        return $this->aggregator;
    }

    public function getFormData(): array
    {
        return [
            [
                'job_at' => new RollingDate(RollingDate::T_FIXED_DATE, \DateTimeImmutable::createFromFormat('Y-m-d', '2020-01-01')),
            ],
        ];
    }

    public function getQueryBuilders(): array
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(acp.id)')
                ->from(AccompanyingPeriod::class, 'acp')
                ->join('acp.job', 'acpjob'),
            $em->createQueryBuilder()
                ->select('count(acp.id)')
                ->from(AccompanyingPeriod::class, 'acp'),
        ];
    }
}
