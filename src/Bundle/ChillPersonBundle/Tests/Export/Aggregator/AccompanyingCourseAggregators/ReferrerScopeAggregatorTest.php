<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Repository\ScopeRepositoryInterface;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators\ReferrerScopeAggregator;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @internal
 *
 * @coversNothing
 */
final class ReferrerScopeAggregatorTest extends AbstractAggregatorTest
{
    use ProphecyTrait;

    public function getAggregator()
    {
        $translatableStringHelper = $this->prophesize(TranslatableStringHelperInterface::class);
        $translatableStringHelper->localize(Argument::type('array'))->willReturn('localized');

        $scopeRepository = $this->prophesize(ScopeRepositoryInterface::class);
        $scopeRepository->find(Argument::type('int'))->willReturn(
            (new Scope())->setName(['fr' => 'scope'])
        );

        $dateConverter = $this->prophesize(RollingDateConverterInterface::class);
        $dateConverter->convert(Argument::type(RollingDate::class))->willReturn(new \DateTimeImmutable());

        return new ReferrerScopeAggregator(
            $scopeRepository->reveal(),
            $translatableStringHelper->reveal(),
            $dateConverter->reveal()
        );
    }

    public function getFormData()
    {
        return [
            [
                'date_calc' => new RollingDate(RollingDate::T_TODAY),
            ],
        ];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(acp.id)')
                ->from(AccompanyingPeriod::class, 'acp'),
        ];
    }
}
