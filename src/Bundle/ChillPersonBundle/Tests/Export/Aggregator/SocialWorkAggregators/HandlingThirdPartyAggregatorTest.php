<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Aggregator\SocialWorkAggregators;

use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWork;
use Chill\PersonBundle\Export\Aggregator\SocialWorkAggregators\HandlingThirdPartyAggregator;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class HandlingThirdPartyAggregatorTest extends AbstractAggregatorTest
{
    private static HandlingThirdPartyAggregator $handlingThirdPartyAggregator;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::bootKernel();
        self::$handlingThirdPartyAggregator = self::$container->get(HandlingThirdPartyAggregator::class);
    }

    public function getAggregator()
    {
        return self::$handlingThirdPartyAggregator;
    }

    public function getFormData()
    {
        return [
            [],
        ];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();

        $em = self::$container
            ->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(acpw.id)')
                ->from(AccompanyingPeriodWork::class, 'acpw'),
        ];
    }
}
