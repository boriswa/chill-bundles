<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Aggregator\SocialWorkAggregators;

use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWork;
use Chill\PersonBundle\Export\Aggregator\SocialWorkAggregators\GoalAggregator;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class GoalAggregatorTest extends AbstractAggregatorTest
{
    private GoalAggregator $aggregator;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->aggregator = self::$container->get('chill.person.export.aggregator_goal');
    }

    public function getAggregator()
    {
        return $this->aggregator;
    }

    public function getFormData(): array
    {
        return [
            [],
        ];
    }

    public function getQueryBuilders(): array
    {
        self::bootKernel();

        $em = self::$container
            ->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(acpw.id)')
                ->from(AccompanyingPeriodWork::class, 'acpw'),
        ];
    }
}
