<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Aggregator\PersonAggregators;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Export\Aggregator\PersonAggregators\PostalCodeAggregator;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class PostalCodeAggregatorTest extends AbstractAggregatorTest
{
    private RollingDateConverterInterface $rollingDateConverter;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->rollingDateConverter = self::$container->get(RollingDateConverterInterface::class);
    }

    public function getAggregator()
    {
        return new PostalCodeAggregator($this->rollingDateConverter);
    }

    public function getFormData()
    {
        return [
            ['calc_date' => new RollingDate(RollingDate::T_TODAY)],
        ];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();

        $em = self::$container
            ->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(person.id)')
                ->from(Person::class, 'person'),
        ];
    }
}
