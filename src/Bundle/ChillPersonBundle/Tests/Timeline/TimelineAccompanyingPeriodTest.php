<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Timeline;

use Chill\MainBundle\Test\PrepareClientTrait;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * This class tests entries are shown for closing and opening
 * periods in timeline.
 *
 * @internal
 *
 * @coversNothing
 */
final class TimelineAccompanyingPeriodTest extends WebTestCase
{
    use PrepareClientTrait;

    public function provideDataPersonWithAccompanyingPeriod()
    {
        self::bootKernel();

        $qb = self::$container->get(EntityManagerInterface::class)
            ->createQueryBuilder();
        $personIds = $qb
            ->from(Person::class, 'p')
            ->join('p.accompanyingPeriodParticipations', 'part')
            ->join('part.accompanyingPeriod', 'period')
            ->join('p.centerCurrent', 'cc')
            ->join('cc.center', 'center')
            ->select('p.id')
            ->where($qb->expr()->eq('center.name', ':center'))
            ->setParameter('center', 'Center A')
            ->setMaxResults(1000)
            ->getQuery()
            ->getResult();

        \shuffle($personIds);

        yield [\array_pop($personIds)['id']];

        yield [\array_pop($personIds)['id']];

        yield [\array_pop($personIds)['id']];
    }

    /**
     * @dataProvider provideDataPersonWithAccompanyingPeriod
     */
    public function testEntriesAreShown(mixed $personId): never
    {
        $this->markTestSkipped('page does not work');

        $client = $this->getClientAuthenticated();

        $crawler = $client->request('GET', "/en/person/{$personId}/timeline");

        $this->assertTrue(
            $client->getResponse()->isSuccessful(),
            'the timeline page loads sucessfully'
        );
        $this->assertGreaterThan(
            0,
            $crawler->filter('.timeline div')->count(),
            'the timeline page contains multiple div inside a .timeline element'
        );
        $this->assertStringContainsString(
            'est ouvert',
            $crawler->filter('.timeline')->text(),
            "the text 'est ouvert' is present"
        );
    }
}
