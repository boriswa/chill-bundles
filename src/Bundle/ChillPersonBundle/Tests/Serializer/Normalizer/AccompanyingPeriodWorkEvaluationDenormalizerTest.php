<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Serializer\Normalizer;

use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluation;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocument;
use Chill\PersonBundle\Serializer\Normalizer\AccompanyingPeriodWorkEvaluationDenormalizer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * @internal
 *
 * @coversNothing
 */
final class AccompanyingPeriodWorkEvaluationDenormalizerTest extends TestCase
{
    private const ENCODED_DATA = '        {
            "type": "accompanying_period_work_evaluation",
            "key": 0,
            "evaluation": {
                "id": 100,
                "type": "social_work_evaluation"
            },
            "startDate": {
                "datetime": "2022-04-29T00:00:00+02:00"
            },
            "endDate": null,
            "maxDate": null,
            "warningInterval": "P0D",
            "comment": "",
            "documents": [
                {
                    "type": "accompanying_period_work_evaluation_document",
                    "id": 1,
                    "storedObject": {
                        "creationDate": {
                            "datetime": "2022-05-30T21:22:47+0200"
                        },
                        "datas": [],
                        "filename": "ZeoWSjqVc2qN1XUHptTV6S",
                        "id": 11,
                        "iv": [
                            89,
                            164,
                            162,
                            48,
                            155,
                            159,
                            69,
                            135,
                            191,
                            241,
                            241,
                            8,
                            17,
                            56,
                            183,
                            224
                        ],
                        "keyInfos": {
                            "alg": "A256CBC",
                            "ext": true,
                            "k": "uW4d7si_hi--VQHxi76ZllKQiYzaEJYGN8KBrWXxi7s",
                            "key_ops": [
                                "encrypt",
                                "decrypt"
                            ],
                            "kty": "oct"
                        },
                        "title": "",
                        "type": "application/vnd.oasis.opendocument.text",
                        "uuid": "b9dd9eff-a7cf-4a29-89e3-57efc9ed215b"
                    },
                    "title": "test",
                    "key": 2,
                    "workflows_availables": [
                        {
                            "name": "vendee_internal",
                            "text": "Suivi CD85"
                        }
                    ],
                    "workflows": []
                }
            ],
            "id": 382
        }';

    public function testAssociatedDocumentIsTheSame(): never
    {
        $this->markTestIncomplete('not yet finished');
        $evaluation = new AccompanyingPeriodWorkEvaluation();
        $doc = new AccompanyingPeriodWorkEvaluationDocument();
        $doc->setStoredObject($storedObject = new StoredObject());
        $reflectionProperty = new \ReflectionProperty(AccompanyingPeriodWorkEvaluationDocument::class, 'id');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($doc, 1);

        $evaluation->addDocument($doc);

        $data = json_decode(self::ENCODED_DATA);
        $context =
            [AbstractNormalizer::OBJECT_TO_POPULATE => $evaluation, 'groups' => ['write']];

        $denormalizer = new AccompanyingPeriodWorkEvaluationDenormalizer();

        /*
        $this->assertTrue(
            $denormalizer->supportsDenormalization(
                $data,
                AccompanyingPeriodWorkEvaluation::class,
                'json',
                $context
            )
        );
         */

        $denormalizedEvaluation = $denormalizer->denormalize(
            $data,
            AccompanyingPeriodWorkEvaluation::class,
            'json',
            $context
        );

        $this->assertSame($evaluation, $denormalizedEvaluation);
        $this->assertCount(1, $evaluation->getDocuments());
        $this->assertSame($doc, $evaluation->getDocuments()->first());
        $this->assertNotSame($storedObject, $evaluation->getDocuments()->first()->getStoredObject());
    }
}
