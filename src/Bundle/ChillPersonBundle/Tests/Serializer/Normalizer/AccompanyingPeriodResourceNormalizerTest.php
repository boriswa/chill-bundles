<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Serializer\Normalizer;

use Chill\PersonBundle\Entity\AccompanyingPeriod\Resource;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class AccompanyingPeriodResourceNormalizerTest extends KernelTestCase
{
    private NormalizerInterface $normalizer;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->normalizer = self::$container->get(NormalizerInterface::class);
    }

    public function testNormalizeNullHasSameValueAsNotNull()
    {
        $nullResource = $this->normalizer->normalize(null, 'docgen', ['groups' => 'docgen:read', 'docgen:expects' => Resource::class]);
        $notNull = $this->normalizer->normalize(new Resource(), 'docgen', ['groups' => 'docgen:read', 'docgen:expects' => Resource::class]);

        $this->assertEqualsCanonicalizing(array_keys($notNull), array_keys($nullResource));
    }

    public function testNormalizeResource()
    {
        $resource = new Resource();
        $resource
            ->setComment('blabla')
            ->setResource(new ThirdParty());

        $expected = [
            'type' => 'accompanying_period_resource',
            'isNull' => false,
            'comment' => 'blabla',
        ];

        $actual = $this->normalizer->normalize($resource, 'docgen', ['groups' => 'docgen:read', 'docgen:expects' => Resource::class]);

        // we do not test for sub array (person, thirdparty). We then check first for base value...
        foreach ($expected as $key => $value) {
            $this->assertArrayHasKey($key, $actual);
            $this->assertEquals($value, $actual[$key]);
        }

        // ... and then for the existence of some values
        $this->assertArrayHasKey('person', $actual);
        $this->assertArrayHasKey('thirdParty', $actual);
    }
}
