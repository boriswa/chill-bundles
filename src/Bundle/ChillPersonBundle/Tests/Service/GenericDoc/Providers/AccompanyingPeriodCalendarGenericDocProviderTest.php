<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Service\GenericDoc\Providers;

use Chill\CalendarBundle\Security\Voter\CalendarVoter;
use Chill\CalendarBundle\Service\GenericDoc\Providers\AccompanyingPeriodCalendarGenericDocProvider;
use Chill\DocStoreBundle\GenericDoc\FetchQueryToSqlBuilder;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Security;

/**
 * @internal
 *
 * @coversNothing
 */
class AccompanyingPeriodCalendarGenericDocProviderTest extends KernelTestCase
{
    use ProphecyTrait;

    private Security $security;

    private EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->security = self::$container->get(Security::class);
        $this->entityManager = self::$container->get(EntityManagerInterface::class);
    }

    /**
     * @dataProvider provideDataForAccompanyingPeriod
     */
    public function testBuildFetchQueryForAccompanyingPeriod(AccompanyingPeriod $accompanyingPeriod, ?\DateTimeImmutable $startDate, ?\DateTimeImmutable $endDate, ?string $content): void
    {
        $provider = new AccompanyingPeriodCalendarGenericDocProvider($this->security, $this->entityManager);

        $query = $provider->buildFetchQueryForAccompanyingPeriod($accompanyingPeriod, $startDate, $endDate, $content);

        ['sql' => $sql, 'params' => $params, 'types' => $types] = (new FetchQueryToSqlBuilder())->toSql($query);

        $nb = $this->entityManager->getConnection()->fetchOne("SELECT COUNT(*) FROM ({$sql}) AS sq", $params, $types);

        self::assertIsInt($nb);
    }

    /**
     * @dataProvider provideDataForPerson
     */
    public function testBuildFetchQueryForPerson(Person $person, ?\DateTimeImmutable $startDate, ?\DateTimeImmutable $endDate, ?string $content): void
    {
        $security = $this->prophesize(Security::class);
        $security->isGranted(CalendarVoter::SEE, Argument::any())->willReturn(true);

        $provider = new AccompanyingPeriodCalendarGenericDocProvider($security->reveal(), $this->entityManager);

        $query = $provider->buildFetchQueryForPerson($person, $startDate, $endDate, $content);

        ['sql' => $sql, 'params' => $params, 'types' => $types] = (new FetchQueryToSqlBuilder())->toSql($query);

        $nb = $this->entityManager->getConnection()->fetchOne("SELECT COUNT(*) FROM ({$sql}) AS sq", $params, $types);

        self::assertIsInt($nb);
        self::assertStringNotContainsStringIgnoringCase('FALSE = TRUE', $sql);
        self::assertStringNotContainsStringIgnoringCase('TRUE = FALSE', $sql);
    }

    /**
     * @dataProvider provideDataForPerson
     */
    public function testBuildFetchQueryForPersonWithoutAnyRight(Person $person, ?\DateTimeImmutable $startDate, ?\DateTimeImmutable $endDate, ?string $content): void
    {
        $security = $this->prophesize(Security::class);
        $security->isGranted(CalendarVoter::SEE, Argument::any())->willReturn(false);

        $provider = new AccompanyingPeriodCalendarGenericDocProvider($security->reveal(), $this->entityManager);

        $query = $provider->buildFetchQueryForPerson($person, $startDate, $endDate, $content);

        ['sql' => $sql, 'params' => $params, 'types' => $types] = (new FetchQueryToSqlBuilder())->toSql($query);

        $nb = $this->entityManager->getConnection()->fetchOne("SELECT COUNT(*) FROM ({$sql}) AS sq", $params, $types);

        self::assertIsInt($nb);
        self::assertStringContainsStringIgnoringCase('TRUE = FALSE', $sql);
    }

    public function provideDataForPerson(): iterable
    {
        $this->setUp();

        if (null === $person = $this->entityManager->createQuery('SELECT p FROM '.Person::class.' p WHERE SIZE(p.accompanyingPeriodParticipations) > 0')
            ->setMaxResults(1)->getSingleResult()) {
            throw new \RuntimeException('There is no person');
        }

        yield [$person, null, null, null];
        yield [$person, new \DateTimeImmutable('1 year ago'), null, null];
        yield [$person, new \DateTimeImmutable('1 year ago'), new \DateTimeImmutable('6 month ago'), null];
        yield [$person, new \DateTimeImmutable('1 year ago'), new \DateTimeImmutable('6 month ago'), 'text'];
        yield [$person, null, null, 'text'];
        yield [$person, null, new \DateTimeImmutable('6 month ago'), null];
    }

    public function provideDataForAccompanyingPeriod(): iterable
    {
        $this->setUp();

        if (null === $period = $this->entityManager->createQuery('SELECT p FROM '.AccompanyingPeriod::class.' p ')
            ->setMaxResults(1)->getSingleResult()) {
            throw new \RuntimeException('There is no accompanying period');
        }

        yield [$period, null, null, null];
        yield [$period, new \DateTimeImmutable('1 year ago'), null, null];
        yield [$period, new \DateTimeImmutable('1 year ago'), new \DateTimeImmutable('6 month ago'), null];
        yield [$period, new \DateTimeImmutable('1 year ago'), new \DateTimeImmutable('6 month ago'), 'text'];
        yield [$period, null, null, 'text'];
        yield [$period, null, new \DateTimeImmutable('6 month ago'), null];
    }
}
