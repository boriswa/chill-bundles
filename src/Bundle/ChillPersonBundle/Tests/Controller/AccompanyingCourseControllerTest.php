<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Controller;

use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Test\PrepareClientTrait;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class AccompanyingCourseControllerTest extends WebTestCase
{
    use PrepareClientTrait;

    private KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = $this->getClientAuthenticated();
    }

    protected function tearDown(): void
    {
        self::ensureKernelShutdown();
    }

    public function dataGenerateRandomUsers(): \Iterator
    {
        self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        $period = new AccompanyingPeriod(new \DateTime('1 week ago'));
        $user = $em->getRepository(User::class)
            ->findOneByUsernameCanonical('center a_social');
        $period->setCreatedBy($user);
        // $period->setCreatedAt(new \DateTime('yesterday'));

        $center = $em->getRepository(Center::class)
            ->findOneBy(['name' => 'Center A']);

        $personIds = $em->createQuery('SELECT p.id FROM '.
            Person::class.' p JOIN p.centerCurrent cc '.
            ' WHERE cc.center = :center')
            ->setParameter('center', $center)
            ->setMaxResults(100)
            ->getScalarResult();

        yield [\array_pop($personIds), \array_pop($personIds)];

        self::ensureKernelShutdown();
    }

    public function testNewWithoutUsers()
    {
        $this->client->request('GET', '/fr/person/parcours/new');

        $this->assertResponseRedirects();
        $location = $this->client->getResponse()->headers->get('Location');

        $this->assertEquals(1, \preg_match('|^\\/[^\\/]+\\/parcours/([\\d]+)/edit$|', (string) $location));
    }

    /**
     * @dataProvider dataGenerateRandomUsers
     */
    public function testWithNewUsers(mixed $personId0, mixed $personId1)
    {
        $this->client->request('GET', '/fr/person/parcours/new', [
            'person_id' => [
                $personId0,
                $personId1,
            ],
        ]);

        $this->assertResponseRedirects();
        $location = $this->client->getResponse()->headers->get('Location');
        $matches = [];

        $this->assertEquals(1, \preg_match('|^\\/[^\\/]+\\/parcours/([\\d]+)/edit$|', (string) $location, $matches));
        $id = $matches[1];

        $period = self::$container->get(EntityManagerInterface::class)
            ->getRepository(AccompanyingPeriod::class)
            ->find($id);

        $this->assertNotNull($period);

        $this->assertEquals(2, \count($period->getParticipations()));
    }
}
