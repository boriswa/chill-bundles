<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Repository;

use Chill\MainBundle\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class UserRepositoryTest extends KernelTestCase
{
    private UserRepository $userRepository;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        $this->userRepository = self::$container->get(UserRepository::class);
    }

    public function testFindAllAsArray(): void
    {
        $userIterator = $this->userRepository->findAllAsArray('fr');

        self::assertIsIterable($userIterator);
        $i = 0;
        foreach ($userIterator as $u) {
            self::assertIsArray($u);
            ++$i;
        }
        self::assertGreaterThan(0, $i);
    }
}
