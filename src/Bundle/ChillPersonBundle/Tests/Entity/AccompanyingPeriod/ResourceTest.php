<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Entity\AccompanyingPeriod;

use Chill\PersonBundle\Entity\AccompanyingPeriod\Resource;
use Chill\PersonBundle\Entity\Person;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class ResourceTest extends TestCase
{
    public function testSetResource()
    {
        $person = new Person();
        $thirdParty = new ThirdParty();

        $resource = new Resource();

        $resource->setResource($person);

        $this->assertSame($person, $resource->getResource());
        $this->assertNull($resource->getThirdParty());

        $resource->setResource($thirdParty);

        $this->assertSame($thirdParty, $resource->getResource());
        $this->assertNull($resource->getPerson());

        // we repeat adding a person, to ensure that third party is
        // well reset
        $resource->setResource($person);
        $this->assertSame($person, $resource->getResource());
        $this->assertNull($resource->getThirdParty());

        $resource->setResource(null);
        $this->assertNull($resource->getThirdParty());
        $this->assertNull($resource->getPerson());
        $this->assertNull($resource->getResource());
    }
}
