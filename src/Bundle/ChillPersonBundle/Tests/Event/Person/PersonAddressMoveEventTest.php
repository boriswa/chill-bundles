<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Event\Person;

use Chill\MainBundle\Entity\Address;
use Chill\PersonBundle\Entity\Household\Household;
use Chill\PersonBundle\Entity\Household\HouseholdMember;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Event\Person\PersonAddressMoveEvent;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class PersonAddressMoveEventTest extends TestCase
{
    public function testPersonChangeAddress()
    {
        $person = new Person();

        $household = (new Household())->addAddress(
            ($previousAddress = new Address())->setValidFrom(new \DateTime('1 year ago'))
        );
        $household->addAddress(
            ($nextAddress = new Address())->setValidFrom(new \DateTime('1 month ago'))
        );
        $member = new HouseholdMember();
        $member
            ->setPerson($person)
            ->setHousehold($household)
            ->setStartDate(new \DateTimeImmutable('1 year ago'))
            ->setEndDate(new \DateTimeImmutable('tomorrow'));

        $event = new PersonAddressMoveEvent($person);
        $event
            ->setPreviousAddress($previousAddress)
            ->setNextAddress($nextAddress);

        $this->assertSame($previousAddress, $event->getPreviousAddress());
        $this->assertSame($nextAddress, $event->getNextAddress());
        $this->assertEquals((new \DateTime('1 month ago'))->format('Y-m-d'), $nextAddress->getValidFrom()->format('Y-m-d'));
        $this->assertEquals((new \DateTime('1 month ago'))->format('Y-m-d'), $event->getMoveDate()->format('Y-m-d'));
    }

    public function testPersonChangeHousehold()
    {
        $person = new Person();

        $previousHousehold = (new Household())->addAddress(
            ($previousAddress = new Address())->setValidFrom(new \DateTime('1 year ago'))
        );
        $previousMembership = new HouseholdMember();
        $previousMembership
            ->setPerson($person)
            ->setHousehold($previousHousehold)
            ->setStartDate(new \DateTimeImmutable('1 year ago'))
            ->setEndDate(new \DateTimeImmutable('tomorrow'));

        $nextHousehold = (new Household())->addAddress(
            ($nextAddress = new Address())->setValidFrom(new \DateTime('tomorrow'))
        );
        $nextMembership = new HouseholdMember();
        $nextMembership
            ->setPerson($person)
            ->setHousehold($nextHousehold)
            ->setStartDate(new \DateTimeImmutable('tomorrow'));

        $event = new PersonAddressMoveEvent($person);
        $event
            ->setPreviousMembership($previousMembership)
            ->setNextMembership($nextMembership);

        $this->assertTrue($event->personChangeHousehold());
        $this->assertSame($previousAddress, $event->getPreviousAddress());
        $this->assertSame($nextAddress, $event->getNextAddress());
        $this->assertTrue($event->personChangeAddress());
        $this->assertFalse($event->personLeaveWithoutHousehold());
        $this->assertEquals(new \DateTimeImmutable('tomorrow'), $event->getMoveDate());
    }

    public function testPersonLeaveHousehold()
    {
        $person = new Person();

        $previousHousehold = (new Household())->addAddress(
            ($previousAddress = new Address())->setValidFrom(new \DateTime('1 year ago'))
        );
        $previousMembership = new HouseholdMember();
        $previousMembership
            ->setPerson($person)
            ->setHousehold($previousHousehold)
            ->setStartDate(new \DateTimeImmutable('1 year ago'))
            ->setEndDate(new \DateTimeImmutable('tomorrow'));

        $event = new PersonAddressMoveEvent($person);
        $event
            ->setPreviousMembership($previousMembership);

        $this->assertTrue($event->personChangeHousehold());
        $this->assertSame($previousAddress, $event->getPreviousAddress());
        $this->assertNull($event->getNextAddress());
        $this->assertTrue($event->personChangeAddress());
        $this->assertTrue($event->personLeaveWithoutHousehold());
        $this->assertEquals(new \DateTimeImmutable('tomorrow'), $event->getMoveDate());
    }
}
