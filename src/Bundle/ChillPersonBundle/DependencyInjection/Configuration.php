<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    private string $validationBirthdateNotAfterInfos = 'The period before today during which'
        .' any birthdate is not allowed. The birthdate is expressed as ISO8601 : '
        .'https://en.wikipedia.org/wiki/ISO_8601#Durations';

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('cl_chill_person');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->canBeDisabled()
            ->children()
            ->arrayNode('validation')
            ->canBeDisabled()
            ->children()
            ->booleanNode('center_required')
            ->info('Enable a center for each person entity. If disabled, you must provide your own center provider')
            ->defaultValue(true)
            ->end()
            ->scalarNode('birthdate_not_after')
            ->info($this->validationBirthdateNotAfterInfos)
            ->defaultValue('P1D')
            ->validate()
            ->ifTrue(static function ($period) {
                try {
                    $interval = new \DateInterval($period);
                } catch (\Exception) {
                    return true;
                }

                return false;
            })
            ->thenInvalid('Invalid period for birthdate validation : "%s" '
                                                .'The parameter should match duration as defined by ISO8601 : '
                                                .'https://en.wikipedia.org/wiki/ISO_8601#Durations')
            ->end() // birthdate_not_after, parent = children of validation
            ->end() // children for 'validation', parent = validation
            ->end() // validation, parent = children of root
            ->end() // children of root, parent = root
            ->booleanNode('create_person_allowed')
            ->defaultTrue()
            ->end()
            ->booleanNode('create_parcours_allowed')
            ->defaultTrue()
            ->end()
            ->arrayNode('person_fields')
            ->canBeDisabled()
            ->children()
            ->append($this->addFieldNode('place_of_birth'))
            ->append($this->addFieldNode('email'))
            ->append($this->addFieldNode('phonenumber'))
            ->append($this->addFieldNode('mobilenumber'))
            ->append($this->addFieldNode('contact_info'))
            ->append($this->addFieldNode('nationality'))
            ->append($this->addFieldNode('country_of_birth'))
            ->append($this->addFieldNode('marital_status'))
            ->append($this->addFieldNode('civility'))
            ->append($this->addFieldNode('spoken_languages'))
            ->append($this->addFieldNode('address'))
            ->append($this->addFieldNode('accompanying_period'))
            ->append($this->addFieldNode('memo'))
            ->append($this->addFieldNode('number_of_children'))
            ->append($this->addFieldNode('acceptEmail'))
            ->append($this->addFieldNode('deathdate'))
            ->arrayNode('alt_names')
            ->defaultValue([])
            ->arrayPrototype()
            ->children()
            ->scalarNode('key')
            ->isRequired()->cannotBeEmpty()
            ->end()
            ->arrayNode('labels')
            ->children()
            ->scalarNode('lang')->isRequired()->cannotBeEmpty()
            ->example('fr')
            ->end()
            ->scalarNode('label')->isRequired()->cannotBeEmpty()
            ->example('Nom de jeune fille')
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end() // children for 'person_fields', parent = array 'person_fields'
            ->end() // person_fields, parent = children of root
            ->arrayNode('accompanying_periods_fields')
            ->canBeDisabled()
            ->children()
            ->append($this->addFieldNode('user'))
            ->append($this->addFieldNode('createdBy'))
            ->append($this->addFieldNode('step'))
            ->append($this->addFieldNode('origin'))
            ->append($this->addFieldNode('intensity'))
            ->append($this->addFieldNode('scopes'))
            ->append($this->addFieldNode('requestor'))
            ->append($this->addFieldNode('anonymous'))
            ->append($this->addFieldNode('emergency'))
            ->append($this->addFieldNode('confidential'))
            ->end() // children for 'accompanying_person_fields', parent = array 'person_fields'
            ->end() // paccompanying_person_fields, parent = children of root
            ->booleanNode('allow_multiple_simultaneous_accompanying_periods')
            ->info('Can we have more than one simultaneous accompanying period in the same time. Default false.')
            ->defaultValue(false)
            ->end()
            ->arrayNode('accompanying_period_lifecycle_delays')
            ->addDefaultsIfNotSet()
            ->info('Delays before marking an accompanying period as inactive')
            ->children()
            ->booleanNode('mark_inactive')->defaultTrue()->end()
            ->scalarNode('mark_inactive_short_after')->defaultValue('P6M')->end()
            ->scalarNode('mark_inactive_long_after')->defaultValue('P2Y')->end()
            ->end()
            ->end() // end of 'accompanying_period_lifecycle_delays
            ->end() // children of 'root', parent = root
        ;

        return $treeBuilder;
    }

    private function addFieldNode($key)
    {
        $tree = new TreeBuilder($key, 'enum');
        $node = $tree->getRootNode();

        $info = match ($key) {
            'accompanying_period' => 'If the accompanying periods are shown',
            default => "If the field {$key} must be shown",
        };

        $node
            ->values(['hidden', 'visible'])
            ->defaultValue('visible')
            ->info($info)
            ->end();

        return $node;
    }
}
