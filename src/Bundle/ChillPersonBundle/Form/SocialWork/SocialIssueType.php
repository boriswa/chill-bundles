<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Form\SocialWork;

use Chill\MainBundle\Form\Type\ChillDateType;
use Chill\MainBundle\Form\Type\TranslatableStringFormType;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\PersonBundle\Entity\SocialWork\SocialIssue;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SocialIssueType extends AbstractType
{
    public function __construct(protected TranslatableStringHelperInterface $translatableStringHelper)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TranslatableStringFormType::class, [
                'label' => 'Nom',
            ]);

        if ('create' === $options['step']) {
            $builder
                ->add('parent', EntityType::class, [
                    'class' => SocialIssue::class,
                    'required' => false,
                    'choice_label' => fn (SocialIssue $issue): ?string => $this->translatableStringHelper->localize($issue->getTitle()),
                    'mapped' => 'create' === $options['step'],
                ]);
        }

        $builder
            ->add('ordering', NumberType::class, [
                'required' => true,
                'scale' => 6,
            ])
            ->add('desactivationDate', ChillDateType::class, [
                'label' => 'goal.desactivationDate',
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('class', SocialIssue::class);

        $resolver->setRequired('step')
            ->setAllowedValues('step', ['create', 'edit']);
    }
}
