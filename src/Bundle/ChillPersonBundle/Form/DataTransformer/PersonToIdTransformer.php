<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Form\DataTransformer;

use Chill\MainBundle\Form\DataTransformer\IdToEntityDataTransformer;
use Chill\PersonBundle\Repository\PersonRepository;

class PersonToIdTransformer extends IdToEntityDataTransformer
{
    public function __construct(PersonRepository $repository)
    {
        parent::__construct($repository, false);
    }
}
