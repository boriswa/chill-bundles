<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Form;

use Chill\MainBundle\Form\Type\ChillDateType;
use Chill\MainBundle\Form\Type\CommentType;
use Chill\PersonBundle\Entity\Household\Household;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HouseholdType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('commentMembers', CommentType::class, [
                'label' => 'household.comment_membership',
                'required' => false,
            ])
            ->add('waitingForBirth', CheckboxType::class, [
                'required' => false,
                'label' => 'household.expecting_birth',
            ])
            ->add('waitingForBirthDate', ChillDateType::class, [
                'required' => false,
                'label' => 'household.date_expecting_birth',
                'input' => 'datetime_immutable',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Household::class,
        ]);
    }
}
