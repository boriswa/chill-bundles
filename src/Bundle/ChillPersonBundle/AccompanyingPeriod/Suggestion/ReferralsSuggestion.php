<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\AccompanyingPeriod\Suggestion;

use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Entity\AccompanyingPeriod;

/**
 * Basic implementation: this suggestion does not return any suggestion.
 */
final class ReferralsSuggestion implements ReferralsSuggestionInterface
{
    public function countReferralSuggested(AccompanyingPeriod $period, ?array $options = []): int
    {
        return 0;
    }

    /**
     * @return array|User[]
     */
    public function findReferralSuggested(AccompanyingPeriod $period, int $limit = 50, int $start = 0): array
    {
        return [];
    }
}
