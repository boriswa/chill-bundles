<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\AccompanyingPeriod\Lifecycle;

use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Workflow\Registry;

/**
 * Change the step of an accompanying period.
 *
 * This should be invoked through scripts (not in the in context of an http request, or an
 * action from a user).
 */
class AccompanyingPeriodStepChanger
{
    private const LOG_PREFIX = '[AccompanyingPeriodStepChanger] ';

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $logger,
        private readonly Registry $workflowRegistry,
    ) {
    }

    public function __invoke(AccompanyingPeriod $period, string $transition, ?string $workflowName = null): void
    {
        $workflow = $this->workflowRegistry->get($period, $workflowName);

        if (!$workflow->can($period, $transition)) {
            $this->logger->info(self::LOG_PREFIX.'not able to apply the transition on period', [
                'period_id' => $period->getId(),
                'transition' => $transition,
            ]);

            return;
        }

        $workflow->apply($period, $transition);

        $this->entityManager->flush();

        $this->logger->info(self::LOG_PREFIX.'could apply a transition', [
            'period_id' => $period->getId(),
            'transition' => $transition,
        ]);
    }
}
