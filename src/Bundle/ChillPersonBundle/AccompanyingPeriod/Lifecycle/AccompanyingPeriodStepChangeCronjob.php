<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\AccompanyingPeriod\Lifecycle;

use Chill\MainBundle\Cron\CronJobInterface;
use Chill\MainBundle\Entity\CronJobExecution;
use Symfony\Component\Clock\ClockInterface;

readonly class AccompanyingPeriodStepChangeCronjob implements CronJobInterface
{
    public function __construct(
        private ClockInterface $clock,
        private AccompanyingPeriodStepChangeRequestor $requestor,
    ) {
    }

    public function canRun(?CronJobExecution $cronJobExecution): bool
    {
        $now = $this->clock->now();

        if (null !== $cronJobExecution && $now->sub(new \DateInterval('P1D')) < $cronJobExecution->getLastStart()) {
            return false;
        }

        return in_array((int) $now->format('H'), [1, 2, 3, 4, 5, 6], true);
    }

    public function getKey(): string
    {
        return 'accompanying-period-step-change';
    }

    public function run(array $lastExecutionData): ?array
    {
        ($this->requestor)();

        return null;
    }
}
