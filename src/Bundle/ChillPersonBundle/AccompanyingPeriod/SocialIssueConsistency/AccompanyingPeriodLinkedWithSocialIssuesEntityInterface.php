<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\AccompanyingPeriod\SocialIssueConsistency;

use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\SocialWork\SocialIssue;
use Doctrine\Common\Collections\Collection;

/**
 * This interface must be implemented on entities which:.
 *
 * * have both social issue and is linked to an AccompanyingPeriod
 * * when the social issues in the entity should be added to the accompanying period
 *
 * A doctrine listener will list social issues which are associated to the entity, but
 * not on the AccompanyingPeriod, and push them back to the accompanying period.
 */
interface AccompanyingPeriodLinkedWithSocialIssuesEntityInterface
{
    public function getAccompanyingPeriod(): ?AccompanyingPeriod;

    /**
     * @return Collection|SocialIssue[]
     */
    public function getSocialIssues(): Collection;

    public function removeSocialIssue(SocialIssue $issue): self;
}
