<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Command;

use Chill\PersonBundle\Service\AccompanyingPeriod\OldDraftAccompanyingPeriodRemoverInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveOldDraftAccompanyingPeriodCommand extends Command
{
    public function __construct(private readonly LoggerInterface $logger, private readonly OldDraftAccompanyingPeriodRemoverInterface $remover)
    {
        parent::__construct('chill:person:remove-old-draft-period');
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Remove draft accompanying period which are still draft and unused')
            ->addArgument('interval', InputArgument::OPTIONAL, 'The interval for unactive periods', 'P15D');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->logger->info('['.$this->getName().'] started', [
            'interval' => $input->getArgument('interval'),
        ]);

        try {
            $interval = new \DateInterval($input->getArgument('interval'));
        } catch (\Exception $e) {
            $this->logger->error('['.$this->getName().'] bad interval');

            throw $e;
        }

        $this->remover->remove($interval);

        $this->logger->info('['.$this->getName().'] end of command');

        return 0;
    }
}
