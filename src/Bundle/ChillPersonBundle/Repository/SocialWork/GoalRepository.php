<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository\SocialWork;

use Chill\PersonBundle\Entity\SocialWork\Goal;
use Chill\PersonBundle\Entity\SocialWork\SocialAction;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ObjectRepository;

final class GoalRepository implements ObjectRepository
{
    private readonly EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Goal::class);
    }

    public function countBySocialActionWithDescendants(SocialAction $action): int
    {
        $qb = $this->buildQueryBySocialActionWithDescendants($action);
        $qb->select('COUNT(g)');

        return $qb
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function find($id): ?Goal
    {
        return $this->repository->find($id);
    }

    /**
     * @return array<int, Goal>
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return array<int, Goal>
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @return Goal[]
     */
    public function findBySocialActionWithDescendants(SocialAction $action, array $orderBy = [], ?int $limit = null, ?int $offset = null): array
    {
        $qb = $this->buildQueryBySocialActionWithDescendants($action);
        $qb->select('g');

        $qb->andWhere(
            $qb->expr()->orX(
                $qb->expr()->isNull('g.desactivationDate'),
                $qb->expr()->gt('g.desactivationDate', ':now')
            )
        )
            ->setParameter('now', new \DateTime('now'));

        foreach ($orderBy as $sort => $order) {
            $qb->addOrderBy('g.'.$sort, $order);
        }

        return $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }

    public function findOneBy(array $criteria, ?array $orderBy = null): ?Goal
    {
        return $this->repository->findOneBy($criteria, $orderBy);
    }

    /**
     * @return class-string
     */
    public function getClassName(): string
    {
        return Goal::class;
    }

    private function buildQueryBySocialActionWithDescendants(SocialAction $action): QueryBuilder
    {
        $actions = $action->getDescendantsWithThis();

        $qb = $this->repository->createQueryBuilder('g');

        $orx = $qb->expr()->orX();
        $i = 0;

        foreach ($actions as $act) {
            $orx->add(":action_{$i} MEMBER OF g.socialActions");
            $qb->setParameter("action_{$i}", $act);
        }
        $qb->where($orx);

        return $qb;
    }
}
