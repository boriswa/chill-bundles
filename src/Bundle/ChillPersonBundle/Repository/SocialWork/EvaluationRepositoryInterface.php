<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository\SocialWork;

use Chill\PersonBundle\Entity\SocialWork\Evaluation;
use Doctrine\Persistence\ObjectRepository;

interface EvaluationRepositoryInterface extends ObjectRepository
{
    public function find($id): ?Evaluation;

    /**
     * @return array<int, Evaluation>
     */
    public function findAll(): array;

    /**
     * @return array<int, Evaluation>
     */
    public function findAllActive(): array;

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return array<int, Evaluation>
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array;

    public function findOneBy(array $criteria, ?array $orderBy = null): ?Evaluation;

    /**
     * @return class-string
     */
    public function getClassName(): string;
}
