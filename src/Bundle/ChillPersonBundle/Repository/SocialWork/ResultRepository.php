<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository\SocialWork;

use Chill\PersonBundle\Entity\SocialWork\Goal;
use Chill\PersonBundle\Entity\SocialWork\Result;
use Chill\PersonBundle\Entity\SocialWork\SocialAction;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ObjectRepository;

final class ResultRepository implements ObjectRepository
{
    private readonly EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Result::class);
    }

    public function countByGoal(Goal $goal): int
    {
        $qb = $this->buildQueryByGoal($goal);
        $qb->select('COUNT(r)');

        return $qb
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countBySocialActionWithDescendants(SocialAction $action): int
    {
        $qb = $this->buildQueryBySocialActionWithDescendants($action);
        $qb->select('COUNT(r)');

        return $qb
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function find($id): ?Result
    {
        return $this->repository->find($id);
    }

    /**
     * @return array<int, Result>
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return array<int, Result>
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @return array<Result>
     */
    public function findByGoal(Goal $goal, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        $qb = $this->buildQueryByGoal($goal);

        if (null !== $orderBy) {
            foreach ($orderBy as $sort => $order) {
                $qb->addOrderBy('r.'.$sort, $order);
            }
        }

        return $qb
            ->select('r')
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Result[]
     */
    public function findBySocialActionWithDescendants(SocialAction $action, array $orderBy = [], ?int $limit = null, ?int $offset = null): array
    {
        $qb = $this->buildQueryBySocialActionWithDescendants($action);
        $qb->select('r');

        foreach ($orderBy as $sort => $order) {
            $qb->addOrderBy('r.'.$sort, $order);
        }

        return $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }

    public function findOneBy(array $criteria, ?array $orderBy = null): ?Result
    {
        return $this->repository->findOneBy($criteria, $orderBy);
    }

    /**
     * @return class-string
     */
    public function getClassName(): string
    {
        return Result::class;
    }

    private function buildQueryByGoal(Goal $goal): QueryBuilder
    {
        $qb = $this->repository->createQueryBuilder('r');

        $qb->where(':goal MEMBER OF r.goals')
            ->setParameter('goal', $goal);

        return $qb;
    }

    private function buildQueryBySocialActionWithDescendants(SocialAction $action): QueryBuilder
    {
        $actions = $action->getDescendantsWithThis();

        $qb = $this->repository->createQueryBuilder('r');

        $orx = $qb->expr()->orX();
        $i = 0;

        foreach ($actions as $act) {
            $orx->add(":action_{$i} MEMBER OF r.socialActions");
            $qb->setParameter("action_{$i}", $act);
        }
        $qb->where($orx);

        return $qb;
    }
}
