<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository;

use Chill\MainBundle\Search\SearchApiQuery;
use Chill\PersonBundle\Entity\Person;

interface PersonACLAwareRepositoryInterface
{
    public function buildAuthorizedQuery(
        ?string $default = null,
        ?string $firstname = null,
        ?string $lastname = null,
        ?\DateTimeInterface $birthdate = null,
        ?\DateTimeInterface $birthdateBefore = null,
        ?\DateTimeInterface $birthdateAfter = null,
        ?string $gender = null,
        ?string $countryCode = null,
        ?string $phonenumber = null,
        ?string $city = null
    ): SearchApiQuery;

    public function countBySearchCriteria(
        ?string $default = null,
        ?string $firstname = null,
        ?string $lastname = null,
        ?\DateTimeInterface $birthdate = null,
        ?\DateTimeInterface $birthdateBefore = null,
        ?\DateTimeInterface $birthdateAfter = null,
        ?string $gender = null,
        ?string $countryCode = null,
        ?string $phonenumber = null,
        ?string $city = null
    );

    /**
     * @return array|Person[]
     */
    public function findBySearchCriteria(
        int $start,
        int $limit,
        bool $simplify = false,
        ?string $default = null,
        ?string $firstname = null,
        ?string $lastname = null,
        ?\DateTimeInterface $birthdate = null,
        ?\DateTimeInterface $birthdateBefore = null,
        ?\DateTimeInterface $birthdateAfter = null,
        ?string $gender = null,
        ?string $countryCode = null,
        ?string $phonenumber = null,
        ?string $city = null
    ): array;
}
