<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository\Household;

use Chill\PersonBundle\Entity\Household\PersonHouseholdAddress;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;

final class PersonHouseholdAddressRepository implements ObjectRepository
{
    private readonly EntityRepository $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->repository = $em->getRepository(PersonHouseholdAddress::class);
    }

    public function find($id, $lockMode = null, $lockVersion = null): ?PersonHouseholdAddress
    {
        return $this->repository->find($id, $lockMode, $lockVersion);
    }

    /**
     * @return PersonHouseholdAddress[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return PersonHouseholdAddress[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria, ?array $orderBy = null): ?PersonHouseholdAddress
    {
        return $this->repository->findOneBy($criteria, $orderBy);
    }

    public function getClassName()
    {
        return PersonHouseholdAddress::class;
    }
}
