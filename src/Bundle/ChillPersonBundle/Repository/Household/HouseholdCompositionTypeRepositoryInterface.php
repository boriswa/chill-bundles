<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository\Household;

use Chill\PersonBundle\Entity\Household\HouseholdCompositionType;
use Doctrine\Persistence\ObjectRepository;

interface HouseholdCompositionTypeRepositoryInterface extends ObjectRepository
{
    public function find($id): ?HouseholdCompositionType;

    /**
     * @return array|HouseholdCompositionType[]|object[]
     */
    public function findAll(): array;

    /**
     * @return array|HouseholdCompositionType[]
     */
    public function findAllActive(): array;

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return array|HouseholdCompositionType[]|object[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array;

    public function findOneBy(array $criteria): ?HouseholdCompositionType;

    public function getClassName(): string;
}
