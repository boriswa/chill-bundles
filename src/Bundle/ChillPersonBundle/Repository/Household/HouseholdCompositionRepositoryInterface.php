<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository\Household;

use Chill\PersonBundle\Entity\Household\Household;
use Chill\PersonBundle\Entity\Household\HouseholdComposition;
use Doctrine\Persistence\ObjectRepository;

interface HouseholdCompositionRepositoryInterface extends ObjectRepository
{
    public function countByHousehold(Household $household): int;

    public function find($id): ?HouseholdComposition;

    /**
     * @return array|HouseholdComposition[]
     */
    public function findAll(): array;

    /**
     * @param int $limit
     * @param int $offset
     *
     * @return array|object[]|HouseholdComposition[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array;

    /**
     * @return array|HouseholdComposition[]|object[]
     */
    public function findByHousehold(Household $household, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array;

    public function findOneBy(array $criteria): ?HouseholdComposition;

    public function getClassName(): string;
}
