<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository\Household;

use Chill\PersonBundle\Entity\Household\HouseholdCompositionType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

final class HouseholdCompositionTypeRepository implements HouseholdCompositionTypeRepositoryInterface
{
    private readonly EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(HouseholdCompositionType::class);
    }

    public function find($id): ?HouseholdCompositionType
    {
        return $this->repository->find($id);
    }

    /**
     * @return array|HouseholdCompositionType[]|object[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @return array|HouseholdCompositionType[]
     */
    public function findAllActive(): array
    {
        return $this->findBy(['active' => true]);
    }

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return array|HouseholdCompositionType[]|object[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): ?HouseholdCompositionType
    {
        return $this->repository->findOneBy($criteria);
    }

    public function getClassName(): string
    {
        return HouseholdCompositionType::class;
    }
}
