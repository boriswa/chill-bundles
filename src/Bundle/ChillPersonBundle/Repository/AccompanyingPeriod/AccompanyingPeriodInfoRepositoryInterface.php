<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository\AccompanyingPeriod;

use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodInfo;
use Doctrine\Persistence\ObjectRepository;

/**
 * @template-extends ObjectRepository<AccompanyingPeriodInfo>
 */
interface AccompanyingPeriodInfoRepositoryInterface extends ObjectRepository
{
    /**
     * Return a list of id for inactive accompanying periods.
     *
     * @param list<AccompanyingPeriod::STEP_*> $statuses
     *
     * @return list<int>
     */
    public function findAccompanyingPeriodIdInactiveAfter(\DateInterval $interval, array $statuses = []): array;

    /**
     * @param list<AccompanyingPeriod::STEP_*> $statuses
     *
     * @return list<int>
     */
    public function findAccompanyingPeriodIdActiveSince(\DateInterval $interval, array $statuses = []): array;
}
