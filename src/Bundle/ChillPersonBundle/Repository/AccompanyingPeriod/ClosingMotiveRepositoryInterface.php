<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository\AccompanyingPeriod;

use Doctrine\Persistence\ObjectRepository;

interface ClosingMotiveRepositoryInterface extends ObjectRepository
{
    public function getActiveClosingMotive(bool $onlyLeaf = true);
}
