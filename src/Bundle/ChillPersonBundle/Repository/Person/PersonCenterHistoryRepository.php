<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository\Person;

use Chill\PersonBundle\Entity\Person\PersonCenterHistory;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class PersonCenterHistoryRepository implements PersonCenterHistoryInterface
{
    private readonly EntityRepository $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->repository = $em->getRepository($this->getClassName());
    }

    public function find($id): ?PersonCenterHistory
    {
        return $this->repository->find($id);
    }

    /**
     * @return array|PersonCenterHistory[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @return array|PersonCenterHistory[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): ?PersonCenterHistory
    {
        return $this->repository->findOneBy($criteria);
    }

    public function getClassName(): string
    {
        return PersonCenterHistory::class;
    }
}
