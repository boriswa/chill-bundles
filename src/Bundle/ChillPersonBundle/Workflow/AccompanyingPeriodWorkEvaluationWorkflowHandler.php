<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Workflow;

use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\MainBundle\Workflow\EntityWorkflowHandlerInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluation;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocument;
use Chill\PersonBundle\Repository\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationRepository;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodWorkEvaluationVoter;
use Symfony\Contracts\Translation\TranslatorInterface;

class AccompanyingPeriodWorkEvaluationWorkflowHandler implements EntityWorkflowHandlerInterface
{
    public function __construct(private readonly AccompanyingPeriodWorkEvaluationRepository $repository, private readonly TranslatableStringHelperInterface $translatableStringHelper, private readonly TranslatorInterface $translator)
    {
    }

    public function getDeletionRoles(): array
    {
        return ['_'];
    }

    public function getEntityData(EntityWorkflow $entityWorkflow, array $options = []): array
    {
        $evaluation = $this->getRelatedEntity($entityWorkflow);

        return [
            'persons' => $evaluation->getAccompanyingPeriodWork()->getPersons(),
        ];
    }

    public function getEntityTitle(EntityWorkflow $entityWorkflow, array $options = []): string
    {
        $evaluation = $this->getRelatedEntity($entityWorkflow);

        return $this->translator->trans(
            'workflow.Evaluation (n°%eval%)',
            ['%eval%' => $entityWorkflow->getRelatedEntityId()]
        ).' - '.$this->translatableStringHelper->localize($evaluation->getEvaluation()->getTitle());
    }

    public function getRelatedEntity(EntityWorkflow $entityWorkflow): ?AccompanyingPeriodWorkEvaluation
    {
        return $this->repository->find($entityWorkflow->getRelatedEntityId());
    }

    /**
     * @param AccompanyingPeriodWorkEvaluation $object
     */
    public function getRelatedObjects(object $object): array
    {
        $relateds = [];
        $relateds[] = ['entityClass' => AccompanyingPeriodWorkEvaluation::class, 'entityId' => $object->getId()];

        foreach ($object->getDocuments() as $doc) {
            $relateds[] = ['entityClass' => AccompanyingPeriodWorkEvaluationDocument::class, 'entityId' => $doc->getId()];
        }

        return $relateds;
    }

    public function getRoleShow(EntityWorkflow $entityWorkflow): ?string
    {
        return AccompanyingPeriodWorkEvaluationVoter::SEE;
    }

    public function getSuggestedUsers(EntityWorkflow $entityWorkflow): array
    {
        $suggestedUsers = $entityWorkflow->getUsersInvolved();

        $referrer = $this->getRelatedEntity($entityWorkflow)
            ->getAccompanyingPeriodWork()
            ->getAccompanyingPeriod()
            ->getUser();

        $suggestedUsers[spl_object_hash($referrer)] = $referrer;

        return $suggestedUsers;
    }

    public function getTemplate(EntityWorkflow $entityWorkflow, array $options = []): string
    {
        return '@ChillPerson/Workflow/_evaluation.html.twig';
    }

    public function getTemplateData(EntityWorkflow $entityWorkflow, array $options = []): array
    {
        return [
            'entity_workflow' => $entityWorkflow,
            'evaluation' => $this->getRelatedEntity($entityWorkflow),
        ];
    }

    public function isObjectSupported(object $object): bool
    {
        return $object instanceof AccompanyingPeriodWorkEvaluation;
    }

    public function supports(EntityWorkflow $entityWorkflow, array $options = []): bool
    {
        return AccompanyingPeriodWorkEvaluation::class === $entityWorkflow->getRelatedEntityClass();
    }

    public function supportsFreeze(EntityWorkflow $entityWorkflow, array $options = []): bool
    {
        return false;
    }
}
