<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Widget\PersonListWidget;

use Chill\MainBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Interface to implement on classes called in configuration for
 * PersonListWidget (`person_list`), under the key `filtering_class` :.
 *
 * ```
 * widgets:
 *      homepage:
 *          person_list:
 *              # where \FQDN\To\Class implements PersonFiltering
 *              class_filtering: \FQDN\To\Class
 * ```
 */
interface PersonFilteringInterface
{
    /**
     * Return an array of persons id to show.
     *
     * Those ids are inserted into the query like this (where ids is the array
     * returned by this class) :
     *
     * ```
     * SELECT p FROM ChillPersonBundle:Persons p
     * WHERE p.id IN (:ids)
     * AND
     * -- security/authorization statement: restraint person to authorized centers
     * p.center in :authorized_centers
     * ```
     *
     * Example of use : filtering based on custom field data :
     * ```
     *
     * class HomepagePersonFiltering implements PersonFilteringInterface {
     *
     * public function getPersonIds(EntityManager $em, User $user)
     * {
     * $rsmBuilder = new ResultSetMappingBuilder($em);
     * $rsmBuilder->addScalarResult('id', 'id', Types::BIGINT);
     *
     * $personTable = $em->getClassMetadata('ChillPersonBundle:Person')
     * ->getTableName();
     * $personIdColumn = $em->getClassMetadata('ChillPersonBundle:Person')
     * ->getColumnName('id');
     * $personCfDataColumn = $em->getClassMetadata('ChillPersonBundle:Person')
     * ->getColumnName('cfData');
     *
     * return $em->createNativeQuery(sprintf("SELECT %s FROM %s WHERE "
     * . "jsonb_exists(%s, 'school-2fb5440e-192c-11e6-b2fd-74d02b0c9b55')",
     * $personIdColumn, $personTable, $personCfDataColumn), $rsmBuilder)
     * ->getScalarResult();
     * }
     * }
     * ```
     *
     * @return int[] an array of persons id to show
     */
    public function getPersonIds(EntityManagerInterface $em, UserInterface $user);
}
