<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Widget;

use Chill\MainBundle\DependencyInjection\Widget\Factory\AbstractWidgetFactory;
use Symfony\Component\Config\Definition\Builder\NodeBuilder;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * add configuration for the person_list widget.
 */
class PersonListWidgetFactory extends AbstractWidgetFactory
{
    public function configureOptions($place, NodeBuilder $node)
    {
        $node->booleanNode('only_active')
            ->defaultTrue()
            ->end();
        $node->integerNode('number_of_items')
            ->defaultValue(50)
            ->end();
        $node->scalarNode('filtering_class')
            ->defaultNull()
            ->end();
        $node->arrayNode('custom_fields')
            ->prototype('scalar')->end()
            ->info('Add some custom field to the view. Add the slug of some custom field'
                        .' if you want to override the view and show their value in the list')
            ->example(['custom-field-slug-1', 'custom-field-slug-2'])
            ->requiresAtLeastOneElement()
            ->end();
    }

    public function getAllowedPlaces()
    {
        return ['homepage'];
    }

    public function getServiceId(ContainerBuilder $containerBuilder, $place, $order, array $config)
    {
        return 'chill_person.widget.person_list';
    }

    public function getWidgetAlias()
    {
        return 'person_list';
    }
}
