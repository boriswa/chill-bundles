<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\DataFixtures\Helper;

use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;

trait RandomPersonHelperTrait
{
    private ?int $nbOfPersons = null;

    protected function getRandomPerson(EntityManagerInterface $em): Person
    {
        $qb = $em->createQueryBuilder();
        $qb
            ->from(Person::class, 'p');

        if (null === $this->nbOfPersons) {
            $this->nbOfPersons = $qb
                ->select('COUNT(p)')
                ->getQuery()
                ->getSingleScalarResult();
        }

        return $qb
            ->select('p')
            ->setMaxResults(1)
            ->setFirstResult(\random_int(0, $this->nbOfPersons))
            ->getQuery()
            ->getSingleResult();
    }
}
