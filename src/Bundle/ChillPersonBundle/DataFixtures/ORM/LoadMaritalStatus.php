<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\DataFixtures\ORM;

use Chill\PersonBundle\Entity\MaritalStatus;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Load marital status into database.
 */
class LoadMaritalStatus extends AbstractFixture implements OrderedFixtureInterface
{
    private array $maritalStatuses = [
        ['id' => 'single', 'name' => ['en' => 'single', 'fr' => 'célibataire']],
        ['id' => 'married', 'name' => ['en' => 'married', 'fr' => 'marié(e)']],
        ['id' => 'widow', 'name' => ['en' => 'widow', 'fr' => 'veuf – veuve ']],
        ['id' => 'separat', 'name' => ['en' => 'separated', 'fr' => 'séparé(e)']],
        ['id' => 'divorce', 'name' => ['en' => 'divorced', 'fr' => 'divorcé(e)']],
        ['id' => 'legalco', 'name' => ['en' => 'legal cohabitant', 'fr' => 'cohabitant(e) légal(e)']],
        ['id' => 'unknown', 'name' => ['en' => 'unknown', 'fr' => 'indéterminé']],
    ];

    public function getOrder()
    {
        return 9999;
    }

    public function load(ObjectManager $manager)
    {
        echo "loading maritalStatuses... \n";

        foreach ($this->maritalStatuses as $ms) {
            echo $ms['name']['en'].' ';
            $new_ms = new MaritalStatus();
            $new_ms->setId($ms['id']);
            $new_ms->setName($ms['name']);
            $this->addReference('ms_'.$ms['id'], $new_ms);
            $manager->persist($new_ms);
        }

        $manager->flush();
    }
}
