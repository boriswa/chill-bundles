<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Service\EntityInfo\AccompanyingPeriodInfoQueryPart;

use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluation;
use Chill\PersonBundle\Service\EntityInfo\AccompanyingPeriodInfoUnionQueryPartInterface;

class AccompanyingPeriodWorkEvaluationDocumentUpdateQueryPartForAccompanyingPeriodInfo implements AccompanyingPeriodInfoUnionQueryPartInterface
{
    public function getAccompanyingPeriodIdColumn(): string
    {
        return 'cpapw.accompanyingperiod_id';
    }

    public function getRelatedEntityColumn(): string
    {
        return AccompanyingPeriodWorkEvaluation::class;
    }

    public function getRelatedEntityIdColumn(): string
    {
        return 'e.id';
    }

    public function getUserIdColumn(): string
    {
        return 'e.updatedby_id';
    }

    public function getDateTimeColumn(): string
    {
        return 'e.updatedAt';
    }

    public function getMetadataColumn(): string
    {
        return "'{}'::jsonb";
    }

    public function getDiscriminator(): string
    {
        return 'accompanying_period_work_evaluation_updated_at';
    }

    public function getFromStatement(): string
    {
        return 'chill_person_accompanying_period_work_evaluation e
            JOIN chill_person_accompanying_period_work cpapw ON cpapw.id = e.accompanyingperiodwork_id';
    }

    public function getWhereClause(): string
    {
        return 'e.updatedAt IS NOT NULL';
    }
}
