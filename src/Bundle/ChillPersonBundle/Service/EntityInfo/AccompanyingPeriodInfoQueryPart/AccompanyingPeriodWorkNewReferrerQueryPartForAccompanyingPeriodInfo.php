<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Service\EntityInfo\AccompanyingPeriodInfoQueryPart;

use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWork;
use Chill\PersonBundle\Service\EntityInfo\AccompanyingPeriodInfoUnionQueryPartInterface;

class AccompanyingPeriodWorkNewReferrerQueryPartForAccompanyingPeriodInfo implements AccompanyingPeriodInfoUnionQueryPartInterface
{
    public function getAccompanyingPeriodIdColumn(): string
    {
        return 'w.accompanyingperiod_id';
    }

    public function getRelatedEntityColumn(): string
    {
        return AccompanyingPeriodWork::class;
    }

    public function getRelatedEntityIdColumn(): string
    {
        return 'w.id';
    }

    public function getUserIdColumn(): string
    {
        return 'cpapwr.user_id';
    }

    public function getDateTimeColumn(): string
    {
        return 'cpapwr.startDate';
    }

    public function getMetadataColumn(): string
    {
        return "'{}'::jsonb";
    }

    public function getDiscriminator(): string
    {
        return 'accompanying_period_work_referrer_new';
    }

    public function getFromStatement(): string
    {
        return 'chill_person_accompanying_period_work w
            JOIN chill_person_accompanying_period_work_referrer cpapwr on w.id = cpapwr.accompanyingperiodwork_id';
    }

    public function getWhereClause(): string
    {
        return '';
    }
}
