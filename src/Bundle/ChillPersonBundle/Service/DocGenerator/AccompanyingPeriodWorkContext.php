<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Service\DocGenerator;

use Chill\DocGeneratorBundle\Context\DocGeneratorContextWithPublicFormInterface;
use Chill\DocGeneratorBundle\Entity\DocGeneratorTemplate;
use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWork;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Generate a context for an @see{AccompanyingPeriodWork}.
 *
 * Although there isn't any document associated to AccompanyingPeriodWork, this context
 * is use by @see{AccompanyingPeriodWorkEvaluationContext}.
 *
 * @implements DocGeneratorContextWithPublicFormInterface<AccompanyingPeriodWork>
 */
class AccompanyingPeriodWorkContext implements DocGeneratorContextWithPublicFormInterface
{
    public function __construct(private readonly AccompanyingPeriodContext $periodContext, private readonly NormalizerInterface $normalizer)
    {
    }

    public function adminFormReverseTransform(array $data): array
    {
        return $this->periodContext->adminFormReverseTransform($data);
    }

    public function adminFormTransform(array $data): array
    {
        return $this->periodContext->adminFormTransform($data);
    }

    public function buildAdminForm(FormBuilderInterface $builder): void
    {
        $this->periodContext->buildAdminForm($builder);

        $builder->remove('category');
    }

    public function buildPublicForm(FormBuilderInterface $builder, DocGeneratorTemplate $template, $entity): void
    {
        $this->periodContext->buildPublicForm($builder, $template, $entity->getAccompanyingPeriod());
    }

    public function getData(DocGeneratorTemplate $template, $entity, array $contextGenerationData = []): array
    {
        $data = $this->periodContext->getData($template, $entity->getAccompanyingPeriod(), $contextGenerationData);
        $data['work'] = $this->normalizer->normalize($entity, 'docgen', [
            AbstractNormalizer::GROUPS => ['docgen:read'],
            'docgen:expects' => AccompanyingPeriodWork::class,
        ]);

        return $data;
    }

    public function getDescription(): string
    {
        return 'docgen.A context for accompanying period work';
    }

    public function getEntityClass(): string
    {
        return AccompanyingPeriodWork::class;
    }

    public function getFormData(DocGeneratorTemplate $template, $entity): array
    {
        return $this->periodContext->getFormData($template, $entity->getAccompanyingPeriod());
    }

    public static function getKey(): string
    {
        return 'accompanying_period_work_regular';
    }

    public function getName(): string
    {
        return 'docgen.Accompanying period work';
    }

    public function hasAdminForm(): bool
    {
        return $this->periodContext->hasAdminForm();
    }

    public function hasPublicForm(DocGeneratorTemplate $template, $entity): bool
    {
        return $this->periodContext->hasPublicForm($template, $entity->getAccompanyingPeriod());
    }

    public function contextGenerationDataNormalize(DocGeneratorTemplate $template, $entity, array $data): array
    {
        return $this->periodContext->contextGenerationDataNormalize($template, $entity->getAccompanyingPeriod(), $data);
    }

    public function contextGenerationDataDenormalize(DocGeneratorTemplate $template, $entity, array $data): array
    {
        return $this->periodContext->contextGenerationDataDenormalize($template, $entity->getAccompanyingPeriod(), $data);
    }

    public function storeGenerated(DocGeneratorTemplate $template, StoredObject $storedObject, object $entity, array $contextGenerationData): void
    {
        // currently, no document associated with a AccompanyingPeriodWork
    }
}
