<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AdminPersonMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            return;
        }

        $menu->addChild('Person', [
            'route' => 'chill_person_admin_index',
        ])
            ->setAttribute('class', 'list-group-item-header')
            ->setExtras([
                'order' => 2000,
            ]);

        $menu->addChild('Civility', [
            'route' => 'chill_crud_main_civility_index',
        ])->setExtras(['order' => 2010]);

        $menu->addChild('Marital status', [
            'route' => 'chill_crud_person_marital-status_index',
        ])->setExtras(['order' => 2020]);

        $menu->addChild('person_admin.person_resource_kind', [
            'route' => 'chill_crud_person_resource-kind_index',
        ])->setExtras(['order' => 2030]);
    }

    public static function getMenuIds(): array
    {
        return ['admin_section', 'admin_person'];
    }
}
