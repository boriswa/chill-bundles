<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AdminSocialWorkMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            return;
        }

        $menu->addChild('person_admin.social_work', [
            'route' => 'chill_social-work_admin_index',
        ])
            ->setAttribute('class', 'list-group-item-header')
            ->setExtras([
                'order' => 2300,
            ]);

        $menu->addChild('person_admin.social_action', [
            'route' => 'chill_crud_social_action_index',
        ])->setExtras(['order' => 2301]);

        $menu->addChild('person_admin.social_issue', [
            'route' => 'chill_crud_social_issue_index',
        ])->setExtras(['order' => 2302]);

        $menu->addChild('person_admin.social_goal', [
            'route' => 'chill_crud_social_goal_index',
        ])->setExtras(['order' => 2310]);

        $menu->addChild('person_admin.social_evaluation', [
            'route' => 'chill_crud_social_evaluation_index',
        ])->setExtras(['order' => 2320]);

        $menu->addChild('person_admin.social_result', [
            'route' => 'chill_crud_social_result_index',
        ])->setExtras(['order' => 2330]);
    }

    public static function getMenuIds(): array
    {
        return ['admin_section', 'admin_social_work'];
    }
}
