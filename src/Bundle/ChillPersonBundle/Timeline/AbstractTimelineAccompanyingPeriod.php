<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Timeline;

use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\MainBundle\Timeline\TimelineProviderInterface;
use Chill\MainBundle\Timeline\TimelineSingleQuery;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\AccompanyingPeriodParticipation;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Security;

/**
 * Provide method to build timeline for accompanying periods.
 *
 * This class is resued by TimelineAccompanyingPeriodOpening (for opening)
 * and TimelineAccompanyingPeriodClosing (for closing)
 */
abstract class AbstractTimelineAccompanyingPeriod implements TimelineProviderInterface
{
    private const SUPPORTED_CONTEXTS = ['person', 'center'];

    public function __construct(protected EntityManager $em, private readonly Security $security, private readonly AuthorizationHelper $authorizationHelper)
    {
    }

    public function getEntities(array $ids)
    {
        $periods = $this->em
            ->getRepository('ChillPersonBundle:AccompanyingPeriod')
            ->findBy(['id' => $ids]);

        // set results in an associative array with id as indexes
        $results = [];

        foreach ($periods as $period) {
            $results[$period->getId()] = $period;
        }

        return $results;
    }

    /**
     * prepare fetchQuery without `WHERE` and `TYPE` clause.
     *
     * @param string $context
     *
     * @return array
     *
     * @throws \LogicException
     */
    protected function basicFetchQuery($context, array $args)
    {
        if (false === \in_array($context, self::SUPPORTED_CONTEXTS, true)) {
            throw new \LogicException('TimelineAccompanyingPeriod is not able to render context '.$context);
        }

        $metadata = $this->em
            ->getClassMetadata(AccompanyingPeriod::class);
        [$where, $parameters] = $this->buildWhereClause($context, $args);

        return TimelineSingleQuery::fromArray([
            'id' => "{$metadata->getTableName()}.{$metadata->getColumnName('id')}",
            'FROM' => $this->buildFromClause($context),
            'WHERE' => $where,
            'parameters' => $parameters,
        ]);
    }

    protected function buildWhereClause($context, array $args): array
    {
        $participation = $this->em->getClassMetadata(AccompanyingPeriodParticipation::class);
        $join = $participation->getAssociationMapping('person')['joinColumns'][0];
        $person = $this->em->getClassMetadata(Person::class);
        $joinCenter = $person->getAssociationMapping('center')['joinColumns'][0];

        if ('center' === $context) {
            $allowedCenters = $this->authorizationHelper->filterReachableCenters($this->security->getUser(), $args['centers'], PersonVoter::SEE);
            $params = [];
            $questionMarks = [];
            $query = "{$person->getTableName()}.{$joinCenter['name']} IN (";

            foreach ($allowedCenters as $c) {
                $questionMarks[] = '?';
                $params[] = $c->getId();
            }
            $query .= \implode(', ', $questionMarks).')';

            return [$query, $params];
        }

        if ('person' === $context) {
            return ["{$participation->getTableName()}.{$join['name']} = ?", [$args['person']->getId()]];
        }

        throw new \LogicException("this context is not supported: {$context}");
    }

    /**
     * return the expected response for TimelineProviderInterface::getEntityTemplate.
     *
     * @param string $template the template for rendering
     * @param string $context
     *
     * @return array
     */
    protected function getBasicEntityTemplate($template, mixed $entity, $context, array $args)
    {
        return [
            'template' => $template,
            'template_data' => ['period' => $entity, 'context' => $context],
        ];
    }

    private function buildFromClause($context)
    {
        $period = $this->em->getClassMetadata(AccompanyingPeriod::class);
        $participation = $this->em->getClassMetadata(AccompanyingPeriodParticipation::class);
        $person = $this->em->getClassMetadata(Person::class);
        $join = $participation->getAssociationMapping('accompanyingPeriod')['joinColumns'][0];
        $joinPerson = $participation->getAssociationMapping('person')['joinColumns'][0];

        if ('person' === $context) {
            return "{$period->getTableName()} ".
                "JOIN {$participation->getTableName()} ".
                "ON {$participation->getTableName()}.{$join['name']} = ".
                "{$period->getTableName()}.{$join['referencedColumnName']}";
        }

        return "{$period->getTableName()} ".
                "JOIN {$participation->getTableName()} ".
                "ON {$participation->getTableName()}.{$join['name']} = ".
                "{$period->getTableName()}.{$join['referencedColumnName']} ".
                "JOIN {$person->getTableName()} ".
                "ON {$participation->getTableName()}.{$joinPerson['name']} = ".
                "{$person->getTableName()}.{$joinPerson['referencedColumnName']}";
    }
}
