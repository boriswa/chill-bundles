<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Helper;

use Doctrine\ORM\QueryBuilder;

/**
 * Filter accompanying period list and related, removing confidential ones
 * based on ACL rules.
 */
interface FilterListAccompanyingPeriodHelperInterface
{
    public function addFilterAccompanyingPeriods(QueryBuilder &$qb, array $requiredModifiers, array $acl, array $data = []): void;
}
