<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Helper;

use Doctrine\ORM\QueryBuilder;

/**
 * This interface provides methods for customizing the list of person.
 */
interface CustomizeListPersonHelperInterface
{
    /**
     * Alter the keys possibles (column header) for the list.
     */
    public function alterKeys(array $existing): array;

    public function alterSelect(QueryBuilder $qb, \DateTimeImmutable $computedDate): void;

    /**
     * Return the callable which transform the doctrine result representation.
     *
     * If the key is not managed by the current implementation, this method must return null.
     *
     * @return callable|null return null if the key is not managed by the current implementation
     */
    public function getLabels(string $key, array $values, array $data): ?callable;
}
