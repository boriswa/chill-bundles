<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Export\FilterInterface;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\Query\Expr\Andx;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class IntensityFilter implements FilterInterface
{
    private const CHOICES = [
        'is occasional' => 'occasional',
        'is regular' => 'regular',
    ];

    private const DEFAULT_CHOICE = 'occasional';

    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $where = $qb->getDQLPart('where');
        $clause = $qb->expr()->eq('acp.intensity', ':intensity');

        if ($where instanceof Andx) {
            $where->add($clause);
        } else {
            $where = $qb->expr()->andX($clause);
        }

        $qb->add('where', $where);
        $qb->setParameter('intensity', $data['accepted_intensities']);
    }

    public function applyOn(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('accepted_intensities', ChoiceType::class, [
            'choices' => self::CHOICES,
            'multiple' => false,
            'expanded' => true,
            'empty_data' => self::DEFAULT_CHOICE,
        ]);
    }

    public function getFormDefaultData(): array
    {
        return ['accepted_intensities' => self::DEFAULT_CHOICE];
    }

    public function describeAction($data, $format = 'string'): array
    {
        return [
            'Filtered by intensity: only %intensity%', [
                '%intensity%' => $this->translator->trans($data['accepted_intensities']),
            ],
        ];
    }

    public function getTitle(): string
    {
        return 'Filter by intensity';
    }
}
