<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Entity\User\UserJobHistory;
use Chill\MainBundle\Entity\UserJob;
use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Repository\UserJobRepositoryInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

class UserJobFilter implements FilterInterface
{
    private const PREFIX = 'acp_filter_user_job';

    public function __construct(
        private readonly TranslatableStringHelper $translatableStringHelper,
        private readonly UserJobRepositoryInterface $userJobRepository,
    ) {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $p = self::PREFIX;

        $qb
            ->leftJoin(
                'acp.userHistories',
                "{$p}_userHistory",
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq("{$p}_userHistory.accompanyingPeriod", 'acp.id'),
                    $qb->expr()->andX(
                        $qb->expr()->gte('COALESCE(acp.closingDate, CURRENT_TIMESTAMP())', "{$p}_userHistory.startDate"),
                        $qb->expr()->orX(
                            $qb->expr()->isNull("{$p}_userHistory.endDate"),
                            $qb->expr()->lt('COALESCE(acp.closingDate, CURRENT_TIMESTAMP())', "{$p}_userHistory.endDate")
                        )
                    )
                )
            )
            ->leftJoin(
                UserJobHistory::class,
                "{$p}_jobHistory",
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq("{$p}_jobHistory.user", "{$p}_userHistory.user"),
                    $qb->expr()->andX(
                        $qb->expr()->lte("{$p}_jobHistory.startDate", "{$p}_userHistory.startDate"),
                        $qb->expr()->orX(
                            $qb->expr()->isNull("{$p}_jobHistory".'.endDate'),
                            $qb->expr()->gt("{$p}_jobHistory.endDate", "{$p}_userHistory.startDate")
                        )
                    )
                )
            )
            ->andWhere($qb->expr()->in("{$p}_jobHistory.job", ":{$p}_job"))
            ->setParameter(
                "{$p}_job",
                $data['jobs'],
            )
        ;
    }

    public function applyOn()
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder
            ->add('jobs', EntityType::class, [
                'class' => UserJob::class,
                'choices' => $this->userJobRepository->findAllActive(),
                'multiple' => true,
                'expanded' => true,
                'choice_label' => fn (UserJob $job) => $this->translatableStringHelper->localize($job->getLabel()),
                'label' => 'Job',
            ]);
    }

    public function describeAction($data, $format = 'string')
    {
        return [
            'export.filter.course.by_user_job.Filtered by user job: only %job%', [
                '%job%' => implode(
                    ', ',
                    array_map(
                        fn (UserJob $job) => $this->translatableStringHelper->localize($job->getLabel()),
                        $data['jobs'] instanceof Collection ? $data['jobs']->toArray() : $data['jobs']
                    )
                ),
            ],
        ];
    }

    public function getFormDefaultData(): array
    {
        return [
            'jobs' => [],
        ];
    }

    public function getTitle(): string
    {
        return 'export.filter.course.by_user_job.Filter by user job';
    }
}
