<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Form\Type\PickRollingDateType;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class ActiveOneDayBetweenDatesFilter implements FilterInterface
{
    public function __construct(private readonly RollingDateConverterInterface $rollingDateConverter)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $clause = "OVERLAPSI (acp.openingDate, acp.closingDate), (:datefrom, :dateto) = 'TRUE'";

        $qb->andWhere($clause);
        $qb->setParameter(
            'datefrom',
            $this->rollingDateConverter->convert($data['date_from'])
        );
        $qb->setParameter(
            'dateto',
            $this->rollingDateConverter->convert($data['date_to'])
        );
    }

    public function applyOn(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder
            ->add('date_from', PickRollingDateType::class, [])
            ->add('date_to', PickRollingDateType::class, []);
    }

    public function getFormDefaultData(): array
    {
        return ['date_from' => new RollingDate(RollingDate::T_YEAR_PREVIOUS_START), 'date_to' => new RollingDate(RollingDate::T_TODAY)];
    }

    public function describeAction($data, $format = 'string'): array
    {
        return ['Filtered by actives courses: at least one day between %datefrom% and %dateto%', [
            '%datefrom%' => $this->rollingDateConverter->convert($data['date_from'])->format('d-m-Y'),
            '%dateto%' => $this->rollingDateConverter->convert($data['date_to'])->format('d-m-Y'),
        ]];
    }

    public function getTitle(): string
    {
        return 'Filter by active at least one day between dates';
    }
}
