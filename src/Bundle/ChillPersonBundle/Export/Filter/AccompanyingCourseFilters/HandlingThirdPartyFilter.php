<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Export\FilterInterface;
use Chill\PersonBundle\Export\Declarations;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Chill\ThirdPartyBundle\Form\Type\PickThirdpartyDynamicType;
use Chill\ThirdPartyBundle\Templating\Entity\ThirdPartyRender;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class HandlingThirdPartyFilter implements FilterInterface
{
    private const PREFIX = 'acpw_handling_3party_filter';

    public function __construct(
        private ThirdPartyRender $thirdPartyRender,
    ) {
    }

    public function getTitle()
    {
        return 'export.filter.work.by_handling3party.title';
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('handling_3parties', PickThirdpartyDynamicType::class, [
            'label' => 'export.filter.work.by_handling3party.pick_3parties',
            'multiple' => true,
        ]);
    }

    public function getFormDefaultData(): array
    {
        return ['handling_3parties' => []];
    }

    public function describeAction($data, $format = 'string')
    {
        return [
            'export.filter.work.by_handling3party.Only 3 parties %3parties%',
            [
                '%3parties%' => implode(
                    ', ',
                    array_map(
                        fn (ThirdParty $thirdParty) => $this->thirdPartyRender->renderString($thirdParty, []),
                        $data['handling_3parties'] instanceof Collection ? $data['handling_3parties']->toArray() : $data['handling_3parties']
                    )
                ),
            ],
        ];
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $p = self::PREFIX;

        $qb->andWhere("acpw.handlingThierParty IN (:{$p}_3ps)");
        $qb->setParameter("{$p}_3ps", $data['handling_3parties']);
    }

    public function applyOn()
    {
        return Declarations::SOCIAL_WORK_ACTION_TYPE;
    }
}
