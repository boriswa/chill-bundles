<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Filter\PersonFilters;

use Chill\MainBundle\Export\ExportElementValidatedInterface;
use Chill\MainBundle\Export\FilterInterface;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class GenderFilter implements
    ExportElementValidatedInterface,
    FilterInterface
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $where = $qb->getDQLPart('where');
        $isIn = $qb->expr()->in('person.gender', ':person_gender');

        if (!\in_array('null', $data['accepted_genders'], true)) {
            $clause = $isIn;
        } else {
            $clause = $qb->expr()->orX($isIn, $qb->expr()->isNull('person.gender'));
        }

        if ($where instanceof Expr\Andx) {
            $where->add($clause);
        } else {
            $where = $qb->expr()->andX($clause);
        }

        $qb->add('where', $where);
        $qb->setParameter('person_gender', \array_filter(
            $data['accepted_genders'],
            static fn ($el) => 'null' !== $el
        ));
    }

    public function applyOn()
    {
        return Declarations::PERSON_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('accepted_genders', ChoiceType::class, [
            'choices' => [
                'Woman' => Person::FEMALE_GENDER,
                'Man' => Person::MALE_GENDER,
                'Both' => Person::BOTH_GENDER,
                'Unknown' => Person::NO_INFORMATION,
                'Not given' => 'null',
            ],
            'multiple' => true,
            'expanded' => true,
        ]);
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function describeAction($data, $format = 'string')
    {
        $genders = [];

        foreach ($data['accepted_genders'] as $g) {
            if ('null' === $g) {
                $genders[] = $this->translator->trans('Not given');
            } else {
                $genders[] = $this->translator->trans($g);
            }
        }

        return [
            'Filtering by genders: only %genders%',
            ['%genders%' => \implode(', ', $genders)],
        ];
    }

    /**
     * A title which will be used in the label for the form.
     *
     * @return string
     */
    public function getTitle()
    {
        return 'Filter by person gender';
    }

    public function validateForm($data, ExecutionContextInterface $context)
    {
        if (!\is_array($data['accepted_genders']) || 0 === \count($data['accepted_genders'])) {
            $context->buildViolation('You should select an option')
                ->addViolation();
        }
    }
}
