<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Filter\PersonFilters;

use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Entity\MaritalStatus;
use Chill\PersonBundle\Export\Declarations;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class MaritalStatusFilter implements FilterInterface
{
    public function __construct(private readonly TranslatableStringHelper $translatableStringHelper)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(\Doctrine\ORM\QueryBuilder $qb, $data)
    {
        $qb->andWhere(
            $qb->expr()->in('person.maritalStatus', ':maritalStatus')
        );
        $qb->setParameter('maritalStatus', $data['maritalStatus']);
    }

    public function applyOn()
    {
        return Declarations::PERSON_TYPE;
    }

    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder)
    {
        $builder->add('maritalStatus', EntityType::class, [
            'class' => MaritalStatus::class,
            'choice_label' => fn (MaritalStatus $ms) => $this->translatableStringHelper->localize(
                $ms->getName()
            ),
            'multiple' => true,
            'expanded' => true,
        ]);
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function describeAction($data, $format = 'string')
    {
        return ['Filtered by person\'s marital status'];
    }

    public function getTitle()
    {
        return 'Filter by person\'s marital status';
    }
}
