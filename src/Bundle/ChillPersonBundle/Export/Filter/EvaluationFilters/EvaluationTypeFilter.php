<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Filter\EvaluationFilters;

use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Entity\SocialWork\Evaluation;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Repository\SocialWork\EvaluationRepositoryInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class EvaluationTypeFilter implements FilterInterface
{
    public function __construct(private TranslatableStringHelper $translatableStringHelper, private EvaluationRepositoryInterface $evaluationRepository)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $qb->andWhere(
            $qb->expr()->in('workeval.evaluation', ':evaluationtype')
        );
        $qb->setParameter('evaluationtype', $data['accepted_evaluationtype']);
    }

    public function applyOn(): string
    {
        return Declarations::EVAL_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $evaluations = $this->evaluationRepository->findAllActive();

        usort($evaluations, fn (Evaluation $a, Evaluation $b) => $this->translatableStringHelper->localize($a->getTitle()) <=> $this->translatableStringHelper->localize($b->getTitle()));

        $builder->add('accepted_evaluationtype', EntityType::class, [
            'class' => Evaluation::class,
            'choices' => $evaluations,
            'choice_label' => fn (Evaluation $ev): string => $this->translatableStringHelper->localize($ev->getTitle()),
            'multiple' => true,
            'expanded' => false,
            'attr' => ['class' => 'select2'],
        ]);
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function describeAction($data, $format = 'string'): array
    {
        $evals = [];

        foreach ($data['accepted_evaluationtype'] as $ev) {
            $evals[] = $this->translatableStringHelper->localize($ev->getTitle());
        }

        return ['Filtered by evaluation type: only %evals%', [
            '%evals%' => implode(', ', $evals),
        ]];
    }

    public function getTitle(): string
    {
        return 'Filter by evaluation type';
    }
}
