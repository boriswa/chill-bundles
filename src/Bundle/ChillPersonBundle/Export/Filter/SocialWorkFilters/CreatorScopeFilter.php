<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Filter\SocialWorkFilters;

use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Entity\User\UserScopeHistory;
use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Repository\ScopeRepository;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

class CreatorScopeFilter implements FilterInterface
{
    private const PREFIX = 'acpw_filter_creator_scope';

    public function __construct(
        private readonly ScopeRepository $scopeRepository,
        private readonly TranslatableStringHelper $translatableStringHelper
    ) {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $p = self::PREFIX;

        $qb
            ->leftJoin(
                UserScopeHistory::class,
                "{$p}_history",
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq("{$p}_history.user", 'acpw.createdBy'),
                    $qb->expr()->andX(
                        $qb->expr()->lte("{$p}_history.startDate", 'acpw.createdAt'),
                        $qb->expr()->orX(
                            $qb->expr()->isNull("{$p}_history.endDate"),
                            $qb->expr()->gt("{$p}_history.endDate", 'acpw.createdAt')
                        )
                    )
                )
            )
            ->andWhere($qb->expr()->in("{$p}_history.scope", ":{$p}_scopes"))
            ->setParameter("{$p}_scopes", $data['scopes']);
    }

    public function applyOn(): string
    {
        return Declarations::SOCIAL_WORK_ACTION_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder
            ->add('scopes', EntityType::class, [
                'class' => Scope::class,
                'choices' => $this->scopeRepository->findAllActive(),
                'choice_label' => fn (Scope $s) => $this->translatableStringHelper->localize($s->getName()),
                'multiple' => true,
                'expanded' => true,
                'label' => 'Scope',
            ]);
    }

    public function describeAction($data, $format = 'string'): array
    {
        $creatorScopes = [];

        foreach ($data['scopes'] as $s) {
            $creatorScopes[] = $this->translatableStringHelper->localize(
                $s->getName()
            );
        }

        return ['export.filter.work.by_creator_scope.Filtered by creator scope: only %scopes%', [
            '%scopes%' => implode(', ', $creatorScopes),
        ]];
    }

    public function getFormDefaultData(): array
    {
        return [
            'scopes' => [],
        ];
    }

    public function getTitle(): string
    {
        return 'export.filter.work.by_creator_scope.title';
    }
}
