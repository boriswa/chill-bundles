<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Export;

use Chill\MainBundle\Export\ExportInterface;
use Chill\MainBundle\Export\FormatterInterface;
use Chill\MainBundle\Export\GroupedExportInterface;
use Chill\PersonBundle\Entity\Person\PersonCenterHistory;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Repository\PersonRepository;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormBuilderInterface;

class CountPerson implements ExportInterface, GroupedExportInterface
{
    private readonly bool $filterStatsByCenters;

    public function __construct(
        protected PersonRepository $personRepository,
        ParameterBagInterface $parameterBag,
    ) {
        $this->filterStatsByCenters = $parameterBag->get('chill_main')['acl']['filter_stats_by_center'];
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // No form necessary
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getAllowedFormattersTypes()
    {
        return [FormatterInterface::TYPE_TABULAR];
    }

    public function getDescription()
    {
        return 'Count people by various parameters.';
    }

    public function getGroup(): string
    {
        return 'Exports of persons';
    }

    public function getLabels($key, array $values, $data)
    {
        if ('export_result' !== $key) {
            throw new \LogicException("the key {$key} is not used by this export");
        }

        $labels = array_combine($values, $values);
        $labels['_header'] = $this->getTitle();

        return static fn ($value) => $labels[$value];
    }

    public function getQueryKeys($data)
    {
        return ['export_result'];
    }

    public function getResult($query, $data)
    {
        return $query->getQuery()->getResult(Query::HYDRATE_SCALAR);
    }

    public function getTitle()
    {
        return 'Count people';
    }

    public function getType()
    {
        return Declarations::PERSON_TYPE;
    }

    /**
     * Initiate the query.
     *
     * @return QueryBuilder
     */
    public function initiateQuery(array $requiredModifiers, array $acl, array $data = [])
    {
        $centers = array_map(static fn ($el) => $el['center'], $acl);

        $qb = $this->personRepository->createQueryBuilder('person');

        $qb->select('COUNT(DISTINCT person.id) AS export_result');

        if ($this->filterStatsByCenters) {
            $qb
                ->andWhere(
                    $qb->expr()->exists(
                        'SELECT 1 FROM '.PersonCenterHistory::class.' pch WHERE pch.person = person.id AND pch.center IN (:authorized_centers)'
                    )
                )
                ->setParameter('authorized_centers', $centers);
        }

        return $qb;
    }

    public function requiredRole(): string
    {
        return PersonVoter::STATS;
    }

    public function supportsModifiers()
    {
        return [
            Declarations::PERSON_TYPE,
        ];
    }
}
