<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Export;

use Chill\MainBundle\Export\AccompanyingCourseExportHelper;
use Chill\MainBundle\Export\ExportInterface;
use Chill\MainBundle\Export\FormatterInterface;
use Chill\MainBundle\Export\GroupedExportInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person\PersonCenterHistory;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodVoter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormBuilderInterface;

class CountPersonWithAccompanyingCourse implements ExportInterface, GroupedExportInterface
{
    private readonly EntityRepository $repository;
    private readonly bool $filterStatsByCenters;

    public function __construct(
        EntityManagerInterface $em,
        ParameterBagInterface $parameterBag,
    ) {
        $this->repository = $em->getRepository(AccompanyingPeriod::class);
        $this->filterStatsByCenters = $parameterBag->get('chill_main')['acl']['filter_stats_by_center'];
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // TODO: Implement buildForm() method.
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getAllowedFormattersTypes(): array
    {
        return [FormatterInterface::TYPE_TABULAR];
    }

    public function getDescription(): string
    {
        return 'Count people participating in an accompanying course by various parameters.';
    }

    public function getGroup(): string
    {
        return 'Exports of persons';
    }

    public function getLabels($key, array $values, $data)
    {
        if ('export_result' !== $key) {
            throw new \LogicException("the key {$key} is not used by this export");
        }

        $labels = array_combine($values, $values);
        $labels['_header'] = $this->getTitle();

        return static fn ($value) => $labels[$value];
    }

    public function getQueryKeys($data): array
    {
        return ['export_result'];
    }

    public function getResult($query, $data)
    {
        return $query->getQuery()->getResult(Query::HYDRATE_SCALAR);
    }

    public function getTitle(): string
    {
        return 'Count people participating in an accompanying course';
    }

    public function getType(): string
    {
        return Declarations::HOUSEHOLD_TYPE;
    }

    public function initiateQuery(array $requiredModifiers, array $acl, array $data = [])
    {
        $centers = array_map(static fn ($el) => $el['center'], $acl);

        $qb = $this->repository->createQueryBuilder('acp');

        if (!\in_array('acppart', $qb->getAllAliases(), true)) {
            $qb->join('acp.participations', 'acppart');
        }

        if (!\in_array('partperson', $qb->getAllAliases(), true)) {
            $qb->join('acppart.person', 'person');
        }

        if ($this->filterStatsByCenters) {
            $qb->andWhere(
                $qb->expr()->exists(
                    'SELECT 1 FROM '.PersonCenterHistory::class.' pch WHERE pch.person = person.id AND pch.center IN (:authorized_centers)'
                )
            )->setParameter('authorized_centers', $centers);
        }

        AccompanyingCourseExportHelper::addClosingMotiveExclusionClause($qb);

        $qb->select('COUNT(DISTINCT person.id) AS export_result');

        return $qb;
    }

    public function requiredRole(): string
    {
        return AccompanyingPeriodVoter::STATS;
    }

    public function supportsModifiers(): array
    {
        return [
            Declarations::PERSON_TYPE,
            Declarations::ACP_TYPE,
        ];
    }
}
