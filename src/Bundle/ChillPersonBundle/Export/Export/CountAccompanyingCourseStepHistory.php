<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Export;

use Chill\MainBundle\Export\AccompanyingCourseExportHelper;
use Chill\MainBundle\Export\ExportInterface;
use Chill\MainBundle\Export\FormatterInterface;
use Chill\MainBundle\Export\GroupedExportInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person\PersonCenterHistory;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodVoter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormBuilderInterface;

class CountAccompanyingCourseStepHistory implements ExportInterface, GroupedExportInterface
{
    protected EntityRepository $repository;
    private readonly bool $filterStatsByCenters;

    public function __construct(
        private readonly EntityManagerInterface $em,
        ParameterBagInterface $parameterBag,
    ) {
        $this->repository = $em->getRepository(AccompanyingPeriod::class);
        $this->filterStatsByCenters = $parameterBag->get('chill_main')['acl']['filter_stats_by_center'];
    }

    public function buildForm(FormBuilderInterface $builder): void
    {
        // Nothing to add here
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getAllowedFormattersTypes(): array
    {
        return [FormatterInterface::TYPE_TABULAR];
    }

    public function getDescription(): string
    {
        return 'export.export.acp_closing.description';
    }

    public function getGroup(): string
    {
        return 'Exports of accompanying courses';
    }

    public function getLabels($key, array $values, $data)
    {
        if ('export_result' !== $key) {
            throw new \LogicException("the key {$key} is not used by this export");
        }

        return fn ($value) => '_header' === $value ? $this->getTitle() : $value;
    }

    public function getQueryKeys($data): array
    {
        return ['export_result'];
    }

    public function getResult($query, $data)
    {
        return $query->getQuery()->getResult(Query::HYDRATE_SCALAR);
    }

    public function getTitle(): string
    {
        return 'export.export.acp_closing.title';
    }

    public function getType(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function initiateQuery(array $requiredModifiers, array $acl, array $data = []): QueryBuilder
    {
        $centers = array_map(static fn ($el) => $el['center'], $acl);

        $qb = $this->em->createQueryBuilder()
            ->select('COUNT(DISTINCT acpstephistory.id) As export_result')
            ->from(AccompanyingPeriod\AccompanyingPeriodStepHistory::class, 'acpstephistory')
            ->join('acpstephistory.period', 'acp');

        $qb
            ->leftJoin('acp.participations', 'acppart')
            ->leftJoin('acppart.person', 'person');

        if ($this->filterStatsByCenters) {
            $qb
                ->andWhere(
                    $qb->expr()->exists(
                        'SELECT 1 FROM '.PersonCenterHistory::class.' acl_count_person_history WHERE acl_count_person_history.person = person
                    AND acl_count_person_history.center IN (:authorized_centers)
                    '
                    )
                )
                ->setParameter('authorized_centers', $centers);
        }

        AccompanyingCourseExportHelper::addClosingMotiveExclusionClause($qb);

        return $qb;
    }

    public function requiredRole(): string
    {
        return AccompanyingPeriodVoter::STATS;
    }

    public function supportsModifiers(): array
    {
        return [
            Declarations::ACP_TYPE,
            Declarations::PERSON_TYPE,
            Declarations::ACP_STEP_HISTORY,
        ];
    }
}
