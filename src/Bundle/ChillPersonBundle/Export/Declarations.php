<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export;

/**
 * This class declare constants used for the export framework.
 */
abstract class Declarations
{
    final public const ACP_TYPE = 'accompanying_period';

    final public const ACP_STEP_HISTORY = 'accompanying_period_step_history';

    final public const EVAL_TYPE = 'evaluation';

    final public const HOUSEHOLD_TYPE = 'household';

    /**
     * @deprecated consider using the PERSON_TYPE instead
     */
    final public const PERSON_IMPLIED_IN = 'person_implied_in';

    final public const PERSON_TYPE = 'person';

    final public const SOCIAL_WORK_ACTION_TYPE = 'social_actions';
}
