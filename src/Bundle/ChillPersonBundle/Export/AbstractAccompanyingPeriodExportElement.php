<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export;

use Doctrine\ORM\QueryBuilder;

class AbstractAccompanyingPeriodExportElement
{
    /**
     * Add the accompanying period alias to the query.
     *
     * @throws \LogicException if the "person" alias is not present and attaching accompanying period is not possible
     */
    protected function addJoinAccompanyingPeriod(QueryBuilder $query): void
    {
        if (false === \in_array('person', $query->getAllAliases(), true)) {
            throw new \LogicException("the alias 'person' does not exists in ".'query builder');
        }

        if (!\in_array('acppart', $query->getAllAliases(), true)) {
            $query->join('person.accompanyingPeriodParticipations', 'acppart');
        }

        if (!\in_array('acp', $query->getAllAliases(), true)) {
            $query->join('acppart.accompanyingPeriod', 'acp');
        }
    }
}
