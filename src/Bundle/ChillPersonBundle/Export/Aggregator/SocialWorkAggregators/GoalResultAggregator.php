<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\SocialWorkAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Repository\SocialWork\GoalRepository;
use Chill\PersonBundle\Repository\SocialWork\ResultRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class GoalResultAggregator implements AggregatorInterface
{
    public function __construct(private readonly ResultRepository $resultRepository, private readonly GoalRepository $goalRepository, private readonly TranslatableStringHelper $translatableStringHelper)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('goal', $qb->getAllAliases(), true)) {
            $qb->leftJoin('acpw.goals', 'goal');
        }

        if (!\in_array('goalresult', $qb->getAllAliases(), true)) {
            $qb->leftJoin('goal.results', 'goalresult');
        }

        $qb->addSelect('IDENTITY(goal.goal) as goal_aggregator');
        $qb->addSelect('goalresult.id as result_aggregator');
        $qb->addGroupBy('goal_aggregator')->addGroupBy('result_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::SOCIAL_WORK_ACTION_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value) use ($key): string {
            if (null === $value || '' === $value) {
                return '';
            }

            switch ($key) {
                case 'goal_aggregator':
                    if ('_header' === $value) {
                        return 'Goal Type';
                    }

                    $g = $this->goalRepository->find($value);

                    return $this->translatableStringHelper->localize(
                        $g->getTitle()
                    );

                case 'result_aggregator':
                    if ('_header' === $value) {
                        return 'Result Type';
                    }

                    $r = $this->resultRepository->find($value);

                    return $this->translatableStringHelper->localize(
                        $r->getTitle()
                    );

                default:
                    throw new \LogicException();
            }
        };
    }

    public function getQueryKeys($data): array
    {
        return [
            'goal_aggregator',
            'result_aggregator',
        ];
    }

    public function getTitle(): string
    {
        return 'Group social work actions by goal and result';
    }
}
