<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\SocialWorkAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Repository\SocialWork\SocialActionRepository;
use Chill\PersonBundle\Repository\SocialWork\SocialIssueRepository;
use Chill\PersonBundle\Templating\Entity\SocialActionRender;
use Chill\PersonBundle\Templating\Entity\SocialIssueRender;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class ActionTypeAggregator implements AggregatorInterface
{
    public function __construct(private SocialActionRepository $socialActionRepository, private SocialActionRender $actionRender, private SocialIssueRender $socialIssueRender, private SocialIssueRepository $socialIssueRepository)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('acpwsocialaction', $qb->getAllAliases(), true)) {
            $qb->leftJoin('acpw.socialAction', 'acpwsocialaction');
        }

        if (!\in_array('acpwsocialissue', $qb->getAllAliases(), true)) {
            $qb->leftJoin('acpwsocialaction.issue', 'acpwsocialissue');
        }

        $qb
            ->addSelect('acpwsocialissue.id as social_action_type_aggregator')
            ->addSelect('acpwsocialaction.id as action_type_aggregator')
            ->addGroupBy('action_type_aggregator')
            ->addGroupBy('social_action_type_aggregator');
    }

    public function applyOn()
    {
        return Declarations::SOCIAL_WORK_ACTION_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return match ($key) {
            'action_type_aggregator' => function ($value): string {
                if ('_header' === $value) {
                    return 'Social Action Type';
                }

                if (null === $value || '' === $value || null === $sa = $this->socialActionRepository->find($value)) {
                    return '';
                }

                return $this->actionRender->renderString($sa, []);
            },
            'social_action_type_aggregator' => function ($value): string {
                if ('_header' === $value) {
                    return 'Social Issue';
                }

                if (null === $value || null === $si = $this->socialIssueRepository->find($value)) {
                    return '';
                }

                return $this->socialIssueRender->renderString($si, []);
            },
            default => throw new \LogicException('this key is not supported: '.$key),
        };
    }

    public function getQueryKeys($data)
    {
        return ['social_action_type_aggregator', 'action_type_aggregator'];
    }

    public function getTitle()
    {
        return 'Group social work actions by action type';
    }
}
