<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\PersonAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class GenderAggregator implements AggregatorInterface
{
    public function __construct(private TranslatorInterface $translator)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $qb->addSelect('person.gender as gender');

        $qb->addGroupBy('gender');
    }

    public function applyOn()
    {
        return Declarations::PERSON_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value) {
            switch ($value) {
                case Person::FEMALE_GENDER:
                    return $this->translator->trans('woman');

                case Person::MALE_GENDER:
                    return $this->translator->trans('man');

                case Person::BOTH_GENDER:
                    return $this->translator->trans('both');

                case Person::NO_INFORMATION:
                    return $this->translator->trans('unknown');

                case null:
                case '':
                    return $this->translator->trans('Not given');

                case '_header':
                    return $this->translator->trans('Gender');

                default:
                    throw new \LogicException(sprintf('The value %s is not valid', $value));
            }
        };
    }

    public function getQueryKeys($data)
    {
        return ['gender'];
    }

    public function getTitle()
    {
        return 'Group people by gender';
    }
}
