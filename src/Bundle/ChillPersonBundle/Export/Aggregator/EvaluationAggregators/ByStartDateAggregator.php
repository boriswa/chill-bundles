<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\EvaluationAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

final class ByStartDateAggregator implements AggregatorInterface
{
    private const CHOICES = [
        'by week' => 'week',
        'by month' => 'month',
        'by year' => 'year',
    ];

    private const DEFAULT_CHOICE = 'year';

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $fmt = match ($data['frequency']) {
            'week' => 'YYYY-IW',
            'month' => 'YYYY-MM',
            'year' => 'YYYY',
            default => throw new \LogicException(sprintf("The frequency data '%s' is invalid.", $data['frequency'])),
        };

        $qb->addSelect(sprintf("TO_CHAR(workeval.startDate, '%s') AS eval_by_start_date_aggregator", $fmt));
        $qb->addGroupBy(' eval_by_start_date_aggregator');
        $qb->addOrderBy(' eval_by_start_date_aggregator', 'ASC');
    }

    public function applyOn(): string
    {
        return Declarations::EVAL_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('frequency', ChoiceType::class, [
            'choices' => self::CHOICES,
            'multiple' => false,
            'expanded' => true,
            'empty_data' => self::DEFAULT_CHOICE,
        ]);
    }

    public function getFormDefaultData(): array
    {
        return ['frequency' => self::DEFAULT_CHOICE];
    }

    public function getLabels($key, array $values, $data)
    {
        return static function ($value): string {
            if ('_header' === $value) {
                return 'export.aggregator.eval.by_start_date_period.Start date period';
            }

            if (null === $value) {
                return '';
            }

            return $value;
        };
    }

    public function getQueryKeys($data): array
    {
        return ['eval_by_start_date_aggregator'];
    }

    public function getTitle(): string
    {
        return 'export.aggregator.eval.by_start_date_period.Group by start date evaluations';
    }
}
