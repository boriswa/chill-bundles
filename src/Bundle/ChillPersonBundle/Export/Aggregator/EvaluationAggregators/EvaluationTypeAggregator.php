<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\EvaluationAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Repository\SocialWork\EvaluationRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class EvaluationTypeAggregator implements AggregatorInterface
{
    public function __construct(private readonly EvaluationRepository $evaluationRepository, private readonly TranslatableStringHelper $translatableStringHelper)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $qb->addSelect('IDENTITY(workeval.evaluation) AS eval_evaluationtype_aggregator');
        $qb->addGroupBy('eval_evaluationtype_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::EVAL_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Evaluation type';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            $ev = $this->evaluationRepository->find($value);

            return $this->translatableStringHelper->localize($ev->getTitle());
        };
    }

    public function getQueryKeys($data): array
    {
        return ['eval_evaluationtype_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group by evaluation type';
    }
}
