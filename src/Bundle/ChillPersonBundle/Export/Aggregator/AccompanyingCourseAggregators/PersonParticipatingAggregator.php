<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Export\Helper\LabelPersonHelper;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class PersonParticipatingAggregator implements AggregatorInterface
{
    private const KEY = 'acp_person_part_agg';

    public function __construct(
        private LabelPersonHelper $labelPersonHelper,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // nothing to do here
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, mixed $data)
    {
        return match ($key) {
            self::KEY => $this->labelPersonHelper->getLabel($key, $values, 'export.aggregator.course.by-user.header'),
            default => throw new \UnexpectedValueException('key not supported: '.$key),
        };
    }

    public function getQueryKeys($data)
    {
        return [self::KEY];
    }

    public function getTitle()
    {
        return 'export.aggregator.course.by-user.title';
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $k = self::KEY;

        if (!in_array('acppart', $qb->getAllAliases(), true)) {
            $qb->join('acp.participations', 'acppart');
        }

        $qb->addSelect("IDENTITY(acppart.person) AS {$k}")
            ->addGroupBy($k);
    }

    public function applyOn()
    {
        return Declarations::ACP_TYPE;
    }
}
