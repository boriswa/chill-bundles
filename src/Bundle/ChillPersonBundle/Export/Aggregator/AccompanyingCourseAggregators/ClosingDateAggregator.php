<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Export\Enum\DateGroupingChoiceEnum;
use Chill\PersonBundle\Tests\Export\Aggregator\AccompanyingCourseAggregators\ClosingDateAggregatorTest;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * @see ClosingDateAggregatorTest
 */
final readonly class ClosingDateAggregator implements AggregatorInterface
{
    private const PREFIX = 'acp_closing_date_agg';

    public function buildForm(FormBuilderInterface $builder): void
    {
        $builder->add('frequency', ChoiceType::class, [
            'choices' => array_combine(
                array_map(fn (DateGroupingChoiceEnum $c) => 'export.enum.frequency.'.$c->value, DateGroupingChoiceEnum::cases()),
                array_map(fn (DateGroupingChoiceEnum $c) => $c->value, DateGroupingChoiceEnum::cases()),
            ),
            'label' => 'export.aggregator.course.by_closing_date.frequency',
            'multiple' => false,
            'expanded' => true,
        ]);
    }

    public function getFormDefaultData(): array
    {
        return [
            'frequency' => 'year',
        ];
    }

    public function getLabels($key, array $values, mixed $data)
    {
        return function (?string $value): string {
            if ('_header' === $value) {
                return 'export.aggregator.course.by_closing_date.header';
            }

            return (string) $value;
        };
    }

    public function getQueryKeys($data)
    {
        return [self::PREFIX.'_closing_date'];
    }

    public function getTitle()
    {
        return 'export.aggregator.course.by_closing_date.title';
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $p = self::PREFIX;

        $qb->addSelect(sprintf("TO_CHAR(acp.closingDate, '%s') AS {$p}_closing_date", $data['frequency']));
        $qb->addGroupBy("{$p}_closing_date");
        $qb->addOrderBy("{$p}_closing_date", 'DESC');
    }

    public function applyOn()
    {
        return Declarations::ACP_TYPE;
    }
}
