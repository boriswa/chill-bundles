<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Entity\User\UserScopeHistory;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Repository\ScopeRepositoryInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

readonly class ReferrerScopeAggregator implements AggregatorInterface
{
    private const PREFIX = 'acp_agg_referrer_scope';

    public function __construct(
        private ScopeRepositoryInterface $scopeRepository,
        private TranslatableStringHelperInterface $translatableStringHelper,
    ) {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $p = self::PREFIX;

        $qb
            ->leftJoin(
                'acp.userHistories',
                "{$p}_userHistory",
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq("{$p}_userHistory.accompanyingPeriod", 'acp.id'),
                    $qb->expr()->andX(
                        $qb->expr()->gte('COALESCE(acp.closingDate, CURRENT_TIMESTAMP())', "{$p}_userHistory.startDate"),
                        $qb->expr()->orX(
                            $qb->expr()->isNull("{$p}_userHistory.endDate"),
                            $qb->expr()->lt('COALESCE(acp.closingDate, CURRENT_TIMESTAMP())', "{$p}_userHistory.endDate")
                        )
                    )
                )
            )
            ->leftJoin(
                UserScopeHistory::class,
                "{$p}_scopeHistory",
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq("{$p}_scopeHistory.user", "{$p}_userHistory.user"),
                    $qb->expr()->andX(
                        $qb->expr()->lte("{$p}_scopeHistory.startDate", "{$p}_userHistory.startDate"),
                        $qb->expr()->orX(
                            $qb->expr()->isNull("{$p}_scopeHistory.endDate"),
                            $qb->expr()->gt("{$p}_scopeHistory.endDate", "{$p}_userHistory.startDate")
                        )
                    )
                )
            )
            ->addSelect("IDENTITY({$p}_scopeHistory.scope) AS {$p}_select")
            ->addGroupBy("{$p}_select");
    }

    public function applyOn(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value) {
            if ('_header' === $value) {
                return 'export.aggregator.course.by_user_scope.Referrer\'s scope';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            $scope = $this->scopeRepository->find($value);

            if (null === $scope) {
                throw new \LogicException('no scope found with this id: '.$value);
            }

            return $this->translatableStringHelper->localize($scope->getName());
        };
    }

    public function getQueryKeys($data): array
    {
        return [self::PREFIX.'_select'];
    }

    public function getTitle(): string
    {
        return 'export.aggregator.course.by_user_scope.Group course by referrer\'s scope';
    }
}
