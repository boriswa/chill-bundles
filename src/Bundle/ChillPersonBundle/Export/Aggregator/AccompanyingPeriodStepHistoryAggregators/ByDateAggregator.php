<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\AccompanyingPeriodStepHistoryAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Export\Enum\DateGroupingChoiceEnum;
use Chill\PersonBundle\Tests\Export\Aggregator\AccompanyingPeriodStepHistoryAggregators\ByDateAggregatorTest;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * @see ByDateAggregatorTest
 */
final readonly class ByDateAggregator implements AggregatorInterface
{
    private const KEY = 'acpstephistory_by_date_agg';

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('frequency', ChoiceType::class, [
            'choices' => array_combine(
                array_map(fn (DateGroupingChoiceEnum $c) => 'export.enum.frequency.'.$c->value, DateGroupingChoiceEnum::cases()),
                array_map(fn (DateGroupingChoiceEnum $c) => $c->value, DateGroupingChoiceEnum::cases()),
            ),
            'label' => 'export.aggregator.course.by_opening_date.frequency',
            'multiple' => false,
            'expanded' => true,
        ]);
    }

    public function getFormDefaultData(): array
    {
        return ['frequency' => DateGroupingChoiceEnum::YEAR->value];
    }

    public function getLabels($key, array $values, mixed $data)
    {
        return function (?string $value): string {
            if ('_header' === $value) {
                return 'export.aggregator.step_history.by_date.header';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            return $value;
        };
    }

    public function getQueryKeys($data)
    {
        return [self::KEY];
    }

    public function getTitle()
    {
        return 'export.aggregator.step_history.by_date.title';
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $p = self::KEY;

        $qb->addSelect(sprintf("TO_CHAR(acpstephistory.startDate, '%s') AS {$p}", $data['frequency']));
        $qb->addGroupBy($p);
        $qb->addOrderBy($p, 'DESC');
    }

    public function applyOn()
    {
        return Declarations::ACP_STEP_HISTORY;
    }
}
