<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class CalculatorCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $manager = $container->getDefinition(\Chill\BudgetBundle\Calculator\CalculatorManager::class);

        foreach ($container->findTaggedServiceIds('chill_budget.calculator') as $id => $tags) {
            foreach ($tags as $tag) {
                $reference = new Reference($id);

                $manager->addMethodCall('addCalculator', [$reference, $tag['default'] ?? false]);
            }
        }
    }
}
