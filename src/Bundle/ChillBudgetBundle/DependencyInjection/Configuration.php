<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('chill_budget');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()

                // ressources
            ->arrayNode('resources')->defaultValue([])
            ->setDeprecated('Chill', '2.0', 'Since the introduction of budget admin entities, config is no longer used')
            ->arrayPrototype()
            ->children()
            ->scalarNode('key')->isRequired()->cannotBeEmpty()
            ->info('the key stored in database')
            ->example('salary')
            ->end()
            ->booleanNode('active')->defaultTrue()->end()
            ->arrayNode('labels')->isRequired()->requiresAtLeastOneElement()
            ->arrayPrototype()
            ->children()
            ->scalarNode('lang')->isRequired()->cannotBeEmpty()
            ->example('fr')
            ->end()
            ->scalarNode('label')->isRequired()->cannotBeEmpty()
            ->example('Salaire')
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->arrayNode('charges')->defaultValue([])
            ->setDeprecated('Chill', '2.0', 'Since the introduction of budget admin entities, config is no longer used')
            ->arrayPrototype()
            ->children()
            ->scalarNode('key')->isRequired()->cannotBeEmpty()
            ->info('the key stored in database')
            ->example('salary')
            ->end()
            ->booleanNode('active')->defaultTrue()->end()
            ->arrayNode('labels')->isRequired()->requiresAtLeastOneElement()
            ->arrayPrototype()
            ->children()
            ->scalarNode('lang')->isRequired()->cannotBeEmpty()
            ->example('fr')
            ->end()
            ->scalarNode('label')->isRequired()->cannotBeEmpty()
            ->example('Salaire')
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()

            ->end();

        return $treeBuilder;
    }
}
