<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Budget;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221116163445 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE chill_budget.charge_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_budget.resource_type_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_budget.charge_type');
        $this->addSql('DROP TABLE chill_budget.resource_type');
    }

    public function getDescription(): string
    {
        return 'Create resource type and charge type';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_budget.charge_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_budget.resource_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_budget.charge_type (id INT NOT NULL, isActive BOOLEAN DEFAULT TRUE NOT NULL, name JSONB DEFAULT \'{}\'::jsonb NOT NULL, ordering DOUBLE PRECISION DEFAULT \'0\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE chill_budget.resource_type (id INT NOT NULL, isActive BOOLEAN DEFAULT TRUE NOT NULL, name JSONB DEFAULT \'{}\'::jsonb NOT NULL, ordering DOUBLE PRECISION DEFAULT \'0\' NOT NULL, PRIMARY KEY(id))');
    }
}
