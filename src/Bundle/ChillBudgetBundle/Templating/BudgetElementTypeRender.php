<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\Templating;

use Chill\BudgetBundle\Entity\ChargeKind;
use Chill\BudgetBundle\Entity\ResourceKind;
use Chill\MainBundle\Templating\Entity\ChillEntityRenderInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;

/**
 * @implements ChillEntityRenderInterface<ResourceKind|ChargeKind>
 */
final readonly class BudgetElementTypeRender implements ChillEntityRenderInterface
{
    public function __construct(private TranslatableStringHelperInterface $translatableStringHelper, private \Twig\Environment $engine)
    {
    }

    public function renderBox($entity, array $options): string
    {
        return $this->engine->render('@ChillBudget/Entity/budget_element_type.html.twig', [
            'entity' => $entity,
            'options' => $options,
        ]);
    }

    public function renderString($entity, array $options): string
    {
        $title = '';

        if (null !== $entity->getName()) {
            return $this->translatableStringHelper->localize($entity->getName());
        }

        return $title;
    }

    public function supports($entity, array $options): bool
    {
        return $entity instanceof ChargeKind || $entity instanceof ResourceKind;
    }
}
