<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\Calculator;

use Chill\BudgetBundle\Entity\Charge;
use Chill\BudgetBundle\Entity\Resource;

interface CalculatorInterface
{
    /**
     * @param array<Charge|resource> $elements
     */
    public function calculate(array $elements): ?CalculatorResult;

    public function getAlias();
}
