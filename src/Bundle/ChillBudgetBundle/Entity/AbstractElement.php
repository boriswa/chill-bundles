<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\Entity;

use Chill\PersonBundle\Entity\Household\Household;
use Chill\PersonBundle\Entity\Person;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Type\Decimal;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AbstractElement.
 *
 * @ORM\MappedSuperclass
 */
abstract class AbstractElement
{
    /**
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2)
     *
     * @Assert\GreaterThan(
     *     value=0
     * )
     *
     * @Assert\NotNull(
     *     message="The amount cannot be empty"
     * )
     */
    private string $amount;

    /**
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private ?string $comment = null;

    /**
     * @ORM\Column(name="endDate", type="datetime_immutable", nullable=true)
     *
     * @Assert\GreaterThan(
     *     propertyPath="startDate",
     *     message="The budget element's end date must be after the start date"
     * )
     */
    private ?\DateTimeImmutable $endDate = null;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="\Chill\PersonBundle\Entity\Household\Household"
     * )
     */
    private ?Household $household = null;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="\Chill\PersonBundle\Entity\Person"
     * )
     */
    private ?Person $person = null;

    /**
     * @ORM\Column(name="startDate", type="datetime_immutable")
     *
     * @Assert\Date
     */
    private \DateTimeImmutable $startDate;

    /**
     * @ORM\Column(name="type", type="string", length=255)
     */
    private string $type = '';

    /* Getters and Setters */

    public function getAmount(): float
    {
        return (float) $this->amount;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getHousehold(): ?Household
    {
        return $this->household;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function getStartDate(): \DateTimeImmutable
    {
        return $this->startDate;
    }

    public function getType(): string
    {
        return $this->type;
    }

    abstract public function isCharge(): bool;

    public function isEmpty()
    {
        return 0 === $this->amount;
    }

    abstract public function isResource(): bool;

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function setComment(?string $comment = null): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function setEndDate(?\DateTimeInterface $endDate = null): self
    {
        if ($endDate instanceof \DateTime) {
            $this->endDate = \DateTimeImmutable::createFromMutable($endDate);
        } elseif (null === $endDate) {
            $this->endDate = null;
        } else {
            $this->endDate = $endDate;
        }

        return $this;
    }

    public function setHousehold(?Household $household): self
    {
        $this->household = $household;

        return $this;
    }

    public function setPerson(Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        if ($startDate instanceof \DateTime) {
            $this->startDate = \DateTimeImmutable::createFromMutable($startDate);
        } elseif (null === $startDate) {
            $this->startDate = null;
        } else {
            $this->startDate = $startDate;
        }

        return $this;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }
}
