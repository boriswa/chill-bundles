<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ChillCustomFieldsExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../config'));
        $loader->load('services.yaml');
        $loader->load('services/fixtures.yaml');
        $loader->load('services/controller.yaml');
        $loader->load('services/command.yaml');
        $loader->load('services/menu.yaml');

        // add at least a blank array at 'customizable_entities' options
        // $customizable_entities = (isset($config['customizables_entities'])
        //        && $config['customizables_entities'] !== FALSE)
        //        ? $config['customizables_entities'] : array();

        $container->setParameter(
            'chill_custom_fields.customizables_entities',
            $config['customizables_entities']
        );
        $container->setParameter(
            'chill_custom_fields.show_empty_values',
            $config['show_empty_values_in_views']
        );
    }

    /** (non-PHPdoc).
     * @see \Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface::prepend()
     */
    public function prepend(ContainerBuilder $container)
    {
        // add form layout to twig resources
        $twigConfig = [
            'form_themes' => [
                '@ChillCustomFields/Form/fields.html.twig',
            ],
        ];
        $container->prependExtensionConfig('twig', $twigConfig);

        // add routes for custom bundle
        $container->prependExtensionConfig('chill_main', [
            'routing' => [
                'resources' => [
                    '@ChillCustomFieldsBundle/config/routes.yaml',
                ],
            ],
        ]);
    }
}
