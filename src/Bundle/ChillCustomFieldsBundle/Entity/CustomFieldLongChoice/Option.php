<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\Entity\CustomFieldLongChoice;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(
 * repositoryClass="Chill\CustomFieldsBundle\EntityRepository\CustomFieldLongChoice\OptionRepository")
 *
 * @ORM\Table(name="custom_field_long_choice_options")
 */
class Option
{
    /**
     * @ORM\Column(type="boolean")
     */
    private bool $active = true;

    /**
     * @var Collection<Option>
     *
     * @ORM\OneToMany(
     *     targetEntity="Chill\CustomFieldsBundle\Entity\CustomFieldLongChoice\Option",
     * mappedBy="parent")
     */
    private Collection $children;

    /**
     * @ORM\Id
     *
     * @ORM\Column(name="id", type="integer")
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=50, name="internal_key")
     */
    private string $internalKey = '';

    /**
     * @ORM\Column(type="string", length=15)
     */
    private ?string $key = null;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Chill\CustomFieldsBundle\Entity\CustomFieldLongChoice\Option",
     * inversedBy="children")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Option $parent = null;

    /**
     * A json representation of text (multilingual).
     *
     * @ORM\Column(type="json")
     */
    private ?array $text = null;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * @return bool
     */
    public function getActive()
    {
        return $this->isActive();
    }

    /**
     * @return Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getInternalKey()
    {
        return $this->internalKey;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return Option
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return array
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return bool
     */
    public function hasParent()
    {
        return null === $this->parent ? false : true;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return $this
     */
    public function setInternalKey($internal_key)
    {
        $this->internalKey = $internal_key;

        return $this;
    }

    /**
     * @return $this
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * @return $this
     */
    public function setParent(?Option $parent = null)
    {
        $this->parent = $parent;
        $this->key = $parent->getKey();

        return $this;
    }

    /**
     * @return $this
     */
    public function setText(array $text)
    {
        $this->text = $text;

        return $this;
    }
}
