<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CustomFieldsDefaultGroup.
 *
 * @ORM\Entity
 *
 * @ORM\Table(
 *     name="customfieldsdefaultgroup",
 *     uniqueConstraints={@ORM\UniqueConstraint(
 *         name="unique_entity",
 *         columns={"entity"}
 *     )})
 */
class CustomFieldsDefaultGroup
{
    /**
     * @ORM\ManyToOne(
     * targetEntity="Chill\CustomFieldsBundle\Entity\CustomFieldsGroup")
     *
     *     sf4 check: option inversedBy="customFields" return inconsistent error mapping !!
     */
    private ?CustomFieldsGroup $customFieldsGroup = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $entity = null;

    /**
     * @ORM\Id
     *
     * @ORM\Column(name="id", type="integer")
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * Get customFieldsGroup.
     *
     * @return CustomFieldsGroup
     */
    public function getCustomFieldsGroup()
    {
        return $this->customFieldsGroup;
    }

    /**
     * Get entity.
     *
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customFieldsGroup.
     *
     * @param CustomFieldsGroup $customFieldsGroup *
     *
     * @return CustomFieldsDefaultGroup
     */
    public function setCustomFieldsGroup($customFieldsGroup)
    {
        $this->customFieldsGroup = $customFieldsGroup;

        return $this;
    }

    /**
     * Set entity.
     *
     * @param string $entity
     *
     * @return CustomFieldsDefaultGroup
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }
}
