<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\Service;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Receive all the services tagged with 'chill.custom_field'.
 *
 * The services tagged with 'chill.custom_field' are services used to declare
 * a new custom field type. The tag must contain a 'type' variable (that must
 * be unique), this type is used to identify this custom field in the form
 * declration
 *
 * For example (in services.yml) :
 *  services:
 *      chill.icp2.type:
 *          tags:
 *              - { name: 'chill.custom_field', type: 'ICPC2' }
 */
class CustomFieldProvider implements ContainerAwareInterface
{
    /**
     * @var Container The container
     */
    private $container;

    /**
     * @var array The services indexes by the type
     */
    private array $servicesByType = [];

    /**
     * Add a new custom field to the provider.
     *
     * @param type $serviceName The name of the service (declared in service.yml)
     * @param type $type        The type of the service (that is used in the form to
     *                          add this type)
     */
    public function addCustomField($serviceName, $type)
    {
        $this->servicesByType[$type] = $serviceName;
    }

    /**
     * Get all the custom fields known by the provider.
     *
     * @return array array of the known custom fields indexed by the type
     */
    public function getAllFields()
    {
        return $this->servicesByType;
    }

    /**
     * Get a custom field stored in the provider. The custom field is identified
     * by its type.
     *
     * @param string $type The type of the wanted service
     *
     * @return CustomFieldInterface
     */
    public function getCustomFieldByType($type)
    {
        if (isset($this->servicesByType[$type])) {
            return $this->servicesByType[$type];
        }

        throw new \LogicException('the custom field with type '.$type.' is not found');
    }

    /**
     * (non-PHPdoc).
     *
     * @see \Symfony\Component\DependencyInjection\ContainerAwareInterface::setContainer()
     */
    public function setContainer(?ContainerInterface $container = null)
    {
        if (null === $container) {
            throw new \LogicException('container should not be null');
        }

        $this->container = $container;
    }
}
