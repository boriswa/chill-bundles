<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\CustomFields;

use Chill\CustomFieldsBundle\Entity\CustomField;
use Symfony\Component\Form\FormBuilderInterface;

interface CustomFieldInterface
{
    /**
     * Return a form type to edit the custom field. This form is shown to the
     * user.
     *
     * @param \Chill\CustomFieldsBundle\CustomField\FormBuilderInterface $builder
     * @param \Chill\CustomFieldsBundle\CustomField\CustomField          $customField
     *
     * @return \Symfony\Component\Form\FormTypeInterface the form type
     */
    public function buildForm(FormBuilderInterface $builder, CustomField $customField);

    /**
     * Return a formType which allow to edit option for the custom type.
     * This FormType is shown in admin.
     *
     * @param \Chill\CustomFieldsBundle\CustomField\FormBuilderInterface $builder
     *
     * @return \Symfony\Component\Form\FormTypeInterface|null the form type
     */
    public function buildOptionsForm(FormBuilderInterface $builder);

    /**
     * Transform the representation of the value, stored in db, into the
     * value which may be used in the process.
     *
     * @param \Chill\CustomFieldsBundle\CustomField\CustomField $customField
     */
    public function deserialize($serialized, CustomField $customField);

    public function getName();

    /**
     * Return if the value can be considered as empty.
     *
     * @param mixed $value the value passed throug the deserialize function
     */
    public function isEmptyValue($value, CustomField $customField);

    /**
     * Return a repsentation of the value of the CustomField.
     *
     * @param mixed                                             $value       the raw value, **not deserialized** (= as stored in the db)
     * @param \Chill\CustomFieldsBundle\CustomField\CustomField $customField
     *
     * @return string an html representation of the value
     */
    public function render($value, CustomField $customField, $documentType = 'html');

    /**
     * Transform the value into a format that can be stored in DB.
     *
     * @param \Chill\CustomFieldsBundle\CustomField\CustomField $customField
     */
    public function serialize($value, CustomField $customField);
}
