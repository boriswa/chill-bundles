<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\CustomFields;

use Chill\CustomFieldsBundle\Entity\CustomField;

abstract class AbstractCustomField implements CustomFieldInterface
{
    public function isEmptyValue($value, CustomField $customField)
    {
        return empty($value) && false !== $value;
    }
}
