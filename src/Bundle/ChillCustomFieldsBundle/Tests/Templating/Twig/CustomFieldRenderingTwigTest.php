<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFields\Tests\Templating\Twig;

use Chill\CustomFieldsBundle\Entity\CustomField;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test the rendering of twig function which renders custom fields.
 *
 * @internal
 *
 * @coversNothing
 */
final class CustomFieldRenderingTwigTest extends KernelTestCase
{
    private ?object $cfProvider = null;

    private ?object $cfRendering = null;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->cfRendering = self::$kernel->getContainer()
            ->get('chill.custom_field.twig.custom_fields_rendering');

        $this->cfProvider = self::$kernel->getContainer()
            ->get('chill.custom_field.provider');

        // set locale to fr
        $prophet = new \Prophecy\Prophet();
        $request = $prophet->prophesize();
        $request->willExtend(\Symfony\Component\HttpFoundation\Request::class);
        $request->getLocale()->willReturn('fr');
        self::$kernel->getContainer()->get('request_stack')
            ->push($request->reveal());
    }

    public function testIsEmpty()
    {
        $cf = $this->getSimpleCustomFieldText();

        // value is not empty
        $fields = [
            'test' => 'My tailor is rich',
        ];

        $result = $this->cfRendering->isEmptyValue($fields, $cf);

        $this->assertFalse($result);

        // value is empty
        $fields = [
            'text' => '',
        ];

        $result = $this->cfRendering->isEmptyValue($fields, $cf);

        $this->assertTrue($result);
    }

    public function testLabelRendering()
    {
        $cf = $this->getSimpleCustomFieldText();

        $text = $this->cfRendering->renderLabel($cf);

        $this->assertContains(
            'Test',
            $text,
            "The rendering text should contains the 'test' text"
        );
    }

    public function testWidgetRendering()
    {
        $cf = $this->getSimpleCustomFieldText();
        $fields = [
            'test' => 'My tailor is rich',
        ];

        $text = $this->cfRendering->renderWidget($fields, $cf);

        $this->assertContains(
            'My tailor is rich',
            $text,
            "The rendering text should contains the 'test' text"
        );
    }

    /**
     * @return CustomField
     */
    private function getSimpleCustomFieldText()
    {
        return (new CustomField())
            ->setSlug('test')
            ->setName(['fr' => 'Test'])
            ->setType('text')
            ->setOrdering(10)
            ->setOptions(['maxLength' => 255])
            ->setActive(true);
    }
}
