<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFields\Tests\Templating\Twig;

use Chill\CustomFieldsBundle\Entity\CustomField;
use Chill\CustomFieldsBundle\Entity\CustomFieldsGroup;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test the rendering of a custom fields group through
 * the `chill_custom_fields_group_widget`.
 *
 * @internal
 *
 * @coversNothing
 */
final class CustomFieldsGroupRenderingTwigTest extends KernelTestCase
{
    private ?object $cfProvider = null;

    private ?object $cfRendering = null;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->cfRendering = self::$kernel->getContainer()
            ->get('chill.custom_field.twig.custom_fields_group_rendering');

        $this->cfProvider = self::$kernel->getContainer()
            ->get('chill.custom_field.provider');

        // set locale to fr
        $prophet = new \Prophecy\Prophet();
        $request = $prophet->prophesize();
        $request->willExtend(\Symfony\Component\HttpFoundation\Request::class);
        $request->getLocale()->willReturn('fr');
        self::$kernel->getContainer()->get('request_stack')
            ->push($request->reveal());
    }

    public function testRenderingWidget()
    {
        $cfGroup = $this->getCustomFieldsGroup();

        $text = $this->cfRendering->renderWidget([
            'horses' => 'I like horses',
            'sure' => 'Yes !',
        ], $cfGroup);

        $this->assertContains('Do you like horses', $text);
        $this->assertContains('I like horses', $text);
        $this->assertContains('Are you sure', $text);
        $this->assertContains('Yes', $text);
    }

    public function testRenderingWidgetDoNotShowEmpty()
    {
        $cfGroup = $this->getCustomFieldsGroup();
        $cfGroup->addCustomField($this->getSimpleCustomFieldText('empty', 'Do not answer'));

        $text = $this->cfRendering->renderWidget([
            'horses' => 'I like horses',
            'sure' => 'Yes !',
        ], $cfGroup, 'html', ['show_empty' => false]);

        $this->assertContains('Do you like horses', $text);
        $this->assertContains('I like horses', $text);
        $this->assertContains('Are you sure', $text);
        $this->assertContains('Yes', $text);
        $this->assertNotContains('Do not answer', $text);
    }

    /**
     * @return CustomFieldsGroup
     */
    private function getCustomFieldsGroup()
    {
        return (new CustomFieldsGroup())
            ->setEntity('\Dummy')
            ->setName(['fr' => 'A cf group'])
            ->addCustomField($this->getSimpleCustomFieldText('horses', 'Do you like horses ?.'))
            ->addCustomField($this->getSimpleCustomFieldText('sure', 'Are you sure ?'));
    }

    /**
     * @return CustomField
     */
    private function getSimpleCustomFieldText(mixed $slug, mixed $name)
    {
        return (new CustomField())
            ->setSlug($slug)
            ->setName(['fr' => $name])
            ->setType('text')
            ->setOrdering(10)
            ->setOptions(['maxLength' => 255])
            ->setActive(true);
    }
}
