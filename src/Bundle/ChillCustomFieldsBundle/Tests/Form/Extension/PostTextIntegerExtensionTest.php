<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFields\Tests\Form\Extension;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

/**
 * Test the post-text extension.
 *
 * @internal
 *
 * @coversNothing
 */
final class PostTextIntegerExtensionTest extends KernelTestCase
{
    /**
     * @var \Symfony\Component\Form\FormBuilderInterface
     */
    private $formBuilder;

    protected function setUp(): void
    {
        self::bootKernel();

        $container = self::$kernel->getContainer();

        $this->formBuilder = $container->get('form.factory')
            ->createBuilder('form', null);
    }

    public function testCreateView()
    {
        $form = $this->formBuilder->add('test', IntegerType::class, [
            'post_text' => 'my text',
        ])->getForm();

        $view = $form->createView();

        $this->assertEquals('my text', $view['test']->vars['post_text']);
    }
}
