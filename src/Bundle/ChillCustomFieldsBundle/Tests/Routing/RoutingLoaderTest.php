<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\Tests\Routing;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Test that routes are correctly loaded.
 *
 * @internal
 *
 * @coversNothing
 */
final class RoutingLoaderTest extends WebTestCase
{
    public function testRoutesAreLoaded()
    {
        $client = self::createClient();

        $client->request('GET', '/fr/admin/customfield/');

        $this->assertEquals(
            Response::HTTP_OK,
            $client->getResponse()->getStatusCode()
        );
    }
}
