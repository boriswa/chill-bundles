<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Form\Type;

use Chill\ActivityBundle\Entity\ActivityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActivityFieldPresence extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'choices' => [
                    'Invisible' => ActivityType::FIELD_INVISIBLE,
                    'Optional' => ActivityType::FIELD_OPTIONAL,
                    'Required' => ActivityType::FIELD_REQUIRED,
                ],
            ]
        );
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}
