<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Form\Type;

use Chill\ActivityBundle\Entity\ActivityType;
use Chill\ActivityBundle\Repository\ActivityTypeRepositoryInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TranslatableActivityType extends AbstractType
{
    public function __construct(protected TranslatableStringHelperInterface $translatableStringHelper, protected ActivityTypeRepositoryInterface $activityTypeRepository)
    {
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'class' => ActivityType::class,
                'active_only' => true,
                'choices' => $this->activityTypeRepository->findAllActive(),
                'choice_label' => fn (ActivityType $type) => $this->translatableStringHelper->localize($type->getName()),
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'translatable_activity_type';
    }

    public function getParent()
    {
        return EntityType::class;
    }
}
