<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Form\Type;

use Chill\ActivityBundle\Entity\ActivityReasonCategory;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Description of TranslatableActivityReasonCategory.
 */
class TranslatableActivityReasonCategoryType extends AbstractType
{
    public function __construct(private readonly TranslatableStringHelperInterface $translatableStringHelper, private readonly TranslatorInterface $translator)
    {
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'class' => ActivityReasonCategory::class,
                'choice_label' => fn (ActivityReasonCategory $category) => $this->translatableStringHelper->localize($category->getName())
                    .(!$category->getActive() ? ' ('.$this->translator->trans('inactive').')' : ''),
            ]
        );
    }

    public function getParent()
    {
        return EntityType::class;
    }
}
