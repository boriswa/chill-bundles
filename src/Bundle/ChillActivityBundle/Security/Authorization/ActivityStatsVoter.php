<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Security\Authorization;

use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Security\Authorization\AbstractChillVoter;
use Chill\MainBundle\Security\Authorization\VoterHelperFactoryInterface;
use Chill\MainBundle\Security\Authorization\VoterHelperInterface;
use Chill\MainBundle\Security\ProvideRoleHierarchyInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class ActivityStatsVoter extends AbstractChillVoter implements ProvideRoleHierarchyInterface
{
    final public const LISTS = 'CHILL_ACTIVITY_LIST';

    final public const STATS = 'CHILL_ACTIVITY_STATS';

    protected VoterHelperInterface $helper;

    public function __construct(VoterHelperFactoryInterface $voterHelperFactory)
    {
        $this->helper = $voterHelperFactory
            ->generate(self::class)
            ->addCheckFor(Center::class, [self::STATS, self::LISTS])
            ->build();
    }

    public function getRoles(): array
    {
        return $this->getAttributes();
    }

    public function getRolesWithHierarchy(): array
    {
        return ['Activity' => $this->getRoles()];
    }

    public function getRolesWithoutScope(): array
    {
        return $this->getAttributes();
    }

    protected function supports($attribute, $subject)
    {
        return $this->helper->supports($attribute, $subject);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        return $this->helper->voteOnAttribute($attribute, $subject, $token);
    }

    private function getAttributes()
    {
        return [self::STATS, self::LISTS];
    }
}
