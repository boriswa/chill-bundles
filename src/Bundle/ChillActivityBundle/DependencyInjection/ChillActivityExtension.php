<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\DependencyInjection;

use Chill\ActivityBundle\Security\Authorization\ActivityVoter;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ChillActivityExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('chill_activity.form.time_duration', $config['form']['time_duration']);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../config'));
        $loader->load('services.yaml');
        $loader->load('services/export.yaml');
        $loader->load('services/repositories.yaml');
        $loader->load('services/fixtures.yaml');
        $loader->load('services/controller.yaml');
        $loader->load('services/form.yaml');
        $loader->load('services/templating.yaml');
        $loader->load('services/accompanyingPeriodConsistency.yaml');
        $loader->load('services/doctrine.entitylistener.yaml');
    }

    public function prepend(ContainerBuilder $container)
    {
        $this->prependRoutes($container);
        $this->prependAuthorization($container);
        $this->prependCruds($container);
    }

    public function prependAuthorization(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('security', [
            'role_hierarchy' => [
                ActivityVoter::UPDATE => [ActivityVoter::SEE_DETAILS],
                ActivityVoter::CREATE_PERSON => [ActivityVoter::SEE_DETAILS],
                ActivityVoter::CREATE_ACCOMPANYING_COURSE => [ActivityVoter::SEE_DETAILS],
                ActivityVoter::DELETE => [ActivityVoter::SEE_DETAILS],
                ActivityVoter::SEE_DETAILS => [ActivityVoter::SEE],
                ActivityVoter::FULL => [
                    ActivityVoter::DELETE,
                    ActivityVoter::UPDATE,
                ],
            ],
        ]);
    }

    /** (non-PHPdoc).
     * @see \Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface::prepend()
     */
    public function prependRoutes(ContainerBuilder $container)
    {
        // add routes for custom bundle
        $container->prependExtensionConfig('chill_main', [
            'routing' => [
                'resources' => [
                    '@ChillActivityBundle/config/routes.yaml',
                ],
            ],
        ]);
    }

    protected function prependCruds(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('chill_main', [
            'cruds' => [
                [
                    'class' => \Chill\ActivityBundle\Entity\ActivityType::class,
                    'name' => 'activity_type',
                    'base_path' => '/admin/activity/type',
                    'form_class' => \Chill\ActivityBundle\Form\ActivityTypeType::class,
                    'controller' => \Chill\ActivityBundle\Controller\AdminActivityTypeController::class,
                    'actions' => [
                        'index' => [
                            'template' => '@ChillActivity/ActivityType/index.html.twig',
                            'role' => 'ROLE_ADMIN',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillActivity/ActivityType/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillActivity/ActivityType/edit.html.twig',
                        ],
                    ],
                ],
                [
                    'class' => \Chill\ActivityBundle\Entity\ActivityTypeCategory::class,
                    'name' => 'activity_type_category',
                    'base_path' => '/admin/activity/type_category',
                    'form_class' => \Chill\ActivityBundle\Form\ActivityTypeCategoryType::class,
                    'controller' => \Chill\ActivityBundle\Controller\AdminActivityTypeCategoryController::class,
                    'actions' => [
                        'index' => [
                            'template' => '@ChillActivity/ActivityTypeCategory/index.html.twig',
                            'role' => 'ROLE_ADMIN',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillActivity/ActivityTypeCategory/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillActivity/ActivityTypeCategory/edit.html.twig',
                        ],
                    ],
                ],
                [
                    'class' => \Chill\ActivityBundle\Entity\ActivityPresence::class,
                    'name' => 'activity_presence',
                    'base_path' => '/admin/activity/presence',
                    'form_class' => \Chill\ActivityBundle\Form\ActivityPresenceType::class,
                    'controller' => \Chill\ActivityBundle\Controller\AdminActivityPresenceController::class,
                    'actions' => [
                        'index' => [
                            'template' => '@ChillActivity/ActivityPresence/index.html.twig',
                            'role' => 'ROLE_ADMIN',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillActivity/ActivityPresence/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillActivity/ActivityPresence/edit.html.twig',
                        ],
                    ],
                ],
            ],
        ]);
    }
}
