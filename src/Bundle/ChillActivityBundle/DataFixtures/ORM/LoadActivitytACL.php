<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\DataFixtures\ORM;

use Chill\ActivityBundle\Security\Authorization\ActivityStatsVoter;
use Chill\ActivityBundle\Security\Authorization\ActivityVoter;
use Chill\MainBundle\DataFixtures\ORM\LoadPermissionsGroup;
use Chill\MainBundle\DataFixtures\ORM\LoadScopes;
use Chill\MainBundle\Entity\RoleScope;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Add a role CHILL_ACTIVITY_UPDATE & CHILL_ACTIVITY_CREATE for all groups except administrative,
 * and a role CHILL_ACTIVITY_SEE for administrative.
 */
class LoadActivitytACL extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 16000;
    }

    public function load(ObjectManager $manager)
    {
        foreach (LoadPermissionsGroup::$refs as $permissionsGroupRef) {
            $permissionsGroup = $this->getReference($permissionsGroupRef);

            foreach (LoadScopes::$references as $scopeRef) {
                $scope = $this->getReference($scopeRef);
                // create permission group
                switch ($permissionsGroup->getName()) {
                    case 'social':
                        if ('administrative' === $scope->getName()['en']) {
                            break 2; // we do not want any power on administrative
                        }

                        break;

                    case 'administrative':
                    case 'direction':
                        if (\in_array($scope->getName()['en'], ['administrative', 'social'], true)) {
                            break 2; // we do not want any power on social or administrative
                        }

                        break;
                }

                printf(
                    'Adding CHILL_ACTIVITY_UPDATE & CHILL_ACTIVITY_CREATE & CHILL_ACTIVITY_DELETE, and stats and list permissions to %s '
                        ."permission group, scope '%s' \n",
                    $permissionsGroup->getName(),
                    $scope->getName()['en']
                );
                $roleScopeUpdate = (new RoleScope())
                    ->setRole('CHILL_ACTIVITY_UPDATE')
                    ->setScope($scope);
                $permissionsGroup->addRoleScope($roleScopeUpdate);
                $roleScopeCreate = (new RoleScope())
                    ->setRole(ActivityVoter::CREATE_ACCOMPANYING_COURSE)
                    ->setScope($scope);
                $roleScopeCreate = (new RoleScope())
                    ->setRole(ActivityVoter::CREATE_PERSON)
                    ->setScope($scope);
                $permissionsGroup->addRoleScope($roleScopeCreate);
                $roleScopeDelete = (new RoleScope())
                    ->setRole('CHILL_ACTIVITY_DELETE')
                    ->setScope($scope);
                $permissionsGroup->addRoleScope($roleScopeDelete);
                $roleScopeList = (new RoleScope())
                    ->setRole(ActivityStatsVoter::LISTS);
                $permissionsGroup->addRoleScope($roleScopeList);
                $roleScopeStat = (new RoleScope())
                    ->setRole(ActivityStatsVoter::STATS);
                $permissionsGroup->addRoleScope($roleScopeStat);

                $manager->persist($roleScopeUpdate);
                $manager->persist($roleScopeCreate);
                $manager->persist($roleScopeDelete);
            }
        }

        $manager->flush();
    }
}
