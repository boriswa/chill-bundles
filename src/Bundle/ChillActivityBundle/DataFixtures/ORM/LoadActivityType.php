<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\DataFixtures\ORM;

use Chill\ActivityBundle\Entity\ActivityType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Description of LoadActivityType.
 */
class LoadActivityType extends Fixture implements OrderedFixtureInterface
{
    public static $references = [];

    public function getOrder()
    {
        return 16100;
    }

    public function load(ObjectManager $manager)
    {
        $types = [
            // Exange
            [
                'name' => ['fr' => 'Entretien physique avec l\'usager'],
                'category' => 'exchange', ],
            [
                'name' => ['fr' => 'Appel téléphonique', 'en' => 'Telephone call', 'nl' => 'Telefoon appel'],
                'category' => 'exchange', ],
            [
                'name' => ['fr' => 'Courriel', 'en' => 'Email', 'nl' => 'Email'],
                'category' => 'exchange', ],
            // Meeting
            [
                'name' => ['fr' => 'Point technique encadrant'],
                'category' => 'meeting', ],
            [
                'name' => ['fr' => 'Réunion avec des partenaires'],
                'category' => 'meeting', ],
            [
                'name' => ['fr' => 'Commission pluridisciplinaire et pluri-institutionnelle'],
                'category' => 'meeting', ],
        ];

        foreach ($types as $t) {
            echo 'Creating activity type : '.$t['name']['fr'].' (cat:'.$t['category']." \n";
            $activityType = (new ActivityType())
                ->setName($t['name'])
                ->setCategory($this->getReference('activity_type_cat_'.$t['category']))
                ->setSocialIssuesVisible(1)
                ->setSocialActionsVisible(1);
            $manager->persist($activityType);
            $reference = 'activity_type_'.$t['name']['fr'];
            $this->addReference($reference, $activityType);
            static::$references[] = $reference;
        }

        $manager->flush();
    }
}
