<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Export\LinkedToPerson;

use Chill\ActivityBundle\Export\Declarations;
use Chill\ActivityBundle\Repository\ActivityRepository;
use Chill\ActivityBundle\Security\Authorization\ActivityStatsVoter;
use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Export\ExportInterface;
use Chill\MainBundle\Export\FormatterInterface;
use Chill\MainBundle\Export\GroupedExportInterface;
use Chill\PersonBundle\Export\Declarations as PersonDeclarations;
use Doctrine\ORM\Query;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * This export allow to compute stats on activity duration.
 *
 * The desired stat must be given in constructor.
 */
class StatActivityDuration implements ExportInterface, GroupedExportInterface
{
    final public const SUM = 'sum';
    private readonly bool $filterStatsByCenters;

    /**
     * @param string $action the stat to perform
     */
    public function __construct(
        private readonly ActivityRepository $activityRepository,
        ParameterBagInterface $parameterBag,
        /**
         * The action for this report.
         */
        protected string $action = 'sum'
    ) {
        $this->filterStatsByCenters = $parameterBag->get('chill_main')['acl']['filter_stats_by_center'];
    }

    public function buildForm(FormBuilderInterface $builder)
    {
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getAllowedFormattersTypes()
    {
        return [FormatterInterface::TYPE_TABULAR];
    }

    public function getDescription()
    {
        if (self::SUM === $this->action) {
            return 'Sum activities linked to a person duration by various parameters.';
        }

        throw new \LogicException('this action is not supported: '.$this->action);
    }

    public function getGroup(): string
    {
        return 'Exports of activities linked to a person';
    }

    public function getLabels($key, array $values, $data)
    {
        if ('export_stat_activity' !== $key) {
            throw new \LogicException(sprintf('The key %s is not used by this export', $key));
        }

        $header = self::SUM === $this->action ? 'Sum activities linked to a person duration' : false;

        return static fn (string $value) => '_header' === $value ? $header : $value;
    }

    public function getQueryKeys($data)
    {
        return ['export_stat_activity'];
    }

    public function getResult($query, $data)
    {
        return $query->getQuery()->getResult(Query::HYDRATE_SCALAR);
    }

    public function getTitle()
    {
        if (self::SUM === $this->action) {
            return 'Sum activity linked to a person duration';
        }

        throw new \LogicException('This action is not supported: '.$this->action);
    }

    public function getType(): string
    {
        return Declarations::ACTIVITY;
    }

    public function initiateQuery(array $requiredModifiers, array $acl, array $data = [])
    {
        $centers = array_map(
            static fn (array $el): Center => $el['center'],
            $acl
        );

        $qb = $this->activityRepository->createQueryBuilder('activity');

        $select = null;

        if (self::SUM === $this->action) {
            $select = 'SUM(activity.durationTime) AS export_stat_activity';
        }

        $qb->select($select)
            ->join('activity.person', 'person');

        if ($this->filterStatsByCenters) {
            $qb
                ->join('person.centerHistory', 'centerHistory')
                ->where(
                    $qb->expr()->andX(
                        $qb->expr()->lte('centerHistory.startDate', 'activity.date'),
                        $qb->expr()->orX(
                            $qb->expr()->isNull('centerHistory.endDate'),
                            $qb->expr()->gt('centerHistory.endDate', 'activity.date')
                        )
                    )
                )
                ->andWhere($qb->expr()->in('centerHistory.center', ':centers'))
                ->setParameter('centers', $centers);
        }

        return $qb;
    }

    public function requiredRole(): string
    {
        return ActivityStatsVoter::STATS;
    }

    public function supportsModifiers()
    {
        return [
            Declarations::ACTIVITY,
            Declarations::ACTIVITY_PERSON,
            PersonDeclarations::PERSON_TYPE,
        ];
    }
}
