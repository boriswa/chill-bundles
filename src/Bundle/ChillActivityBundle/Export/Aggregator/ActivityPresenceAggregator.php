<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Aggregator;

use Chill\ActivityBundle\Export\Declarations;
use Chill\ActivityBundle\Repository\ActivityPresenceRepositoryInterface;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class ActivityPresenceAggregator implements AggregatorInterface
{
    public function __construct(private ActivityPresenceRepositoryInterface $activityPresenceRepository, private TranslatableStringHelperInterface $translatableStringHelper)
    {
    }

    public function buildForm(FormBuilderInterface $builder)
    {
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, mixed $data)
    {
        return function (int|string|null $value): string {
            if ('_header' === $value) {
                return 'export.aggregator.activity.by_activity_presence.header';
            }

            if (null === $value || '' === $value || null === $presence = $this->activityPresenceRepository->find($value)) {
                return '';
            }

            return $this->translatableStringHelper->localize($presence->getName());
        };
    }

    public function getQueryKeys($data)
    {
        return ['activity_presence_aggregator_attendee'];
    }

    public function getTitle(): string
    {
        return 'export.aggregator.activity.by_activity_presence.Group activity by presence';
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data): void
    {
        $qb->addSelect('IDENTITY(activity.attendee) AS activity_presence_aggregator_attendee');
        $qb->addGroupBy('activity_presence_aggregator_attendee');
    }

    public function applyOn()
    {
        return Declarations::ACTIVITY;
    }
}
