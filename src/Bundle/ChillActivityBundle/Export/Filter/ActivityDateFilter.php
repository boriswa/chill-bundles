<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Filter;

use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Form\Type\PickRollingDateType;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ActivityDateFilter implements FilterInterface
{
    public function __construct(protected TranslatorInterface $translator, private readonly RollingDateConverterInterface $rollingDateConverter)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $where = $qb->getDQLPart('where');
        $clause = $qb->expr()->between(
            'activity.date',
            ':date_from',
            ':date_to'
        );

        if ($where instanceof Expr\Andx) {
            $where->add($clause);
        } else {
            $where = $qb->expr()->andX($clause);
        }

        $qb->add('where', $where);
        $qb->setParameter(
            'date_from',
            $this->rollingDateConverter->convert($data['date_from'])
        );
        $qb->setParameter(
            'date_to',
            $this->rollingDateConverter->convert($data['date_to'])
        );
    }

    public function applyOn(): string
    {
        return Declarations::ACTIVITY;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder
            ->add('date_from', PickRollingDateType::class, [
                'label' => 'Activities after this date',
            ])
            ->add('date_to', PickRollingDateType::class, [
                'label' => 'Activities before this date',
            ]);
    }

    public function getFormDefaultData(): array
    {
        return ['date_from' => new RollingDate(RollingDate::T_YEAR_PREVIOUS_START), 'date_to' => new RollingDate(RollingDate::T_TODAY)];
    }

    public function describeAction($data, $format = 'string')
    {
        return [
            'Filtered by date of activity: only between %date_from% and %date_to%',
            [
                '%date_from%' => $this->rollingDateConverter->convert($data['date_from'])->format('d-m-Y'),
                '%date_to%' => $this->rollingDateConverter->convert($data['date_to'])->format('d-m-Y'),
            ],
        ];
    }

    public function getTitle()
    {
        return 'Filtered by date activity';
    }
}
