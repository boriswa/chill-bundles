<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Filter;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Export\FilterInterface;
use Doctrine\ORM\Query\Expr\Andx;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class SentReceivedFilter implements FilterInterface
{
    private const CHOICES = [
        'export.filter.activity.by_sent_received.is sent' => Activity::SENTRECEIVED_SENT,
        'export.filter.activity.by_sent_received.is received' => Activity::SENTRECEIVED_RECEIVED,
    ];

    private const DEFAULT_CHOICE = Activity::SENTRECEIVED_SENT;

    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $where = $qb->getDQLPart('where');

        $clause = $qb->expr()->eq('activity.sentReceived', ':sentreceived');

        if ($where instanceof Andx) {
            $where->add($clause);
        } else {
            $where = $qb->expr()->andX($clause);
        }

        $qb->add('where', $where);
        $qb->setParameter('sentreceived', $data['accepted_sentreceived']);
    }

    public function applyOn(): string
    {
        return Declarations::ACTIVITY;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('accepted_sentreceived', ChoiceType::class, [
            'choices' => self::CHOICES,
            'multiple' => false,
            'expanded' => true,
            'empty_data' => self::DEFAULT_CHOICE,
            'label' => 'export.filter.activity.by_sent_received.Sent or received',
        ]);
    }

    public function getFormDefaultData(): array
    {
        return ['accepted_sentreceived' => self::DEFAULT_CHOICE];
    }

    public function describeAction($data, $format = 'string'): array
    {
        $sentreceived = array_flip(self::CHOICES)[$data['accepted_sentreceived']];

        return ['Filtered activity by sentreceived: only %sentreceived%', [
            '%sentreceived%' => $this->translator->trans($sentreceived),
        ]];
    }

    public function getTitle(): string
    {
        return 'Filter activity by sentreceived';
    }
}
