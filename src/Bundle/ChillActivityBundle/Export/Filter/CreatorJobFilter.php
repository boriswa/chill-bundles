<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Filter;

use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Entity\User\UserJobHistory;
use Chill\MainBundle\Entity\UserJob;
use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Repository\UserJobRepositoryInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class CreatorJobFilter implements FilterInterface
{
    private const PREFIX = 'acp_act_filter_creator_job';

    public function __construct(
        private TranslatableStringHelper $translatableStringHelper,
        private TranslatorInterface $translator,
        private UserJobRepositoryInterface $userJobRepository,
    ) {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $p = self::PREFIX;

        $qb
            ->leftJoin('activity.createdBy', "{$p}_user")
            ->leftJoin(
                UserJobHistory::class,
                "{$p}_history",
                Join::WITH,
                $qb->expr()->eq("{$p}_history.user", "{$p}_user")
            )
            // job_at based on activity.date
            ->andWhere(
                $qb->expr()->andX(
                    $qb->expr()->lte("{$p}_history.startDate", 'activity.date'),
                    $qb->expr()->orX(
                        $qb->expr()->isNull("{$p}_history.endDate"),
                        $qb->expr()->gt("{$p}_history.endDate", 'activity.date')
                    )
                )
            )
            ->andWhere(
                $qb->expr()->in("{$p}_history.job", ":{$p}_jobs")
            )
            ->setParameter(
                "{$p}_jobs",
                $data['jobs'],
            );
    }

    public function applyOn(): string
    {
        return Declarations::ACTIVITY;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder
            ->add('jobs', EntityType::class, [
                'choices' => $this->userJobRepository->findAllActive(),
                'class' => UserJob::class,
                'choice_label' => fn (UserJob $s) => $this->translatableStringHelper->localize(
                    $s->getLabel()
                ).($s->isActive() ? '' : '('.$this->translator->trans('inactive').')'),
                'label' => 'export.filter.activity.by_creator_job.job_form_label',
                'multiple' => true,
                'expanded' => true,
            ]);
    }

    public function describeAction($data, $format = 'string'): array
    {
        $jobs = array_map(
            fn (UserJob $job) => $this->translatableStringHelper->localize($job->getLabel()),
            $data['jobs'] instanceof Collection ? $data['jobs']->toArray() : $data['jobs']
        );

        return ['export.filter.activity.by_creator_job.Filtered activity by user job: only %jobs%', [
            '%jobs%' => implode(', ', $jobs),
        ]];
    }

    public function getFormDefaultData(): array
    {
        return [
            'jobs' => [],
        ];
    }

    public function getTitle(): string
    {
        return 'export.filter.activity.by_creator_job.Filter activity by user job';
    }
}
