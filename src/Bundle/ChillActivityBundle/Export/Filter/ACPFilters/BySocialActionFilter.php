<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Filter\ACPFilters;

use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Export\FilterInterface;
use Chill\PersonBundle\Entity\SocialWork\SocialAction;
use Chill\PersonBundle\Form\Type\PickSocialActionType;
use Chill\PersonBundle\Templating\Entity\SocialActionRender;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class BySocialActionFilter implements FilterInterface
{
    public function __construct(private readonly SocialActionRender $actionRender)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('actsocialaction', $qb->getAllAliases(), true)) {
            $qb->join('activity.socialActions', 'actsocialaction');
        }

        $clause = $qb->expr()->in('actsocialaction.id', ':socialactions');

        $qb->andWhere($clause)
            ->setParameter(
                'socialactions',
                SocialAction::getDescendantsWithThisForActions($data['accepted_socialactions'])
            );
    }

    public function applyOn(): string
    {
        return Declarations::ACTIVITY_ACP;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('accepted_socialactions', PickSocialActionType::class, [
            'multiple' => true,
        ]);
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function describeAction($data, $format = 'string'): array
    {
        $actions = [];

        foreach ($data['accepted_socialactions'] as $action) {
            $actions[] = $this->actionRender->renderString($action, [
                'show_and_children' => true,
            ]);
        }

        return ['Filtered activity by linked socialaction: only %actions%', [
            '%actions%' => implode(', ', $actions),
        ]];
    }

    public function getTitle(): string
    {
        return 'Filter activity by linked socialaction';
    }
}
