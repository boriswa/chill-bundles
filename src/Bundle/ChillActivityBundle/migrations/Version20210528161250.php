<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Activity;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add socialIssues & socialActions fields to Activity.
 */
final class Version20210528161250 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE chill_activity_activity_chill_person_socialissue');
        $this->addSql('DROP TABLE chill_activity_activity_chill_person_socialaction');
    }

    public function getDescription(): string
    {
        return 'Add socialIssues & socialActions fields to Activity';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE chill_activity_activity_chill_person_socialissue (activity_id INT NOT NULL, socialissue_id INT NOT NULL, PRIMARY KEY(activity_id, socialissue_id))');
        $this->addSql('CREATE INDEX IDX_3DA33F2681C06096 ON chill_activity_activity_chill_person_socialissue (activity_id)');
        $this->addSql('CREATE INDEX IDX_3DA33F26A549916C ON chill_activity_activity_chill_person_socialissue (socialissue_id)');
        $this->addSql('CREATE TABLE chill_activity_activity_chill_person_socialaction (activity_id INT NOT NULL, socialaction_id INT NOT NULL, PRIMARY KEY(activity_id, socialaction_id))');
        $this->addSql('CREATE INDEX IDX_548F1AD881C06096 ON chill_activity_activity_chill_person_socialaction (activity_id)');
        $this->addSql('CREATE INDEX IDX_548F1AD83DC32179 ON chill_activity_activity_chill_person_socialaction (socialaction_id)');
        $this->addSql('ALTER TABLE chill_activity_activity_chill_person_socialissue ADD CONSTRAINT FK_3DA33F2681C06096 FOREIGN KEY (activity_id) REFERENCES activity (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_activity_activity_chill_person_socialissue ADD CONSTRAINT FK_3DA33F26A549916C FOREIGN KEY (socialissue_id) REFERENCES chill_person_social_issue (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_activity_activity_chill_person_socialaction ADD CONSTRAINT FK_548F1AD881C06096 FOREIGN KEY (activity_id) REFERENCES activity (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_activity_activity_chill_person_socialaction ADD CONSTRAINT FK_548F1AD83DC32179 FOREIGN KEY (socialaction_id) REFERENCES chill_person_social_action (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
