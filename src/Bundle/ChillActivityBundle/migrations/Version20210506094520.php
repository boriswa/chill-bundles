<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Activity;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210506094520 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activitytype DROP CONSTRAINT FK_B38CD05112469DE2');
        $this->addSql('ALTER TABLE activitytype DROP category_id');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activitytype ADD category_id INT DEFAULT 1');
        $this->addSql('ALTER TABLE activitytype ADD CONSTRAINT FK_B38CD05112469DE2 FOREIGN KEY (category_id) REFERENCES activitytypecategory (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
