<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Activity;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210506090417 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activitytypecategory DROP ordering');
        $this->addSql('ALTER TABLE activitytype DROP ordering');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activitytype ADD ordering DOUBLE PRECISION DEFAULT \'0.0\' NOT NULL');
        $this->addSql('ALTER TABLE activitytypecategory ADD ordering DOUBLE PRECISION DEFAULT \'0.0\' NOT NULL');
    }
}
