<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Activity;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210408122329 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activitytype DROP personVisible');
        $this->addSql('ALTER TABLE activitytype DROP personLabel');
        $this->addSql('ALTER TABLE activitytype DROP userVisible');
        $this->addSql('ALTER TABLE activitytype DROP userLabel');
        $this->addSql('ALTER TABLE activitytype DROP dateVisible');
        $this->addSql('ALTER TABLE activitytype DROP dateLabel');
        $this->addSql('ALTER TABLE activitytype DROP placeVisible');
        $this->addSql('ALTER TABLE activitytype DROP placeLabel');
        $this->addSql('ALTER TABLE activitytype DROP personsVisible');
        $this->addSql('ALTER TABLE activitytype DROP personsLabel');
        $this->addSql('ALTER TABLE activitytype DROP thirdpartyVisible');
        $this->addSql('ALTER TABLE activitytype DROP thirdpartyLabel');
        $this->addSql('ALTER TABLE activitytype DROP durationTimeVisible');
        $this->addSql('ALTER TABLE activitytype DROP durationTimeLabel');
        $this->addSql('ALTER TABLE activitytype DROP attendeeVisible');
        $this->addSql('ALTER TABLE activitytype DROP attendeeLabel');
        $this->addSql('ALTER TABLE activitytype DROP reasonsVisible');
        $this->addSql('ALTER TABLE activitytype DROP reasonsLabel');
        $this->addSql('ALTER TABLE activitytype DROP commentVisible');
        $this->addSql('ALTER TABLE activitytype DROP commentLabel');
        $this->addSql('ALTER TABLE activitytype DROP sentReceivedVisible');
        $this->addSql('ALTER TABLE activitytype DROP sentReceivedLabel');
        $this->addSql('ALTER TABLE activitytype DROP documentVisible');
        $this->addSql('ALTER TABLE activitytype DROP documentLabel');
        $this->addSql('ALTER TABLE activitytype DROP emergencyVisible');
        $this->addSql('ALTER TABLE activitytype DROP emergencyLabel');
        $this->addSql('ALTER TABLE activitytype DROP accompanyingPeriodVisible');
        $this->addSql('ALTER TABLE activitytype DROP accompanyingPeriodLabel');
        $this->addSql('ALTER TABLE activitytype DROP socialDataVisible');
        $this->addSql('ALTER TABLE activitytype DROP socialDataLabel');
        $this->addSql('ALTER TABLE activitytype ALTER name DROP NOT NULL');
        $this->addSql('ALTER TABLE activitytype ALTER active SET DEFAULT \'true\'');
        $this->addSql('COMMENT ON COLUMN activitytype.name IS NULL');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activitytype ADD personVisible SMALLINT DEFAULT 2 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD personLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD userVisible SMALLINT DEFAULT 2 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD userLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD dateVisible SMALLINT DEFAULT 2 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD dateLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD placeVisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD placeLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD personsVisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD personsLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD thirdpartyVisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD thirdpartyLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD durationTimeVisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD durationTimeLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD attendeeVisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD attendeeLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD reasonsVisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD reasonsLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD commentVisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD commentLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD sentReceivedVisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD sentReceivedLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD documentVisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD documentLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD emergencyVisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD emergencyLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD accompanyingPeriodVisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD accompanyingPeriodLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD socialDataVisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD socialDataLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype ALTER name SET NOT NULL');
        $this->addSql('ALTER TABLE activitytype ALTER active DROP DEFAULT');
    }
}
