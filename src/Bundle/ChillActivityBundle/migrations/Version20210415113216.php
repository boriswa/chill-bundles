<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Activity;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210415113216 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activitytype ADD thirdpartyvisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD thirdpartylabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD documentvisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD documentlabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype DROP thirdPartiesVisible');
        $this->addSql('ALTER TABLE activitytype DROP thirdPartiesLabel');
        $this->addSql('ALTER TABLE activitytype DROP documentsVisible');
        $this->addSql('ALTER TABLE activitytype DROP documentsLabel');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activitytype ADD thirdPartiesVisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD thirdPartiesLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD documentsVisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD documentsLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype DROP thirdpartyvisible');
        $this->addSql('ALTER TABLE activitytype DROP thirdpartylabel');
        $this->addSql('ALTER TABLE activitytype DROP documentvisible');
        $this->addSql('ALTER TABLE activitytype DROP documentlabel');
    }
}
