<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Menu;

use Chill\ActivityBundle\Security\Authorization\ActivityVoter;
use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @implements LocalMenuBuilderInterface<array{accompanyingCourse: AccompanyingPeriod}>
 */
class AccompanyingCourseMenuBuilder implements LocalMenuBuilderInterface
{
    public function __construct(protected Security $security, protected TranslatorInterface $translator)
    {
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        $period = $parameters['accompanyingCourse'];

        if (
            AccompanyingPeriod::STEP_DRAFT !== $period->getStep()
            && $this->security->isGranted(ActivityVoter::SEE, $period)
        ) {
            $menu->addChild($this->translator->trans('Activity'), [
                'route' => 'chill_activity_activity_list',
                'routeParameters' => [
                    'accompanying_period_id' => $period->getId(),
                ], ])
                ->setExtras(['order' => 40]);
        }
    }

    public static function getMenuIds(): array
    {
        return ['accompanyingCourse'];
    }
}
