<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Repository;

use Chill\ActivityBundle\Entity\ActivityReasonCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ActivityReasonCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActivityReasonCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActivityReasonCategory[]    findAll()
 * @method ActivityReasonCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActivityReasonCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ActivityReasonCategory::class);
    }
}
