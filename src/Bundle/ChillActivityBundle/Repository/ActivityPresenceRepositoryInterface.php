<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Repository;

use Chill\ActivityBundle\Entity\ActivityPresence;

interface ActivityPresenceRepositoryInterface
{
    public function find($id): ?ActivityPresence;

    /**
     * @return array|ActivityPresence[]
     */
    public function findAll(): array;

    /**
     * @return array|ActivityPresence[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array;

    public function findOneBy(array $criteria): ?ActivityPresence;

    public function getClassName(): string;
}
