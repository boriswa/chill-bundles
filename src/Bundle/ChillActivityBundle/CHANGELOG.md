
Version 1.5.1
=============

- [report activity count] fix error: do not show centers which are not selected in results.

Version 1.5.2
=============

- [aggregate by activity type] fix translation in aggregate activity type
- fix some translation in export
- fix error when persons not loaded by other aggregators / filters
- add "filter by activity type" filter

Version 1.5.3
=============

- add privacy events to activity list / show / edit

Version 1.5.4
=============

- [report activity]: add aggregator for activity users
- fix bug: error when extracting activities without filter / aggregators selecting persons

Version 1.5.5
=============

- [activity] replace dropdown for selecting reasons and use chillEntity for reason rendering
- fix bug: error when trying to edit activity of which the type has been deactivated

