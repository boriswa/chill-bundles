<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Controller;

use Chill\ActivityBundle\Entity\ActivityReasonCategory;
use Chill\ActivityBundle\Form\ActivityReasonCategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * ActivityReasonCategory controller.
 */
class ActivityReasonCategoryController extends AbstractController
{
    /**
     * Creates a new ActivityReasonCategory entity.
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/activityreasoncategory/create", name="chill_activity_activityreasoncategory_create", methods={"POST"})
     */
    public function createAction(Request $request)
    {
        $entity = new ActivityReasonCategory();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('chill_activity_activityreasoncategory_show', ['id' => $entity->getId()]);
        }

        return $this->render('@ChillActivity/ActivityReasonCategory/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing ActivityReasonCategory entity.
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/activityreasoncategory/{id}/edit", name="chill_activity_activityreasoncategory_edit")
     */
    public function editAction(mixed $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(ActivityReasonCategory::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ActivityReasonCategory entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('@ChillActivity/ActivityReasonCategory/edit.html.twig', [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        ]);
    }

    /**
     * Lists all ActivityReasonCategory entities.
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/activityreasoncategory/", name="chill_activity_activityreasoncategory")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository(ActivityReasonCategory::class)->findAll();

        return $this->render('@ChillActivity/ActivityReasonCategory/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * Displays a form to create a new ActivityReasonCategory entity.
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/activityreasoncategory/new", name="chill_activity_activityreasoncategory_new")
     */
    public function newAction()
    {
        $entity = new ActivityReasonCategory();
        $form = $this->createCreateForm($entity);

        return $this->render('@ChillActivity/ActivityReasonCategory/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a ActivityReasonCategory entity.
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/activityreasoncategory/{id}/show", name="chill_activity_activityreasoncategory_show")
     */
    public function showAction(mixed $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(ActivityReasonCategory::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ActivityReasonCategory entity.');
        }

        return $this->render('@ChillActivity/ActivityReasonCategory/show.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * Edits an existing ActivityReasonCategory entity.
     *
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/activityreasoncategory/{id}/update", name="chill_activity_activityreasoncategory_update", methods={"POST", "PUT"})
     */
    public function updateAction(Request $request, mixed $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(ActivityReasonCategory::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ActivityReasonCategory entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();

            return $this->redirectToRoute('chill_activity_activityreasoncategory_edit', ['id' => $id]);
        }

        return $this->render('@ChillActivity/ActivityReasonCategory/edit.html.twig', [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        ]);
    }

    /**
     * Creates a form to create a ActivityReasonCategory entity.
     *
     * @param ActivityReasonCategory $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ActivityReasonCategory $entity)
    {
        $form = $this->createForm(ActivityReasonCategoryType::class, $entity, [
            'action' => $this->generateUrl('chill_activity_activityreasoncategory_create'),
            'method' => 'POST',
        ]);

        $form->add('submit', SubmitType::class, ['label' => 'Create']);

        return $form;
    }

    /**
     * Creates a form to edit a ActivityReasonCategory entity.
     *
     * @param ActivityReasonCategory $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ActivityReasonCategory $entity)
    {
        $form = $this->createForm(ActivityReasonCategoryType::class, $entity, [
            'action' => $this->generateUrl('chill_activity_activityreasoncategory_update', ['id' => $entity->getId()]),
            'method' => 'PUT',
        ]);

        $form->add('submit', SubmitType::class, ['label' => 'Update']);

        return $form;
    }
}
