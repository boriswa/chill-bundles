<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Entity;

use Chill\ActivityBundle\Validator\Constraints as ActivityValidator;
use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateTrait;
use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Entity\Embeddable\CommentEmbeddable;
use Chill\MainBundle\Entity\Embeddable\PrivateCommentEmbeddable;
use Chill\MainBundle\Entity\HasCentersInterface;
use Chill\MainBundle\Entity\HasScopesInterface;
use Chill\MainBundle\Entity\Location;
use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Validator\Constraints\Entity\UserCircleConsistency;
use Chill\PersonBundle\AccompanyingPeriod\SocialIssueConsistency\AccompanyingPeriodLinkedWithSocialIssuesEntityInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Entity\SocialWork\SocialAction;
use Chill\PersonBundle\Entity\SocialWork\SocialIssue;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\DiscriminatorMap;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Activity.
 *
 * @ORM\Entity(repositoryClass="Chill\ActivityBundle\Repository\ActivityRepository")
 *
 * @ORM\Table(name="activity")
 *
 * @ORM\HasLifecycleCallbacks
 *
 * @DiscriminatorMap(typeProperty="type", mapping={
 *     "activity": Activity::class
 * })
 *
 * @ActivityValidator\ActivityValidity
 *
 * TODO see if necessary
 * UserCircleConsistency(
 *     "CHILL_ACTIVITY_SEE_DETAILS",
 *     getUserFunction="getUser",
 * path="scope")
 */
class Activity implements AccompanyingPeriodLinkedWithSocialIssuesEntityInterface, HasCentersInterface, HasScopesInterface, TrackCreationInterface, TrackUpdateInterface
{
    use TrackCreationTrait;

    use TrackUpdateTrait;

    final public const SENTRECEIVED_RECEIVED = 'received';

    final public const SENTRECEIVED_SENT = 'sent';

    /**
     * @ORM\ManyToOne(targetEntity="Chill\PersonBundle\Entity\AccompanyingPeriod")
     *
     * @Groups({"read"})
     */
    private ?AccompanyingPeriod $accompanyingPeriod = null;

    /**
     * @ORM\ManyToOne(targetEntity="Chill\ActivityBundle\Entity\ActivityType")
     *
     * @Groups({"read", "docgen:read"})
     *
     * @SerializedName("activityType")
     *
     * @ORM\JoinColumn(name="type_id")
     */
    private ActivityType $activityType;

    /**
     * @ORM\ManyToOne(targetEntity="Chill\ActivityBundle\Entity\ActivityPresence")
     *
     * @Groups({"docgen:read"})
     */
    private ?ActivityPresence $attendee = null;

    /**
     * @ORM\Embedded(class="Chill\MainBundle\Entity\Embeddable\CommentEmbeddable", columnPrefix="comment_")
     *
     * @Groups({"docgen:read"})
     */
    private CommentEmbeddable $comment;

    /**
     * @ORM\Column(type="datetime")
     *
     * @Groups({"docgen:read"})
     */
    private \DateTime $date;

    /**
     * @ORM\ManyToMany(targetEntity="Chill\DocStoreBundle\Entity\StoredObject", cascade={"persist"})
     *
     * @Assert\Valid(traverse=true)
     *
     * @var Collection<StoredObject>
     */
    private Collection $documents;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private ?\DateTime $durationTime = null;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     *
     * @Groups({"docgen:read"})
     */
    private bool $emergency = false;

    /**
     * @ORM\Id
     *
     * @ORM\Column(name="id", type="integer")
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups({"read", "docgen:read"})
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="Chill\MainBundle\Entity\Location")
     *
     * @groups({"read", "docgen:read"})
     */
    private ?Location $location = null;

    /**
     * @ORM\ManyToOne(targetEntity="Chill\PersonBundle\Entity\Person")
     */
    private ?Person $person = null;

    /**
     * @ORM\ManyToMany(targetEntity="Chill\PersonBundle\Entity\Person")
     *
     * @Groups({"read", "docgen:read"})
     *
     * @var Collection<Person>
     */
    private Collection $persons;

    /**
     * @ORM\Embedded(class="Chill\MainBundle\Entity\Embeddable\PrivateCommentEmbeddable", columnPrefix="privateComment_")
     */
    private PrivateCommentEmbeddable $privateComment;

    /**
     * @ORM\ManyToMany(targetEntity="Chill\ActivityBundle\Entity\ActivityReason")
     *
     * @Groups({"docgen:read"})
     *
     * @var Collection<ActivityReason>
     */
    private Collection $reasons;

    /**
     * @ORM\ManyToOne(targetEntity="Chill\MainBundle\Entity\Scope")
     *
     * @Groups({"docgen:read"})
     */
    private ?Scope $scope = null;

    /**
     * @ORM\Column(type="string", options={"default": ""})
     *
     * @Groups({"docgen:read"})
     */
    private string $sentReceived = '';

    /**
     * @ORM\ManyToMany(targetEntity="Chill\PersonBundle\Entity\SocialWork\SocialAction")
     *
     * @ORM\JoinTable(name="chill_activity_activity_chill_person_socialaction")
     *
     * @Groups({"read", "docgen:read"})
     *
     * @var Collection<SocialAction>
     */
    private Collection $socialActions;

    /**
     * @ORM\ManyToMany(targetEntity="Chill\PersonBundle\Entity\SocialWork\SocialIssue")
     *
     * @ORM\JoinTable(name="chill_activity_activity_chill_person_socialissue")
     *
     * @Groups({"read", "docgen:read"})
     *
     * @var Collection<SocialIssue>
     */
    private Collection $socialIssues;

    /**
     * @ORM\ManyToMany(targetEntity="Chill\ThirdPartyBundle\Entity\ThirdParty")
     *
     * @Groups({"read", "docgen:read"})
     *
     * @var Collection<ThirdParty>
     */
    private Collection $thirdParties;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private ?\DateTime $travelTime = null;

    /**
     * @ORM\ManyToOne(targetEntity="Chill\MainBundle\Entity\User")
     *
     * @Groups({"docgen:read"})
     */
    private ?User $user = null;

    /**
     * @ORM\ManyToMany(targetEntity="Chill\MainBundle\Entity\User")
     *
     * @Groups({"read", "docgen:read"})
     *
     * @var Collection<User>
     */
    private Collection $users;

    public function __construct()
    {
        $this->reasons = new ArrayCollection();
        $this->comment = new CommentEmbeddable();
        $this->privateComment = new PrivateCommentEmbeddable();
        $this->persons = new ArrayCollection();
        $this->thirdParties = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->socialIssues = new ArrayCollection();
        $this->socialActions = new ArrayCollection();
    }

    public function addDocument(StoredObject $document): self
    {
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Add a person to the person list.
     */
    public function addPerson(?Person $person): self
    {
        if (null !== $person) {
            if (!$this->persons->contains($person)) {
                $this->persons[] = $person;
            }
        }

        return $this;
    }

    public function addReason(ActivityReason $reason): self
    {
        $this->reasons->add($reason);

        return $this;
    }

    public function addSocialAction(SocialAction $socialAction): self
    {
        if (!$this->socialActions->contains($socialAction)) {
            $this->socialActions[] = $socialAction;
            $this->ensureSocialActionConsistency();
        }

        return $this;
    }

    /**
     * Add a social issue.
     *
     * Note: the social issue consistency (the fact that only youngest social issues
     * are kept) is processed by an entity listener:
     *
     * @see{\Chill\PersonBundle\AccompanyingPeriod\SocialIssueConsistency\AccompanyingPeriodSocialIssueConsistencyEntityListener}
     */
    public function addSocialIssue(SocialIssue $socialIssue): self
    {
        if (!$this->socialIssues->contains($socialIssue)) {
            $this->socialIssues[] = $socialIssue;
        }

        if (null !== $this->getAccompanyingPeriod()) {
            $this->getAccompanyingPeriod()->addSocialIssue($socialIssue);
        }

        return $this;
    }

    public function addThirdParty(?ThirdParty $thirdParty): self
    {
        if (null !== $thirdParty) {
            if (!$this->thirdParties->contains($thirdParty)) {
                $this->thirdParties[] = $thirdParty;
            }
        }

        return $this;
    }

    public function addUser(?User $user): self
    {
        if (null !== $user) {
            if (!$this->users->contains($user)) {
                $this->users[] = $user;
            }
        }

        return $this;
    }

    public function getAccompanyingPeriod(): ?AccompanyingPeriod
    {
        return $this->accompanyingPeriod;
    }

    public function getActivityType(): ActivityType
    {
        return $this->activityType;
    }

    public function getAttendee(): ?ActivityPresence
    {
        return $this->attendee;
    }

    /**
     * get the center
     * center is extracted from person.
     */
    public function getCenters(): iterable
    {
        if ($this->person instanceof Person) {
            return [$this->person->getCenter()];
        }

        if ($this->getAccompanyingPeriod() instanceof AccompanyingPeriod) {
            return $this->getAccompanyingPeriod()->getCenters() ?? [];
        }

        return [];
    }

    public function getComment(): CommentEmbeddable
    {
        return $this->comment;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    /**
     * @Groups({"docgen:read"})
     */
    public function getDurationMinute(): int
    {
        if (null === $this->durationTime) {
            return 0;
        }

        return (int) round(($this->durationTime->getTimestamp() + $this->durationTime->getOffset()) / 60.0, 0);
    }

    public function getDurationTime(): ?\DateTime
    {
        return $this->durationTime;
    }

    public function getEmergency(): bool
    {
        return $this->emergency;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function getPersons(): Collection
    {
        return $this->persons;
    }

    public function getPersonsAssociated(): array
    {
        if (null !== $this->accompanyingPeriod) {
            $personsAssociated = [];

            foreach ($this->accompanyingPeriod->getOpenParticipations() as $participation) {
                if ($this->persons->contains($participation->getPerson())) {
                    $personsAssociated[] = $participation->getPerson();
                }
            }

            return $personsAssociated;
        }

        return [];
    }

    public function getPersonsNotAssociated(): array
    {
        if (null !== $this->accompanyingPeriod) {
            $personsNotAssociated = [];

            // TODO better semantic with:  return $this->persons->filter(...);
            foreach ($this->persons as $person) {
                if (null === $this->accompanyingPeriod->getOpenParticipationContainsPerson($person)) {
                    $personsNotAssociated[] = $person;
                }
            }

            return $personsNotAssociated;
        }

        return [];
    }

    public function getPrivateComment(): PrivateCommentEmbeddable
    {
        return $this->privateComment;
    }

    public function getReasons(): Collection
    {
        return $this->reasons;
    }

    public function getScope(): ?Scope
    {
        return $this->scope;
    }

    public function getScopes(): iterable
    {
        if (null !== $this->getAccompanyingPeriod()) {
            return $this->getAccompanyingPeriod()->getScopes();
        }

        if (null !== $this->getPerson()) {
            return [$this->scope];
        }

        return [];
    }

    public function getSentReceived(): string
    {
        return $this->sentReceived;
    }

    public function getSocialActions(): Collection
    {
        return $this->socialActions;
    }

    public function getSocialIssues(): Collection
    {
        return $this->socialIssues;
    }

    public function getThirdParties(): Collection
    {
        return $this->thirdParties;
    }

    public function getTravelTime(): ?\DateTime
    {
        return $this->travelTime;
    }

    /**
     * @Groups({"docgen:read"})
     */
    public function getTravelTimeMinute(): int
    {
        if (null === $this->travelTime) {
            return 0;
        }

        return (int) round(($this->travelTime->getTimestamp() + $this->travelTime->getOffset()) / 60.0, 0);
    }

    /**
     * @deprecated
     */
    public function getType(): ActivityType
    {
        return $this->activityType;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function isEmergency(): bool
    {
        return $this->getEmergency();
    }

    public function removeDocument(StoredObject $document): void
    {
        $this->documents->removeElement($document);
    }

    public function removePerson(Person $person): void
    {
        $this->persons->removeElement($person);
    }

    public function removeReason(ActivityReason $reason): void
    {
        $this->reasons->removeElement($reason);
    }

    public function removeSocialAction(SocialAction $socialAction): self
    {
        $this->socialActions->removeElement($socialAction);

        return $this;
    }

    public function removeSocialIssue(SocialIssue $socialIssue): self
    {
        $this->socialIssues->removeElement($socialIssue);

        return $this;
    }

    public function removeThirdParty(ThirdParty $thirdParty): void
    {
        $this->thirdParties->removeElement($thirdParty);
    }

    public function removeUser(User $user): void
    {
        $this->users->removeElement($user);
    }

    public function setAccompanyingPeriod(?AccompanyingPeriod $accompanyingPeriod): self
    {
        $this->accompanyingPeriod = $accompanyingPeriod;

        foreach ($this->getSocialIssues() as $issue) {
            $this->accompanyingPeriod->addSocialIssue($issue);
        }

        return $this;
    }

    public function setActivityType(ActivityType $activityType): self
    {
        $this->activityType = $activityType;

        return $this;
    }

    public function setAttendee(ActivityPresence $attendee): self
    {
        $this->attendee = $attendee;

        return $this;
    }

    public function setComment(CommentEmbeddable $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function setDate(\DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function setDocuments(Collection $documents): self
    {
        $this->documents = $documents;

        return $this;
    }

    public function setDurationTime(?\DateTime $durationTime): self
    {
        $this->durationTime = $durationTime;

        return $this;
    }

    public function setEmergency(bool $emergency): self
    {
        $this->emergency = $emergency;

        return $this;
    }

    public function setLocation(?Location $location): Activity
    {
        $this->location = $location;

        return $this;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function setPersons(?Collection $persons): self
    {
        $this->persons = $persons;

        return $this;
    }

    public function setPrivateComment(PrivateCommentEmbeddable $privateComment): self
    {
        $this->privateComment = $privateComment;

        return $this;
    }

    public function setReasons(?ArrayCollection $reasons): self
    {
        $this->reasons = $reasons;

        return $this;
    }

    public function setScope(Scope $scope): self
    {
        $this->scope = $scope;

        return $this;
    }

    public function setSentReceived(?string $sentReceived): self
    {
        $this->sentReceived = (string) $sentReceived;

        return $this;
    }

    public function setThirdParties(?Collection $thirdParties): self
    {
        $this->thirdParties = $thirdParties;

        return $this;
    }

    public function setTravelTime(\DateTime $travelTime): self
    {
        $this->travelTime = $travelTime;

        return $this;
    }

    /**
     * @deprecated
     */
    public function setType(ActivityType $activityType): self
    {
        $this->activityType = $activityType;

        return $this;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function setUsers(Collection $users): self
    {
        $this->users = $users;

        return $this;
    }

    private function ensureSocialActionConsistency(): void
    {
        $ancestors = SocialAction::findAncestorSocialActions($this->getSocialActions());

        foreach ($ancestors as $ancestor) {
            $this->removeSocialAction($ancestor);
        }
    }
}
