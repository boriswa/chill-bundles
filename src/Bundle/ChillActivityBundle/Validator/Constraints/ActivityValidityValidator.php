<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Validator\Constraints;

use Chill\ActivityBundle\Entity\Activity;
use Chill\PersonBundle\Entity\SocialWork\SocialIssue;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class ActivityValidityValidator extends ConstraintValidator
{
    public function validate($activity, Constraint $constraint)
    {
        if (!$constraint instanceof ActivityValidity) {
            throw new UnexpectedTypeException($constraint, ActivityValidity::class);
        }

        if (!$activity instanceof Activity) {
            throw new UnexpectedValueException($activity, Activity::class);
        }

        if (2 === $activity->getActivityType()->getPersonsVisible() && 0 === \count($activity->getPersons())) {
            $this->context
                ->buildViolation($constraint->noPersonsMessage)
                ->addViolation();
        }

        if (2 === $activity->getActivityType()->getUsersVisible() && 0 === \count($activity->getUsers())) {
            $this->context
                ->buildViolation($constraint->noUsersMessage)
                ->addViolation();
        }

        if (2 === $activity->getActivityType()->getThirdPartiesVisible() && 0 === \count($activity->getThirdParties())) {
            $this->context
                ->buildViolation($constraint->noThirdPartiesMessage)
                ->addViolation();
        }

        if (2 === $activity->getActivityType()->getUserVisible() && null === $activity->getUser()) {
            $this->context
                ->buildViolation($constraint->makeIsRequiredMessage('user'))
                ->addViolation();
        }

        if (2 === $activity->getActivityType()->getDateVisible() && null === $activity->getDate()) {
            $this->context
                ->buildViolation($constraint->makeIsRequiredMessage('date'))
                ->addViolation();
        }

        if (2 === $activity->getActivityType()->getLocationVisible() && null === $activity->getLocation()) {
            $this->context
                ->buildViolation($constraint->makeIsRequiredMessage('location'))
                ->addViolation();
        }

        if (2 === $activity->getActivityType()->getDurationTimeVisible() && null === $activity->getDurationTime()) {
            $this->context
                ->buildViolation($constraint->makeIsRequiredMessage('duration time'))
                ->addViolation();
        }

        if (2 === $activity->getActivityType()->getTravelTimeVisible() && null === $activity->getTravelTime()) {
            $this->context
                ->buildViolation($constraint->makeIsRequiredMessage('travel time'))
                ->addViolation();
        }

        if (2 === $activity->getActivityType()->getAttendeeVisible() && null === $activity->getAttendee()) {
            $this->context
                ->buildViolation($constraint->makeIsRequiredMessage('attendee'))
                ->addViolation();
        }

        if (2 === $activity->getActivityType()->getReasonsVisible() && null === $activity->getReasons()) {
            $this->context
                ->buildViolation($constraint->makeIsRequiredMessage('reasons'))
                ->addViolation();
        }

        if (2 === $activity->getActivityType()->getCommentVisible() && null === $activity->getComment()) {
            $this->context
                ->buildViolation($constraint->makeIsRequiredMessage('comment'))
                ->addViolation();
        }

        if (2 === $activity->getActivityType()->getSentReceivedVisible() && null === $activity->getSentReceived()) {
            $this->context
                ->buildViolation($constraint->makeIsRequiredMessage('sent/received'))
                ->addViolation();
        }

        if (2 === $activity->getActivityType()->getDocumentsVisible() && null === $activity->getDocuments()) {
            $this->context
                ->buildViolation($constraint->makeIsRequiredMessage('document'))
                ->addViolation();
        }

        if (2 === $activity->getActivityType()->getEmergencyVisible() && null === $activity->getEmergency()) {
            $this->context
                ->buildViolation($constraint->makeIsRequiredMessage('emergency'))
                ->addViolation();
        }

        if (2 === $activity->getActivityType()->getSocialIssuesVisible() && 0 === $activity->getSocialIssues()->count()) {
            $this->context
                ->buildViolation($constraint->socialIssuesMessage)
                ->addViolation();
        }

        if (2 === $activity->getActivityType()->getSocialActionsVisible() && 0 === $activity->getSocialActions()->count()) {
            // check if a social action may be added
            $actions = [];

            foreach ($activity->getSocialIssues() as $socialIssue) {
                /** @var SocialIssue $socialIssue */
                $actions = \array_merge($actions, $socialIssue->getRecursiveSocialActions()->toArray());
            }

            if (0 < \count($actions)) {
                $this->context
                    ->buildViolation($constraint->socialActionsMessage)
                    ->addViolation();
            }
        }
    }
}
