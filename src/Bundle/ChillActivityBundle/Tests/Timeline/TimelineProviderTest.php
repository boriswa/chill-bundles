<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Timeline;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class TimelineProviderTest extends WebTestCase
{
    /**
     * @doesNotPerformAssertions
     */
    public function testAnActivityIsShownOnTimeline(): never
    {
        $this->markTestSkipped('we have to write fixtures before writing this tests');
    }
}
