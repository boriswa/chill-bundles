<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Aggregator\ACPAggregators;

use Chill\ActivityBundle\Export\Aggregator\ACPAggregators\ByActivityTypeAggregator;
use Chill\ActivityBundle\Repository\ActivityTypeRepositoryInterface;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class ByActivityTypeAggregatorTest extends AbstractAggregatorTest
{
    private RollingDateConverterInterface $rollingDateConverter;
    private ActivityTypeRepositoryInterface $activityTypeRepository;
    private TranslatableStringHelperInterface $translatableStringHelper;

    protected function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->rollingDateConverter = self::$container->get(RollingDateConverterInterface::class);
        $this->activityTypeRepository = self::$container->get(ActivityTypeRepositoryInterface::class);
        $this->translatableStringHelper = self::$container->get(TranslatableStringHelperInterface::class);
    }

    public function getAggregator()
    {
        return new ByActivityTypeAggregator(
            $this->rollingDateConverter,
            $this->activityTypeRepository,
            $this->translatableStringHelper,
        );
    }

    public function getFormData()
    {
        return [
            [
                'after_date' => null,
                'before_date' => null,
            ],
            [
                'after_date' => new RollingDate(RollingDate::T_YEAR_PREVIOUS_START),
                'before_date' => null,
            ],
            [
                'after_date' => null,
                'before_date' => new RollingDate(RollingDate::T_TODAY),
            ],
            [
                'after_date' => new RollingDate(RollingDate::T_YEAR_PREVIOUS_START),
                'before_date' => new RollingDate(RollingDate::T_TODAY),
            ],
        ];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(distinct acp.id)')
                ->from(AccompanyingPeriod::class, 'acp'),
        ];
    }
}
