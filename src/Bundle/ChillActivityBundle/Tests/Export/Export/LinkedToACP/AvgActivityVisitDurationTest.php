<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Export\LinkedToACP;

use Chill\ActivityBundle\Export\Export\LinkedToACP\AvgActivityVisitDuration;
use Chill\MainBundle\Test\Export\AbstractExportTest;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class AvgActivityVisitDurationTest extends AbstractExportTest
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function getExport()
    {
        $em = self::$container->get(EntityManagerInterface::class);

        yield new AvgActivityVisitDuration($em, $this->getParameters(true));
        yield new AvgActivityVisitDuration($em, $this->getParameters(false));
    }

    public function getFormData(): array
    {
        return [
            [],
        ];
    }

    public function getModifiersCombination(): array
    {
        return [
            ['activity'],
            ['activity', 'accompanying_period'],
        ];
    }
}
