<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Export\LinkedToACP;

use Chill\ActivityBundle\Export\Export\LinkedToACP\SumActivityVisitDuration;
use Chill\MainBundle\Test\Export\AbstractExportTest;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class SumActivityVisitDurationTest extends AbstractExportTest
{
    private SumActivityVisitDuration $export;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->export = self::$container->get('chill.activity.export.sum_activity_visit_duration_linked_to_acp');
    }

    public function getExport()
    {
        $em = self::$container->get(EntityManagerInterface::class);

        yield new SumActivityVisitDuration($em, $this->getParameters(true));
        yield new SumActivityVisitDuration($em, $this->getParameters(false));
    }

    public function getFormData(): array
    {
        return [
            [],
        ];
    }

    public function getModifiersCombination(): array
    {
        return [
            ['activity'],
            ['activity', 'accompanying_period'],
        ];
    }
}
