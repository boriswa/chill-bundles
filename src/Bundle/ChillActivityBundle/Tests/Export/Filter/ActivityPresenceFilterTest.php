<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Filter;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Export\Filter\ActivityPresenceFilter;
use Chill\ActivityBundle\Repository\ActivityPresenceRepositoryInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class ActivityPresenceFilterTest extends AbstractFilterTest
{
    private TranslatableStringHelperInterface $translatableStringHelper;
    private TranslatorInterface $translator;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        $this->translator = self::$container->get(TranslatorInterface::class);
        $this->translatableStringHelper = self::$container->get(TranslatableStringHelperInterface::class);
    }

    public function getFilter()
    {
        return new ActivityPresenceFilter($this->translatableStringHelper, $this->translator);
    }

    public function getFormData()
    {
        self::bootKernel();

        $presences = self::$container->get(ActivityPresenceRepositoryInterface::class)
            ->findAll();

        return [
            [
                'presences' => $presences,
            ],
            [
                'presences' => new ArrayCollection($presences),
            ],
        ];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        yield $em->createQueryBuilder()
            ->select('count(activity.id)')
            ->from(Activity::class, 'activity');

        self::ensureKernelShutdown();
    }
}
