Version 1.5.1
=============

- fix bug in filter task form: allow to show the list of users, which was hidden when the user had access to multiple centers;
- add assignee in task list;
- fix some translation; 
- add a filtering by center on list;
- add color in boxes for task statuses;

Version 1.5.4
=============

- adding indexes on place event and task

Version 1.5.5
=============

- Fix error on the "see more" link which was not showed
- Layout of the task list

Version 1.5.6
=============

- fix: validation error on warning date interval is not shown
- add privacy events to task show / list;
- add privacy events to task edit / update;

Version 1.5.7
==============

- fix error when showing task list without person in context (issue #3) ;

Version 1.5.8
=============

- add returnPath to page Show and List for Single tasks ;

Version 1.5.9
=============

- better exception description when task workflow is not found ;

Version 1.5.10
==============

- load webpack config using a `configure` function ;

Version 1.5.11
==============

- [task] fix loading of chill task list ;

