<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\TaskBundle\Event\UI;

use Chill\TaskBundle\Entity\AbstractTask;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Workflow\Transition;

class UIEvent extends \Symfony\Contracts\EventDispatcher\Event
{
    final public const EDIT_FORM = 'chill_task.edit_form';

    final public const EDIT_PAGE = 'chill_task.edit_page';

    final public const SHOW_TRANSITION_PAGE = 'chill_task.show_transition_page';

    /**
     * @var FormInterface|null
     */
    protected $form;

    /**
     * @var Response|null
     */
    protected $response;

    /**
     * @var AbstractTask
     */
    protected $task;

    /**
     * @var Transition
     */
    protected $transition;

    /**
     * @param string $kind
     */
    public function __construct(protected $kind, AbstractTask $task)
    {
        $this->task = $task;
    }

    public function getForm(): ?FormInterface
    {
        return $this->form;
    }

    /**
     * @return string
     */
    public function getKind()
    {
        return $this->kind;
    }

    public function getResponse(): Response
    {
        return $this->response;
    }

    public function getTask(): AbstractTask
    {
        return $this->task;
    }

    /**
     * @return Transition
     */
    public function getTransition()
    {
        return $this->transition;
    }

    public function hasResponse()
    {
        return $this->response instanceof Response;
    }

    public function setForm(FormInterface $form)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * @return $this
     */
    public function setResponse(Response $response)
    {
        $this->response = $response;

        return $this;
    }

    public function setTransition(Transition $transition)
    {
        $this->transition = $transition;

        return $this;
    }
}
