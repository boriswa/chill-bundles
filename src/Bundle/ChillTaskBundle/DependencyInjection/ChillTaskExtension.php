<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\TaskBundle\DependencyInjection;

use Chill\TaskBundle\Security\Authorization\TaskVoter;
use Chill\TaskBundle\Workflow\TaskWorkflowManager;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @see http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ChillTaskExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../config'));
        $loader->load('services/controller.yaml');
        $loader->load('services/security.yaml');
        $loader->load('services/repositories.yaml');
        $loader->load('services/workflow.yaml');
        $loader->load('services/templating.yaml');
        $loader->load('services/menu.yaml');
        $loader->load('services/event.yaml');
        $loader->load('services/timeline.yaml');
        $loader->load('services/fixtures.yaml');
        $loader->load('services/form.yaml');
    }

    public function prepend(ContainerBuilder $container)
    {
        $this->prependAuthorization($container);
        $this->prependRoute($container);
        $this->prependWorkflows($container);
    }

    protected function prependAuthorization(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('security', [
            'role_hierarchy' => [
                TaskVoter::UPDATE => [TaskVoter::SHOW],
                TaskVoter::CREATE_COURSE => [TaskVoter::SHOW],
                TaskVoter::CREATE_PERSON => [TaskVoter::SHOW],
            ],
        ]);
    }

    protected function prependRoute(ContainerBuilder $container)
    {
        // declare routes for task bundle
        $container->prependExtensionConfig('chill_main', [
            'routing' => [
                'resources' => [
                    '@ChillTaskBundle/config/routes.yaml',
                ],
            ],
        ]);
    }

    protected function prependWorkflows(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('framework', [
            'workflows' => [
                'task_default' => [
                    'marking_store' => [
                        'type' => 'method',
                        'property' => 'currentStates',
                    ],
                    'type' =>  'workflow',
                    'support_strategy' => TaskWorkflowManager::class,
                    'places' => ['new', 'in_progress', 'closed', 'canceled'],
                    'initial_marking' => ['new'],
                    'transitions' => [
                        'start' => [
                            'from' => 'new',
                            'to' => 'in_progress',
                        ],
                        'close' => [
                            'from' => ['new', 'in_progress'],
                            'to' => 'closed',
                        ],
                        'cancel' => [
                            'from' => ['new', 'in_progress'],
                            'to' => 'canceled',
                        ],
                    ],
                ],
            ],
        ]);
    }
}
