<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\TaskBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * RecurringTask.
 *
 * @ORM\Table(name="chill_task.recurring_task")
 *
 * @ORM\Entity(repositoryClass="Chill\TaskBundle\Repository\RecurringTaskRepository")
 */
class RecurringTask extends AbstractTask
{
    /**
     * @ORM\Column(name="first_occurence_end_date", type="date")
     */
    private ?\DateTime $firstOccurenceEndDate = null;

    /**
     * @ORM\Column(name="id", type="integer")
     *
     * @ORM\Id
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="last_occurence_end_date", type="date")
     */
    private ?\DateTime $lastOccurenceEndDate = null;

    /**
     * @ORM\Column(name="occurence_frequency", type="string", length=255)
     */
    private ?string $occurenceFrequency = null;

    /**
     * @ORM\Column(name="occurence_start_date", type="dateinterval")
     */
    private $occurenceStartDate;

    /**
     * @ORM\Column(name="occurence_warning_interval", type="dateinterval", nullable=true)
     */
    private $occurenceWarningInterval;

    /**
     * @var Collection<SingleTask>
     *
     * @ORM\OneToMany(
     *     targetEntity="SingleTask",
     *     mappedBy="recurringTask"
     * )
     */
    private Collection $singleTasks;

    public function __construct()
    {
        $this->singleTasks = new ArrayCollection();
    }

    /**
     * Get firstOccurenceEndDate.
     *
     * @return \DateTime
     */
    public function getFirstOccurenceEndDate()
    {
        return $this->firstOccurenceEndDate;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get lastOccurenceEndDate.
     *
     * @return \DateTime
     */
    public function getLastOccurenceEndDate()
    {
        return $this->lastOccurenceEndDate;
    }

    /**
     * Get occurenceFrequency.
     *
     * @return string
     */
    public function getOccurenceFrequency()
    {
        return $this->occurenceFrequency;
    }

    /**
     * Get occurenceStartDate.
     */
    public function getOccurenceStartDate()
    {
        return $this->occurenceStartDate;
    }

    /**
     * Get occurenceWarningInterval.
     */
    public function getOccurenceWarningInterval()
    {
        return $this->occurenceWarningInterval;
    }

    /**
     * Set firstOccurenceEndDate.
     *
     * @param \DateTime $firstOccurenceEndDate
     *
     * @return RecurringTask
     */
    public function setFirstOccurenceEndDate($firstOccurenceEndDate)
    {
        $this->firstOccurenceEndDate = $firstOccurenceEndDate;

        return $this;
    }

    /**
     * Set lastOccurenceEndDate.
     *
     * @param \DateTime $lastOccurenceEndDate
     *
     * @return RecurringTask
     */
    public function setLastOccurenceEndDate($lastOccurenceEndDate)
    {
        $this->lastOccurenceEndDate = $lastOccurenceEndDate;

        return $this;
    }

    /**
     * Set occurenceFrequency.
     *
     * @param string $occurenceFrequency
     *
     * @return RecurringTask
     */
    public function setOccurenceFrequency($occurenceFrequency)
    {
        $this->occurenceFrequency = $occurenceFrequency;

        return $this;
    }

    /**
     * Set occurenceStartDate.
     *
     * @return RecurringTask
     */
    public function setOccurenceStartDate(mixed $occurenceStartDate)
    {
        $this->occurenceStartDate = $occurenceStartDate;

        return $this;
    }

    /**
     * Set occurenceWarningInterval.
     *
     * @return RecurringTask
     */
    public function setOccurenceWarningInterval(mixed $occurenceWarningInterval)
    {
        $this->occurenceWarningInterval = $occurenceWarningInterval;

        return $this;
    }
}
