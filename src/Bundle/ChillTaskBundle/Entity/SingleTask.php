<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\TaskBundle\Entity;

use Chill\TaskBundle\Entity\Task\SingleTaskPlaceEvent;
use DateInterval;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SingleTask.
 *
 * @ORM\Table(
 *     name="chill_task.single_task",
 *     indexes={
 *
 *         @ORM\Index(
 *             name="by_type",
 *             columns={ "type" }
 *         ),
 *         @ORM\Index(
 *             name="by_current_state",
 *             columns={ "current_states" }
 *         ),
 *         @ORM\Index(
 *             name="by_end_date",
 *             columns={ "end_date" }
 *         )
 *     }
 * )
 *
 * @ORM\Entity(repositoryClass="Chill\TaskBundle\Repository\SingleTaskRepository")
 */
class SingleTask extends AbstractTask
{
    /**
     * @ORM\Column(name="end_date", type="date", nullable=true)
     *
     * @Assert\Date
     *
     * @Serializer\Groups({"read"})
     */
    private ?\DateTime $endDate = null;

    /**
     * @ORM\Column(name="id", type="integer")
     *
     * @ORM\Id
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Groups({"read"})
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="RecurringTask",
     *     inversedBy="singleTasks"
     * )
     */
    private ?RecurringTask $recurringTask = null;

    /**
     * @ORM\Column(name="start_date", type="date", nullable=true)
     *
     * @Serializer\Groups({"read"})
     *
     * @Assert\Date
     *
     * @Assert\Expression(
     *     "value === null or this.getEndDate() === null or value < this.getEndDate()",
     *     message="The start date must be before the end date"
     * )
     * @Assert\Expression(
     *     "value === null or this.getWarningDate() === null or this.getWarningDate() > this.getStartDate()",
     *     message="The start date must be before warning date"
     * )
     */
    private ?\DateTime $startDate = null;

    /**
     * @var Collection<SingleTaskPlaceEvent>
     *
     * @ORM\OneToMany(
     *     targetEntity="\Chill\TaskBundle\Entity\Task\SingleTaskPlaceEvent",
     *     mappedBy="task",
     *     cascade={ "remove" }
     * )
     */
    private Collection $taskPlaceEvents;

    /**
     * @ORM\Column(name="warning_interval", type="dateinterval", nullable=true)
     *
     * @Serializer\Groups({"read"})
     *
     * @Assert\Expression(
     *     "!(value != null and this.getEndDate() == null)",
     *     message="An end date is required if a warning interval is set"
     * )
     */
    private ?\DateInterval $warningInterval = null;

    public function __construct()
    {
        $this->taskPlaceEvents = new ArrayCollection();
    }

    /**
     * Get endDate.
     */
    public function getEndDate(): ?\DateTime
    {
        return $this->endDate;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getRecurringTask(): RecurringTask
    {
        return $this->recurringTask;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    public function getTaskPlaceEvents(): Collection
    {
        return $this->taskPlaceEvents;
    }

    /**
     * Get the Warning date, computed from the difference between the
     * end date and the warning interval.
     *
     * Return null if warningDate or endDate is null
     *
     * @return \DateTimeImmutable
     *
     * @Serializer\Groups({"read"})
     */
    public function getWarningDate()
    {
        if (null === $this->getWarningInterval()) {
            return null;
        }

        if (null === $this->getEndDate()) {
            return null;
        }

        return \DateTimeImmutable::createFromMutable($this->getEndDate())
            ->sub($this->getWarningInterval());
    }

    /**
     * Get warningInterval.
     */
    public function getWarningInterval(): ?\DateInterval
    {
        return $this->warningInterval;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime $endDate
     *
     * @return SingleTask
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function setRecurringTask(RecurringTask $recurringTask)
    {
        $this->recurringTask = $recurringTask;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime $startDate
     *
     * @return SingleTask
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function setTaskPlaceEvents(Collection $taskPlaceEvents)
    {
        $this->taskPlaceEvents = $taskPlaceEvents;

        return $this;
    }

    /**
     * Set warningInterval.
     *
     * @return SingleTask
     */
    public function setWarningInterval(?\DateInterval $warningInterval)
    {
        $this->warningInterval = $warningInterval;

        return $this;
    }
}
