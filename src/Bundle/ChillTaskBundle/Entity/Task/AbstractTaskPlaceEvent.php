<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\TaskBundle\Entity\Task;

use Chill\MainBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * AbstractTaskPlaceEvent.
 *
 * @ORM\MappedSuperclass
 */
class AbstractTaskPlaceEvent
{
    /**
     * @ORM\ManyToOne(
     *     targetEntity="\Chill\MainBundle\Entity\User"
     * )
     */
    private ?User $author = null;

    /**
     * @ORM\Column(name="data", type="json")
     */
    private array $data = [];

    /**
     * @var datetime_immutable
     *
     * @ORM\Column(name="datetime", type="datetime_immutable")
     */
    private $datetime;

    /**
     * @ORM\Column(name="transition", type="string", length=255)
     */
    private string $transition = '';

    public function __construct()
    {
        $this->datetime = new \DateTimeImmutable('now');
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    /**
     * Get data.
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Get datetime.
     *
     * @return datetime_immutable
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Get transition.
     *
     * @return string
     */
    public function getTransition()
    {
        return $this->transition;
    }

    public function setAuthor(User $author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Set data.
     *
     * @return AbstractTaskPlaceEvent
     */
    public function setData(array $data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Set datetime.
     *
     * @param datetime_immutable $datetime
     *
     * @return AbstractTaskPlaceEvent
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Set transition.
     *
     * @param string $transition
     *
     * @return AbstractTaskPlaceEvent
     */
    public function setTransition($transition)
    {
        $this->transition = $transition;

        return $this;
    }
}
