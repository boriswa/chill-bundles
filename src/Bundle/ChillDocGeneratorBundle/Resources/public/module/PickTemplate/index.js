import {createApp} from 'vue';
import PickTemplate from 'ChillDocGeneratorAssets/vuejs/_components/PickTemplate.vue';
import {fetchTemplates} from 'ChillDocGeneratorAssets/api/pickTemplate.js';
import {_createI18n} from 'ChillMainAssets/vuejs/_js/i18n';

const i18n = _createI18n({});

document.querySelectorAll('div[data-docgen-template-picker]').forEach(el => {
    fetchTemplates(el.dataset.entityClass).then(templates => {
        let
            picker = {
                template: '<pick-template :templates="this.templates" :entityId="this.entityId"></pick-template>',
                components: {
                    PickTemplate,
                },
                data() {
                    return {
                        templates: templates,
                        entityId: el.dataset.entityId,
                    }
                },
            }
        ;
        createApp(picker).use(i18n).mount(el);
    })

});
