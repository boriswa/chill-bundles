<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\GeneratorDriver\Exception;

/**
 * A exception to throw when there are syntax errors in the
 * template file.
 */
class TemplateException extends \RuntimeException
{
    public function __construct(private readonly array $errors, $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct('Error while generating document from template', $code, $previous);
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
