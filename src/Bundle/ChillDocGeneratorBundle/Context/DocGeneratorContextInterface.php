<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\Context;

use Chill\DocGeneratorBundle\Entity\DocGeneratorTemplate;
use Chill\DocStoreBundle\Entity\StoredObject;

/**
 * Interface for context for document generation.
 *
 * @template T of object
 */
interface DocGeneratorContextInterface
{
    /**
     * Get the data that will be injected to the generated document.
     *
     * @param T $entity
     */
    public function getData(DocGeneratorTemplate $template, object $entity, array $contextGenerationData = []): array;

    public function getDescription(): string;

    public function getEntityClass(): string;

    public static function getKey(): string;

    public function getName(): string;

    /**
     * @param T $entity
     */
    public function storeGenerated(DocGeneratorTemplate $template, StoredObject $storedObject, object $entity, array $contextGenerationData): void;
}
