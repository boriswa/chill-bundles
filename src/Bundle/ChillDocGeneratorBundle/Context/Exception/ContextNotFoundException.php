<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\Context\Exception;

class ContextNotFoundException extends \RuntimeException
{
    public function __construct($contextName)
    {
        parent::__construct(sprintf('the context with name %s is not found', $contextName));
    }
}
