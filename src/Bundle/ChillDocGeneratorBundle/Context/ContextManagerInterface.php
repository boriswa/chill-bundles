<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\Context;

use Chill\DocGeneratorBundle\Context\Exception\ContextNotFoundException;
use Chill\DocGeneratorBundle\Entity\DocGeneratorTemplate;

interface ContextManagerInterface
{
    /**
     * @throws ContextNotFoundException when the context is not found
     */
    public function getContextByDocGeneratorTemplate(DocGeneratorTemplate $docGeneratorTemplate): DocGeneratorContextInterface;

    /**
     * @throws ContextNotFoundException when the context is not found
     */
    public function getContextByKey(string $searchedKey): DocGeneratorContextInterface;

    public function getContexts(): array;
}
