<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\Context;

use Chill\DocGeneratorBundle\Context\Exception\ContextNotFoundException;
use Chill\DocGeneratorBundle\Entity\DocGeneratorTemplate;

final readonly class ContextManager implements ContextManagerInterface
{
    /**
     * @param \Chill\DocGeneratorBundle\Context\DocGeneratorContextInterface[] $contexts
     */
    public function __construct(private iterable $contexts)
    {
    }

    public function getContextByDocGeneratorTemplate(DocGeneratorTemplate $docGeneratorTemplate): DocGeneratorContextInterface
    {
        foreach ($this->contexts as $key => $context) {
            if ($docGeneratorTemplate->getContext() === $key) {
                return $context;
            }
        }

        throw new ContextNotFoundException($docGeneratorTemplate->getContext());
    }

    public function getContextByKey(string $searchedKey): DocGeneratorContextInterface
    {
        foreach ($this->contexts as $key => $context) {
            if ($searchedKey === $key) {
                return $context;
            }
        }

        throw new ContextNotFoundException($searchedKey);
    }

    public function getContexts(): array
    {
        return iterator_to_array($this->contexts);
    }
}
