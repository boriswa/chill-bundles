<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\DocGenerator;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Using DocStore objects inside the DocGenTemplate.
 */
final class Version20211103111010 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_docgen_template DROP CONSTRAINT FK_49A347E893CB796C');
        $this->addSql('DROP INDEX IDX_49A347E893CB796C');
        $this->addSql('ALTER TABLE chill_docgen_template ADD file VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE chill_docgen_template DROP file_id');
        $this->addSql('ALTER TABLE chill_docgen_template ALTER entities DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_docgen_template ALTER context DROP NOT NULL');
    }

    public function getDescription(): string
    {
        return 'Using DocStore objects inside the DocGenTemplate';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_docgen_template ADD file_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_docgen_template DROP file');
        $this->addSql('ALTER TABLE chill_docgen_template ALTER entities SET NOT NULL');
        $this->addSql('ALTER TABLE chill_docgen_template ALTER context SET NOT NULL');
        $this->addSql('ALTER TABLE chill_docgen_template ADD CONSTRAINT FK_49A347E893CB796C FOREIGN KEY (file_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_49A347E893CB796C ON chill_docgen_template (file_id)');
    }
}
