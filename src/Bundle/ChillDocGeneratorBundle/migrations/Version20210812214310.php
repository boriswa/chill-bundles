<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\DocGenerator;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add entities and context fields to DocGenTemplate.
 */
final class Version20210812214310 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_docgen_template DROP entities');
        $this->addSql('ALTER TABLE chill_docgen_template DROP context');
        $this->addSql('COMMENT ON COLUMN chill_docgen_template.name IS NULL');
    }

    public function getDescription(): string
    {
        return 'Add entities and context fields to DocGenTemplate';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_docgen_template ADD entities TEXT');
        $this->addSql('ALTER TABLE chill_docgen_template ADD context VARCHAR(255)');
        $this->addSql('COMMENT ON COLUMN chill_docgen_template.entities IS \'(DC2Type:simple_array)\'');
    }
}
