<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\DocGenerator;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211201191757 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_docgen_template DROP active');
        $this->addSql('ALTER TABLE chill_docgen_template DROP entity');
        $this->addSql('ALTER TABLE chill_docgen_template DROP template_options');
        $this->addSql('ALTER TABLE chill_docgen_template ADD entities TEXT');
        $this->addSql('COMMENT ON COLUMN chill_docgen_template.entities IS \'(DC2Type:simple_array)\'');
    }

    public function getDescription(): string
    {
        return 'Add options, active and link to entity in docgen_template';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_docgen_template ADD active BOOLEAN DEFAULT true NOT NULL');
        $this->addSql('ALTER TABLE chill_docgen_template ADD entity VARCHAR(255) NOT NULL DEFAULT \'\'');
        $this->addSql('ALTER TABLE chill_docgen_template ADD template_options JSONB NOT NULL DEFAULT \'[]\'::jsonb ');
        $this->addSql('COMMENT ON COLUMN chill_docgen_template.template_options IS \'(DC2Type:json)\'');
        $this->addSql('ALTER TABLE chill_docgen_template DROP entities');
    }
}
