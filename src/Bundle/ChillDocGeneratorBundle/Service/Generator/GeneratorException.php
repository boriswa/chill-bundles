<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\Service\Generator;

class GeneratorException extends \RuntimeException
{
    /**
     * @param string[] $errors
     */
    public function __construct(private readonly array $errors = [], ?\Throwable $previous = null)
    {
        parent::__construct(
            'Could not generate the document',
            15252,
            $previous
        );
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
