<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\Service\Messenger;

use Chill\DocGeneratorBundle\Entity\DocGeneratorTemplate;
use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\MainBundle\Entity\User;

final readonly class RequestGenerationMessage
{
    private int $creatorId;

    private int $templateId;

    private int $destinationStoredObjectId;

    private \DateTimeImmutable $createdAt;

    private ?string $sendResultToEmail;

    public function __construct(
        User $creator,
        DocGeneratorTemplate $template,
        private int $entityId,
        StoredObject $destinationStoredObject,
        private array $contextGenerationData,
        private bool $isTest = false,
        ?string $sendResultToEmail = null,
        private bool $dumpOnly = false,
    ) {
        $this->creatorId = $creator->getId();
        $this->templateId = $template->getId();
        $this->destinationStoredObjectId = $destinationStoredObject->getId();
        $this->createdAt = new \DateTimeImmutable('now');
        $this->sendResultToEmail = $sendResultToEmail ?? $creator->getEmail();
    }

    public function getCreatorId(): int
    {
        return $this->creatorId;
    }

    public function getDestinationStoredObjectId(): int
    {
        return $this->destinationStoredObjectId;
    }

    public function getTemplateId(): int
    {
        return $this->templateId;
    }

    public function getEntityId(): int
    {
        return $this->entityId;
    }

    public function getContextGenerationData(): array
    {
        return $this->contextGenerationData;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function isTest(): bool
    {
        return $this->isTest;
    }

    public function getSendResultToEmail(): ?string
    {
        return $this->sendResultToEmail;
    }

    public function isDumpOnly(): bool
    {
        return $this->dumpOnly;
    }
}
